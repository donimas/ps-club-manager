/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PsClubManagerTestModule } from '../../../test.module';
import { ClubJoystickComponent } from '../../../../../../main/webapp/app/entities/club-joystick/club-joystick.component';
import { ClubJoystickService } from '../../../../../../main/webapp/app/entities/club-joystick/club-joystick.service';
import { ClubJoystick } from '../../../../../../main/webapp/app/entities/club-joystick/club-joystick.model';

describe('Component Tests', () => {

    describe('ClubJoystick Management Component', () => {
        let comp: ClubJoystickComponent;
        let fixture: ComponentFixture<ClubJoystickComponent>;
        let service: ClubJoystickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [ClubJoystickComponent],
                providers: [
                    ClubJoystickService
                ]
            })
            .overrideTemplate(ClubJoystickComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClubJoystickComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClubJoystickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new ClubJoystick(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.clubJoysticks[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
