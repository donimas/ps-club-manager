/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { PsClubManagerTestModule } from '../../../test.module';
import { ClubJoystickDialogComponent } from '../../../../../../main/webapp/app/entities/club-joystick/club-joystick-dialog.component';
import { ClubJoystickService } from '../../../../../../main/webapp/app/entities/club-joystick/club-joystick.service';
import { ClubJoystick } from '../../../../../../main/webapp/app/entities/club-joystick/club-joystick.model';
import { ClubService } from '../../../../../../main/webapp/app/entities/club';
import { JoystickDictionaryService } from '../../../../../../main/webapp/app/entities/joystick-dictionary';
import { TariffService } from '../../../../../../main/webapp/app/entities/tariff';

describe('Component Tests', () => {

    describe('ClubJoystick Management Dialog Component', () => {
        let comp: ClubJoystickDialogComponent;
        let fixture: ComponentFixture<ClubJoystickDialogComponent>;
        let service: ClubJoystickService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [ClubJoystickDialogComponent],
                providers: [
                    ClubService,
                    JoystickDictionaryService,
                    TariffService,
                    ClubJoystickService
                ]
            })
            .overrideTemplate(ClubJoystickDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClubJoystickDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClubJoystickService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ClubJoystick(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.clubJoystick = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'clubJoystickListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ClubJoystick();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.clubJoystick = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'clubJoystickListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
