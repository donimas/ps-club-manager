/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { PsClubManagerTestModule } from '../../../test.module';
import { ClubJoystickDetailComponent } from '../../../../../../main/webapp/app/entities/club-joystick/club-joystick-detail.component';
import { ClubJoystickService } from '../../../../../../main/webapp/app/entities/club-joystick/club-joystick.service';
import { ClubJoystick } from '../../../../../../main/webapp/app/entities/club-joystick/club-joystick.model';

describe('Component Tests', () => {

    describe('ClubJoystick Management Detail Component', () => {
        let comp: ClubJoystickDetailComponent;
        let fixture: ComponentFixture<ClubJoystickDetailComponent>;
        let service: ClubJoystickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [ClubJoystickDetailComponent],
                providers: [
                    ClubJoystickService
                ]
            })
            .overrideTemplate(ClubJoystickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClubJoystickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClubJoystickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new ClubJoystick(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.clubJoystick).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
