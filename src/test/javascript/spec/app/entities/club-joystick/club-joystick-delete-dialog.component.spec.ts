/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { PsClubManagerTestModule } from '../../../test.module';
import { ClubJoystickDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/club-joystick/club-joystick-delete-dialog.component';
import { ClubJoystickService } from '../../../../../../main/webapp/app/entities/club-joystick/club-joystick.service';

describe('Component Tests', () => {

    describe('ClubJoystick Management Delete Component', () => {
        let comp: ClubJoystickDeleteDialogComponent;
        let fixture: ComponentFixture<ClubJoystickDeleteDialogComponent>;
        let service: ClubJoystickService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [ClubJoystickDeleteDialogComponent],
                providers: [
                    ClubJoystickService
                ]
            })
            .overrideTemplate(ClubJoystickDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClubJoystickDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClubJoystickService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
