/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PsClubManagerTestModule } from '../../../test.module';
import { AttachmentFileComponent } from '../../../../../../main/webapp/app/entities/attachment-file/attachment-file.component';
import { AttachmentFileService } from '../../../../../../main/webapp/app/entities/attachment-file/attachment-file.service';
import { AttachmentFile } from '../../../../../../main/webapp/app/entities/attachment-file/attachment-file.model';

describe('Component Tests', () => {

    describe('AttachmentFile Management Component', () => {
        let comp: AttachmentFileComponent;
        let fixture: ComponentFixture<AttachmentFileComponent>;
        let service: AttachmentFileService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [AttachmentFileComponent],
                providers: [
                    AttachmentFileService
                ]
            })
            .overrideTemplate(AttachmentFileComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AttachmentFileComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AttachmentFileService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new AttachmentFile(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.attachmentFiles[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
