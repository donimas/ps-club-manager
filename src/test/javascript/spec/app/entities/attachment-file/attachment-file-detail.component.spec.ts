/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { PsClubManagerTestModule } from '../../../test.module';
import { AttachmentFileDetailComponent } from '../../../../../../main/webapp/app/entities/attachment-file/attachment-file-detail.component';
import { AttachmentFileService } from '../../../../../../main/webapp/app/entities/attachment-file/attachment-file.service';
import { AttachmentFile } from '../../../../../../main/webapp/app/entities/attachment-file/attachment-file.model';

describe('Component Tests', () => {

    describe('AttachmentFile Management Detail Component', () => {
        let comp: AttachmentFileDetailComponent;
        let fixture: ComponentFixture<AttachmentFileDetailComponent>;
        let service: AttachmentFileService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [AttachmentFileDetailComponent],
                providers: [
                    AttachmentFileService
                ]
            })
            .overrideTemplate(AttachmentFileDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AttachmentFileDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AttachmentFileService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new AttachmentFile(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.attachmentFile).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
