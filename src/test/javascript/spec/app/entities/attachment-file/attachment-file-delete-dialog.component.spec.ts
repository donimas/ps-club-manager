/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { PsClubManagerTestModule } from '../../../test.module';
import { AttachmentFileDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/attachment-file/attachment-file-delete-dialog.component';
import { AttachmentFileService } from '../../../../../../main/webapp/app/entities/attachment-file/attachment-file.service';

describe('Component Tests', () => {

    describe('AttachmentFile Management Delete Component', () => {
        let comp: AttachmentFileDeleteDialogComponent;
        let fixture: ComponentFixture<AttachmentFileDeleteDialogComponent>;
        let service: AttachmentFileService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [AttachmentFileDeleteDialogComponent],
                providers: [
                    AttachmentFileService
                ]
            })
            .overrideTemplate(AttachmentFileDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AttachmentFileDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AttachmentFileService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
