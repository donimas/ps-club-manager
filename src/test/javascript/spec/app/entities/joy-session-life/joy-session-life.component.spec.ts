/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PsClubManagerTestModule } from '../../../test.module';
import { JoySessionLifeComponent } from '../../../../../../main/webapp/app/entities/joy-session-life/joy-session-life.component';
import { JoySessionLifeService } from '../../../../../../main/webapp/app/entities/joy-session-life/joy-session-life.service';
import { JoySessionLife } from '../../../../../../main/webapp/app/entities/joy-session-life/joy-session-life.model';

describe('Component Tests', () => {

    describe('JoySessionLife Management Component', () => {
        let comp: JoySessionLifeComponent;
        let fixture: ComponentFixture<JoySessionLifeComponent>;
        let service: JoySessionLifeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [JoySessionLifeComponent],
                providers: [
                    JoySessionLifeService
                ]
            })
            .overrideTemplate(JoySessionLifeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(JoySessionLifeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(JoySessionLifeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new JoySessionLife(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.joySessionLives[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
