/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { PsClubManagerTestModule } from '../../../test.module';
import { JoySessionLifeDetailComponent } from '../../../../../../main/webapp/app/entities/joy-session-life/joy-session-life-detail.component';
import { JoySessionLifeService } from '../../../../../../main/webapp/app/entities/joy-session-life/joy-session-life.service';
import { JoySessionLife } from '../../../../../../main/webapp/app/entities/joy-session-life/joy-session-life.model';

describe('Component Tests', () => {

    describe('JoySessionLife Management Detail Component', () => {
        let comp: JoySessionLifeDetailComponent;
        let fixture: ComponentFixture<JoySessionLifeDetailComponent>;
        let service: JoySessionLifeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [JoySessionLifeDetailComponent],
                providers: [
                    JoySessionLifeService
                ]
            })
            .overrideTemplate(JoySessionLifeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(JoySessionLifeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(JoySessionLifeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new JoySessionLife(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.joySessionLife).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
