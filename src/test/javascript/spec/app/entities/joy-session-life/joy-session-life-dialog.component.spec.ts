/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { PsClubManagerTestModule } from '../../../test.module';
import { JoySessionLifeDialogComponent } from '../../../../../../main/webapp/app/entities/joy-session-life/joy-session-life-dialog.component';
import { JoySessionLifeService } from '../../../../../../main/webapp/app/entities/joy-session-life/joy-session-life.service';
import { JoySessionLife } from '../../../../../../main/webapp/app/entities/joy-session-life/joy-session-life.model';
import { PricePackageService } from '../../../../../../main/webapp/app/entities/price-package';

describe('Component Tests', () => {

    describe('JoySessionLife Management Dialog Component', () => {
        let comp: JoySessionLifeDialogComponent;
        let fixture: ComponentFixture<JoySessionLifeDialogComponent>;
        let service: JoySessionLifeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [JoySessionLifeDialogComponent],
                providers: [
                    PricePackageService,
                    JoySessionLifeService
                ]
            })
            .overrideTemplate(JoySessionLifeDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(JoySessionLifeDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(JoySessionLifeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new JoySessionLife(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.joySessionLife = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'joySessionLifeListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new JoySessionLife();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.joySessionLife = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'joySessionLifeListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
