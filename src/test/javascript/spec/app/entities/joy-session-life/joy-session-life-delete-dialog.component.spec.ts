/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { PsClubManagerTestModule } from '../../../test.module';
import { JoySessionLifeDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/joy-session-life/joy-session-life-delete-dialog.component';
import { JoySessionLifeService } from '../../../../../../main/webapp/app/entities/joy-session-life/joy-session-life.service';

describe('Component Tests', () => {

    describe('JoySessionLife Management Delete Component', () => {
        let comp: JoySessionLifeDeleteDialogComponent;
        let fixture: ComponentFixture<JoySessionLifeDeleteDialogComponent>;
        let service: JoySessionLifeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [JoySessionLifeDeleteDialogComponent],
                providers: [
                    JoySessionLifeService
                ]
            })
            .overrideTemplate(JoySessionLifeDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(JoySessionLifeDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(JoySessionLifeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
