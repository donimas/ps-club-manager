/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PsClubManagerTestModule } from '../../../test.module';
import { JoystickDictionaryComponent } from '../../../../../../main/webapp/app/entities/joystick-dictionary/joystick-dictionary.component';
import { JoystickDictionaryService } from '../../../../../../main/webapp/app/entities/joystick-dictionary/joystick-dictionary.service';
import { JoystickDictionary } from '../../../../../../main/webapp/app/entities/joystick-dictionary/joystick-dictionary.model';

describe('Component Tests', () => {

    describe('JoystickDictionary Management Component', () => {
        let comp: JoystickDictionaryComponent;
        let fixture: ComponentFixture<JoystickDictionaryComponent>;
        let service: JoystickDictionaryService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [JoystickDictionaryComponent],
                providers: [
                    JoystickDictionaryService
                ]
            })
            .overrideTemplate(JoystickDictionaryComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(JoystickDictionaryComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(JoystickDictionaryService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new JoystickDictionary(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.joystickDictionaries[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
