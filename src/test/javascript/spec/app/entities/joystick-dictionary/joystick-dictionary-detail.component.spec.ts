/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { PsClubManagerTestModule } from '../../../test.module';
import { JoystickDictionaryDetailComponent } from '../../../../../../main/webapp/app/entities/joystick-dictionary/joystick-dictionary-detail.component';
import { JoystickDictionaryService } from '../../../../../../main/webapp/app/entities/joystick-dictionary/joystick-dictionary.service';
import { JoystickDictionary } from '../../../../../../main/webapp/app/entities/joystick-dictionary/joystick-dictionary.model';

describe('Component Tests', () => {

    describe('JoystickDictionary Management Detail Component', () => {
        let comp: JoystickDictionaryDetailComponent;
        let fixture: ComponentFixture<JoystickDictionaryDetailComponent>;
        let service: JoystickDictionaryService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [JoystickDictionaryDetailComponent],
                providers: [
                    JoystickDictionaryService
                ]
            })
            .overrideTemplate(JoystickDictionaryDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(JoystickDictionaryDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(JoystickDictionaryService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new JoystickDictionary(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.joystickDictionary).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
