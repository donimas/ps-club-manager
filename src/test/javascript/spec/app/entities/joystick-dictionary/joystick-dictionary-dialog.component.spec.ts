/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { PsClubManagerTestModule } from '../../../test.module';
import { JoystickDictionaryDialogComponent } from '../../../../../../main/webapp/app/entities/joystick-dictionary/joystick-dictionary-dialog.component';
import { JoystickDictionaryService } from '../../../../../../main/webapp/app/entities/joystick-dictionary/joystick-dictionary.service';
import { JoystickDictionary } from '../../../../../../main/webapp/app/entities/joystick-dictionary/joystick-dictionary.model';
import { AttachmentFileService } from '../../../../../../main/webapp/app/entities/attachment-file';

describe('Component Tests', () => {

    describe('JoystickDictionary Management Dialog Component', () => {
        let comp: JoystickDictionaryDialogComponent;
        let fixture: ComponentFixture<JoystickDictionaryDialogComponent>;
        let service: JoystickDictionaryService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [JoystickDictionaryDialogComponent],
                providers: [
                    AttachmentFileService,
                    JoystickDictionaryService
                ]
            })
            .overrideTemplate(JoystickDictionaryDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(JoystickDictionaryDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(JoystickDictionaryService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new JoystickDictionary(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.joystickDictionary = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'joystickDictionaryListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new JoystickDictionary();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.joystickDictionary = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'joystickDictionaryListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
