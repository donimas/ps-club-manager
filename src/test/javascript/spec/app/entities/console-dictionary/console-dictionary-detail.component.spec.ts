/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { PsClubManagerTestModule } from '../../../test.module';
import { ConsoleDictionaryDetailComponent } from '../../../../../../main/webapp/app/entities/console-dictionary/console-dictionary-detail.component';
import { ConsoleDictionaryService } from '../../../../../../main/webapp/app/entities/console-dictionary/console-dictionary.service';
import { ConsoleDictionary } from '../../../../../../main/webapp/app/entities/console-dictionary/console-dictionary.model';

describe('Component Tests', () => {

    describe('ConsoleDictionary Management Detail Component', () => {
        let comp: ConsoleDictionaryDetailComponent;
        let fixture: ComponentFixture<ConsoleDictionaryDetailComponent>;
        let service: ConsoleDictionaryService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [ConsoleDictionaryDetailComponent],
                providers: [
                    ConsoleDictionaryService
                ]
            })
            .overrideTemplate(ConsoleDictionaryDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ConsoleDictionaryDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ConsoleDictionaryService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new ConsoleDictionary(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.consoleDictionary).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
