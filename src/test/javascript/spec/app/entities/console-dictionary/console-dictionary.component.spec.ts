/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PsClubManagerTestModule } from '../../../test.module';
import { ConsoleDictionaryComponent } from '../../../../../../main/webapp/app/entities/console-dictionary/console-dictionary.component';
import { ConsoleDictionaryService } from '../../../../../../main/webapp/app/entities/console-dictionary/console-dictionary.service';
import { ConsoleDictionary } from '../../../../../../main/webapp/app/entities/console-dictionary/console-dictionary.model';

describe('Component Tests', () => {

    describe('ConsoleDictionary Management Component', () => {
        let comp: ConsoleDictionaryComponent;
        let fixture: ComponentFixture<ConsoleDictionaryComponent>;
        let service: ConsoleDictionaryService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [ConsoleDictionaryComponent],
                providers: [
                    ConsoleDictionaryService
                ]
            })
            .overrideTemplate(ConsoleDictionaryComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ConsoleDictionaryComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ConsoleDictionaryService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new ConsoleDictionary(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.consoleDictionaries[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
