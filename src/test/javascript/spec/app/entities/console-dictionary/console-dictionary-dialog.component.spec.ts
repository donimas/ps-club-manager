/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { PsClubManagerTestModule } from '../../../test.module';
import { ConsoleDictionaryDialogComponent } from '../../../../../../main/webapp/app/entities/console-dictionary/console-dictionary-dialog.component';
import { ConsoleDictionaryService } from '../../../../../../main/webapp/app/entities/console-dictionary/console-dictionary.service';
import { ConsoleDictionary } from '../../../../../../main/webapp/app/entities/console-dictionary/console-dictionary.model';
import { AttachmentFileService } from '../../../../../../main/webapp/app/entities/attachment-file';

describe('Component Tests', () => {

    describe('ConsoleDictionary Management Dialog Component', () => {
        let comp: ConsoleDictionaryDialogComponent;
        let fixture: ComponentFixture<ConsoleDictionaryDialogComponent>;
        let service: ConsoleDictionaryService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [ConsoleDictionaryDialogComponent],
                providers: [
                    AttachmentFileService,
                    ConsoleDictionaryService
                ]
            })
            .overrideTemplate(ConsoleDictionaryDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ConsoleDictionaryDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ConsoleDictionaryService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ConsoleDictionary(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.consoleDictionary = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'consoleDictionaryListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ConsoleDictionary();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.consoleDictionary = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'consoleDictionaryListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
