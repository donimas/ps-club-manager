/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PsClubManagerTestModule } from '../../../test.module';
import { GamePleasureComponent } from '../../../../../../main/webapp/app/entities/game-pleasure/game-pleasure.component';
import { GamePleasureService } from '../../../../../../main/webapp/app/entities/game-pleasure/game-pleasure.service';
import { GamePleasure } from '../../../../../../main/webapp/app/entities/game-pleasure/game-pleasure.model';

describe('Component Tests', () => {

    describe('GamePleasure Management Component', () => {
        let comp: GamePleasureComponent;
        let fixture: ComponentFixture<GamePleasureComponent>;
        let service: GamePleasureService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [GamePleasureComponent],
                providers: [
                    GamePleasureService
                ]
            })
            .overrideTemplate(GamePleasureComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(GamePleasureComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(GamePleasureService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new GamePleasure(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.gamePleasures[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
