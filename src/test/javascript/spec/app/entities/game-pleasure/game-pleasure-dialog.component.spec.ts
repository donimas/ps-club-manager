/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { PsClubManagerTestModule } from '../../../test.module';
import { GamePleasureDialogComponent } from '../../../../../../main/webapp/app/entities/game-pleasure/game-pleasure-dialog.component';
import { GamePleasureService } from '../../../../../../main/webapp/app/entities/game-pleasure/game-pleasure.service';
import { GamePleasure } from '../../../../../../main/webapp/app/entities/game-pleasure/game-pleasure.model';
import { SessionService } from '../../../../../../main/webapp/app/entities/session';
import { ClubService } from '../../../../../../main/webapp/app/entities/club';
import { ClientService } from '../../../../../../main/webapp/app/entities/client';

describe('Component Tests', () => {

    describe('GamePleasure Management Dialog Component', () => {
        let comp: GamePleasureDialogComponent;
        let fixture: ComponentFixture<GamePleasureDialogComponent>;
        let service: GamePleasureService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [GamePleasureDialogComponent],
                providers: [
                    SessionService,
                    ClubService,
                    ClientService,
                    GamePleasureService
                ]
            })
            .overrideTemplate(GamePleasureDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(GamePleasureDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(GamePleasureService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new GamePleasure(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.gamePleasure = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'gamePleasureListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new GamePleasure();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.gamePleasure = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'gamePleasureListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
