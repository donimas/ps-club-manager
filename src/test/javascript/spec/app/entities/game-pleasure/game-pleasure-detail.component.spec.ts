/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { PsClubManagerTestModule } from '../../../test.module';
import { GamePleasureDetailComponent } from '../../../../../../main/webapp/app/entities/game-pleasure/game-pleasure-detail.component';
import { GamePleasureService } from '../../../../../../main/webapp/app/entities/game-pleasure/game-pleasure.service';
import { GamePleasure } from '../../../../../../main/webapp/app/entities/game-pleasure/game-pleasure.model';

describe('Component Tests', () => {

    describe('GamePleasure Management Detail Component', () => {
        let comp: GamePleasureDetailComponent;
        let fixture: ComponentFixture<GamePleasureDetailComponent>;
        let service: GamePleasureService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [GamePleasureDetailComponent],
                providers: [
                    GamePleasureService
                ]
            })
            .overrideTemplate(GamePleasureDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(GamePleasureDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(GamePleasureService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new GamePleasure(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.gamePleasure).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
