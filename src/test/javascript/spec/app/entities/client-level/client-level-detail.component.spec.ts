/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { PsClubManagerTestModule } from '../../../test.module';
import { ClientLevelDetailComponent } from '../../../../../../main/webapp/app/entities/client-level/client-level-detail.component';
import { ClientLevelService } from '../../../../../../main/webapp/app/entities/client-level/client-level.service';
import { ClientLevel } from '../../../../../../main/webapp/app/entities/client-level/client-level.model';

describe('Component Tests', () => {

    describe('ClientLevel Management Detail Component', () => {
        let comp: ClientLevelDetailComponent;
        let fixture: ComponentFixture<ClientLevelDetailComponent>;
        let service: ClientLevelService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [ClientLevelDetailComponent],
                providers: [
                    ClientLevelService
                ]
            })
            .overrideTemplate(ClientLevelDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClientLevelDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClientLevelService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new ClientLevel(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.clientLevel).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
