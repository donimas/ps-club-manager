/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PsClubManagerTestModule } from '../../../test.module';
import { ClientLevelComponent } from '../../../../../../main/webapp/app/entities/client-level/client-level.component';
import { ClientLevelService } from '../../../../../../main/webapp/app/entities/client-level/client-level.service';
import { ClientLevel } from '../../../../../../main/webapp/app/entities/client-level/client-level.model';

describe('Component Tests', () => {

    describe('ClientLevel Management Component', () => {
        let comp: ClientLevelComponent;
        let fixture: ComponentFixture<ClientLevelComponent>;
        let service: ClientLevelService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [ClientLevelComponent],
                providers: [
                    ClientLevelService
                ]
            })
            .overrideTemplate(ClientLevelComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClientLevelComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClientLevelService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new ClientLevel(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.clientLevels[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
