/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { PsClubManagerTestModule } from '../../../test.module';
import { ZoneDictionaryDetailComponent } from '../../../../../../main/webapp/app/entities/zone-dictionary/zone-dictionary-detail.component';
import { ZoneDictionaryService } from '../../../../../../main/webapp/app/entities/zone-dictionary/zone-dictionary.service';
import { ZoneDictionary } from '../../../../../../main/webapp/app/entities/zone-dictionary/zone-dictionary.model';

describe('Component Tests', () => {

    describe('ZoneDictionary Management Detail Component', () => {
        let comp: ZoneDictionaryDetailComponent;
        let fixture: ComponentFixture<ZoneDictionaryDetailComponent>;
        let service: ZoneDictionaryService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [ZoneDictionaryDetailComponent],
                providers: [
                    ZoneDictionaryService
                ]
            })
            .overrideTemplate(ZoneDictionaryDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ZoneDictionaryDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ZoneDictionaryService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new ZoneDictionary(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.zoneDictionary).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
