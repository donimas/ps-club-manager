/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PsClubManagerTestModule } from '../../../test.module';
import { ZoneDictionaryComponent } from '../../../../../../main/webapp/app/entities/zone-dictionary/zone-dictionary.component';
import { ZoneDictionaryService } from '../../../../../../main/webapp/app/entities/zone-dictionary/zone-dictionary.service';
import { ZoneDictionary } from '../../../../../../main/webapp/app/entities/zone-dictionary/zone-dictionary.model';

describe('Component Tests', () => {

    describe('ZoneDictionary Management Component', () => {
        let comp: ZoneDictionaryComponent;
        let fixture: ComponentFixture<ZoneDictionaryComponent>;
        let service: ZoneDictionaryService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [ZoneDictionaryComponent],
                providers: [
                    ZoneDictionaryService
                ]
            })
            .overrideTemplate(ZoneDictionaryComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ZoneDictionaryComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ZoneDictionaryService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new ZoneDictionary(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.zoneDictionaries[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
