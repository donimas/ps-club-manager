/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PsClubManagerTestModule } from '../../../test.module';
import { CityDictionaryComponent } from '../../../../../../main/webapp/app/entities/city-dictionary/city-dictionary.component';
import { CityDictionaryService } from '../../../../../../main/webapp/app/entities/city-dictionary/city-dictionary.service';
import { CityDictionary } from '../../../../../../main/webapp/app/entities/city-dictionary/city-dictionary.model';

describe('Component Tests', () => {

    describe('CityDictionary Management Component', () => {
        let comp: CityDictionaryComponent;
        let fixture: ComponentFixture<CityDictionaryComponent>;
        let service: CityDictionaryService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [CityDictionaryComponent],
                providers: [
                    CityDictionaryService
                ]
            })
            .overrideTemplate(CityDictionaryComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CityDictionaryComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CityDictionaryService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new CityDictionary(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.cityDictionaries[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
