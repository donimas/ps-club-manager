/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { PsClubManagerTestModule } from '../../../test.module';
import { CityDictionaryDetailComponent } from '../../../../../../main/webapp/app/entities/city-dictionary/city-dictionary-detail.component';
import { CityDictionaryService } from '../../../../../../main/webapp/app/entities/city-dictionary/city-dictionary.service';
import { CityDictionary } from '../../../../../../main/webapp/app/entities/city-dictionary/city-dictionary.model';

describe('Component Tests', () => {

    describe('CityDictionary Management Detail Component', () => {
        let comp: CityDictionaryDetailComponent;
        let fixture: ComponentFixture<CityDictionaryDetailComponent>;
        let service: CityDictionaryService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [CityDictionaryDetailComponent],
                providers: [
                    CityDictionaryService
                ]
            })
            .overrideTemplate(CityDictionaryDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CityDictionaryDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CityDictionaryService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new CityDictionary(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.cityDictionary).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
