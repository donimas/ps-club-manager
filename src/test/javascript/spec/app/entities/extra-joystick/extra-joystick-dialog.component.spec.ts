/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { PsClubManagerTestModule } from '../../../test.module';
import { ExtraJoystickDialogComponent } from '../../../../../../main/webapp/app/entities/extra-joystick/extra-joystick-dialog.component';
import { ExtraJoystickService } from '../../../../../../main/webapp/app/entities/extra-joystick/extra-joystick.service';
import { ExtraJoystick } from '../../../../../../main/webapp/app/entities/extra-joystick/extra-joystick.model';
import { SessionService } from '../../../../../../main/webapp/app/entities/session';
import { ClubJoystickService } from '../../../../../../main/webapp/app/entities/club-joystick';
import { TariffService } from '../../../../../../main/webapp/app/entities/tariff';

describe('Component Tests', () => {

    describe('ExtraJoystick Management Dialog Component', () => {
        let comp: ExtraJoystickDialogComponent;
        let fixture: ComponentFixture<ExtraJoystickDialogComponent>;
        let service: ExtraJoystickService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [ExtraJoystickDialogComponent],
                providers: [
                    SessionService,
                    ClubJoystickService,
                    TariffService,
                    ExtraJoystickService
                ]
            })
            .overrideTemplate(ExtraJoystickDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ExtraJoystickDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ExtraJoystickService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ExtraJoystick(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.extraJoystick = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'extraJoystickListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ExtraJoystick();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.extraJoystick = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'extraJoystickListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
