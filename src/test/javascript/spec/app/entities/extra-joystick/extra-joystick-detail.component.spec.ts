/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { PsClubManagerTestModule } from '../../../test.module';
import { ExtraJoystickDetailComponent } from '../../../../../../main/webapp/app/entities/extra-joystick/extra-joystick-detail.component';
import { ExtraJoystickService } from '../../../../../../main/webapp/app/entities/extra-joystick/extra-joystick.service';
import { ExtraJoystick } from '../../../../../../main/webapp/app/entities/extra-joystick/extra-joystick.model';

describe('Component Tests', () => {

    describe('ExtraJoystick Management Detail Component', () => {
        let comp: ExtraJoystickDetailComponent;
        let fixture: ComponentFixture<ExtraJoystickDetailComponent>;
        let service: ExtraJoystickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [ExtraJoystickDetailComponent],
                providers: [
                    ExtraJoystickService
                ]
            })
            .overrideTemplate(ExtraJoystickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ExtraJoystickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ExtraJoystickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new ExtraJoystick(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.extraJoystick).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
