/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PsClubManagerTestModule } from '../../../test.module';
import { ExtraJoystickComponent } from '../../../../../../main/webapp/app/entities/extra-joystick/extra-joystick.component';
import { ExtraJoystickService } from '../../../../../../main/webapp/app/entities/extra-joystick/extra-joystick.service';
import { ExtraJoystick } from '../../../../../../main/webapp/app/entities/extra-joystick/extra-joystick.model';

describe('Component Tests', () => {

    describe('ExtraJoystick Management Component', () => {
        let comp: ExtraJoystickComponent;
        let fixture: ComponentFixture<ExtraJoystickComponent>;
        let service: ExtraJoystickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [ExtraJoystickComponent],
                providers: [
                    ExtraJoystickService
                ]
            })
            .overrideTemplate(ExtraJoystickComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ExtraJoystickComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ExtraJoystickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new ExtraJoystick(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.extraJoysticks[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
