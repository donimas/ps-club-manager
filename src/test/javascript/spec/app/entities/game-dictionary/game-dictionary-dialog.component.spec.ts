/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { PsClubManagerTestModule } from '../../../test.module';
import { GameDictionaryDialogComponent } from '../../../../../../main/webapp/app/entities/game-dictionary/game-dictionary-dialog.component';
import { GameDictionaryService } from '../../../../../../main/webapp/app/entities/game-dictionary/game-dictionary.service';
import { GameDictionary } from '../../../../../../main/webapp/app/entities/game-dictionary/game-dictionary.model';
import { AttachmentFileService } from '../../../../../../main/webapp/app/entities/attachment-file';

describe('Component Tests', () => {

    describe('GameDictionary Management Dialog Component', () => {
        let comp: GameDictionaryDialogComponent;
        let fixture: ComponentFixture<GameDictionaryDialogComponent>;
        let service: GameDictionaryService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [GameDictionaryDialogComponent],
                providers: [
                    AttachmentFileService,
                    GameDictionaryService
                ]
            })
            .overrideTemplate(GameDictionaryDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(GameDictionaryDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(GameDictionaryService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new GameDictionary(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.gameDictionary = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'gameDictionaryListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new GameDictionary();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.gameDictionary = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'gameDictionaryListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
