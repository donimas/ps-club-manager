/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { PsClubManagerTestModule } from '../../../test.module';
import { GameDictionaryDetailComponent } from '../../../../../../main/webapp/app/entities/game-dictionary/game-dictionary-detail.component';
import { GameDictionaryService } from '../../../../../../main/webapp/app/entities/game-dictionary/game-dictionary.service';
import { GameDictionary } from '../../../../../../main/webapp/app/entities/game-dictionary/game-dictionary.model';

describe('Component Tests', () => {

    describe('GameDictionary Management Detail Component', () => {
        let comp: GameDictionaryDetailComponent;
        let fixture: ComponentFixture<GameDictionaryDetailComponent>;
        let service: GameDictionaryService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [GameDictionaryDetailComponent],
                providers: [
                    GameDictionaryService
                ]
            })
            .overrideTemplate(GameDictionaryDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(GameDictionaryDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(GameDictionaryService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new GameDictionary(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.gameDictionary).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
