/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PsClubManagerTestModule } from '../../../test.module';
import { GameDictionaryComponent } from '../../../../../../main/webapp/app/entities/game-dictionary/game-dictionary.component';
import { GameDictionaryService } from '../../../../../../main/webapp/app/entities/game-dictionary/game-dictionary.service';
import { GameDictionary } from '../../../../../../main/webapp/app/entities/game-dictionary/game-dictionary.model';

describe('Component Tests', () => {

    describe('GameDictionary Management Component', () => {
        let comp: GameDictionaryComponent;
        let fixture: ComponentFixture<GameDictionaryComponent>;
        let service: GameDictionaryService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [GameDictionaryComponent],
                providers: [
                    GameDictionaryService
                ]
            })
            .overrideTemplate(GameDictionaryComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(GameDictionaryComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(GameDictionaryService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new GameDictionary(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.gameDictionaries[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
