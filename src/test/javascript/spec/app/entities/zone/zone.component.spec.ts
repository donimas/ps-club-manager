/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PsClubManagerTestModule } from '../../../test.module';
import { ZoneComponent } from '../../../../../../main/webapp/app/entities/zone/zone.component';
import { ZoneService } from '../../../../../../main/webapp/app/entities/zone/zone.service';
import { Zone } from '../../../../../../main/webapp/app/entities/zone/zone.model';

describe('Component Tests', () => {

    describe('Zone Management Component', () => {
        let comp: ZoneComponent;
        let fixture: ComponentFixture<ZoneComponent>;
        let service: ZoneService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [ZoneComponent],
                providers: [
                    ZoneService
                ]
            })
            .overrideTemplate(ZoneComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ZoneComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ZoneService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Zone(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.zones[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
