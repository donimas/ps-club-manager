/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { PsClubManagerTestModule } from '../../../test.module';
import { ZoneDetailComponent } from '../../../../../../main/webapp/app/entities/zone/zone-detail.component';
import { ZoneService } from '../../../../../../main/webapp/app/entities/zone/zone.service';
import { Zone } from '../../../../../../main/webapp/app/entities/zone/zone.model';

describe('Component Tests', () => {

    describe('Zone Management Detail Component', () => {
        let comp: ZoneDetailComponent;
        let fixture: ComponentFixture<ZoneDetailComponent>;
        let service: ZoneService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [ZoneDetailComponent],
                providers: [
                    ZoneService
                ]
            })
            .overrideTemplate(ZoneDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ZoneDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ZoneService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Zone(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.zone).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
