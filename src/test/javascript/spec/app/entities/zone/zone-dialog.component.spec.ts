/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { PsClubManagerTestModule } from '../../../test.module';
import { ZoneDialogComponent } from '../../../../../../main/webapp/app/entities/zone/zone-dialog.component';
import { ZoneService } from '../../../../../../main/webapp/app/entities/zone/zone.service';
import { Zone } from '../../../../../../main/webapp/app/entities/zone/zone.model';
import { TariffService } from '../../../../../../main/webapp/app/entities/tariff';
import { ConsoleDictionaryService } from '../../../../../../main/webapp/app/entities/console-dictionary';
import { ZoneDictionaryService } from '../../../../../../main/webapp/app/entities/zone-dictionary';
import { ClubService } from '../../../../../../main/webapp/app/entities/club';

describe('Component Tests', () => {

    describe('Zone Management Dialog Component', () => {
        let comp: ZoneDialogComponent;
        let fixture: ComponentFixture<ZoneDialogComponent>;
        let service: ZoneService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [ZoneDialogComponent],
                providers: [
                    TariffService,
                    ConsoleDictionaryService,
                    ZoneDictionaryService,
                    ClubService,
                    ZoneService
                ]
            })
            .overrideTemplate(ZoneDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ZoneDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ZoneService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Zone(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.zone = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'zoneListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Zone();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.zone = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'zoneListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
