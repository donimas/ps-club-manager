/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PsClubManagerTestModule } from '../../../test.module';
import { PricePackageComponent } from '../../../../../../main/webapp/app/entities/price-package/price-package.component';
import { PricePackageService } from '../../../../../../main/webapp/app/entities/price-package/price-package.service';
import { PricePackage } from '../../../../../../main/webapp/app/entities/price-package/price-package.model';

describe('Component Tests', () => {

    describe('PricePackage Management Component', () => {
        let comp: PricePackageComponent;
        let fixture: ComponentFixture<PricePackageComponent>;
        let service: PricePackageService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [PricePackageComponent],
                providers: [
                    PricePackageService
                ]
            })
            .overrideTemplate(PricePackageComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PricePackageComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PricePackageService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new PricePackage(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.pricePackages[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
