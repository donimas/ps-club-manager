/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { PsClubManagerTestModule } from '../../../test.module';
import { PricePackageDetailComponent } from '../../../../../../main/webapp/app/entities/price-package/price-package-detail.component';
import { PricePackageService } from '../../../../../../main/webapp/app/entities/price-package/price-package.service';
import { PricePackage } from '../../../../../../main/webapp/app/entities/price-package/price-package.model';

describe('Component Tests', () => {

    describe('PricePackage Management Detail Component', () => {
        let comp: PricePackageDetailComponent;
        let fixture: ComponentFixture<PricePackageDetailComponent>;
        let service: PricePackageService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [PricePackageDetailComponent],
                providers: [
                    PricePackageService
                ]
            })
            .overrideTemplate(PricePackageDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PricePackageDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PricePackageService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new PricePackage(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.pricePackage).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
