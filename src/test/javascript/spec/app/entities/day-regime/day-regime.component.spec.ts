/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PsClubManagerTestModule } from '../../../test.module';
import { DayRegimeComponent } from '../../../../../../main/webapp/app/entities/day-regime/day-regime.component';
import { DayRegimeService } from '../../../../../../main/webapp/app/entities/day-regime/day-regime.service';
import { DayRegime } from '../../../../../../main/webapp/app/entities/day-regime/day-regime.model';

describe('Component Tests', () => {

    describe('DayRegime Management Component', () => {
        let comp: DayRegimeComponent;
        let fixture: ComponentFixture<DayRegimeComponent>;
        let service: DayRegimeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [DayRegimeComponent],
                providers: [
                    DayRegimeService
                ]
            })
            .overrideTemplate(DayRegimeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DayRegimeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DayRegimeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new DayRegime(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.dayRegimes[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
