/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { PsClubManagerTestModule } from '../../../test.module';
import { DayRegimeDetailComponent } from '../../../../../../main/webapp/app/entities/day-regime/day-regime-detail.component';
import { DayRegimeService } from '../../../../../../main/webapp/app/entities/day-regime/day-regime.service';
import { DayRegime } from '../../../../../../main/webapp/app/entities/day-regime/day-regime.model';

describe('Component Tests', () => {

    describe('DayRegime Management Detail Component', () => {
        let comp: DayRegimeDetailComponent;
        let fixture: ComponentFixture<DayRegimeDetailComponent>;
        let service: DayRegimeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PsClubManagerTestModule],
                declarations: [DayRegimeDetailComponent],
                providers: [
                    DayRegimeService
                ]
            })
            .overrideTemplate(DayRegimeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DayRegimeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DayRegimeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new DayRegime(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.dayRegime).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
