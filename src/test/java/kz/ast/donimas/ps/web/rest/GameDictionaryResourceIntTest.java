package kz.ast.donimas.ps.web.rest;

import kz.ast.donimas.ps.PsClubManagerApp;

import kz.ast.donimas.ps.domain.GameDictionary;
import kz.ast.donimas.ps.repository.GameDictionaryRepository;
import kz.ast.donimas.ps.service.GameDictionaryService;
import kz.ast.donimas.ps.repository.search.GameDictionarySearchRepository;
import kz.ast.donimas.ps.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static kz.ast.donimas.ps.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the GameDictionaryResource REST controller.
 *
 * @see GameDictionaryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PsClubManagerApp.class)
public class GameDictionaryResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    @Autowired
    private GameDictionaryRepository gameDictionaryRepository;

    @Autowired
    private GameDictionaryService gameDictionaryService;

    @Autowired
    private GameDictionarySearchRepository gameDictionarySearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restGameDictionaryMockMvc;

    private GameDictionary gameDictionary;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final GameDictionaryResource gameDictionaryResource = new GameDictionaryResource(gameDictionaryService);
        this.restGameDictionaryMockMvc = MockMvcBuilders.standaloneSetup(gameDictionaryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GameDictionary createEntity(EntityManager em) {
        GameDictionary gameDictionary = new GameDictionary()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .code(DEFAULT_CODE);
        return gameDictionary;
    }

    @Before
    public void initTest() {
        gameDictionarySearchRepository.deleteAll();
        gameDictionary = createEntity(em);
    }

    @Test
    @Transactional
    public void createGameDictionary() throws Exception {
        int databaseSizeBeforeCreate = gameDictionaryRepository.findAll().size();

        // Create the GameDictionary
        restGameDictionaryMockMvc.perform(post("/api/game-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(gameDictionary)))
            .andExpect(status().isCreated());

        // Validate the GameDictionary in the database
        List<GameDictionary> gameDictionaryList = gameDictionaryRepository.findAll();
        assertThat(gameDictionaryList).hasSize(databaseSizeBeforeCreate + 1);
        GameDictionary testGameDictionary = gameDictionaryList.get(gameDictionaryList.size() - 1);
        assertThat(testGameDictionary.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testGameDictionary.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testGameDictionary.getCode()).isEqualTo(DEFAULT_CODE);

        // Validate the GameDictionary in Elasticsearch
        GameDictionary gameDictionaryEs = gameDictionarySearchRepository.findOne(testGameDictionary.getId());
        assertThat(gameDictionaryEs).isEqualToIgnoringGivenFields(testGameDictionary);
    }

    @Test
    @Transactional
    public void createGameDictionaryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = gameDictionaryRepository.findAll().size();

        // Create the GameDictionary with an existing ID
        gameDictionary.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restGameDictionaryMockMvc.perform(post("/api/game-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(gameDictionary)))
            .andExpect(status().isBadRequest());

        // Validate the GameDictionary in the database
        List<GameDictionary> gameDictionaryList = gameDictionaryRepository.findAll();
        assertThat(gameDictionaryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = gameDictionaryRepository.findAll().size();
        // set the field null
        gameDictionary.setName(null);

        // Create the GameDictionary, which fails.

        restGameDictionaryMockMvc.perform(post("/api/game-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(gameDictionary)))
            .andExpect(status().isBadRequest());

        List<GameDictionary> gameDictionaryList = gameDictionaryRepository.findAll();
        assertThat(gameDictionaryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = gameDictionaryRepository.findAll().size();
        // set the field null
        gameDictionary.setCode(null);

        // Create the GameDictionary, which fails.

        restGameDictionaryMockMvc.perform(post("/api/game-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(gameDictionary)))
            .andExpect(status().isBadRequest());

        List<GameDictionary> gameDictionaryList = gameDictionaryRepository.findAll();
        assertThat(gameDictionaryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllGameDictionaries() throws Exception {
        // Initialize the database
        gameDictionaryRepository.saveAndFlush(gameDictionary);

        // Get all the gameDictionaryList
        restGameDictionaryMockMvc.perform(get("/api/game-dictionaries?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(gameDictionary.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())));
    }

    @Test
    @Transactional
    public void getGameDictionary() throws Exception {
        // Initialize the database
        gameDictionaryRepository.saveAndFlush(gameDictionary);

        // Get the gameDictionary
        restGameDictionaryMockMvc.perform(get("/api/game-dictionaries/{id}", gameDictionary.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(gameDictionary.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingGameDictionary() throws Exception {
        // Get the gameDictionary
        restGameDictionaryMockMvc.perform(get("/api/game-dictionaries/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGameDictionary() throws Exception {
        // Initialize the database
        gameDictionaryService.save(gameDictionary);

        int databaseSizeBeforeUpdate = gameDictionaryRepository.findAll().size();

        // Update the gameDictionary
        GameDictionary updatedGameDictionary = gameDictionaryRepository.findOne(gameDictionary.getId());
        // Disconnect from session so that the updates on updatedGameDictionary are not directly saved in db
        em.detach(updatedGameDictionary);
        updatedGameDictionary
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .code(UPDATED_CODE);

        restGameDictionaryMockMvc.perform(put("/api/game-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedGameDictionary)))
            .andExpect(status().isOk());

        // Validate the GameDictionary in the database
        List<GameDictionary> gameDictionaryList = gameDictionaryRepository.findAll();
        assertThat(gameDictionaryList).hasSize(databaseSizeBeforeUpdate);
        GameDictionary testGameDictionary = gameDictionaryList.get(gameDictionaryList.size() - 1);
        assertThat(testGameDictionary.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testGameDictionary.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testGameDictionary.getCode()).isEqualTo(UPDATED_CODE);

        // Validate the GameDictionary in Elasticsearch
        GameDictionary gameDictionaryEs = gameDictionarySearchRepository.findOne(testGameDictionary.getId());
        assertThat(gameDictionaryEs).isEqualToIgnoringGivenFields(testGameDictionary);
    }

    @Test
    @Transactional
    public void updateNonExistingGameDictionary() throws Exception {
        int databaseSizeBeforeUpdate = gameDictionaryRepository.findAll().size();

        // Create the GameDictionary

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restGameDictionaryMockMvc.perform(put("/api/game-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(gameDictionary)))
            .andExpect(status().isCreated());

        // Validate the GameDictionary in the database
        List<GameDictionary> gameDictionaryList = gameDictionaryRepository.findAll();
        assertThat(gameDictionaryList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteGameDictionary() throws Exception {
        // Initialize the database
        gameDictionaryService.save(gameDictionary);

        int databaseSizeBeforeDelete = gameDictionaryRepository.findAll().size();

        // Get the gameDictionary
        restGameDictionaryMockMvc.perform(delete("/api/game-dictionaries/{id}", gameDictionary.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean gameDictionaryExistsInEs = gameDictionarySearchRepository.exists(gameDictionary.getId());
        assertThat(gameDictionaryExistsInEs).isFalse();

        // Validate the database is empty
        List<GameDictionary> gameDictionaryList = gameDictionaryRepository.findAll();
        assertThat(gameDictionaryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchGameDictionary() throws Exception {
        // Initialize the database
        gameDictionaryService.save(gameDictionary);

        // Search the gameDictionary
        restGameDictionaryMockMvc.perform(get("/api/_search/game-dictionaries?query=id:" + gameDictionary.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(gameDictionary.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(GameDictionary.class);
        GameDictionary gameDictionary1 = new GameDictionary();
        gameDictionary1.setId(1L);
        GameDictionary gameDictionary2 = new GameDictionary();
        gameDictionary2.setId(gameDictionary1.getId());
        assertThat(gameDictionary1).isEqualTo(gameDictionary2);
        gameDictionary2.setId(2L);
        assertThat(gameDictionary1).isNotEqualTo(gameDictionary2);
        gameDictionary1.setId(null);
        assertThat(gameDictionary1).isNotEqualTo(gameDictionary2);
    }
}
