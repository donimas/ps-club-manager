package kz.ast.donimas.ps.web.rest;

import kz.ast.donimas.ps.PsClubManagerApp;

import kz.ast.donimas.ps.domain.ExtraJoystick;
import kz.ast.donimas.ps.repository.ExtraJoystickRepository;
import kz.ast.donimas.ps.service.ExtraJoystickService;
import kz.ast.donimas.ps.repository.search.ExtraJoystickSearchRepository;
import kz.ast.donimas.ps.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static kz.ast.donimas.ps.web.rest.TestUtil.sameInstant;
import static kz.ast.donimas.ps.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ExtraJoystickResource REST controller.
 *
 * @see ExtraJoystickResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PsClubManagerApp.class)
public class ExtraJoystickResourceIntTest {

    private static final Integer DEFAULT_AMOUNT = 1;
    private static final Integer UPDATED_AMOUNT = 2;

    private static final ZonedDateTime DEFAULT_STARTED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_STARTED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_FINISHED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FINISHED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final BigDecimal DEFAULT_PLAYED_HOURS = new BigDecimal(1);
    private static final BigDecimal UPDATED_PLAYED_HOURS = new BigDecimal(2);

    private static final BigDecimal DEFAULT_PLAYED_MINUTES = new BigDecimal(1);
    private static final BigDecimal UPDATED_PLAYED_MINUTES = new BigDecimal(2);

    private static final BigDecimal DEFAULT_TOTAL_PRICE = new BigDecimal(1);
    private static final BigDecimal UPDATED_TOTAL_PRICE = new BigDecimal(2);

    @Autowired
    private ExtraJoystickRepository extraJoystickRepository;

    @Autowired
    private ExtraJoystickService extraJoystickService;

    @Autowired
    private ExtraJoystickSearchRepository extraJoystickSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restExtraJoystickMockMvc;

    private ExtraJoystick extraJoystick;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ExtraJoystickResource extraJoystickResource = new ExtraJoystickResource(extraJoystickService);
        this.restExtraJoystickMockMvc = MockMvcBuilders.standaloneSetup(extraJoystickResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExtraJoystick createEntity(EntityManager em) {
        ExtraJoystick extraJoystick = new ExtraJoystick()
            .amount(DEFAULT_AMOUNT)
            .startedAt(DEFAULT_STARTED_AT)
            .finishedAt(DEFAULT_FINISHED_AT)
            .playedHours(DEFAULT_PLAYED_HOURS)
            .playedMinutes(DEFAULT_PLAYED_MINUTES)
            .totalPrice(DEFAULT_TOTAL_PRICE);
        return extraJoystick;
    }

    @Before
    public void initTest() {
        extraJoystickSearchRepository.deleteAll();
        extraJoystick = createEntity(em);
    }

    @Test
    @Transactional
    public void createExtraJoystick() throws Exception {
        int databaseSizeBeforeCreate = extraJoystickRepository.findAll().size();

        // Create the ExtraJoystick
        restExtraJoystickMockMvc.perform(post("/api/extra-joysticks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(extraJoystick)))
            .andExpect(status().isCreated());

        // Validate the ExtraJoystick in the database
        List<ExtraJoystick> extraJoystickList = extraJoystickRepository.findAll();
        assertThat(extraJoystickList).hasSize(databaseSizeBeforeCreate + 1);
        ExtraJoystick testExtraJoystick = extraJoystickList.get(extraJoystickList.size() - 1);
        assertThat(testExtraJoystick.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testExtraJoystick.getStartedAt()).isEqualTo(DEFAULT_STARTED_AT);
        assertThat(testExtraJoystick.getFinishedAt()).isEqualTo(DEFAULT_FINISHED_AT);
        assertThat(testExtraJoystick.getPlayedHours()).isEqualTo(DEFAULT_PLAYED_HOURS);
        assertThat(testExtraJoystick.getPlayedMinutes()).isEqualTo(DEFAULT_PLAYED_MINUTES);
        assertThat(testExtraJoystick.getTotalPrice()).isEqualTo(DEFAULT_TOTAL_PRICE);

        // Validate the ExtraJoystick in Elasticsearch
        ExtraJoystick extraJoystickEs = extraJoystickSearchRepository.findOne(testExtraJoystick.getId());
        assertThat(testExtraJoystick.getStartedAt()).isEqualTo(testExtraJoystick.getStartedAt());
        assertThat(testExtraJoystick.getFinishedAt()).isEqualTo(testExtraJoystick.getFinishedAt());
        assertThat(extraJoystickEs).isEqualToIgnoringGivenFields(testExtraJoystick, "startedAt", "finishedAt");
    }

    @Test
    @Transactional
    public void createExtraJoystickWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = extraJoystickRepository.findAll().size();

        // Create the ExtraJoystick with an existing ID
        extraJoystick.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restExtraJoystickMockMvc.perform(post("/api/extra-joysticks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(extraJoystick)))
            .andExpect(status().isBadRequest());

        // Validate the ExtraJoystick in the database
        List<ExtraJoystick> extraJoystickList = extraJoystickRepository.findAll();
        assertThat(extraJoystickList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllExtraJoysticks() throws Exception {
        // Initialize the database
        extraJoystickRepository.saveAndFlush(extraJoystick);

        // Get all the extraJoystickList
        restExtraJoystickMockMvc.perform(get("/api/extra-joysticks?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(extraJoystick.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT)))
            .andExpect(jsonPath("$.[*].startedAt").value(hasItem(sameInstant(DEFAULT_STARTED_AT))))
            .andExpect(jsonPath("$.[*].finishedAt").value(hasItem(sameInstant(DEFAULT_FINISHED_AT))))
            .andExpect(jsonPath("$.[*].playedHours").value(hasItem(DEFAULT_PLAYED_HOURS.intValue())))
            .andExpect(jsonPath("$.[*].playedMinutes").value(hasItem(DEFAULT_PLAYED_MINUTES.intValue())))
            .andExpect(jsonPath("$.[*].totalPrice").value(hasItem(DEFAULT_TOTAL_PRICE.intValue())));
    }

    @Test
    @Transactional
    public void getExtraJoystick() throws Exception {
        // Initialize the database
        extraJoystickRepository.saveAndFlush(extraJoystick);

        // Get the extraJoystick
        restExtraJoystickMockMvc.perform(get("/api/extra-joysticks/{id}", extraJoystick.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(extraJoystick.getId().intValue()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT))
            .andExpect(jsonPath("$.startedAt").value(sameInstant(DEFAULT_STARTED_AT)))
            .andExpect(jsonPath("$.finishedAt").value(sameInstant(DEFAULT_FINISHED_AT)))
            .andExpect(jsonPath("$.playedHours").value(DEFAULT_PLAYED_HOURS.intValue()))
            .andExpect(jsonPath("$.playedMinutes").value(DEFAULT_PLAYED_MINUTES.intValue()))
            .andExpect(jsonPath("$.totalPrice").value(DEFAULT_TOTAL_PRICE.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingExtraJoystick() throws Exception {
        // Get the extraJoystick
        restExtraJoystickMockMvc.perform(get("/api/extra-joysticks/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateExtraJoystick() throws Exception {
        // Initialize the database
        extraJoystickService.save(extraJoystick);

        int databaseSizeBeforeUpdate = extraJoystickRepository.findAll().size();

        // Update the extraJoystick
        ExtraJoystick updatedExtraJoystick = extraJoystickRepository.findOne(extraJoystick.getId());
        // Disconnect from session so that the updates on updatedExtraJoystick are not directly saved in db
        em.detach(updatedExtraJoystick);
        updatedExtraJoystick
            .amount(UPDATED_AMOUNT)
            .startedAt(UPDATED_STARTED_AT)
            .finishedAt(UPDATED_FINISHED_AT)
            .playedHours(UPDATED_PLAYED_HOURS)
            .playedMinutes(UPDATED_PLAYED_MINUTES)
            .totalPrice(UPDATED_TOTAL_PRICE);

        restExtraJoystickMockMvc.perform(put("/api/extra-joysticks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedExtraJoystick)))
            .andExpect(status().isOk());

        // Validate the ExtraJoystick in the database
        List<ExtraJoystick> extraJoystickList = extraJoystickRepository.findAll();
        assertThat(extraJoystickList).hasSize(databaseSizeBeforeUpdate);
        ExtraJoystick testExtraJoystick = extraJoystickList.get(extraJoystickList.size() - 1);
        assertThat(testExtraJoystick.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testExtraJoystick.getStartedAt()).isEqualTo(UPDATED_STARTED_AT);
        assertThat(testExtraJoystick.getFinishedAt()).isEqualTo(UPDATED_FINISHED_AT);
        assertThat(testExtraJoystick.getPlayedHours()).isEqualTo(UPDATED_PLAYED_HOURS);
        assertThat(testExtraJoystick.getPlayedMinutes()).isEqualTo(UPDATED_PLAYED_MINUTES);
        assertThat(testExtraJoystick.getTotalPrice()).isEqualTo(UPDATED_TOTAL_PRICE);

        // Validate the ExtraJoystick in Elasticsearch
        ExtraJoystick extraJoystickEs = extraJoystickSearchRepository.findOne(testExtraJoystick.getId());
        assertThat(testExtraJoystick.getStartedAt()).isEqualTo(testExtraJoystick.getStartedAt());
        assertThat(testExtraJoystick.getFinishedAt()).isEqualTo(testExtraJoystick.getFinishedAt());
        assertThat(extraJoystickEs).isEqualToIgnoringGivenFields(testExtraJoystick, "startedAt", "finishedAt");
    }

    @Test
    @Transactional
    public void updateNonExistingExtraJoystick() throws Exception {
        int databaseSizeBeforeUpdate = extraJoystickRepository.findAll().size();

        // Create the ExtraJoystick

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restExtraJoystickMockMvc.perform(put("/api/extra-joysticks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(extraJoystick)))
            .andExpect(status().isCreated());

        // Validate the ExtraJoystick in the database
        List<ExtraJoystick> extraJoystickList = extraJoystickRepository.findAll();
        assertThat(extraJoystickList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteExtraJoystick() throws Exception {
        // Initialize the database
        extraJoystickService.save(extraJoystick);

        int databaseSizeBeforeDelete = extraJoystickRepository.findAll().size();

        // Get the extraJoystick
        restExtraJoystickMockMvc.perform(delete("/api/extra-joysticks/{id}", extraJoystick.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean extraJoystickExistsInEs = extraJoystickSearchRepository.exists(extraJoystick.getId());
        assertThat(extraJoystickExistsInEs).isFalse();

        // Validate the database is empty
        List<ExtraJoystick> extraJoystickList = extraJoystickRepository.findAll();
        assertThat(extraJoystickList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchExtraJoystick() throws Exception {
        // Initialize the database
        extraJoystickService.save(extraJoystick);

        // Search the extraJoystick
        restExtraJoystickMockMvc.perform(get("/api/_search/extra-joysticks?query=id:" + extraJoystick.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(extraJoystick.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT)))
            .andExpect(jsonPath("$.[*].startedAt").value(hasItem(sameInstant(DEFAULT_STARTED_AT))))
            .andExpect(jsonPath("$.[*].finishedAt").value(hasItem(sameInstant(DEFAULT_FINISHED_AT))))
            .andExpect(jsonPath("$.[*].playedHours").value(hasItem(DEFAULT_PLAYED_HOURS.intValue())))
            .andExpect(jsonPath("$.[*].playedMinutes").value(hasItem(DEFAULT_PLAYED_MINUTES.intValue())))
            .andExpect(jsonPath("$.[*].totalPrice").value(hasItem(DEFAULT_TOTAL_PRICE.intValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExtraJoystick.class);
        ExtraJoystick extraJoystick1 = new ExtraJoystick();
        extraJoystick1.setId(1L);
        ExtraJoystick extraJoystick2 = new ExtraJoystick();
        extraJoystick2.setId(extraJoystick1.getId());
        assertThat(extraJoystick1).isEqualTo(extraJoystick2);
        extraJoystick2.setId(2L);
        assertThat(extraJoystick1).isNotEqualTo(extraJoystick2);
        extraJoystick1.setId(null);
        assertThat(extraJoystick1).isNotEqualTo(extraJoystick2);
    }
}
