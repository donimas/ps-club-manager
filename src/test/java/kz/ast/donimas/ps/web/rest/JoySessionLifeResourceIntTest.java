package kz.ast.donimas.ps.web.rest;

import kz.ast.donimas.ps.PsClubManagerApp;

import kz.ast.donimas.ps.domain.JoySessionLife;
import kz.ast.donimas.ps.repository.JoySessionLifeRepository;
import kz.ast.donimas.ps.service.JoySessionLifeService;
import kz.ast.donimas.ps.repository.search.JoySessionLifeSearchRepository;
import kz.ast.donimas.ps.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static kz.ast.donimas.ps.web.rest.TestUtil.sameInstant;
import static kz.ast.donimas.ps.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the JoySessionLifeResource REST controller.
 *
 * @see JoySessionLifeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PsClubManagerApp.class)
public class JoySessionLifeResourceIntTest {

    private static final ZonedDateTime DEFAULT_STARTED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_STARTED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_FINISHED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FINISHED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final BigDecimal DEFAULT_JOY_HOURS = new BigDecimal(1);
    private static final BigDecimal UPDATED_JOY_HOURS = new BigDecimal(2);

    private static final BigDecimal DEFAULT_JOY_MINUTES = new BigDecimal(1);
    private static final BigDecimal UPDATED_JOY_MINUTES = new BigDecimal(2);

    private static final BigDecimal DEFAULT_TOTAL_PRICE = new BigDecimal(1);
    private static final BigDecimal UPDATED_TOTAL_PRICE = new BigDecimal(2);

    @Autowired
    private JoySessionLifeRepository joySessionLifeRepository;

    @Autowired
    private JoySessionLifeService joySessionLifeService;

    @Autowired
    private JoySessionLifeSearchRepository joySessionLifeSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restJoySessionLifeMockMvc;

    private JoySessionLife joySessionLife;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final JoySessionLifeResource joySessionLifeResource = new JoySessionLifeResource(joySessionLifeService);
        this.restJoySessionLifeMockMvc = MockMvcBuilders.standaloneSetup(joySessionLifeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static JoySessionLife createEntity(EntityManager em) {
        JoySessionLife joySessionLife = new JoySessionLife()
            .startedAt(DEFAULT_STARTED_AT)
            .finishedAt(DEFAULT_FINISHED_AT)
            .joyHours(DEFAULT_JOY_HOURS)
            .joyMinutes(DEFAULT_JOY_MINUTES)
            .totalPrice(DEFAULT_TOTAL_PRICE);
        return joySessionLife;
    }

    @Before
    public void initTest() {
        joySessionLifeSearchRepository.deleteAll();
        joySessionLife = createEntity(em);
    }

    @Test
    @Transactional
    public void createJoySessionLife() throws Exception {
        int databaseSizeBeforeCreate = joySessionLifeRepository.findAll().size();

        // Create the JoySessionLife
        restJoySessionLifeMockMvc.perform(post("/api/joy-session-lives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(joySessionLife)))
            .andExpect(status().isCreated());

        // Validate the JoySessionLife in the database
        List<JoySessionLife> joySessionLifeList = joySessionLifeRepository.findAll();
        assertThat(joySessionLifeList).hasSize(databaseSizeBeforeCreate + 1);
        JoySessionLife testJoySessionLife = joySessionLifeList.get(joySessionLifeList.size() - 1);
        assertThat(testJoySessionLife.getStartedAt()).isEqualTo(DEFAULT_STARTED_AT);
        assertThat(testJoySessionLife.getFinishedAt()).isEqualTo(DEFAULT_FINISHED_AT);
        assertThat(testJoySessionLife.getJoyHours()).isEqualTo(DEFAULT_JOY_HOURS);
        assertThat(testJoySessionLife.getJoyMinutes()).isEqualTo(DEFAULT_JOY_MINUTES);
        assertThat(testJoySessionLife.getTotalPrice()).isEqualTo(DEFAULT_TOTAL_PRICE);

        // Validate the JoySessionLife in Elasticsearch
        JoySessionLife joySessionLifeEs = joySessionLifeSearchRepository.findOne(testJoySessionLife.getId());
        assertThat(testJoySessionLife.getStartedAt()).isEqualTo(testJoySessionLife.getStartedAt());
        assertThat(testJoySessionLife.getFinishedAt()).isEqualTo(testJoySessionLife.getFinishedAt());
        assertThat(joySessionLifeEs).isEqualToIgnoringGivenFields(testJoySessionLife, "startedAt", "finishedAt");
    }

    @Test
    @Transactional
    public void createJoySessionLifeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = joySessionLifeRepository.findAll().size();

        // Create the JoySessionLife with an existing ID
        joySessionLife.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJoySessionLifeMockMvc.perform(post("/api/joy-session-lives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(joySessionLife)))
            .andExpect(status().isBadRequest());

        // Validate the JoySessionLife in the database
        List<JoySessionLife> joySessionLifeList = joySessionLifeRepository.findAll();
        assertThat(joySessionLifeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllJoySessionLives() throws Exception {
        // Initialize the database
        joySessionLifeRepository.saveAndFlush(joySessionLife);

        // Get all the joySessionLifeList
        restJoySessionLifeMockMvc.perform(get("/api/joy-session-lives?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(joySessionLife.getId().intValue())))
            .andExpect(jsonPath("$.[*].startedAt").value(hasItem(sameInstant(DEFAULT_STARTED_AT))))
            .andExpect(jsonPath("$.[*].finishedAt").value(hasItem(sameInstant(DEFAULT_FINISHED_AT))))
            .andExpect(jsonPath("$.[*].joyHours").value(hasItem(DEFAULT_JOY_HOURS.intValue())))
            .andExpect(jsonPath("$.[*].joyMinutes").value(hasItem(DEFAULT_JOY_MINUTES.intValue())))
            .andExpect(jsonPath("$.[*].totalPrice").value(hasItem(DEFAULT_TOTAL_PRICE.intValue())));
    }

    @Test
    @Transactional
    public void getJoySessionLife() throws Exception {
        // Initialize the database
        joySessionLifeRepository.saveAndFlush(joySessionLife);

        // Get the joySessionLife
        restJoySessionLifeMockMvc.perform(get("/api/joy-session-lives/{id}", joySessionLife.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(joySessionLife.getId().intValue()))
            .andExpect(jsonPath("$.startedAt").value(sameInstant(DEFAULT_STARTED_AT)))
            .andExpect(jsonPath("$.finishedAt").value(sameInstant(DEFAULT_FINISHED_AT)))
            .andExpect(jsonPath("$.joyHours").value(DEFAULT_JOY_HOURS.intValue()))
            .andExpect(jsonPath("$.joyMinutes").value(DEFAULT_JOY_MINUTES.intValue()))
            .andExpect(jsonPath("$.totalPrice").value(DEFAULT_TOTAL_PRICE.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingJoySessionLife() throws Exception {
        // Get the joySessionLife
        restJoySessionLifeMockMvc.perform(get("/api/joy-session-lives/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJoySessionLife() throws Exception {
        // Initialize the database
        joySessionLifeService.save(joySessionLife);

        int databaseSizeBeforeUpdate = joySessionLifeRepository.findAll().size();

        // Update the joySessionLife
        JoySessionLife updatedJoySessionLife = joySessionLifeRepository.findOne(joySessionLife.getId());
        // Disconnect from session so that the updates on updatedJoySessionLife are not directly saved in db
        em.detach(updatedJoySessionLife);
        updatedJoySessionLife
            .startedAt(UPDATED_STARTED_AT)
            .finishedAt(UPDATED_FINISHED_AT)
            .joyHours(UPDATED_JOY_HOURS)
            .joyMinutes(UPDATED_JOY_MINUTES)
            .totalPrice(UPDATED_TOTAL_PRICE);

        restJoySessionLifeMockMvc.perform(put("/api/joy-session-lives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedJoySessionLife)))
            .andExpect(status().isOk());

        // Validate the JoySessionLife in the database
        List<JoySessionLife> joySessionLifeList = joySessionLifeRepository.findAll();
        assertThat(joySessionLifeList).hasSize(databaseSizeBeforeUpdate);
        JoySessionLife testJoySessionLife = joySessionLifeList.get(joySessionLifeList.size() - 1);
        assertThat(testJoySessionLife.getStartedAt()).isEqualTo(UPDATED_STARTED_AT);
        assertThat(testJoySessionLife.getFinishedAt()).isEqualTo(UPDATED_FINISHED_AT);
        assertThat(testJoySessionLife.getJoyHours()).isEqualTo(UPDATED_JOY_HOURS);
        assertThat(testJoySessionLife.getJoyMinutes()).isEqualTo(UPDATED_JOY_MINUTES);
        assertThat(testJoySessionLife.getTotalPrice()).isEqualTo(UPDATED_TOTAL_PRICE);

        // Validate the JoySessionLife in Elasticsearch
        JoySessionLife joySessionLifeEs = joySessionLifeSearchRepository.findOne(testJoySessionLife.getId());
        assertThat(testJoySessionLife.getStartedAt()).isEqualTo(testJoySessionLife.getStartedAt());
        assertThat(testJoySessionLife.getFinishedAt()).isEqualTo(testJoySessionLife.getFinishedAt());
        assertThat(joySessionLifeEs).isEqualToIgnoringGivenFields(testJoySessionLife, "startedAt", "finishedAt");
    }

    @Test
    @Transactional
    public void updateNonExistingJoySessionLife() throws Exception {
        int databaseSizeBeforeUpdate = joySessionLifeRepository.findAll().size();

        // Create the JoySessionLife

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restJoySessionLifeMockMvc.perform(put("/api/joy-session-lives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(joySessionLife)))
            .andExpect(status().isCreated());

        // Validate the JoySessionLife in the database
        List<JoySessionLife> joySessionLifeList = joySessionLifeRepository.findAll();
        assertThat(joySessionLifeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteJoySessionLife() throws Exception {
        // Initialize the database
        joySessionLifeService.save(joySessionLife);

        int databaseSizeBeforeDelete = joySessionLifeRepository.findAll().size();

        // Get the joySessionLife
        restJoySessionLifeMockMvc.perform(delete("/api/joy-session-lives/{id}", joySessionLife.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean joySessionLifeExistsInEs = joySessionLifeSearchRepository.exists(joySessionLife.getId());
        assertThat(joySessionLifeExistsInEs).isFalse();

        // Validate the database is empty
        List<JoySessionLife> joySessionLifeList = joySessionLifeRepository.findAll();
        assertThat(joySessionLifeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchJoySessionLife() throws Exception {
        // Initialize the database
        joySessionLifeService.save(joySessionLife);

        // Search the joySessionLife
        restJoySessionLifeMockMvc.perform(get("/api/_search/joy-session-lives?query=id:" + joySessionLife.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(joySessionLife.getId().intValue())))
            .andExpect(jsonPath("$.[*].startedAt").value(hasItem(sameInstant(DEFAULT_STARTED_AT))))
            .andExpect(jsonPath("$.[*].finishedAt").value(hasItem(sameInstant(DEFAULT_FINISHED_AT))))
            .andExpect(jsonPath("$.[*].joyHours").value(hasItem(DEFAULT_JOY_HOURS.intValue())))
            .andExpect(jsonPath("$.[*].joyMinutes").value(hasItem(DEFAULT_JOY_MINUTES.intValue())))
            .andExpect(jsonPath("$.[*].totalPrice").value(hasItem(DEFAULT_TOTAL_PRICE.intValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(JoySessionLife.class);
        JoySessionLife joySessionLife1 = new JoySessionLife();
        joySessionLife1.setId(1L);
        JoySessionLife joySessionLife2 = new JoySessionLife();
        joySessionLife2.setId(joySessionLife1.getId());
        assertThat(joySessionLife1).isEqualTo(joySessionLife2);
        joySessionLife2.setId(2L);
        assertThat(joySessionLife1).isNotEqualTo(joySessionLife2);
        joySessionLife1.setId(null);
        assertThat(joySessionLife1).isNotEqualTo(joySessionLife2);
    }
}
