package kz.ast.donimas.ps.web.rest;

import kz.ast.donimas.ps.PsClubManagerApp;

import kz.ast.donimas.ps.domain.Zone;
import kz.ast.donimas.ps.repository.ZoneRepository;
import kz.ast.donimas.ps.service.ZoneService;
import kz.ast.donimas.ps.repository.search.ZoneSearchRepository;
import kz.ast.donimas.ps.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static kz.ast.donimas.ps.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ZoneResource REST controller.
 *
 * @see ZoneResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PsClubManagerApp.class)
public class ZoneResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_SEATS_AMOUNT = 1;
    private static final Integer UPDATED_SEATS_AMOUNT = 2;

    private static final Integer DEFAULT_JOYSTICKS_AMOUNT_REGULATION = 1;
    private static final Integer UPDATED_JOYSTICKS_AMOUNT_REGULATION = 2;

    private static final Float DEFAULT_TV_DIAGONAL = 1F;
    private static final Float UPDATED_TV_DIAGONAL = 2F;

    private static final Float DEFAULT_TV_RESOLUTION = 1F;
    private static final Float UPDATED_TV_RESOLUTION = 2F;

    private static final Boolean DEFAULT_FLAG_DELETED = false;
    private static final Boolean UPDATED_FLAG_DELETED = true;

    private static final BigDecimal DEFAULT_MINIMUM_PLAY_MINUTES_REGULATION = new BigDecimal(1);
    private static final BigDecimal UPDATED_MINIMUM_PLAY_MINUTES_REGULATION = new BigDecimal(2);

    @Autowired
    private ZoneRepository zoneRepository;

    @Autowired
    private ZoneService zoneService;

    @Autowired
    private ZoneSearchRepository zoneSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restZoneMockMvc;

    private Zone zone;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ZoneResource zoneResource = new ZoneResource(zoneService);
        this.restZoneMockMvc = MockMvcBuilders.standaloneSetup(zoneResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Zone createEntity(EntityManager em) {
        Zone zone = new Zone()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .seatsAmount(DEFAULT_SEATS_AMOUNT)
            .joysticksAmountRegulation(DEFAULT_JOYSTICKS_AMOUNT_REGULATION)
            .tvDiagonal(DEFAULT_TV_DIAGONAL)
            .tvResolution(DEFAULT_TV_RESOLUTION)
            .flagDeleted(DEFAULT_FLAG_DELETED)
            .minimumPlayMinutesRegulation(DEFAULT_MINIMUM_PLAY_MINUTES_REGULATION);
        return zone;
    }

    @Before
    public void initTest() {
        zoneSearchRepository.deleteAll();
        zone = createEntity(em);
    }

    @Test
    @Transactional
    public void createZone() throws Exception {
        int databaseSizeBeforeCreate = zoneRepository.findAll().size();

        // Create the Zone
        restZoneMockMvc.perform(post("/api/zones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(zone)))
            .andExpect(status().isCreated());

        // Validate the Zone in the database
        List<Zone> zoneList = zoneRepository.findAll();
        assertThat(zoneList).hasSize(databaseSizeBeforeCreate + 1);
        Zone testZone = zoneList.get(zoneList.size() - 1);
        assertThat(testZone.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testZone.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testZone.getSeatsAmount()).isEqualTo(DEFAULT_SEATS_AMOUNT);
        assertThat(testZone.getJoysticksAmountRegulation()).isEqualTo(DEFAULT_JOYSTICKS_AMOUNT_REGULATION);
        assertThat(testZone.getTvDiagonal()).isEqualTo(DEFAULT_TV_DIAGONAL);
        assertThat(testZone.getTvResolution()).isEqualTo(DEFAULT_TV_RESOLUTION);
        assertThat(testZone.isFlagDeleted()).isEqualTo(DEFAULT_FLAG_DELETED);
        assertThat(testZone.getMinimumPlayMinutesRegulation()).isEqualTo(DEFAULT_MINIMUM_PLAY_MINUTES_REGULATION);

        // Validate the Zone in Elasticsearch
        Zone zoneEs = zoneSearchRepository.findOne(testZone.getId());
        assertThat(zoneEs).isEqualToIgnoringGivenFields(testZone);
    }

    @Test
    @Transactional
    public void createZoneWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = zoneRepository.findAll().size();

        // Create the Zone with an existing ID
        zone.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restZoneMockMvc.perform(post("/api/zones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(zone)))
            .andExpect(status().isBadRequest());

        // Validate the Zone in the database
        List<Zone> zoneList = zoneRepository.findAll();
        assertThat(zoneList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = zoneRepository.findAll().size();
        // set the field null
        zone.setName(null);

        // Create the Zone, which fails.

        restZoneMockMvc.perform(post("/api/zones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(zone)))
            .andExpect(status().isBadRequest());

        List<Zone> zoneList = zoneRepository.findAll();
        assertThat(zoneList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllZones() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList
        restZoneMockMvc.perform(get("/api/zones?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(zone.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].seatsAmount").value(hasItem(DEFAULT_SEATS_AMOUNT)))
            .andExpect(jsonPath("$.[*].joysticksAmountRegulation").value(hasItem(DEFAULT_JOYSTICKS_AMOUNT_REGULATION)))
            .andExpect(jsonPath("$.[*].tvDiagonal").value(hasItem(DEFAULT_TV_DIAGONAL.doubleValue())))
            .andExpect(jsonPath("$.[*].tvResolution").value(hasItem(DEFAULT_TV_RESOLUTION.doubleValue())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].minimumPlayMinutesRegulation").value(hasItem(DEFAULT_MINIMUM_PLAY_MINUTES_REGULATION.intValue())));
    }

    @Test
    @Transactional
    public void getZone() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get the zone
        restZoneMockMvc.perform(get("/api/zones/{id}", zone.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(zone.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.seatsAmount").value(DEFAULT_SEATS_AMOUNT))
            .andExpect(jsonPath("$.joysticksAmountRegulation").value(DEFAULT_JOYSTICKS_AMOUNT_REGULATION))
            .andExpect(jsonPath("$.tvDiagonal").value(DEFAULT_TV_DIAGONAL.doubleValue()))
            .andExpect(jsonPath("$.tvResolution").value(DEFAULT_TV_RESOLUTION.doubleValue()))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED.booleanValue()))
            .andExpect(jsonPath("$.minimumPlayMinutesRegulation").value(DEFAULT_MINIMUM_PLAY_MINUTES_REGULATION.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingZone() throws Exception {
        // Get the zone
        restZoneMockMvc.perform(get("/api/zones/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateZone() throws Exception {
        // Initialize the database
        zoneService.save(zone);

        int databaseSizeBeforeUpdate = zoneRepository.findAll().size();

        // Update the zone
        Zone updatedZone = zoneRepository.findOne(zone.getId());
        // Disconnect from session so that the updates on updatedZone are not directly saved in db
        em.detach(updatedZone);
        updatedZone
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .seatsAmount(UPDATED_SEATS_AMOUNT)
            .joysticksAmountRegulation(UPDATED_JOYSTICKS_AMOUNT_REGULATION)
            .tvDiagonal(UPDATED_TV_DIAGONAL)
            .tvResolution(UPDATED_TV_RESOLUTION)
            .flagDeleted(UPDATED_FLAG_DELETED)
            .minimumPlayMinutesRegulation(UPDATED_MINIMUM_PLAY_MINUTES_REGULATION);

        restZoneMockMvc.perform(put("/api/zones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedZone)))
            .andExpect(status().isOk());

        // Validate the Zone in the database
        List<Zone> zoneList = zoneRepository.findAll();
        assertThat(zoneList).hasSize(databaseSizeBeforeUpdate);
        Zone testZone = zoneList.get(zoneList.size() - 1);
        assertThat(testZone.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testZone.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testZone.getSeatsAmount()).isEqualTo(UPDATED_SEATS_AMOUNT);
        assertThat(testZone.getJoysticksAmountRegulation()).isEqualTo(UPDATED_JOYSTICKS_AMOUNT_REGULATION);
        assertThat(testZone.getTvDiagonal()).isEqualTo(UPDATED_TV_DIAGONAL);
        assertThat(testZone.getTvResolution()).isEqualTo(UPDATED_TV_RESOLUTION);
        assertThat(testZone.isFlagDeleted()).isEqualTo(UPDATED_FLAG_DELETED);
        assertThat(testZone.getMinimumPlayMinutesRegulation()).isEqualTo(UPDATED_MINIMUM_PLAY_MINUTES_REGULATION);

        // Validate the Zone in Elasticsearch
        Zone zoneEs = zoneSearchRepository.findOne(testZone.getId());
        assertThat(zoneEs).isEqualToIgnoringGivenFields(testZone);
    }

    @Test
    @Transactional
    public void updateNonExistingZone() throws Exception {
        int databaseSizeBeforeUpdate = zoneRepository.findAll().size();

        // Create the Zone

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restZoneMockMvc.perform(put("/api/zones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(zone)))
            .andExpect(status().isCreated());

        // Validate the Zone in the database
        List<Zone> zoneList = zoneRepository.findAll();
        assertThat(zoneList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteZone() throws Exception {
        // Initialize the database
        zoneService.save(zone);

        int databaseSizeBeforeDelete = zoneRepository.findAll().size();

        // Get the zone
        restZoneMockMvc.perform(delete("/api/zones/{id}", zone.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean zoneExistsInEs = zoneSearchRepository.exists(zone.getId());
        assertThat(zoneExistsInEs).isFalse();

        // Validate the database is empty
        List<Zone> zoneList = zoneRepository.findAll();
        assertThat(zoneList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchZone() throws Exception {
        // Initialize the database
        zoneService.save(zone);

        // Search the zone
        restZoneMockMvc.perform(get("/api/_search/zones?query=id:" + zone.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(zone.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].seatsAmount").value(hasItem(DEFAULT_SEATS_AMOUNT)))
            .andExpect(jsonPath("$.[*].joysticksAmountRegulation").value(hasItem(DEFAULT_JOYSTICKS_AMOUNT_REGULATION)))
            .andExpect(jsonPath("$.[*].tvDiagonal").value(hasItem(DEFAULT_TV_DIAGONAL.doubleValue())))
            .andExpect(jsonPath("$.[*].tvResolution").value(hasItem(DEFAULT_TV_RESOLUTION.doubleValue())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].minimumPlayMinutesRegulation").value(hasItem(DEFAULT_MINIMUM_PLAY_MINUTES_REGULATION.intValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Zone.class);
        Zone zone1 = new Zone();
        zone1.setId(1L);
        Zone zone2 = new Zone();
        zone2.setId(zone1.getId());
        assertThat(zone1).isEqualTo(zone2);
        zone2.setId(2L);
        assertThat(zone1).isNotEqualTo(zone2);
        zone1.setId(null);
        assertThat(zone1).isNotEqualTo(zone2);
    }
}
