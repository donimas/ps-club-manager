package kz.ast.donimas.ps.web.rest;

import kz.ast.donimas.ps.PsClubManagerApp;

import kz.ast.donimas.ps.domain.PricePackage;
import kz.ast.donimas.ps.repository.PricePackageRepository;
import kz.ast.donimas.ps.service.PricePackageService;
import kz.ast.donimas.ps.repository.search.PricePackageSearchRepository;
import kz.ast.donimas.ps.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static kz.ast.donimas.ps.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import kz.ast.donimas.ps.domain.enumeration.DurationType;
/**
 * Test class for the PricePackageResource REST controller.
 *
 * @see PricePackageResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PsClubManagerApp.class)
public class PricePackageResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Boolean DEFAULT_FLAG_DELETED = false;
    private static final Boolean UPDATED_FLAG_DELETED = true;

    private static final DurationType DEFAULT_TYPE = DurationType.HOURS;
    private static final DurationType UPDATED_TYPE = DurationType.STARTEND;

    private static final Instant DEFAULT_START_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_START_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_FINISH_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FINISH_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_HOURS_AMOUNT = 1;
    private static final Integer UPDATED_HOURS_AMOUNT = 2;

    private static final BigDecimal DEFAULT_PRICE = new BigDecimal(1);
    private static final BigDecimal UPDATED_PRICE = new BigDecimal(2);

    @Autowired
    private PricePackageRepository pricePackageRepository;

    @Autowired
    private PricePackageService pricePackageService;

    @Autowired
    private PricePackageSearchRepository pricePackageSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPricePackageMockMvc;

    private PricePackage pricePackage;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PricePackageResource pricePackageResource = new PricePackageResource(pricePackageService);
        this.restPricePackageMockMvc = MockMvcBuilders.standaloneSetup(pricePackageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PricePackage createEntity(EntityManager em) {
        PricePackage pricePackage = new PricePackage()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .flagDeleted(DEFAULT_FLAG_DELETED)
            .type(DEFAULT_TYPE)
            .startAt(DEFAULT_START_AT)
            .finishAt(DEFAULT_FINISH_AT)
            .hoursAmount(DEFAULT_HOURS_AMOUNT)
            .price(DEFAULT_PRICE);
        return pricePackage;
    }

    @Before
    public void initTest() {
        pricePackageSearchRepository.deleteAll();
        pricePackage = createEntity(em);
    }

    @Test
    @Transactional
    public void createPricePackage() throws Exception {
        int databaseSizeBeforeCreate = pricePackageRepository.findAll().size();

        // Create the PricePackage
        restPricePackageMockMvc.perform(post("/api/price-packages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pricePackage)))
            .andExpect(status().isCreated());

        // Validate the PricePackage in the database
        List<PricePackage> pricePackageList = pricePackageRepository.findAll();
        assertThat(pricePackageList).hasSize(databaseSizeBeforeCreate + 1);
        PricePackage testPricePackage = pricePackageList.get(pricePackageList.size() - 1);
        assertThat(testPricePackage.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPricePackage.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testPricePackage.isFlagDeleted()).isEqualTo(DEFAULT_FLAG_DELETED);
        assertThat(testPricePackage.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testPricePackage.getStartAt()).isEqualTo(DEFAULT_START_AT);
        assertThat(testPricePackage.getFinishAt()).isEqualTo(DEFAULT_FINISH_AT);
        assertThat(testPricePackage.getHoursAmount()).isEqualTo(DEFAULT_HOURS_AMOUNT);
        assertThat(testPricePackage.getPrice()).isEqualTo(DEFAULT_PRICE);

        // Validate the PricePackage in Elasticsearch
        PricePackage pricePackageEs = pricePackageSearchRepository.findOne(testPricePackage.getId());
        assertThat(pricePackageEs).isEqualToIgnoringGivenFields(testPricePackage);
    }

    @Test
    @Transactional
    public void createPricePackageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pricePackageRepository.findAll().size();

        // Create the PricePackage with an existing ID
        pricePackage.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPricePackageMockMvc.perform(post("/api/price-packages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pricePackage)))
            .andExpect(status().isBadRequest());

        // Validate the PricePackage in the database
        List<PricePackage> pricePackageList = pricePackageRepository.findAll();
        assertThat(pricePackageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = pricePackageRepository.findAll().size();
        // set the field null
        pricePackage.setName(null);

        // Create the PricePackage, which fails.

        restPricePackageMockMvc.perform(post("/api/price-packages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pricePackage)))
            .andExpect(status().isBadRequest());

        List<PricePackage> pricePackageList = pricePackageRepository.findAll();
        assertThat(pricePackageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = pricePackageRepository.findAll().size();
        // set the field null
        pricePackage.setType(null);

        // Create the PricePackage, which fails.

        restPricePackageMockMvc.perform(post("/api/price-packages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pricePackage)))
            .andExpect(status().isBadRequest());

        List<PricePackage> pricePackageList = pricePackageRepository.findAll();
        assertThat(pricePackageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = pricePackageRepository.findAll().size();
        // set the field null
        pricePackage.setPrice(null);

        // Create the PricePackage, which fails.

        restPricePackageMockMvc.perform(post("/api/price-packages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pricePackage)))
            .andExpect(status().isBadRequest());

        List<PricePackage> pricePackageList = pricePackageRepository.findAll();
        assertThat(pricePackageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPricePackages() throws Exception {
        // Initialize the database
        pricePackageRepository.saveAndFlush(pricePackage);

        // Get all the pricePackageList
        restPricePackageMockMvc.perform(get("/api/price-packages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pricePackage.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].startAt").value(hasItem(DEFAULT_START_AT.toString())))
            .andExpect(jsonPath("$.[*].finishAt").value(hasItem(DEFAULT_FINISH_AT.toString())))
            .andExpect(jsonPath("$.[*].hoursAmount").value(hasItem(DEFAULT_HOURS_AMOUNT)))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.intValue())));
    }

    @Test
    @Transactional
    public void getPricePackage() throws Exception {
        // Initialize the database
        pricePackageRepository.saveAndFlush(pricePackage);

        // Get the pricePackage
        restPricePackageMockMvc.perform(get("/api/price-packages/{id}", pricePackage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pricePackage.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED.booleanValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.startAt").value(DEFAULT_START_AT.toString()))
            .andExpect(jsonPath("$.finishAt").value(DEFAULT_FINISH_AT.toString()))
            .andExpect(jsonPath("$.hoursAmount").value(DEFAULT_HOURS_AMOUNT))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingPricePackage() throws Exception {
        // Get the pricePackage
        restPricePackageMockMvc.perform(get("/api/price-packages/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePricePackage() throws Exception {
        // Initialize the database
        pricePackageService.save(pricePackage);

        int databaseSizeBeforeUpdate = pricePackageRepository.findAll().size();

        // Update the pricePackage
        PricePackage updatedPricePackage = pricePackageRepository.findOne(pricePackage.getId());
        // Disconnect from session so that the updates on updatedPricePackage are not directly saved in db
        em.detach(updatedPricePackage);
        updatedPricePackage
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .flagDeleted(UPDATED_FLAG_DELETED)
            .type(UPDATED_TYPE)
            .startAt(UPDATED_START_AT)
            .finishAt(UPDATED_FINISH_AT)
            .hoursAmount(UPDATED_HOURS_AMOUNT)
            .price(UPDATED_PRICE);

        restPricePackageMockMvc.perform(put("/api/price-packages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPricePackage)))
            .andExpect(status().isOk());

        // Validate the PricePackage in the database
        List<PricePackage> pricePackageList = pricePackageRepository.findAll();
        assertThat(pricePackageList).hasSize(databaseSizeBeforeUpdate);
        PricePackage testPricePackage = pricePackageList.get(pricePackageList.size() - 1);
        assertThat(testPricePackage.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPricePackage.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPricePackage.isFlagDeleted()).isEqualTo(UPDATED_FLAG_DELETED);
        assertThat(testPricePackage.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testPricePackage.getStartAt()).isEqualTo(UPDATED_START_AT);
        assertThat(testPricePackage.getFinishAt()).isEqualTo(UPDATED_FINISH_AT);
        assertThat(testPricePackage.getHoursAmount()).isEqualTo(UPDATED_HOURS_AMOUNT);
        assertThat(testPricePackage.getPrice()).isEqualTo(UPDATED_PRICE);

        // Validate the PricePackage in Elasticsearch
        PricePackage pricePackageEs = pricePackageSearchRepository.findOne(testPricePackage.getId());
        assertThat(pricePackageEs).isEqualToIgnoringGivenFields(testPricePackage);
    }

    @Test
    @Transactional
    public void updateNonExistingPricePackage() throws Exception {
        int databaseSizeBeforeUpdate = pricePackageRepository.findAll().size();

        // Create the PricePackage

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPricePackageMockMvc.perform(put("/api/price-packages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pricePackage)))
            .andExpect(status().isCreated());

        // Validate the PricePackage in the database
        List<PricePackage> pricePackageList = pricePackageRepository.findAll();
        assertThat(pricePackageList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePricePackage() throws Exception {
        // Initialize the database
        pricePackageService.save(pricePackage);

        int databaseSizeBeforeDelete = pricePackageRepository.findAll().size();

        // Get the pricePackage
        restPricePackageMockMvc.perform(delete("/api/price-packages/{id}", pricePackage.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean pricePackageExistsInEs = pricePackageSearchRepository.exists(pricePackage.getId());
        assertThat(pricePackageExistsInEs).isFalse();

        // Validate the database is empty
        List<PricePackage> pricePackageList = pricePackageRepository.findAll();
        assertThat(pricePackageList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPricePackage() throws Exception {
        // Initialize the database
        pricePackageService.save(pricePackage);

        // Search the pricePackage
        restPricePackageMockMvc.perform(get("/api/_search/price-packages?query=id:" + pricePackage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pricePackage.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].startAt").value(hasItem(DEFAULT_START_AT.toString())))
            .andExpect(jsonPath("$.[*].finishAt").value(hasItem(DEFAULT_FINISH_AT.toString())))
            .andExpect(jsonPath("$.[*].hoursAmount").value(hasItem(DEFAULT_HOURS_AMOUNT)))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.intValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PricePackage.class);
        PricePackage pricePackage1 = new PricePackage();
        pricePackage1.setId(1L);
        PricePackage pricePackage2 = new PricePackage();
        pricePackage2.setId(pricePackage1.getId());
        assertThat(pricePackage1).isEqualTo(pricePackage2);
        pricePackage2.setId(2L);
        assertThat(pricePackage1).isNotEqualTo(pricePackage2);
        pricePackage1.setId(null);
        assertThat(pricePackage1).isNotEqualTo(pricePackage2);
    }
}
