package kz.ast.donimas.ps.web.rest;

import kz.ast.donimas.ps.PsClubManagerApp;

import kz.ast.donimas.ps.domain.DayRegime;
import kz.ast.donimas.ps.repository.DayRegimeRepository;
import kz.ast.donimas.ps.service.DayRegimeService;
import kz.ast.donimas.ps.repository.search.DayRegimeSearchRepository;
import kz.ast.donimas.ps.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static kz.ast.donimas.ps.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DayRegimeResource REST controller.
 *
 * @see DayRegimeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PsClubManagerApp.class)
public class DayRegimeResourceIntTest {

    private static final Integer DEFAULT_DAY_SEQ = 1;
    private static final Integer UPDATED_DAY_SEQ = 2;

    private static final Instant DEFAULT_OPENED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_OPENED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_CLOSED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CLOSED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_FLAG_ALL_DAY_LONG = false;
    private static final Boolean UPDATED_FLAG_ALL_DAY_LONG = true;

    private static final Boolean DEFAULT_FLAG_TILL_LAST_CLIENT = false;
    private static final Boolean UPDATED_FLAG_TILL_LAST_CLIENT = true;

    @Autowired
    private DayRegimeRepository dayRegimeRepository;

    @Autowired
    private DayRegimeService dayRegimeService;

    @Autowired
    private DayRegimeSearchRepository dayRegimeSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDayRegimeMockMvc;

    private DayRegime dayRegime;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DayRegimeResource dayRegimeResource = new DayRegimeResource(dayRegimeService);
        this.restDayRegimeMockMvc = MockMvcBuilders.standaloneSetup(dayRegimeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DayRegime createEntity(EntityManager em) {
        DayRegime dayRegime = new DayRegime()
            .daySeq(DEFAULT_DAY_SEQ)
            .openedAt(DEFAULT_OPENED_AT)
            .closedAt(DEFAULT_CLOSED_AT)
            .flagAllDayLong(DEFAULT_FLAG_ALL_DAY_LONG)
            .flagTillLastClient(DEFAULT_FLAG_TILL_LAST_CLIENT);
        return dayRegime;
    }

    @Before
    public void initTest() {
        dayRegimeSearchRepository.deleteAll();
        dayRegime = createEntity(em);
    }

    @Test
    @Transactional
    public void createDayRegime() throws Exception {
        int databaseSizeBeforeCreate = dayRegimeRepository.findAll().size();

        // Create the DayRegime
        restDayRegimeMockMvc.perform(post("/api/day-regimes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dayRegime)))
            .andExpect(status().isCreated());

        // Validate the DayRegime in the database
        List<DayRegime> dayRegimeList = dayRegimeRepository.findAll();
        assertThat(dayRegimeList).hasSize(databaseSizeBeforeCreate + 1);
        DayRegime testDayRegime = dayRegimeList.get(dayRegimeList.size() - 1);
        assertThat(testDayRegime.getDaySeq()).isEqualTo(DEFAULT_DAY_SEQ);
        assertThat(testDayRegime.getOpenedAt()).isEqualTo(DEFAULT_OPENED_AT);
        assertThat(testDayRegime.getClosedAt()).isEqualTo(DEFAULT_CLOSED_AT);
        assertThat(testDayRegime.isFlagAllDayLong()).isEqualTo(DEFAULT_FLAG_ALL_DAY_LONG);
        assertThat(testDayRegime.isFlagTillLastClient()).isEqualTo(DEFAULT_FLAG_TILL_LAST_CLIENT);

        // Validate the DayRegime in Elasticsearch
        DayRegime dayRegimeEs = dayRegimeSearchRepository.findOne(testDayRegime.getId());
        assertThat(dayRegimeEs).isEqualToIgnoringGivenFields(testDayRegime);
    }

    @Test
    @Transactional
    public void createDayRegimeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dayRegimeRepository.findAll().size();

        // Create the DayRegime with an existing ID
        dayRegime.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDayRegimeMockMvc.perform(post("/api/day-regimes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dayRegime)))
            .andExpect(status().isBadRequest());

        // Validate the DayRegime in the database
        List<DayRegime> dayRegimeList = dayRegimeRepository.findAll();
        assertThat(dayRegimeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDaySeqIsRequired() throws Exception {
        int databaseSizeBeforeTest = dayRegimeRepository.findAll().size();
        // set the field null
        dayRegime.setDaySeq(null);

        // Create the DayRegime, which fails.

        restDayRegimeMockMvc.perform(post("/api/day-regimes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dayRegime)))
            .andExpect(status().isBadRequest());

        List<DayRegime> dayRegimeList = dayRegimeRepository.findAll();
        assertThat(dayRegimeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDayRegimes() throws Exception {
        // Initialize the database
        dayRegimeRepository.saveAndFlush(dayRegime);

        // Get all the dayRegimeList
        restDayRegimeMockMvc.perform(get("/api/day-regimes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dayRegime.getId().intValue())))
            .andExpect(jsonPath("$.[*].daySeq").value(hasItem(DEFAULT_DAY_SEQ)))
            .andExpect(jsonPath("$.[*].openedAt").value(hasItem(DEFAULT_OPENED_AT.toString())))
            .andExpect(jsonPath("$.[*].closedAt").value(hasItem(DEFAULT_CLOSED_AT.toString())))
            .andExpect(jsonPath("$.[*].flagAllDayLong").value(hasItem(DEFAULT_FLAG_ALL_DAY_LONG.booleanValue())))
            .andExpect(jsonPath("$.[*].flagTillLastClient").value(hasItem(DEFAULT_FLAG_TILL_LAST_CLIENT.booleanValue())));
    }

    @Test
    @Transactional
    public void getDayRegime() throws Exception {
        // Initialize the database
        dayRegimeRepository.saveAndFlush(dayRegime);

        // Get the dayRegime
        restDayRegimeMockMvc.perform(get("/api/day-regimes/{id}", dayRegime.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dayRegime.getId().intValue()))
            .andExpect(jsonPath("$.daySeq").value(DEFAULT_DAY_SEQ))
            .andExpect(jsonPath("$.openedAt").value(DEFAULT_OPENED_AT.toString()))
            .andExpect(jsonPath("$.closedAt").value(DEFAULT_CLOSED_AT.toString()))
            .andExpect(jsonPath("$.flagAllDayLong").value(DEFAULT_FLAG_ALL_DAY_LONG.booleanValue()))
            .andExpect(jsonPath("$.flagTillLastClient").value(DEFAULT_FLAG_TILL_LAST_CLIENT.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingDayRegime() throws Exception {
        // Get the dayRegime
        restDayRegimeMockMvc.perform(get("/api/day-regimes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDayRegime() throws Exception {
        // Initialize the database
        dayRegimeService.save(dayRegime);

        int databaseSizeBeforeUpdate = dayRegimeRepository.findAll().size();

        // Update the dayRegime
        DayRegime updatedDayRegime = dayRegimeRepository.findOne(dayRegime.getId());
        // Disconnect from session so that the updates on updatedDayRegime are not directly saved in db
        em.detach(updatedDayRegime);
        updatedDayRegime
            .daySeq(UPDATED_DAY_SEQ)
            .openedAt(UPDATED_OPENED_AT)
            .closedAt(UPDATED_CLOSED_AT)
            .flagAllDayLong(UPDATED_FLAG_ALL_DAY_LONG)
            .flagTillLastClient(UPDATED_FLAG_TILL_LAST_CLIENT);

        restDayRegimeMockMvc.perform(put("/api/day-regimes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDayRegime)))
            .andExpect(status().isOk());

        // Validate the DayRegime in the database
        List<DayRegime> dayRegimeList = dayRegimeRepository.findAll();
        assertThat(dayRegimeList).hasSize(databaseSizeBeforeUpdate);
        DayRegime testDayRegime = dayRegimeList.get(dayRegimeList.size() - 1);
        assertThat(testDayRegime.getDaySeq()).isEqualTo(UPDATED_DAY_SEQ);
        assertThat(testDayRegime.getOpenedAt()).isEqualTo(UPDATED_OPENED_AT);
        assertThat(testDayRegime.getClosedAt()).isEqualTo(UPDATED_CLOSED_AT);
        assertThat(testDayRegime.isFlagAllDayLong()).isEqualTo(UPDATED_FLAG_ALL_DAY_LONG);
        assertThat(testDayRegime.isFlagTillLastClient()).isEqualTo(UPDATED_FLAG_TILL_LAST_CLIENT);

        // Validate the DayRegime in Elasticsearch
        DayRegime dayRegimeEs = dayRegimeSearchRepository.findOne(testDayRegime.getId());
        assertThat(dayRegimeEs).isEqualToIgnoringGivenFields(testDayRegime);
    }

    @Test
    @Transactional
    public void updateNonExistingDayRegime() throws Exception {
        int databaseSizeBeforeUpdate = dayRegimeRepository.findAll().size();

        // Create the DayRegime

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDayRegimeMockMvc.perform(put("/api/day-regimes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dayRegime)))
            .andExpect(status().isCreated());

        // Validate the DayRegime in the database
        List<DayRegime> dayRegimeList = dayRegimeRepository.findAll();
        assertThat(dayRegimeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDayRegime() throws Exception {
        // Initialize the database
        dayRegimeService.save(dayRegime);

        int databaseSizeBeforeDelete = dayRegimeRepository.findAll().size();

        // Get the dayRegime
        restDayRegimeMockMvc.perform(delete("/api/day-regimes/{id}", dayRegime.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean dayRegimeExistsInEs = dayRegimeSearchRepository.exists(dayRegime.getId());
        assertThat(dayRegimeExistsInEs).isFalse();

        // Validate the database is empty
        List<DayRegime> dayRegimeList = dayRegimeRepository.findAll();
        assertThat(dayRegimeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDayRegime() throws Exception {
        // Initialize the database
        dayRegimeService.save(dayRegime);

        // Search the dayRegime
        restDayRegimeMockMvc.perform(get("/api/_search/day-regimes?query=id:" + dayRegime.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dayRegime.getId().intValue())))
            .andExpect(jsonPath("$.[*].daySeq").value(hasItem(DEFAULT_DAY_SEQ)))
            .andExpect(jsonPath("$.[*].openedAt").value(hasItem(DEFAULT_OPENED_AT.toString())))
            .andExpect(jsonPath("$.[*].closedAt").value(hasItem(DEFAULT_CLOSED_AT.toString())))
            .andExpect(jsonPath("$.[*].flagAllDayLong").value(hasItem(DEFAULT_FLAG_ALL_DAY_LONG.booleanValue())))
            .andExpect(jsonPath("$.[*].flagTillLastClient").value(hasItem(DEFAULT_FLAG_TILL_LAST_CLIENT.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DayRegime.class);
        DayRegime dayRegime1 = new DayRegime();
        dayRegime1.setId(1L);
        DayRegime dayRegime2 = new DayRegime();
        dayRegime2.setId(dayRegime1.getId());
        assertThat(dayRegime1).isEqualTo(dayRegime2);
        dayRegime2.setId(2L);
        assertThat(dayRegime1).isNotEqualTo(dayRegime2);
        dayRegime1.setId(null);
        assertThat(dayRegime1).isNotEqualTo(dayRegime2);
    }
}
