package kz.ast.donimas.ps.web.rest;

import kz.ast.donimas.ps.PsClubManagerApp;

import kz.ast.donimas.ps.domain.Tariff;
import kz.ast.donimas.ps.repository.TariffRepository;
import kz.ast.donimas.ps.service.TariffService;
import kz.ast.donimas.ps.repository.search.TariffSearchRepository;
import kz.ast.donimas.ps.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static kz.ast.donimas.ps.web.rest.TestUtil.sameInstant;
import static kz.ast.donimas.ps.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import kz.ast.donimas.ps.domain.enumeration.CurrencyType;
/**
 * Test class for the TariffResource REST controller.
 *
 * @see TariffResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PsClubManagerApp.class)
public class TariffResourceIntTest {

    private static final BigDecimal DEFAULT_PRICE = new BigDecimal(1);
    private static final BigDecimal UPDATED_PRICE = new BigDecimal(2);

    private static final CurrencyType DEFAULT_CURRENCY = CurrencyType.KZT;
    private static final CurrencyType UPDATED_CURRENCY = CurrencyType.RU;

    private static final ZonedDateTime DEFAULT_CREATED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_FLAG_DELETED = false;
    private static final Boolean UPDATED_FLAG_DELETED = true;

    private static final String DEFAULT_CONTAINER_CLASS = "AAAAAAAAAA";
    private static final String UPDATED_CONTAINER_CLASS = "BBBBBBBBBB";

    private static final Long DEFAULT_CONTAINER_ID = 1L;
    private static final Long UPDATED_CONTAINER_ID = 2L;

    @Autowired
    private TariffRepository tariffRepository;

    @Autowired
    private TariffService tariffService;

    @Autowired
    private TariffSearchRepository tariffSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTariffMockMvc;

    private Tariff tariff;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TariffResource tariffResource = new TariffResource(tariffService);
        this.restTariffMockMvc = MockMvcBuilders.standaloneSetup(tariffResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tariff createEntity(EntityManager em) {
        Tariff tariff = new Tariff()
            .price(DEFAULT_PRICE)
            .currency(DEFAULT_CURRENCY)
            .createdDate(DEFAULT_CREATED_DATE)
            .flagDeleted(DEFAULT_FLAG_DELETED)
            .containerClass(DEFAULT_CONTAINER_CLASS)
            .containerId(DEFAULT_CONTAINER_ID);
        return tariff;
    }

    @Before
    public void initTest() {
        tariffSearchRepository.deleteAll();
        tariff = createEntity(em);
    }

    @Test
    @Transactional
    public void createTariff() throws Exception {
        int databaseSizeBeforeCreate = tariffRepository.findAll().size();

        // Create the Tariff
        restTariffMockMvc.perform(post("/api/tariffs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tariff)))
            .andExpect(status().isCreated());

        // Validate the Tariff in the database
        List<Tariff> tariffList = tariffRepository.findAll();
        assertThat(tariffList).hasSize(databaseSizeBeforeCreate + 1);
        Tariff testTariff = tariffList.get(tariffList.size() - 1);
        assertThat(testTariff.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testTariff.getCurrency()).isEqualTo(DEFAULT_CURRENCY);
        assertThat(testTariff.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testTariff.isFlagDeleted()).isEqualTo(DEFAULT_FLAG_DELETED);
        assertThat(testTariff.getContainerClass()).isEqualTo(DEFAULT_CONTAINER_CLASS);
        assertThat(testTariff.getContainerId()).isEqualTo(DEFAULT_CONTAINER_ID);

        // Validate the Tariff in Elasticsearch
        Tariff tariffEs = tariffSearchRepository.findOne(testTariff.getId());
        assertThat(testTariff.getCreatedDate()).isEqualTo(testTariff.getCreatedDate());
        assertThat(tariffEs).isEqualToIgnoringGivenFields(testTariff, "createdDate");
    }

    @Test
    @Transactional
    public void createTariffWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tariffRepository.findAll().size();

        // Create the Tariff with an existing ID
        tariff.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTariffMockMvc.perform(post("/api/tariffs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tariff)))
            .andExpect(status().isBadRequest());

        // Validate the Tariff in the database
        List<Tariff> tariffList = tariffRepository.findAll();
        assertThat(tariffList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = tariffRepository.findAll().size();
        // set the field null
        tariff.setPrice(null);

        // Create the Tariff, which fails.

        restTariffMockMvc.perform(post("/api/tariffs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tariff)))
            .andExpect(status().isBadRequest());

        List<Tariff> tariffList = tariffRepository.findAll();
        assertThat(tariffList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTariffs() throws Exception {
        // Initialize the database
        tariffRepository.saveAndFlush(tariff);

        // Get all the tariffList
        restTariffMockMvc.perform(get("/api/tariffs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tariff.getId().intValue())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].currency").value(hasItem(DEFAULT_CURRENCY.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(sameInstant(DEFAULT_CREATED_DATE))))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].containerClass").value(hasItem(DEFAULT_CONTAINER_CLASS.toString())))
            .andExpect(jsonPath("$.[*].containerId").value(hasItem(DEFAULT_CONTAINER_ID.intValue())));
    }

    @Test
    @Transactional
    public void getTariff() throws Exception {
        // Initialize the database
        tariffRepository.saveAndFlush(tariff);

        // Get the tariff
        restTariffMockMvc.perform(get("/api/tariffs/{id}", tariff.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tariff.getId().intValue()))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.intValue()))
            .andExpect(jsonPath("$.currency").value(DEFAULT_CURRENCY.toString()))
            .andExpect(jsonPath("$.createdDate").value(sameInstant(DEFAULT_CREATED_DATE)))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED.booleanValue()))
            .andExpect(jsonPath("$.containerClass").value(DEFAULT_CONTAINER_CLASS.toString()))
            .andExpect(jsonPath("$.containerId").value(DEFAULT_CONTAINER_ID.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTariff() throws Exception {
        // Get the tariff
        restTariffMockMvc.perform(get("/api/tariffs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTariff() throws Exception {
        // Initialize the database
        tariffService.save(tariff);

        int databaseSizeBeforeUpdate = tariffRepository.findAll().size();

        // Update the tariff
        Tariff updatedTariff = tariffRepository.findOne(tariff.getId());
        // Disconnect from session so that the updates on updatedTariff are not directly saved in db
        em.detach(updatedTariff);
        updatedTariff
            .price(UPDATED_PRICE)
            .currency(UPDATED_CURRENCY)
            .createdDate(UPDATED_CREATED_DATE)
            .flagDeleted(UPDATED_FLAG_DELETED)
            .containerClass(UPDATED_CONTAINER_CLASS)
            .containerId(UPDATED_CONTAINER_ID);

        restTariffMockMvc.perform(put("/api/tariffs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTariff)))
            .andExpect(status().isOk());

        // Validate the Tariff in the database
        List<Tariff> tariffList = tariffRepository.findAll();
        assertThat(tariffList).hasSize(databaseSizeBeforeUpdate);
        Tariff testTariff = tariffList.get(tariffList.size() - 1);
        assertThat(testTariff.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testTariff.getCurrency()).isEqualTo(UPDATED_CURRENCY);
        assertThat(testTariff.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testTariff.isFlagDeleted()).isEqualTo(UPDATED_FLAG_DELETED);
        assertThat(testTariff.getContainerClass()).isEqualTo(UPDATED_CONTAINER_CLASS);
        assertThat(testTariff.getContainerId()).isEqualTo(UPDATED_CONTAINER_ID);

        // Validate the Tariff in Elasticsearch
        Tariff tariffEs = tariffSearchRepository.findOne(testTariff.getId());
        assertThat(testTariff.getCreatedDate()).isEqualTo(testTariff.getCreatedDate());
        assertThat(tariffEs).isEqualToIgnoringGivenFields(testTariff, "createdDate");
    }

    @Test
    @Transactional
    public void updateNonExistingTariff() throws Exception {
        int databaseSizeBeforeUpdate = tariffRepository.findAll().size();

        // Create the Tariff

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTariffMockMvc.perform(put("/api/tariffs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tariff)))
            .andExpect(status().isCreated());

        // Validate the Tariff in the database
        List<Tariff> tariffList = tariffRepository.findAll();
        assertThat(tariffList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTariff() throws Exception {
        // Initialize the database
        tariffService.save(tariff);

        int databaseSizeBeforeDelete = tariffRepository.findAll().size();

        // Get the tariff
        restTariffMockMvc.perform(delete("/api/tariffs/{id}", tariff.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean tariffExistsInEs = tariffSearchRepository.exists(tariff.getId());
        assertThat(tariffExistsInEs).isFalse();

        // Validate the database is empty
        List<Tariff> tariffList = tariffRepository.findAll();
        assertThat(tariffList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTariff() throws Exception {
        // Initialize the database
        tariffService.save(tariff);

        // Search the tariff
        restTariffMockMvc.perform(get("/api/_search/tariffs?query=id:" + tariff.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tariff.getId().intValue())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].currency").value(hasItem(DEFAULT_CURRENCY.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(sameInstant(DEFAULT_CREATED_DATE))))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].containerClass").value(hasItem(DEFAULT_CONTAINER_CLASS.toString())))
            .andExpect(jsonPath("$.[*].containerId").value(hasItem(DEFAULT_CONTAINER_ID.intValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tariff.class);
        Tariff tariff1 = new Tariff();
        tariff1.setId(1L);
        Tariff tariff2 = new Tariff();
        tariff2.setId(tariff1.getId());
        assertThat(tariff1).isEqualTo(tariff2);
        tariff2.setId(2L);
        assertThat(tariff1).isNotEqualTo(tariff2);
        tariff1.setId(null);
        assertThat(tariff1).isNotEqualTo(tariff2);
    }
}
