package kz.ast.donimas.ps.web.rest;

import kz.ast.donimas.ps.PsClubManagerApp;

import kz.ast.donimas.ps.domain.ConsoleDictionary;
import kz.ast.donimas.ps.repository.ConsoleDictionaryRepository;
import kz.ast.donimas.ps.service.ConsoleDictionaryService;
import kz.ast.donimas.ps.repository.search.ConsoleDictionarySearchRepository;
import kz.ast.donimas.ps.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static kz.ast.donimas.ps.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ConsoleDictionaryResource REST controller.
 *
 * @see ConsoleDictionaryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PsClubManagerApp.class)
public class ConsoleDictionaryResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    @Autowired
    private ConsoleDictionaryRepository consoleDictionaryRepository;

    @Autowired
    private ConsoleDictionaryService consoleDictionaryService;

    @Autowired
    private ConsoleDictionarySearchRepository consoleDictionarySearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restConsoleDictionaryMockMvc;

    private ConsoleDictionary consoleDictionary;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ConsoleDictionaryResource consoleDictionaryResource = new ConsoleDictionaryResource(consoleDictionaryService);
        this.restConsoleDictionaryMockMvc = MockMvcBuilders.standaloneSetup(consoleDictionaryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ConsoleDictionary createEntity(EntityManager em) {
        ConsoleDictionary consoleDictionary = new ConsoleDictionary()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .code(DEFAULT_CODE);
        return consoleDictionary;
    }

    @Before
    public void initTest() {
        consoleDictionarySearchRepository.deleteAll();
        consoleDictionary = createEntity(em);
    }

    @Test
    @Transactional
    public void createConsoleDictionary() throws Exception {
        int databaseSizeBeforeCreate = consoleDictionaryRepository.findAll().size();

        // Create the ConsoleDictionary
        restConsoleDictionaryMockMvc.perform(post("/api/console-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(consoleDictionary)))
            .andExpect(status().isCreated());

        // Validate the ConsoleDictionary in the database
        List<ConsoleDictionary> consoleDictionaryList = consoleDictionaryRepository.findAll();
        assertThat(consoleDictionaryList).hasSize(databaseSizeBeforeCreate + 1);
        ConsoleDictionary testConsoleDictionary = consoleDictionaryList.get(consoleDictionaryList.size() - 1);
        assertThat(testConsoleDictionary.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testConsoleDictionary.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testConsoleDictionary.getCode()).isEqualTo(DEFAULT_CODE);

        // Validate the ConsoleDictionary in Elasticsearch
        ConsoleDictionary consoleDictionaryEs = consoleDictionarySearchRepository.findOne(testConsoleDictionary.getId());
        assertThat(consoleDictionaryEs).isEqualToIgnoringGivenFields(testConsoleDictionary);
    }

    @Test
    @Transactional
    public void createConsoleDictionaryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = consoleDictionaryRepository.findAll().size();

        // Create the ConsoleDictionary with an existing ID
        consoleDictionary.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restConsoleDictionaryMockMvc.perform(post("/api/console-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(consoleDictionary)))
            .andExpect(status().isBadRequest());

        // Validate the ConsoleDictionary in the database
        List<ConsoleDictionary> consoleDictionaryList = consoleDictionaryRepository.findAll();
        assertThat(consoleDictionaryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = consoleDictionaryRepository.findAll().size();
        // set the field null
        consoleDictionary.setName(null);

        // Create the ConsoleDictionary, which fails.

        restConsoleDictionaryMockMvc.perform(post("/api/console-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(consoleDictionary)))
            .andExpect(status().isBadRequest());

        List<ConsoleDictionary> consoleDictionaryList = consoleDictionaryRepository.findAll();
        assertThat(consoleDictionaryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = consoleDictionaryRepository.findAll().size();
        // set the field null
        consoleDictionary.setCode(null);

        // Create the ConsoleDictionary, which fails.

        restConsoleDictionaryMockMvc.perform(post("/api/console-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(consoleDictionary)))
            .andExpect(status().isBadRequest());

        List<ConsoleDictionary> consoleDictionaryList = consoleDictionaryRepository.findAll();
        assertThat(consoleDictionaryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllConsoleDictionaries() throws Exception {
        // Initialize the database
        consoleDictionaryRepository.saveAndFlush(consoleDictionary);

        // Get all the consoleDictionaryList
        restConsoleDictionaryMockMvc.perform(get("/api/console-dictionaries?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(consoleDictionary.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())));
    }

    @Test
    @Transactional
    public void getConsoleDictionary() throws Exception {
        // Initialize the database
        consoleDictionaryRepository.saveAndFlush(consoleDictionary);

        // Get the consoleDictionary
        restConsoleDictionaryMockMvc.perform(get("/api/console-dictionaries/{id}", consoleDictionary.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(consoleDictionary.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingConsoleDictionary() throws Exception {
        // Get the consoleDictionary
        restConsoleDictionaryMockMvc.perform(get("/api/console-dictionaries/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateConsoleDictionary() throws Exception {
        // Initialize the database
        consoleDictionaryService.save(consoleDictionary);

        int databaseSizeBeforeUpdate = consoleDictionaryRepository.findAll().size();

        // Update the consoleDictionary
        ConsoleDictionary updatedConsoleDictionary = consoleDictionaryRepository.findOne(consoleDictionary.getId());
        // Disconnect from session so that the updates on updatedConsoleDictionary are not directly saved in db
        em.detach(updatedConsoleDictionary);
        updatedConsoleDictionary
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .code(UPDATED_CODE);

        restConsoleDictionaryMockMvc.perform(put("/api/console-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedConsoleDictionary)))
            .andExpect(status().isOk());

        // Validate the ConsoleDictionary in the database
        List<ConsoleDictionary> consoleDictionaryList = consoleDictionaryRepository.findAll();
        assertThat(consoleDictionaryList).hasSize(databaseSizeBeforeUpdate);
        ConsoleDictionary testConsoleDictionary = consoleDictionaryList.get(consoleDictionaryList.size() - 1);
        assertThat(testConsoleDictionary.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testConsoleDictionary.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testConsoleDictionary.getCode()).isEqualTo(UPDATED_CODE);

        // Validate the ConsoleDictionary in Elasticsearch
        ConsoleDictionary consoleDictionaryEs = consoleDictionarySearchRepository.findOne(testConsoleDictionary.getId());
        assertThat(consoleDictionaryEs).isEqualToIgnoringGivenFields(testConsoleDictionary);
    }

    @Test
    @Transactional
    public void updateNonExistingConsoleDictionary() throws Exception {
        int databaseSizeBeforeUpdate = consoleDictionaryRepository.findAll().size();

        // Create the ConsoleDictionary

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restConsoleDictionaryMockMvc.perform(put("/api/console-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(consoleDictionary)))
            .andExpect(status().isCreated());

        // Validate the ConsoleDictionary in the database
        List<ConsoleDictionary> consoleDictionaryList = consoleDictionaryRepository.findAll();
        assertThat(consoleDictionaryList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteConsoleDictionary() throws Exception {
        // Initialize the database
        consoleDictionaryService.save(consoleDictionary);

        int databaseSizeBeforeDelete = consoleDictionaryRepository.findAll().size();

        // Get the consoleDictionary
        restConsoleDictionaryMockMvc.perform(delete("/api/console-dictionaries/{id}", consoleDictionary.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean consoleDictionaryExistsInEs = consoleDictionarySearchRepository.exists(consoleDictionary.getId());
        assertThat(consoleDictionaryExistsInEs).isFalse();

        // Validate the database is empty
        List<ConsoleDictionary> consoleDictionaryList = consoleDictionaryRepository.findAll();
        assertThat(consoleDictionaryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchConsoleDictionary() throws Exception {
        // Initialize the database
        consoleDictionaryService.save(consoleDictionary);

        // Search the consoleDictionary
        restConsoleDictionaryMockMvc.perform(get("/api/_search/console-dictionaries?query=id:" + consoleDictionary.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(consoleDictionary.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ConsoleDictionary.class);
        ConsoleDictionary consoleDictionary1 = new ConsoleDictionary();
        consoleDictionary1.setId(1L);
        ConsoleDictionary consoleDictionary2 = new ConsoleDictionary();
        consoleDictionary2.setId(consoleDictionary1.getId());
        assertThat(consoleDictionary1).isEqualTo(consoleDictionary2);
        consoleDictionary2.setId(2L);
        assertThat(consoleDictionary1).isNotEqualTo(consoleDictionary2);
        consoleDictionary1.setId(null);
        assertThat(consoleDictionary1).isNotEqualTo(consoleDictionary2);
    }
}
