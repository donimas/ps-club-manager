package kz.ast.donimas.ps.web.rest;

import kz.ast.donimas.ps.PsClubManagerApp;

import kz.ast.donimas.ps.domain.JoystickDictionary;
import kz.ast.donimas.ps.repository.JoystickDictionaryRepository;
import kz.ast.donimas.ps.service.JoystickDictionaryService;
import kz.ast.donimas.ps.repository.search.JoystickDictionarySearchRepository;
import kz.ast.donimas.ps.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static kz.ast.donimas.ps.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the JoystickDictionaryResource REST controller.
 *
 * @see JoystickDictionaryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PsClubManagerApp.class)
public class JoystickDictionaryResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    @Autowired
    private JoystickDictionaryRepository joystickDictionaryRepository;

    @Autowired
    private JoystickDictionaryService joystickDictionaryService;

    @Autowired
    private JoystickDictionarySearchRepository joystickDictionarySearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restJoystickDictionaryMockMvc;

    private JoystickDictionary joystickDictionary;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final JoystickDictionaryResource joystickDictionaryResource = new JoystickDictionaryResource(joystickDictionaryService);
        this.restJoystickDictionaryMockMvc = MockMvcBuilders.standaloneSetup(joystickDictionaryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static JoystickDictionary createEntity(EntityManager em) {
        JoystickDictionary joystickDictionary = new JoystickDictionary()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .code(DEFAULT_CODE);
        return joystickDictionary;
    }

    @Before
    public void initTest() {
        joystickDictionarySearchRepository.deleteAll();
        joystickDictionary = createEntity(em);
    }

    @Test
    @Transactional
    public void createJoystickDictionary() throws Exception {
        int databaseSizeBeforeCreate = joystickDictionaryRepository.findAll().size();

        // Create the JoystickDictionary
        restJoystickDictionaryMockMvc.perform(post("/api/joystick-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(joystickDictionary)))
            .andExpect(status().isCreated());

        // Validate the JoystickDictionary in the database
        List<JoystickDictionary> joystickDictionaryList = joystickDictionaryRepository.findAll();
        assertThat(joystickDictionaryList).hasSize(databaseSizeBeforeCreate + 1);
        JoystickDictionary testJoystickDictionary = joystickDictionaryList.get(joystickDictionaryList.size() - 1);
        assertThat(testJoystickDictionary.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testJoystickDictionary.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testJoystickDictionary.getCode()).isEqualTo(DEFAULT_CODE);

        // Validate the JoystickDictionary in Elasticsearch
        JoystickDictionary joystickDictionaryEs = joystickDictionarySearchRepository.findOne(testJoystickDictionary.getId());
        assertThat(joystickDictionaryEs).isEqualToIgnoringGivenFields(testJoystickDictionary);
    }

    @Test
    @Transactional
    public void createJoystickDictionaryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = joystickDictionaryRepository.findAll().size();

        // Create the JoystickDictionary with an existing ID
        joystickDictionary.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJoystickDictionaryMockMvc.perform(post("/api/joystick-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(joystickDictionary)))
            .andExpect(status().isBadRequest());

        // Validate the JoystickDictionary in the database
        List<JoystickDictionary> joystickDictionaryList = joystickDictionaryRepository.findAll();
        assertThat(joystickDictionaryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = joystickDictionaryRepository.findAll().size();
        // set the field null
        joystickDictionary.setName(null);

        // Create the JoystickDictionary, which fails.

        restJoystickDictionaryMockMvc.perform(post("/api/joystick-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(joystickDictionary)))
            .andExpect(status().isBadRequest());

        List<JoystickDictionary> joystickDictionaryList = joystickDictionaryRepository.findAll();
        assertThat(joystickDictionaryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = joystickDictionaryRepository.findAll().size();
        // set the field null
        joystickDictionary.setCode(null);

        // Create the JoystickDictionary, which fails.

        restJoystickDictionaryMockMvc.perform(post("/api/joystick-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(joystickDictionary)))
            .andExpect(status().isBadRequest());

        List<JoystickDictionary> joystickDictionaryList = joystickDictionaryRepository.findAll();
        assertThat(joystickDictionaryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllJoystickDictionaries() throws Exception {
        // Initialize the database
        joystickDictionaryRepository.saveAndFlush(joystickDictionary);

        // Get all the joystickDictionaryList
        restJoystickDictionaryMockMvc.perform(get("/api/joystick-dictionaries?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(joystickDictionary.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())));
    }

    @Test
    @Transactional
    public void getJoystickDictionary() throws Exception {
        // Initialize the database
        joystickDictionaryRepository.saveAndFlush(joystickDictionary);

        // Get the joystickDictionary
        restJoystickDictionaryMockMvc.perform(get("/api/joystick-dictionaries/{id}", joystickDictionary.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(joystickDictionary.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingJoystickDictionary() throws Exception {
        // Get the joystickDictionary
        restJoystickDictionaryMockMvc.perform(get("/api/joystick-dictionaries/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJoystickDictionary() throws Exception {
        // Initialize the database
        joystickDictionaryService.save(joystickDictionary);

        int databaseSizeBeforeUpdate = joystickDictionaryRepository.findAll().size();

        // Update the joystickDictionary
        JoystickDictionary updatedJoystickDictionary = joystickDictionaryRepository.findOne(joystickDictionary.getId());
        // Disconnect from session so that the updates on updatedJoystickDictionary are not directly saved in db
        em.detach(updatedJoystickDictionary);
        updatedJoystickDictionary
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .code(UPDATED_CODE);

        restJoystickDictionaryMockMvc.perform(put("/api/joystick-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedJoystickDictionary)))
            .andExpect(status().isOk());

        // Validate the JoystickDictionary in the database
        List<JoystickDictionary> joystickDictionaryList = joystickDictionaryRepository.findAll();
        assertThat(joystickDictionaryList).hasSize(databaseSizeBeforeUpdate);
        JoystickDictionary testJoystickDictionary = joystickDictionaryList.get(joystickDictionaryList.size() - 1);
        assertThat(testJoystickDictionary.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testJoystickDictionary.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testJoystickDictionary.getCode()).isEqualTo(UPDATED_CODE);

        // Validate the JoystickDictionary in Elasticsearch
        JoystickDictionary joystickDictionaryEs = joystickDictionarySearchRepository.findOne(testJoystickDictionary.getId());
        assertThat(joystickDictionaryEs).isEqualToIgnoringGivenFields(testJoystickDictionary);
    }

    @Test
    @Transactional
    public void updateNonExistingJoystickDictionary() throws Exception {
        int databaseSizeBeforeUpdate = joystickDictionaryRepository.findAll().size();

        // Create the JoystickDictionary

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restJoystickDictionaryMockMvc.perform(put("/api/joystick-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(joystickDictionary)))
            .andExpect(status().isCreated());

        // Validate the JoystickDictionary in the database
        List<JoystickDictionary> joystickDictionaryList = joystickDictionaryRepository.findAll();
        assertThat(joystickDictionaryList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteJoystickDictionary() throws Exception {
        // Initialize the database
        joystickDictionaryService.save(joystickDictionary);

        int databaseSizeBeforeDelete = joystickDictionaryRepository.findAll().size();

        // Get the joystickDictionary
        restJoystickDictionaryMockMvc.perform(delete("/api/joystick-dictionaries/{id}", joystickDictionary.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean joystickDictionaryExistsInEs = joystickDictionarySearchRepository.exists(joystickDictionary.getId());
        assertThat(joystickDictionaryExistsInEs).isFalse();

        // Validate the database is empty
        List<JoystickDictionary> joystickDictionaryList = joystickDictionaryRepository.findAll();
        assertThat(joystickDictionaryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchJoystickDictionary() throws Exception {
        // Initialize the database
        joystickDictionaryService.save(joystickDictionary);

        // Search the joystickDictionary
        restJoystickDictionaryMockMvc.perform(get("/api/_search/joystick-dictionaries?query=id:" + joystickDictionary.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(joystickDictionary.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(JoystickDictionary.class);
        JoystickDictionary joystickDictionary1 = new JoystickDictionary();
        joystickDictionary1.setId(1L);
        JoystickDictionary joystickDictionary2 = new JoystickDictionary();
        joystickDictionary2.setId(joystickDictionary1.getId());
        assertThat(joystickDictionary1).isEqualTo(joystickDictionary2);
        joystickDictionary2.setId(2L);
        assertThat(joystickDictionary1).isNotEqualTo(joystickDictionary2);
        joystickDictionary1.setId(null);
        assertThat(joystickDictionary1).isNotEqualTo(joystickDictionary2);
    }
}
