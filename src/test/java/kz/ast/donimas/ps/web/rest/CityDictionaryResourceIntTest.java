package kz.ast.donimas.ps.web.rest;

import kz.ast.donimas.ps.PsClubManagerApp;

import kz.ast.donimas.ps.domain.CityDictionary;
import kz.ast.donimas.ps.repository.CityDictionaryRepository;
import kz.ast.donimas.ps.service.CityDictionaryService;
import kz.ast.donimas.ps.repository.search.CityDictionarySearchRepository;
import kz.ast.donimas.ps.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static kz.ast.donimas.ps.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CityDictionaryResource REST controller.
 *
 * @see CityDictionaryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PsClubManagerApp.class)
public class CityDictionaryResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_POSTAL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_POSTAL_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_LAT = "AAAAAAAAAA";
    private static final String UPDATED_LAT = "BBBBBBBBBB";

    private static final String DEFAULT_LON = "AAAAAAAAAA";
    private static final String UPDATED_LON = "BBBBBBBBBB";

    @Autowired
    private CityDictionaryRepository cityDictionaryRepository;

    @Autowired
    private CityDictionaryService cityDictionaryService;

    @Autowired
    private CityDictionarySearchRepository cityDictionarySearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCityDictionaryMockMvc;

    private CityDictionary cityDictionary;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CityDictionaryResource cityDictionaryResource = new CityDictionaryResource(cityDictionaryService);
        this.restCityDictionaryMockMvc = MockMvcBuilders.standaloneSetup(cityDictionaryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CityDictionary createEntity(EntityManager em) {
        CityDictionary cityDictionary = new CityDictionary()
            .name(DEFAULT_NAME)
            .code(DEFAULT_CODE)
            .postalCode(DEFAULT_POSTAL_CODE)
            .lat(DEFAULT_LAT)
            .lon(DEFAULT_LON);
        return cityDictionary;
    }

    @Before
    public void initTest() {
        cityDictionarySearchRepository.deleteAll();
        cityDictionary = createEntity(em);
    }

    @Test
    @Transactional
    public void createCityDictionary() throws Exception {
        int databaseSizeBeforeCreate = cityDictionaryRepository.findAll().size();

        // Create the CityDictionary
        restCityDictionaryMockMvc.perform(post("/api/city-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cityDictionary)))
            .andExpect(status().isCreated());

        // Validate the CityDictionary in the database
        List<CityDictionary> cityDictionaryList = cityDictionaryRepository.findAll();
        assertThat(cityDictionaryList).hasSize(databaseSizeBeforeCreate + 1);
        CityDictionary testCityDictionary = cityDictionaryList.get(cityDictionaryList.size() - 1);
        assertThat(testCityDictionary.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCityDictionary.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testCityDictionary.getPostalCode()).isEqualTo(DEFAULT_POSTAL_CODE);
        assertThat(testCityDictionary.getLat()).isEqualTo(DEFAULT_LAT);
        assertThat(testCityDictionary.getLon()).isEqualTo(DEFAULT_LON);

        // Validate the CityDictionary in Elasticsearch
        CityDictionary cityDictionaryEs = cityDictionarySearchRepository.findOne(testCityDictionary.getId());
        assertThat(cityDictionaryEs).isEqualToIgnoringGivenFields(testCityDictionary);
    }

    @Test
    @Transactional
    public void createCityDictionaryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cityDictionaryRepository.findAll().size();

        // Create the CityDictionary with an existing ID
        cityDictionary.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCityDictionaryMockMvc.perform(post("/api/city-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cityDictionary)))
            .andExpect(status().isBadRequest());

        // Validate the CityDictionary in the database
        List<CityDictionary> cityDictionaryList = cityDictionaryRepository.findAll();
        assertThat(cityDictionaryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCityDictionaries() throws Exception {
        // Initialize the database
        cityDictionaryRepository.saveAndFlush(cityDictionary);

        // Get all the cityDictionaryList
        restCityDictionaryMockMvc.perform(get("/api/city-dictionaries?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cityDictionary.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE.toString())))
            .andExpect(jsonPath("$.[*].lat").value(hasItem(DEFAULT_LAT.toString())))
            .andExpect(jsonPath("$.[*].lon").value(hasItem(DEFAULT_LON.toString())));
    }

    @Test
    @Transactional
    public void getCityDictionary() throws Exception {
        // Initialize the database
        cityDictionaryRepository.saveAndFlush(cityDictionary);

        // Get the cityDictionary
        restCityDictionaryMockMvc.perform(get("/api/city-dictionaries/{id}", cityDictionary.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(cityDictionary.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.postalCode").value(DEFAULT_POSTAL_CODE.toString()))
            .andExpect(jsonPath("$.lat").value(DEFAULT_LAT.toString()))
            .andExpect(jsonPath("$.lon").value(DEFAULT_LON.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCityDictionary() throws Exception {
        // Get the cityDictionary
        restCityDictionaryMockMvc.perform(get("/api/city-dictionaries/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCityDictionary() throws Exception {
        // Initialize the database
        cityDictionaryService.save(cityDictionary);

        int databaseSizeBeforeUpdate = cityDictionaryRepository.findAll().size();

        // Update the cityDictionary
        CityDictionary updatedCityDictionary = cityDictionaryRepository.findOne(cityDictionary.getId());
        // Disconnect from session so that the updates on updatedCityDictionary are not directly saved in db
        em.detach(updatedCityDictionary);
        updatedCityDictionary
            .name(UPDATED_NAME)
            .code(UPDATED_CODE)
            .postalCode(UPDATED_POSTAL_CODE)
            .lat(UPDATED_LAT)
            .lon(UPDATED_LON);

        restCityDictionaryMockMvc.perform(put("/api/city-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCityDictionary)))
            .andExpect(status().isOk());

        // Validate the CityDictionary in the database
        List<CityDictionary> cityDictionaryList = cityDictionaryRepository.findAll();
        assertThat(cityDictionaryList).hasSize(databaseSizeBeforeUpdate);
        CityDictionary testCityDictionary = cityDictionaryList.get(cityDictionaryList.size() - 1);
        assertThat(testCityDictionary.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCityDictionary.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCityDictionary.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
        assertThat(testCityDictionary.getLat()).isEqualTo(UPDATED_LAT);
        assertThat(testCityDictionary.getLon()).isEqualTo(UPDATED_LON);

        // Validate the CityDictionary in Elasticsearch
        CityDictionary cityDictionaryEs = cityDictionarySearchRepository.findOne(testCityDictionary.getId());
        assertThat(cityDictionaryEs).isEqualToIgnoringGivenFields(testCityDictionary);
    }

    @Test
    @Transactional
    public void updateNonExistingCityDictionary() throws Exception {
        int databaseSizeBeforeUpdate = cityDictionaryRepository.findAll().size();

        // Create the CityDictionary

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCityDictionaryMockMvc.perform(put("/api/city-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cityDictionary)))
            .andExpect(status().isCreated());

        // Validate the CityDictionary in the database
        List<CityDictionary> cityDictionaryList = cityDictionaryRepository.findAll();
        assertThat(cityDictionaryList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCityDictionary() throws Exception {
        // Initialize the database
        cityDictionaryService.save(cityDictionary);

        int databaseSizeBeforeDelete = cityDictionaryRepository.findAll().size();

        // Get the cityDictionary
        restCityDictionaryMockMvc.perform(delete("/api/city-dictionaries/{id}", cityDictionary.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean cityDictionaryExistsInEs = cityDictionarySearchRepository.exists(cityDictionary.getId());
        assertThat(cityDictionaryExistsInEs).isFalse();

        // Validate the database is empty
        List<CityDictionary> cityDictionaryList = cityDictionaryRepository.findAll();
        assertThat(cityDictionaryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCityDictionary() throws Exception {
        // Initialize the database
        cityDictionaryService.save(cityDictionary);

        // Search the cityDictionary
        restCityDictionaryMockMvc.perform(get("/api/_search/city-dictionaries?query=id:" + cityDictionary.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cityDictionary.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE.toString())))
            .andExpect(jsonPath("$.[*].lat").value(hasItem(DEFAULT_LAT.toString())))
            .andExpect(jsonPath("$.[*].lon").value(hasItem(DEFAULT_LON.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CityDictionary.class);
        CityDictionary cityDictionary1 = new CityDictionary();
        cityDictionary1.setId(1L);
        CityDictionary cityDictionary2 = new CityDictionary();
        cityDictionary2.setId(cityDictionary1.getId());
        assertThat(cityDictionary1).isEqualTo(cityDictionary2);
        cityDictionary2.setId(2L);
        assertThat(cityDictionary1).isNotEqualTo(cityDictionary2);
        cityDictionary1.setId(null);
        assertThat(cityDictionary1).isNotEqualTo(cityDictionary2);
    }
}
