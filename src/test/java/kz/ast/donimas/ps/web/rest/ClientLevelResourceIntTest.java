package kz.ast.donimas.ps.web.rest;

import kz.ast.donimas.ps.PsClubManagerApp;

import kz.ast.donimas.ps.domain.ClientLevel;
import kz.ast.donimas.ps.repository.ClientLevelRepository;
import kz.ast.donimas.ps.service.ClientLevelService;
import kz.ast.donimas.ps.repository.search.ClientLevelSearchRepository;
import kz.ast.donimas.ps.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static kz.ast.donimas.ps.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ClientLevelResource REST controller.
 *
 * @see ClientLevelResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PsClubManagerApp.class)
public class ClientLevelResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_GAMES_AMOUNT = 1;
    private static final Integer UPDATED_GAMES_AMOUNT = 2;

    private static final BigDecimal DEFAULT_DISCOUNT_PERCENT = new BigDecimal(1);
    private static final BigDecimal UPDATED_DISCOUNT_PERCENT = new BigDecimal(2);

    private static final Boolean DEFAULT_FLAG_DELETED = false;
    private static final Boolean UPDATED_FLAG_DELETED = true;

    @Autowired
    private ClientLevelRepository clientLevelRepository;

    @Autowired
    private ClientLevelService clientLevelService;

    @Autowired
    private ClientLevelSearchRepository clientLevelSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restClientLevelMockMvc;

    private ClientLevel clientLevel;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClientLevelResource clientLevelResource = new ClientLevelResource(clientLevelService);
        this.restClientLevelMockMvc = MockMvcBuilders.standaloneSetup(clientLevelResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientLevel createEntity(EntityManager em) {
        ClientLevel clientLevel = new ClientLevel()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .gamesAmount(DEFAULT_GAMES_AMOUNT)
            .discountPercent(DEFAULT_DISCOUNT_PERCENT)
            .flagDeleted(DEFAULT_FLAG_DELETED);
        return clientLevel;
    }

    @Before
    public void initTest() {
        clientLevelSearchRepository.deleteAll();
        clientLevel = createEntity(em);
    }

    @Test
    @Transactional
    public void createClientLevel() throws Exception {
        int databaseSizeBeforeCreate = clientLevelRepository.findAll().size();

        // Create the ClientLevel
        restClientLevelMockMvc.perform(post("/api/client-levels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientLevel)))
            .andExpect(status().isCreated());

        // Validate the ClientLevel in the database
        List<ClientLevel> clientLevelList = clientLevelRepository.findAll();
        assertThat(clientLevelList).hasSize(databaseSizeBeforeCreate + 1);
        ClientLevel testClientLevel = clientLevelList.get(clientLevelList.size() - 1);
        assertThat(testClientLevel.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testClientLevel.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testClientLevel.getGamesAmount()).isEqualTo(DEFAULT_GAMES_AMOUNT);
        assertThat(testClientLevel.getDiscountPercent()).isEqualTo(DEFAULT_DISCOUNT_PERCENT);
        assertThat(testClientLevel.isFlagDeleted()).isEqualTo(DEFAULT_FLAG_DELETED);

        // Validate the ClientLevel in Elasticsearch
        ClientLevel clientLevelEs = clientLevelSearchRepository.findOne(testClientLevel.getId());
        assertThat(clientLevelEs).isEqualToIgnoringGivenFields(testClientLevel);
    }

    @Test
    @Transactional
    public void createClientLevelWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clientLevelRepository.findAll().size();

        // Create the ClientLevel with an existing ID
        clientLevel.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClientLevelMockMvc.perform(post("/api/client-levels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientLevel)))
            .andExpect(status().isBadRequest());

        // Validate the ClientLevel in the database
        List<ClientLevel> clientLevelList = clientLevelRepository.findAll();
        assertThat(clientLevelList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientLevelRepository.findAll().size();
        // set the field null
        clientLevel.setName(null);

        // Create the ClientLevel, which fails.

        restClientLevelMockMvc.perform(post("/api/client-levels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientLevel)))
            .andExpect(status().isBadRequest());

        List<ClientLevel> clientLevelList = clientLevelRepository.findAll();
        assertThat(clientLevelList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllClientLevels() throws Exception {
        // Initialize the database
        clientLevelRepository.saveAndFlush(clientLevel);

        // Get all the clientLevelList
        restClientLevelMockMvc.perform(get("/api/client-levels?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientLevel.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].gamesAmount").value(hasItem(DEFAULT_GAMES_AMOUNT)))
            .andExpect(jsonPath("$.[*].discountPercent").value(hasItem(DEFAULT_DISCOUNT_PERCENT.intValue())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void getClientLevel() throws Exception {
        // Initialize the database
        clientLevelRepository.saveAndFlush(clientLevel);

        // Get the clientLevel
        restClientLevelMockMvc.perform(get("/api/client-levels/{id}", clientLevel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clientLevel.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.gamesAmount").value(DEFAULT_GAMES_AMOUNT))
            .andExpect(jsonPath("$.discountPercent").value(DEFAULT_DISCOUNT_PERCENT.intValue()))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingClientLevel() throws Exception {
        // Get the clientLevel
        restClientLevelMockMvc.perform(get("/api/client-levels/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClientLevel() throws Exception {
        // Initialize the database
        clientLevelService.save(clientLevel);

        int databaseSizeBeforeUpdate = clientLevelRepository.findAll().size();

        // Update the clientLevel
        ClientLevel updatedClientLevel = clientLevelRepository.findOne(clientLevel.getId());
        // Disconnect from session so that the updates on updatedClientLevel are not directly saved in db
        em.detach(updatedClientLevel);
        updatedClientLevel
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .gamesAmount(UPDATED_GAMES_AMOUNT)
            .discountPercent(UPDATED_DISCOUNT_PERCENT)
            .flagDeleted(UPDATED_FLAG_DELETED);

        restClientLevelMockMvc.perform(put("/api/client-levels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedClientLevel)))
            .andExpect(status().isOk());

        // Validate the ClientLevel in the database
        List<ClientLevel> clientLevelList = clientLevelRepository.findAll();
        assertThat(clientLevelList).hasSize(databaseSizeBeforeUpdate);
        ClientLevel testClientLevel = clientLevelList.get(clientLevelList.size() - 1);
        assertThat(testClientLevel.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testClientLevel.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testClientLevel.getGamesAmount()).isEqualTo(UPDATED_GAMES_AMOUNT);
        assertThat(testClientLevel.getDiscountPercent()).isEqualTo(UPDATED_DISCOUNT_PERCENT);
        assertThat(testClientLevel.isFlagDeleted()).isEqualTo(UPDATED_FLAG_DELETED);

        // Validate the ClientLevel in Elasticsearch
        ClientLevel clientLevelEs = clientLevelSearchRepository.findOne(testClientLevel.getId());
        assertThat(clientLevelEs).isEqualToIgnoringGivenFields(testClientLevel);
    }

    @Test
    @Transactional
    public void updateNonExistingClientLevel() throws Exception {
        int databaseSizeBeforeUpdate = clientLevelRepository.findAll().size();

        // Create the ClientLevel

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restClientLevelMockMvc.perform(put("/api/client-levels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientLevel)))
            .andExpect(status().isCreated());

        // Validate the ClientLevel in the database
        List<ClientLevel> clientLevelList = clientLevelRepository.findAll();
        assertThat(clientLevelList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteClientLevel() throws Exception {
        // Initialize the database
        clientLevelService.save(clientLevel);

        int databaseSizeBeforeDelete = clientLevelRepository.findAll().size();

        // Get the clientLevel
        restClientLevelMockMvc.perform(delete("/api/client-levels/{id}", clientLevel.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean clientLevelExistsInEs = clientLevelSearchRepository.exists(clientLevel.getId());
        assertThat(clientLevelExistsInEs).isFalse();

        // Validate the database is empty
        List<ClientLevel> clientLevelList = clientLevelRepository.findAll();
        assertThat(clientLevelList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchClientLevel() throws Exception {
        // Initialize the database
        clientLevelService.save(clientLevel);

        // Search the clientLevel
        restClientLevelMockMvc.perform(get("/api/_search/client-levels?query=id:" + clientLevel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientLevel.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].gamesAmount").value(hasItem(DEFAULT_GAMES_AMOUNT)))
            .andExpect(jsonPath("$.[*].discountPercent").value(hasItem(DEFAULT_DISCOUNT_PERCENT.intValue())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientLevel.class);
        ClientLevel clientLevel1 = new ClientLevel();
        clientLevel1.setId(1L);
        ClientLevel clientLevel2 = new ClientLevel();
        clientLevel2.setId(clientLevel1.getId());
        assertThat(clientLevel1).isEqualTo(clientLevel2);
        clientLevel2.setId(2L);
        assertThat(clientLevel1).isNotEqualTo(clientLevel2);
        clientLevel1.setId(null);
        assertThat(clientLevel1).isNotEqualTo(clientLevel2);
    }
}
