package kz.ast.donimas.ps.web.rest;

import kz.ast.donimas.ps.PsClubManagerApp;

import kz.ast.donimas.ps.domain.ClubJoystick;
import kz.ast.donimas.ps.repository.ClubJoystickRepository;
import kz.ast.donimas.ps.service.ClubJoystickService;
import kz.ast.donimas.ps.repository.search.ClubJoystickSearchRepository;
import kz.ast.donimas.ps.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static kz.ast.donimas.ps.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ClubJoystickResource REST controller.
 *
 * @see ClubJoystickResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PsClubManagerApp.class)
public class ClubJoystickResourceIntTest {

    private static final Integer DEFAULT_AMOUNT = 1;
    private static final Integer UPDATED_AMOUNT = 2;

    @Autowired
    private ClubJoystickRepository clubJoystickRepository;

    @Autowired
    private ClubJoystickService clubJoystickService;

    @Autowired
    private ClubJoystickSearchRepository clubJoystickSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restClubJoystickMockMvc;

    private ClubJoystick clubJoystick;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClubJoystickResource clubJoystickResource = new ClubJoystickResource(clubJoystickService);
        this.restClubJoystickMockMvc = MockMvcBuilders.standaloneSetup(clubJoystickResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClubJoystick createEntity(EntityManager em) {
        ClubJoystick clubJoystick = new ClubJoystick()
            .amount(DEFAULT_AMOUNT);
        return clubJoystick;
    }

    @Before
    public void initTest() {
        clubJoystickSearchRepository.deleteAll();
        clubJoystick = createEntity(em);
    }

    @Test
    @Transactional
    public void createClubJoystick() throws Exception {
        int databaseSizeBeforeCreate = clubJoystickRepository.findAll().size();

        // Create the ClubJoystick
        restClubJoystickMockMvc.perform(post("/api/club-joysticks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clubJoystick)))
            .andExpect(status().isCreated());

        // Validate the ClubJoystick in the database
        List<ClubJoystick> clubJoystickList = clubJoystickRepository.findAll();
        assertThat(clubJoystickList).hasSize(databaseSizeBeforeCreate + 1);
        ClubJoystick testClubJoystick = clubJoystickList.get(clubJoystickList.size() - 1);
        assertThat(testClubJoystick.getAmount()).isEqualTo(DEFAULT_AMOUNT);

        // Validate the ClubJoystick in Elasticsearch
        ClubJoystick clubJoystickEs = clubJoystickSearchRepository.findOne(testClubJoystick.getId());
        assertThat(clubJoystickEs).isEqualToIgnoringGivenFields(testClubJoystick);
    }

    @Test
    @Transactional
    public void createClubJoystickWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clubJoystickRepository.findAll().size();

        // Create the ClubJoystick with an existing ID
        clubJoystick.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClubJoystickMockMvc.perform(post("/api/club-joysticks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clubJoystick)))
            .andExpect(status().isBadRequest());

        // Validate the ClubJoystick in the database
        List<ClubJoystick> clubJoystickList = clubJoystickRepository.findAll();
        assertThat(clubJoystickList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllClubJoysticks() throws Exception {
        // Initialize the database
        clubJoystickRepository.saveAndFlush(clubJoystick);

        // Get all the clubJoystickList
        restClubJoystickMockMvc.perform(get("/api/club-joysticks?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clubJoystick.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT)));
    }

    @Test
    @Transactional
    public void getClubJoystick() throws Exception {
        // Initialize the database
        clubJoystickRepository.saveAndFlush(clubJoystick);

        // Get the clubJoystick
        restClubJoystickMockMvc.perform(get("/api/club-joysticks/{id}", clubJoystick.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clubJoystick.getId().intValue()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT));
    }

    @Test
    @Transactional
    public void getNonExistingClubJoystick() throws Exception {
        // Get the clubJoystick
        restClubJoystickMockMvc.perform(get("/api/club-joysticks/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClubJoystick() throws Exception {
        // Initialize the database
        clubJoystickService.save(clubJoystick);

        int databaseSizeBeforeUpdate = clubJoystickRepository.findAll().size();

        // Update the clubJoystick
        ClubJoystick updatedClubJoystick = clubJoystickRepository.findOne(clubJoystick.getId());
        // Disconnect from session so that the updates on updatedClubJoystick are not directly saved in db
        em.detach(updatedClubJoystick);
        updatedClubJoystick
            .amount(UPDATED_AMOUNT);

        restClubJoystickMockMvc.perform(put("/api/club-joysticks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedClubJoystick)))
            .andExpect(status().isOk());

        // Validate the ClubJoystick in the database
        List<ClubJoystick> clubJoystickList = clubJoystickRepository.findAll();
        assertThat(clubJoystickList).hasSize(databaseSizeBeforeUpdate);
        ClubJoystick testClubJoystick = clubJoystickList.get(clubJoystickList.size() - 1);
        assertThat(testClubJoystick.getAmount()).isEqualTo(UPDATED_AMOUNT);

        // Validate the ClubJoystick in Elasticsearch
        ClubJoystick clubJoystickEs = clubJoystickSearchRepository.findOne(testClubJoystick.getId());
        assertThat(clubJoystickEs).isEqualToIgnoringGivenFields(testClubJoystick);
    }

    @Test
    @Transactional
    public void updateNonExistingClubJoystick() throws Exception {
        int databaseSizeBeforeUpdate = clubJoystickRepository.findAll().size();

        // Create the ClubJoystick

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restClubJoystickMockMvc.perform(put("/api/club-joysticks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clubJoystick)))
            .andExpect(status().isCreated());

        // Validate the ClubJoystick in the database
        List<ClubJoystick> clubJoystickList = clubJoystickRepository.findAll();
        assertThat(clubJoystickList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteClubJoystick() throws Exception {
        // Initialize the database
        clubJoystickService.save(clubJoystick);

        int databaseSizeBeforeDelete = clubJoystickRepository.findAll().size();

        // Get the clubJoystick
        restClubJoystickMockMvc.perform(delete("/api/club-joysticks/{id}", clubJoystick.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean clubJoystickExistsInEs = clubJoystickSearchRepository.exists(clubJoystick.getId());
        assertThat(clubJoystickExistsInEs).isFalse();

        // Validate the database is empty
        List<ClubJoystick> clubJoystickList = clubJoystickRepository.findAll();
        assertThat(clubJoystickList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchClubJoystick() throws Exception {
        // Initialize the database
        clubJoystickService.save(clubJoystick);

        // Search the clubJoystick
        restClubJoystickMockMvc.perform(get("/api/_search/club-joysticks?query=id:" + clubJoystick.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clubJoystick.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClubJoystick.class);
        ClubJoystick clubJoystick1 = new ClubJoystick();
        clubJoystick1.setId(1L);
        ClubJoystick clubJoystick2 = new ClubJoystick();
        clubJoystick2.setId(clubJoystick1.getId());
        assertThat(clubJoystick1).isEqualTo(clubJoystick2);
        clubJoystick2.setId(2L);
        assertThat(clubJoystick1).isNotEqualTo(clubJoystick2);
        clubJoystick1.setId(null);
        assertThat(clubJoystick1).isNotEqualTo(clubJoystick2);
    }
}
