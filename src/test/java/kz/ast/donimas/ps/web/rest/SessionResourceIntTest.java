package kz.ast.donimas.ps.web.rest;

import kz.ast.donimas.ps.PsClubManagerApp;

import kz.ast.donimas.ps.domain.Session;
import kz.ast.donimas.ps.repository.SessionRepository;
import kz.ast.donimas.ps.service.SessionService;
import kz.ast.donimas.ps.repository.search.SessionSearchRepository;
import kz.ast.donimas.ps.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static kz.ast.donimas.ps.web.rest.TestUtil.sameInstant;
import static kz.ast.donimas.ps.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import kz.ast.donimas.ps.domain.enumeration.SessionStatus;
/**
 * Test class for the SessionResource REST controller.
 *
 * @see SessionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PsClubManagerApp.class)
public class SessionResourceIntTest {

    private static final ZonedDateTime DEFAULT_STARTED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_STARTED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_FINISHED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FINISHED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final BigDecimal DEFAULT_PLAYED_HOURS = new BigDecimal(1);
    private static final BigDecimal UPDATED_PLAYED_HOURS = new BigDecimal(2);

    private static final BigDecimal DEFAULT_PLAYED_MINUTES = new BigDecimal(1);
    private static final BigDecimal UPDATED_PLAYED_MINUTES = new BigDecimal(2);

    private static final SessionStatus DEFAULT_STATUS = SessionStatus.STARTED;
    private static final SessionStatus UPDATED_STATUS = SessionStatus.STOPPED;

    private static final BigDecimal DEFAULT_DISCOUNT_PERCENT = new BigDecimal(1);
    private static final BigDecimal UPDATED_DISCOUNT_PERCENT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_DISCOUNT_MINUTES = new BigDecimal(1);
    private static final BigDecimal UPDATED_DISCOUNT_MINUTES = new BigDecimal(2);

    private static final Boolean DEFAULT_FLAG_OPENED_TIME = false;
    private static final Boolean UPDATED_FLAG_OPENED_TIME = true;

    private static final Boolean DEFAULT_FLAG_CANCELLED = false;
    private static final Boolean UPDATED_FLAG_CANCELLED = true;

    private static final BigDecimal DEFAULT_PLANNED_PRICE = new BigDecimal(1);
    private static final BigDecimal UPDATED_PLANNED_PRICE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_TOTAL_PRICE = new BigDecimal(1);
    private static final BigDecimal UPDATED_TOTAL_PRICE = new BigDecimal(2);

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private SessionSearchRepository sessionSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSessionMockMvc;

    private Session session;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SessionResource sessionResource = new SessionResource(sessionService);
        this.restSessionMockMvc = MockMvcBuilders.standaloneSetup(sessionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Session createEntity(EntityManager em) {
        Session session = new Session()
            .startedAt(DEFAULT_STARTED_AT)
            .finishedAt(DEFAULT_FINISHED_AT)
            .playedHours(DEFAULT_PLAYED_HOURS)
            .playedMinutes(DEFAULT_PLAYED_MINUTES)
            .status(DEFAULT_STATUS)
            .discountPercent(DEFAULT_DISCOUNT_PERCENT)
            .discountMinutes(DEFAULT_DISCOUNT_MINUTES)
            .flagOpenedTime(DEFAULT_FLAG_OPENED_TIME)
            .flagCancelled(DEFAULT_FLAG_CANCELLED)
            .plannedPrice(DEFAULT_PLANNED_PRICE)
            .totalPrice(DEFAULT_TOTAL_PRICE);
        return session;
    }

    @Before
    public void initTest() {
        sessionSearchRepository.deleteAll();
        session = createEntity(em);
    }

    @Test
    @Transactional
    public void createSession() throws Exception {
        int databaseSizeBeforeCreate = sessionRepository.findAll().size();

        // Create the Session
        restSessionMockMvc.perform(post("/api/sessions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(session)))
            .andExpect(status().isCreated());

        // Validate the Session in the database
        List<Session> sessionList = sessionRepository.findAll();
        assertThat(sessionList).hasSize(databaseSizeBeforeCreate + 1);
        Session testSession = sessionList.get(sessionList.size() - 1);
        assertThat(testSession.getStartedAt()).isEqualTo(DEFAULT_STARTED_AT);
        assertThat(testSession.getFinishedAt()).isEqualTo(DEFAULT_FINISHED_AT);
        assertThat(testSession.getPlayedHours()).isEqualTo(DEFAULT_PLAYED_HOURS);
        assertThat(testSession.getPlayedMinutes()).isEqualTo(DEFAULT_PLAYED_MINUTES);
        assertThat(testSession.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testSession.getDiscountPercent()).isEqualTo(DEFAULT_DISCOUNT_PERCENT);
        assertThat(testSession.getDiscountMinutes()).isEqualTo(DEFAULT_DISCOUNT_MINUTES);
        assertThat(testSession.isFlagOpenedTime()).isEqualTo(DEFAULT_FLAG_OPENED_TIME);
        assertThat(testSession.isFlagCancelled()).isEqualTo(DEFAULT_FLAG_CANCELLED);
        assertThat(testSession.getPlannedPrice()).isEqualTo(DEFAULT_PLANNED_PRICE);
        assertThat(testSession.getTotalPrice()).isEqualTo(DEFAULT_TOTAL_PRICE);

        // Validate the Session in Elasticsearch
        Session sessionEs = sessionSearchRepository.findOne(testSession.getId());
        assertThat(testSession.getStartedAt()).isEqualTo(testSession.getStartedAt());
        assertThat(testSession.getFinishedAt()).isEqualTo(testSession.getFinishedAt());
        assertThat(sessionEs).isEqualToIgnoringGivenFields(testSession, "startedAt", "finishedAt");
    }

    @Test
    @Transactional
    public void createSessionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sessionRepository.findAll().size();

        // Create the Session with an existing ID
        session.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSessionMockMvc.perform(post("/api/sessions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(session)))
            .andExpect(status().isBadRequest());

        // Validate the Session in the database
        List<Session> sessionList = sessionRepository.findAll();
        assertThat(sessionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkStartedAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = sessionRepository.findAll().size();
        // set the field null
        session.setStartedAt(null);

        // Create the Session, which fails.

        restSessionMockMvc.perform(post("/api/sessions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(session)))
            .andExpect(status().isBadRequest());

        List<Session> sessionList = sessionRepository.findAll();
        assertThat(sessionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFinishedAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = sessionRepository.findAll().size();
        // set the field null
        session.setFinishedAt(null);

        // Create the Session, which fails.

        restSessionMockMvc.perform(post("/api/sessions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(session)))
            .andExpect(status().isBadRequest());

        List<Session> sessionList = sessionRepository.findAll();
        assertThat(sessionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = sessionRepository.findAll().size();
        // set the field null
        session.setStatus(null);

        // Create the Session, which fails.

        restSessionMockMvc.perform(post("/api/sessions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(session)))
            .andExpect(status().isBadRequest());

        List<Session> sessionList = sessionRepository.findAll();
        assertThat(sessionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSessions() throws Exception {
        // Initialize the database
        sessionRepository.saveAndFlush(session);

        // Get all the sessionList
        restSessionMockMvc.perform(get("/api/sessions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(session.getId().intValue())))
            .andExpect(jsonPath("$.[*].startedAt").value(hasItem(sameInstant(DEFAULT_STARTED_AT))))
            .andExpect(jsonPath("$.[*].finishedAt").value(hasItem(sameInstant(DEFAULT_FINISHED_AT))))
            .andExpect(jsonPath("$.[*].playedHours").value(hasItem(DEFAULT_PLAYED_HOURS.intValue())))
            .andExpect(jsonPath("$.[*].playedMinutes").value(hasItem(DEFAULT_PLAYED_MINUTES.intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].discountPercent").value(hasItem(DEFAULT_DISCOUNT_PERCENT.intValue())))
            .andExpect(jsonPath("$.[*].discountMinutes").value(hasItem(DEFAULT_DISCOUNT_MINUTES.intValue())))
            .andExpect(jsonPath("$.[*].flagOpenedTime").value(hasItem(DEFAULT_FLAG_OPENED_TIME.booleanValue())))
            .andExpect(jsonPath("$.[*].flagCancelled").value(hasItem(DEFAULT_FLAG_CANCELLED.booleanValue())))
            .andExpect(jsonPath("$.[*].plannedPrice").value(hasItem(DEFAULT_PLANNED_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].totalPrice").value(hasItem(DEFAULT_TOTAL_PRICE.intValue())));
    }

    @Test
    @Transactional
    public void getSession() throws Exception {
        // Initialize the database
        sessionRepository.saveAndFlush(session);

        // Get the session
        restSessionMockMvc.perform(get("/api/sessions/{id}", session.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(session.getId().intValue()))
            .andExpect(jsonPath("$.startedAt").value(sameInstant(DEFAULT_STARTED_AT)))
            .andExpect(jsonPath("$.finishedAt").value(sameInstant(DEFAULT_FINISHED_AT)))
            .andExpect(jsonPath("$.playedHours").value(DEFAULT_PLAYED_HOURS.intValue()))
            .andExpect(jsonPath("$.playedMinutes").value(DEFAULT_PLAYED_MINUTES.intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.discountPercent").value(DEFAULT_DISCOUNT_PERCENT.intValue()))
            .andExpect(jsonPath("$.discountMinutes").value(DEFAULT_DISCOUNT_MINUTES.intValue()))
            .andExpect(jsonPath("$.flagOpenedTime").value(DEFAULT_FLAG_OPENED_TIME.booleanValue()))
            .andExpect(jsonPath("$.flagCancelled").value(DEFAULT_FLAG_CANCELLED.booleanValue()))
            .andExpect(jsonPath("$.plannedPrice").value(DEFAULT_PLANNED_PRICE.intValue()))
            .andExpect(jsonPath("$.totalPrice").value(DEFAULT_TOTAL_PRICE.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSession() throws Exception {
        // Get the session
        restSessionMockMvc.perform(get("/api/sessions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSession() throws Exception {
        // Initialize the database
        sessionService.save(session);

        int databaseSizeBeforeUpdate = sessionRepository.findAll().size();

        // Update the session
        Session updatedSession = sessionRepository.findOne(session.getId());
        // Disconnect from session so that the updates on updatedSession are not directly saved in db
        em.detach(updatedSession);
        updatedSession
            .startedAt(UPDATED_STARTED_AT)
            .finishedAt(UPDATED_FINISHED_AT)
            .playedHours(UPDATED_PLAYED_HOURS)
            .playedMinutes(UPDATED_PLAYED_MINUTES)
            .status(UPDATED_STATUS)
            .discountPercent(UPDATED_DISCOUNT_PERCENT)
            .discountMinutes(UPDATED_DISCOUNT_MINUTES)
            .flagOpenedTime(UPDATED_FLAG_OPENED_TIME)
            .flagCancelled(UPDATED_FLAG_CANCELLED)
            .plannedPrice(UPDATED_PLANNED_PRICE)
            .totalPrice(UPDATED_TOTAL_PRICE);

        restSessionMockMvc.perform(put("/api/sessions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSession)))
            .andExpect(status().isOk());

        // Validate the Session in the database
        List<Session> sessionList = sessionRepository.findAll();
        assertThat(sessionList).hasSize(databaseSizeBeforeUpdate);
        Session testSession = sessionList.get(sessionList.size() - 1);
        assertThat(testSession.getStartedAt()).isEqualTo(UPDATED_STARTED_AT);
        assertThat(testSession.getFinishedAt()).isEqualTo(UPDATED_FINISHED_AT);
        assertThat(testSession.getPlayedHours()).isEqualTo(UPDATED_PLAYED_HOURS);
        assertThat(testSession.getPlayedMinutes()).isEqualTo(UPDATED_PLAYED_MINUTES);
        assertThat(testSession.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testSession.getDiscountPercent()).isEqualTo(UPDATED_DISCOUNT_PERCENT);
        assertThat(testSession.getDiscountMinutes()).isEqualTo(UPDATED_DISCOUNT_MINUTES);
        assertThat(testSession.isFlagOpenedTime()).isEqualTo(UPDATED_FLAG_OPENED_TIME);
        assertThat(testSession.isFlagCancelled()).isEqualTo(UPDATED_FLAG_CANCELLED);
        assertThat(testSession.getPlannedPrice()).isEqualTo(UPDATED_PLANNED_PRICE);
        assertThat(testSession.getTotalPrice()).isEqualTo(UPDATED_TOTAL_PRICE);

        // Validate the Session in Elasticsearch
        Session sessionEs = sessionSearchRepository.findOne(testSession.getId());
        assertThat(testSession.getStartedAt()).isEqualTo(testSession.getStartedAt());
        assertThat(testSession.getFinishedAt()).isEqualTo(testSession.getFinishedAt());
        assertThat(sessionEs).isEqualToIgnoringGivenFields(testSession, "startedAt", "finishedAt");
    }

    @Test
    @Transactional
    public void updateNonExistingSession() throws Exception {
        int databaseSizeBeforeUpdate = sessionRepository.findAll().size();

        // Create the Session

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSessionMockMvc.perform(put("/api/sessions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(session)))
            .andExpect(status().isCreated());

        // Validate the Session in the database
        List<Session> sessionList = sessionRepository.findAll();
        assertThat(sessionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSession() throws Exception {
        // Initialize the database
        sessionService.save(session);

        int databaseSizeBeforeDelete = sessionRepository.findAll().size();

        // Get the session
        restSessionMockMvc.perform(delete("/api/sessions/{id}", session.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean sessionExistsInEs = sessionSearchRepository.exists(session.getId());
        assertThat(sessionExistsInEs).isFalse();

        // Validate the database is empty
        List<Session> sessionList = sessionRepository.findAll();
        assertThat(sessionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSession() throws Exception {
        // Initialize the database
        sessionService.save(session);

        // Search the session
        restSessionMockMvc.perform(get("/api/_search/sessions?query=id:" + session.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(session.getId().intValue())))
            .andExpect(jsonPath("$.[*].startedAt").value(hasItem(sameInstant(DEFAULT_STARTED_AT))))
            .andExpect(jsonPath("$.[*].finishedAt").value(hasItem(sameInstant(DEFAULT_FINISHED_AT))))
            .andExpect(jsonPath("$.[*].playedHours").value(hasItem(DEFAULT_PLAYED_HOURS.intValue())))
            .andExpect(jsonPath("$.[*].playedMinutes").value(hasItem(DEFAULT_PLAYED_MINUTES.intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].discountPercent").value(hasItem(DEFAULT_DISCOUNT_PERCENT.intValue())))
            .andExpect(jsonPath("$.[*].discountMinutes").value(hasItem(DEFAULT_DISCOUNT_MINUTES.intValue())))
            .andExpect(jsonPath("$.[*].flagOpenedTime").value(hasItem(DEFAULT_FLAG_OPENED_TIME.booleanValue())))
            .andExpect(jsonPath("$.[*].flagCancelled").value(hasItem(DEFAULT_FLAG_CANCELLED.booleanValue())))
            .andExpect(jsonPath("$.[*].plannedPrice").value(hasItem(DEFAULT_PLANNED_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].totalPrice").value(hasItem(DEFAULT_TOTAL_PRICE.intValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Session.class);
        Session session1 = new Session();
        session1.setId(1L);
        Session session2 = new Session();
        session2.setId(session1.getId());
        assertThat(session1).isEqualTo(session2);
        session2.setId(2L);
        assertThat(session1).isNotEqualTo(session2);
        session1.setId(null);
        assertThat(session1).isNotEqualTo(session2);
    }
}
