package kz.ast.donimas.ps.web.rest;

import kz.ast.donimas.ps.PsClubManagerApp;

import kz.ast.donimas.ps.domain.ZoneDictionary;
import kz.ast.donimas.ps.repository.ZoneDictionaryRepository;
import kz.ast.donimas.ps.service.ZoneDictionaryService;
import kz.ast.donimas.ps.repository.search.ZoneDictionarySearchRepository;
import kz.ast.donimas.ps.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static kz.ast.donimas.ps.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ZoneDictionaryResource REST controller.
 *
 * @see ZoneDictionaryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PsClubManagerApp.class)
public class ZoneDictionaryResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    @Autowired
    private ZoneDictionaryRepository zoneDictionaryRepository;

    @Autowired
    private ZoneDictionaryService zoneDictionaryService;

    @Autowired
    private ZoneDictionarySearchRepository zoneDictionarySearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restZoneDictionaryMockMvc;

    private ZoneDictionary zoneDictionary;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ZoneDictionaryResource zoneDictionaryResource = new ZoneDictionaryResource(zoneDictionaryService);
        this.restZoneDictionaryMockMvc = MockMvcBuilders.standaloneSetup(zoneDictionaryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ZoneDictionary createEntity(EntityManager em) {
        ZoneDictionary zoneDictionary = new ZoneDictionary()
            .name(DEFAULT_NAME)
            .code(DEFAULT_CODE);
        return zoneDictionary;
    }

    @Before
    public void initTest() {
        zoneDictionarySearchRepository.deleteAll();
        zoneDictionary = createEntity(em);
    }

    @Test
    @Transactional
    public void createZoneDictionary() throws Exception {
        int databaseSizeBeforeCreate = zoneDictionaryRepository.findAll().size();

        // Create the ZoneDictionary
        restZoneDictionaryMockMvc.perform(post("/api/zone-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(zoneDictionary)))
            .andExpect(status().isCreated());

        // Validate the ZoneDictionary in the database
        List<ZoneDictionary> zoneDictionaryList = zoneDictionaryRepository.findAll();
        assertThat(zoneDictionaryList).hasSize(databaseSizeBeforeCreate + 1);
        ZoneDictionary testZoneDictionary = zoneDictionaryList.get(zoneDictionaryList.size() - 1);
        assertThat(testZoneDictionary.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testZoneDictionary.getCode()).isEqualTo(DEFAULT_CODE);

        // Validate the ZoneDictionary in Elasticsearch
        ZoneDictionary zoneDictionaryEs = zoneDictionarySearchRepository.findOne(testZoneDictionary.getId());
        assertThat(zoneDictionaryEs).isEqualToIgnoringGivenFields(testZoneDictionary);
    }

    @Test
    @Transactional
    public void createZoneDictionaryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = zoneDictionaryRepository.findAll().size();

        // Create the ZoneDictionary with an existing ID
        zoneDictionary.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restZoneDictionaryMockMvc.perform(post("/api/zone-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(zoneDictionary)))
            .andExpect(status().isBadRequest());

        // Validate the ZoneDictionary in the database
        List<ZoneDictionary> zoneDictionaryList = zoneDictionaryRepository.findAll();
        assertThat(zoneDictionaryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = zoneDictionaryRepository.findAll().size();
        // set the field null
        zoneDictionary.setName(null);

        // Create the ZoneDictionary, which fails.

        restZoneDictionaryMockMvc.perform(post("/api/zone-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(zoneDictionary)))
            .andExpect(status().isBadRequest());

        List<ZoneDictionary> zoneDictionaryList = zoneDictionaryRepository.findAll();
        assertThat(zoneDictionaryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = zoneDictionaryRepository.findAll().size();
        // set the field null
        zoneDictionary.setCode(null);

        // Create the ZoneDictionary, which fails.

        restZoneDictionaryMockMvc.perform(post("/api/zone-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(zoneDictionary)))
            .andExpect(status().isBadRequest());

        List<ZoneDictionary> zoneDictionaryList = zoneDictionaryRepository.findAll();
        assertThat(zoneDictionaryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllZoneDictionaries() throws Exception {
        // Initialize the database
        zoneDictionaryRepository.saveAndFlush(zoneDictionary);

        // Get all the zoneDictionaryList
        restZoneDictionaryMockMvc.perform(get("/api/zone-dictionaries?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(zoneDictionary.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())));
    }

    @Test
    @Transactional
    public void getZoneDictionary() throws Exception {
        // Initialize the database
        zoneDictionaryRepository.saveAndFlush(zoneDictionary);

        // Get the zoneDictionary
        restZoneDictionaryMockMvc.perform(get("/api/zone-dictionaries/{id}", zoneDictionary.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(zoneDictionary.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingZoneDictionary() throws Exception {
        // Get the zoneDictionary
        restZoneDictionaryMockMvc.perform(get("/api/zone-dictionaries/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateZoneDictionary() throws Exception {
        // Initialize the database
        zoneDictionaryService.save(zoneDictionary);

        int databaseSizeBeforeUpdate = zoneDictionaryRepository.findAll().size();

        // Update the zoneDictionary
        ZoneDictionary updatedZoneDictionary = zoneDictionaryRepository.findOne(zoneDictionary.getId());
        // Disconnect from session so that the updates on updatedZoneDictionary are not directly saved in db
        em.detach(updatedZoneDictionary);
        updatedZoneDictionary
            .name(UPDATED_NAME)
            .code(UPDATED_CODE);

        restZoneDictionaryMockMvc.perform(put("/api/zone-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedZoneDictionary)))
            .andExpect(status().isOk());

        // Validate the ZoneDictionary in the database
        List<ZoneDictionary> zoneDictionaryList = zoneDictionaryRepository.findAll();
        assertThat(zoneDictionaryList).hasSize(databaseSizeBeforeUpdate);
        ZoneDictionary testZoneDictionary = zoneDictionaryList.get(zoneDictionaryList.size() - 1);
        assertThat(testZoneDictionary.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testZoneDictionary.getCode()).isEqualTo(UPDATED_CODE);

        // Validate the ZoneDictionary in Elasticsearch
        ZoneDictionary zoneDictionaryEs = zoneDictionarySearchRepository.findOne(testZoneDictionary.getId());
        assertThat(zoneDictionaryEs).isEqualToIgnoringGivenFields(testZoneDictionary);
    }

    @Test
    @Transactional
    public void updateNonExistingZoneDictionary() throws Exception {
        int databaseSizeBeforeUpdate = zoneDictionaryRepository.findAll().size();

        // Create the ZoneDictionary

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restZoneDictionaryMockMvc.perform(put("/api/zone-dictionaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(zoneDictionary)))
            .andExpect(status().isCreated());

        // Validate the ZoneDictionary in the database
        List<ZoneDictionary> zoneDictionaryList = zoneDictionaryRepository.findAll();
        assertThat(zoneDictionaryList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteZoneDictionary() throws Exception {
        // Initialize the database
        zoneDictionaryService.save(zoneDictionary);

        int databaseSizeBeforeDelete = zoneDictionaryRepository.findAll().size();

        // Get the zoneDictionary
        restZoneDictionaryMockMvc.perform(delete("/api/zone-dictionaries/{id}", zoneDictionary.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean zoneDictionaryExistsInEs = zoneDictionarySearchRepository.exists(zoneDictionary.getId());
        assertThat(zoneDictionaryExistsInEs).isFalse();

        // Validate the database is empty
        List<ZoneDictionary> zoneDictionaryList = zoneDictionaryRepository.findAll();
        assertThat(zoneDictionaryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchZoneDictionary() throws Exception {
        // Initialize the database
        zoneDictionaryService.save(zoneDictionary);

        // Search the zoneDictionary
        restZoneDictionaryMockMvc.perform(get("/api/_search/zone-dictionaries?query=id:" + zoneDictionary.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(zoneDictionary.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ZoneDictionary.class);
        ZoneDictionary zoneDictionary1 = new ZoneDictionary();
        zoneDictionary1.setId(1L);
        ZoneDictionary zoneDictionary2 = new ZoneDictionary();
        zoneDictionary2.setId(zoneDictionary1.getId());
        assertThat(zoneDictionary1).isEqualTo(zoneDictionary2);
        zoneDictionary2.setId(2L);
        assertThat(zoneDictionary1).isNotEqualTo(zoneDictionary2);
        zoneDictionary1.setId(null);
        assertThat(zoneDictionary1).isNotEqualTo(zoneDictionary2);
    }
}
