package kz.ast.donimas.ps.web.rest;

import kz.ast.donimas.ps.PsClubManagerApp;

import kz.ast.donimas.ps.domain.Club;
import kz.ast.donimas.ps.repository.ClubRepository;
import kz.ast.donimas.ps.service.ClubService;
import kz.ast.donimas.ps.repository.search.ClubSearchRepository;
import kz.ast.donimas.ps.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static kz.ast.donimas.ps.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ClubResource REST controller.
 *
 * @see ClubResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PsClubManagerApp.class)
public class ClubResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_FULL_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_FULL_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_LON = "AAAAAAAAAA";
    private static final String UPDATED_LON = "BBBBBBBBBB";

    private static final String DEFAULT_LAT = "AAAAAAAAAA";
    private static final String UPDATED_LAT = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_TIME_SCHEDULE_REGULATION = new BigDecimal(1);
    private static final BigDecimal UPDATED_TIME_SCHEDULE_REGULATION = new BigDecimal(2);

    private static final Boolean DEFAULT_FLAGHOOKAH = false;
    private static final Boolean UPDATED_FLAGHOOKAH = true;

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE_OPTIONAL = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_OPTIONAL = "BBBBBBBBBB";

    private static final String DEFAULT_VK_URL = "AAAAAAAAAA";
    private static final String UPDATED_VK_URL = "BBBBBBBBBB";

    private static final String DEFAULT_INSTA_URL = "AAAAAAAAAA";
    private static final String UPDATED_INSTA_URL = "BBBBBBBBBB";

    private static final String DEFAULT_TELEGRAM_URL = "AAAAAAAAAA";
    private static final String UPDATED_TELEGRAM_URL = "BBBBBBBBBB";

    private static final String DEFAULT_FACEBOOK_URL = "AAAAAAAAAA";
    private static final String UPDATED_FACEBOOK_URL = "BBBBBBBBBB";

    private static final String DEFAULT_TWITTER_URL = "AAAAAAAAAA";
    private static final String UPDATED_TWITTER_URL = "BBBBBBBBBB";

    @Autowired
    private ClubRepository clubRepository;

    @Autowired
    private ClubService clubService;

    @Autowired
    private ClubSearchRepository clubSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restClubMockMvc;

    private Club club;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClubResource clubResource = new ClubResource(clubService, userService);
        this.restClubMockMvc = MockMvcBuilders.standaloneSetup(clubResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Club createEntity(EntityManager em) {
        Club club = new Club()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .fullAddress(DEFAULT_FULL_ADDRESS)
            .lon(DEFAULT_LON)
            .lat(DEFAULT_LAT)
            .timeScheduleRegulation(DEFAULT_TIME_SCHEDULE_REGULATION)
            .flaghookah(DEFAULT_FLAGHOOKAH)
            .phone(DEFAULT_PHONE)
            .phoneOptional(DEFAULT_PHONE_OPTIONAL)
            .vkUrl(DEFAULT_VK_URL)
            .instaUrl(DEFAULT_INSTA_URL)
            .telegramUrl(DEFAULT_TELEGRAM_URL)
            .facebookUrl(DEFAULT_FACEBOOK_URL)
            .twitterUrl(DEFAULT_TWITTER_URL);
        return club;
    }

    @Before
    public void initTest() {
        clubSearchRepository.deleteAll();
        club = createEntity(em);
    }

    @Test
    @Transactional
    public void createClub() throws Exception {
        int databaseSizeBeforeCreate = clubRepository.findAll().size();

        // Create the Club
        restClubMockMvc.perform(post("/api/clubs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(club)))
            .andExpect(status().isCreated());

        // Validate the Club in the database
        List<Club> clubList = clubRepository.findAll();
        assertThat(clubList).hasSize(databaseSizeBeforeCreate + 1);
        Club testClub = clubList.get(clubList.size() - 1);
        assertThat(testClub.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testClub.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testClub.getFullAddress()).isEqualTo(DEFAULT_FULL_ADDRESS);
        assertThat(testClub.getLon()).isEqualTo(DEFAULT_LON);
        assertThat(testClub.getLat()).isEqualTo(DEFAULT_LAT);
        assertThat(testClub.getTimeScheduleRegulation()).isEqualTo(DEFAULT_TIME_SCHEDULE_REGULATION);
        assertThat(testClub.isFlaghookah()).isEqualTo(DEFAULT_FLAGHOOKAH);
        assertThat(testClub.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testClub.getPhoneOptional()).isEqualTo(DEFAULT_PHONE_OPTIONAL);
        assertThat(testClub.getVkUrl()).isEqualTo(DEFAULT_VK_URL);
        assertThat(testClub.getInstaUrl()).isEqualTo(DEFAULT_INSTA_URL);
        assertThat(testClub.getTelegramUrl()).isEqualTo(DEFAULT_TELEGRAM_URL);
        assertThat(testClub.getFacebookUrl()).isEqualTo(DEFAULT_FACEBOOK_URL);
        assertThat(testClub.getTwitterUrl()).isEqualTo(DEFAULT_TWITTER_URL);

        // Validate the Club in Elasticsearch
        Club clubEs = clubSearchRepository.findOne(testClub.getId());
        assertThat(clubEs).isEqualToIgnoringGivenFields(testClub);
    }

    @Test
    @Transactional
    public void createClubWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clubRepository.findAll().size();

        // Create the Club with an existing ID
        club.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClubMockMvc.perform(post("/api/clubs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(club)))
            .andExpect(status().isBadRequest());

        // Validate the Club in the database
        List<Club> clubList = clubRepository.findAll();
        assertThat(clubList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = clubRepository.findAll().size();
        // set the field null
        club.setName(null);

        // Create the Club, which fails.

        restClubMockMvc.perform(post("/api/clubs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(club)))
            .andExpect(status().isBadRequest());

        List<Club> clubList = clubRepository.findAll();
        assertThat(clubList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFullAddressIsRequired() throws Exception {
        int databaseSizeBeforeTest = clubRepository.findAll().size();
        // set the field null
        club.setFullAddress(null);

        // Create the Club, which fails.

        restClubMockMvc.perform(post("/api/clubs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(club)))
            .andExpect(status().isBadRequest());

        List<Club> clubList = clubRepository.findAll();
        assertThat(clubList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPhoneIsRequired() throws Exception {
        int databaseSizeBeforeTest = clubRepository.findAll().size();
        // set the field null
        club.setPhone(null);

        // Create the Club, which fails.

        restClubMockMvc.perform(post("/api/clubs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(club)))
            .andExpect(status().isBadRequest());

        List<Club> clubList = clubRepository.findAll();
        assertThat(clubList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllClubs() throws Exception {
        // Initialize the database
        clubRepository.saveAndFlush(club);

        // Get all the clubList
        restClubMockMvc.perform(get("/api/clubs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(club.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].fullAddress").value(hasItem(DEFAULT_FULL_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].lon").value(hasItem(DEFAULT_LON.toString())))
            .andExpect(jsonPath("$.[*].lat").value(hasItem(DEFAULT_LAT.toString())))
            .andExpect(jsonPath("$.[*].timeScheduleRegulation").value(hasItem(DEFAULT_TIME_SCHEDULE_REGULATION.intValue())))
            .andExpect(jsonPath("$.[*].flaghookah").value(hasItem(DEFAULT_FLAGHOOKAH.booleanValue())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].phoneOptional").value(hasItem(DEFAULT_PHONE_OPTIONAL.toString())))
            .andExpect(jsonPath("$.[*].vkUrl").value(hasItem(DEFAULT_VK_URL.toString())))
            .andExpect(jsonPath("$.[*].instaUrl").value(hasItem(DEFAULT_INSTA_URL.toString())))
            .andExpect(jsonPath("$.[*].telegramUrl").value(hasItem(DEFAULT_TELEGRAM_URL.toString())))
            .andExpect(jsonPath("$.[*].facebookUrl").value(hasItem(DEFAULT_FACEBOOK_URL.toString())))
            .andExpect(jsonPath("$.[*].twitterUrl").value(hasItem(DEFAULT_TWITTER_URL.toString())));
    }

    @Test
    @Transactional
    public void getClub() throws Exception {
        // Initialize the database
        clubRepository.saveAndFlush(club);

        // Get the club
        restClubMockMvc.perform(get("/api/clubs/{id}", club.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(club.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.fullAddress").value(DEFAULT_FULL_ADDRESS.toString()))
            .andExpect(jsonPath("$.lon").value(DEFAULT_LON.toString()))
            .andExpect(jsonPath("$.lat").value(DEFAULT_LAT.toString()))
            .andExpect(jsonPath("$.timeScheduleRegulation").value(DEFAULT_TIME_SCHEDULE_REGULATION.intValue()))
            .andExpect(jsonPath("$.flaghookah").value(DEFAULT_FLAGHOOKAH.booleanValue()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.toString()))
            .andExpect(jsonPath("$.phoneOptional").value(DEFAULT_PHONE_OPTIONAL.toString()))
            .andExpect(jsonPath("$.vkUrl").value(DEFAULT_VK_URL.toString()))
            .andExpect(jsonPath("$.instaUrl").value(DEFAULT_INSTA_URL.toString()))
            .andExpect(jsonPath("$.telegramUrl").value(DEFAULT_TELEGRAM_URL.toString()))
            .andExpect(jsonPath("$.facebookUrl").value(DEFAULT_FACEBOOK_URL.toString()))
            .andExpect(jsonPath("$.twitterUrl").value(DEFAULT_TWITTER_URL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingClub() throws Exception {
        // Get the club
        restClubMockMvc.perform(get("/api/clubs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClub() throws Exception {
        // Initialize the database
        clubService.save(club);

        int databaseSizeBeforeUpdate = clubRepository.findAll().size();

        // Update the club
        Club updatedClub = clubRepository.findOne(club.getId());
        // Disconnect from session so that the updates on updatedClub are not directly saved in db
        em.detach(updatedClub);
        updatedClub
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .fullAddress(UPDATED_FULL_ADDRESS)
            .lon(UPDATED_LON)
            .lat(UPDATED_LAT)
            .timeScheduleRegulation(UPDATED_TIME_SCHEDULE_REGULATION)
            .flaghookah(UPDATED_FLAGHOOKAH)
            .phone(UPDATED_PHONE)
            .phoneOptional(UPDATED_PHONE_OPTIONAL)
            .vkUrl(UPDATED_VK_URL)
            .instaUrl(UPDATED_INSTA_URL)
            .telegramUrl(UPDATED_TELEGRAM_URL)
            .facebookUrl(UPDATED_FACEBOOK_URL)
            .twitterUrl(UPDATED_TWITTER_URL);

        restClubMockMvc.perform(put("/api/clubs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedClub)))
            .andExpect(status().isOk());

        // Validate the Club in the database
        List<Club> clubList = clubRepository.findAll();
        assertThat(clubList).hasSize(databaseSizeBeforeUpdate);
        Club testClub = clubList.get(clubList.size() - 1);
        assertThat(testClub.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testClub.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testClub.getFullAddress()).isEqualTo(UPDATED_FULL_ADDRESS);
        assertThat(testClub.getLon()).isEqualTo(UPDATED_LON);
        assertThat(testClub.getLat()).isEqualTo(UPDATED_LAT);
        assertThat(testClub.getTimeScheduleRegulation()).isEqualTo(UPDATED_TIME_SCHEDULE_REGULATION);
        assertThat(testClub.isFlaghookah()).isEqualTo(UPDATED_FLAGHOOKAH);
        assertThat(testClub.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testClub.getPhoneOptional()).isEqualTo(UPDATED_PHONE_OPTIONAL);
        assertThat(testClub.getVkUrl()).isEqualTo(UPDATED_VK_URL);
        assertThat(testClub.getInstaUrl()).isEqualTo(UPDATED_INSTA_URL);
        assertThat(testClub.getTelegramUrl()).isEqualTo(UPDATED_TELEGRAM_URL);
        assertThat(testClub.getFacebookUrl()).isEqualTo(UPDATED_FACEBOOK_URL);
        assertThat(testClub.getTwitterUrl()).isEqualTo(UPDATED_TWITTER_URL);

        // Validate the Club in Elasticsearch
        Club clubEs = clubSearchRepository.findOne(testClub.getId());
        assertThat(clubEs).isEqualToIgnoringGivenFields(testClub);
    }

    @Test
    @Transactional
    public void updateNonExistingClub() throws Exception {
        int databaseSizeBeforeUpdate = clubRepository.findAll().size();

        // Create the Club

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restClubMockMvc.perform(put("/api/clubs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(club)))
            .andExpect(status().isCreated());

        // Validate the Club in the database
        List<Club> clubList = clubRepository.findAll();
        assertThat(clubList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteClub() throws Exception {
        // Initialize the database
        clubService.save(club);

        int databaseSizeBeforeDelete = clubRepository.findAll().size();

        // Get the club
        restClubMockMvc.perform(delete("/api/clubs/{id}", club.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean clubExistsInEs = clubSearchRepository.exists(club.getId());
        assertThat(clubExistsInEs).isFalse();

        // Validate the database is empty
        List<Club> clubList = clubRepository.findAll();
        assertThat(clubList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchClub() throws Exception {
        // Initialize the database
        clubService.save(club);

        // Search the club
        restClubMockMvc.perform(get("/api/_search/clubs?query=id:" + club.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(club.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].fullAddress").value(hasItem(DEFAULT_FULL_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].lon").value(hasItem(DEFAULT_LON.toString())))
            .andExpect(jsonPath("$.[*].lat").value(hasItem(DEFAULT_LAT.toString())))
            .andExpect(jsonPath("$.[*].timeScheduleRegulation").value(hasItem(DEFAULT_TIME_SCHEDULE_REGULATION.intValue())))
            .andExpect(jsonPath("$.[*].flaghookah").value(hasItem(DEFAULT_FLAGHOOKAH.booleanValue())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].phoneOptional").value(hasItem(DEFAULT_PHONE_OPTIONAL.toString())))
            .andExpect(jsonPath("$.[*].vkUrl").value(hasItem(DEFAULT_VK_URL.toString())))
            .andExpect(jsonPath("$.[*].instaUrl").value(hasItem(DEFAULT_INSTA_URL.toString())))
            .andExpect(jsonPath("$.[*].telegramUrl").value(hasItem(DEFAULT_TELEGRAM_URL.toString())))
            .andExpect(jsonPath("$.[*].facebookUrl").value(hasItem(DEFAULT_FACEBOOK_URL.toString())))
            .andExpect(jsonPath("$.[*].twitterUrl").value(hasItem(DEFAULT_TWITTER_URL.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Club.class);
        Club club1 = new Club();
        club1.setId(1L);
        Club club2 = new Club();
        club2.setId(club1.getId());
        assertThat(club1).isEqualTo(club2);
        club2.setId(2L);
        assertThat(club1).isNotEqualTo(club2);
        club1.setId(null);
        assertThat(club1).isNotEqualTo(club2);
    }
}
