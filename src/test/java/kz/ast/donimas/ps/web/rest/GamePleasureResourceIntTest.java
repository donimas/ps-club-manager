package kz.ast.donimas.ps.web.rest;

import kz.ast.donimas.ps.PsClubManagerApp;

import kz.ast.donimas.ps.domain.GamePleasure;
import kz.ast.donimas.ps.repository.GamePleasureRepository;
import kz.ast.donimas.ps.service.GamePleasureService;
import kz.ast.donimas.ps.repository.search.GamePleasureSearchRepository;
import kz.ast.donimas.ps.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static kz.ast.donimas.ps.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the GamePleasureResource REST controller.
 *
 * @see GamePleasureResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PsClubManagerApp.class)
public class GamePleasureResourceIntTest {

    private static final BigDecimal DEFAULT_RATING = new BigDecimal(1);
    private static final BigDecimal UPDATED_RATING = new BigDecimal(2);

    private static final String DEFAULT_REVIEW = "AAAAAAAAAA";
    private static final String UPDATED_REVIEW = "BBBBBBBBBB";

    @Autowired
    private GamePleasureRepository gamePleasureRepository;

    @Autowired
    private GamePleasureService gamePleasureService;

    @Autowired
    private GamePleasureSearchRepository gamePleasureSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restGamePleasureMockMvc;

    private GamePleasure gamePleasure;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final GamePleasureResource gamePleasureResource = new GamePleasureResource(gamePleasureService);
        this.restGamePleasureMockMvc = MockMvcBuilders.standaloneSetup(gamePleasureResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GamePleasure createEntity(EntityManager em) {
        GamePleasure gamePleasure = new GamePleasure()
            .rating(DEFAULT_RATING)
            .review(DEFAULT_REVIEW);
        return gamePleasure;
    }

    @Before
    public void initTest() {
        gamePleasureSearchRepository.deleteAll();
        gamePleasure = createEntity(em);
    }

    @Test
    @Transactional
    public void createGamePleasure() throws Exception {
        int databaseSizeBeforeCreate = gamePleasureRepository.findAll().size();

        // Create the GamePleasure
        restGamePleasureMockMvc.perform(post("/api/game-pleasures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(gamePleasure)))
            .andExpect(status().isCreated());

        // Validate the GamePleasure in the database
        List<GamePleasure> gamePleasureList = gamePleasureRepository.findAll();
        assertThat(gamePleasureList).hasSize(databaseSizeBeforeCreate + 1);
        GamePleasure testGamePleasure = gamePleasureList.get(gamePleasureList.size() - 1);
        assertThat(testGamePleasure.getRating()).isEqualTo(DEFAULT_RATING);
        assertThat(testGamePleasure.getReview()).isEqualTo(DEFAULT_REVIEW);

        // Validate the GamePleasure in Elasticsearch
        GamePleasure gamePleasureEs = gamePleasureSearchRepository.findOne(testGamePleasure.getId());
        assertThat(gamePleasureEs).isEqualToIgnoringGivenFields(testGamePleasure);
    }

    @Test
    @Transactional
    public void createGamePleasureWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = gamePleasureRepository.findAll().size();

        // Create the GamePleasure with an existing ID
        gamePleasure.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restGamePleasureMockMvc.perform(post("/api/game-pleasures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(gamePleasure)))
            .andExpect(status().isBadRequest());

        // Validate the GamePleasure in the database
        List<GamePleasure> gamePleasureList = gamePleasureRepository.findAll();
        assertThat(gamePleasureList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllGamePleasures() throws Exception {
        // Initialize the database
        gamePleasureRepository.saveAndFlush(gamePleasure);

        // Get all the gamePleasureList
        restGamePleasureMockMvc.perform(get("/api/game-pleasures?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(gamePleasure.getId().intValue())))
            .andExpect(jsonPath("$.[*].rating").value(hasItem(DEFAULT_RATING.intValue())))
            .andExpect(jsonPath("$.[*].review").value(hasItem(DEFAULT_REVIEW.toString())));
    }

    @Test
    @Transactional
    public void getGamePleasure() throws Exception {
        // Initialize the database
        gamePleasureRepository.saveAndFlush(gamePleasure);

        // Get the gamePleasure
        restGamePleasureMockMvc.perform(get("/api/game-pleasures/{id}", gamePleasure.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(gamePleasure.getId().intValue()))
            .andExpect(jsonPath("$.rating").value(DEFAULT_RATING.intValue()))
            .andExpect(jsonPath("$.review").value(DEFAULT_REVIEW.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingGamePleasure() throws Exception {
        // Get the gamePleasure
        restGamePleasureMockMvc.perform(get("/api/game-pleasures/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGamePleasure() throws Exception {
        // Initialize the database
        gamePleasureService.save(gamePleasure);

        int databaseSizeBeforeUpdate = gamePleasureRepository.findAll().size();

        // Update the gamePleasure
        GamePleasure updatedGamePleasure = gamePleasureRepository.findOne(gamePleasure.getId());
        // Disconnect from session so that the updates on updatedGamePleasure are not directly saved in db
        em.detach(updatedGamePleasure);
        updatedGamePleasure
            .rating(UPDATED_RATING)
            .review(UPDATED_REVIEW);

        restGamePleasureMockMvc.perform(put("/api/game-pleasures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedGamePleasure)))
            .andExpect(status().isOk());

        // Validate the GamePleasure in the database
        List<GamePleasure> gamePleasureList = gamePleasureRepository.findAll();
        assertThat(gamePleasureList).hasSize(databaseSizeBeforeUpdate);
        GamePleasure testGamePleasure = gamePleasureList.get(gamePleasureList.size() - 1);
        assertThat(testGamePleasure.getRating()).isEqualTo(UPDATED_RATING);
        assertThat(testGamePleasure.getReview()).isEqualTo(UPDATED_REVIEW);

        // Validate the GamePleasure in Elasticsearch
        GamePleasure gamePleasureEs = gamePleasureSearchRepository.findOne(testGamePleasure.getId());
        assertThat(gamePleasureEs).isEqualToIgnoringGivenFields(testGamePleasure);
    }

    @Test
    @Transactional
    public void updateNonExistingGamePleasure() throws Exception {
        int databaseSizeBeforeUpdate = gamePleasureRepository.findAll().size();

        // Create the GamePleasure

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restGamePleasureMockMvc.perform(put("/api/game-pleasures")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(gamePleasure)))
            .andExpect(status().isCreated());

        // Validate the GamePleasure in the database
        List<GamePleasure> gamePleasureList = gamePleasureRepository.findAll();
        assertThat(gamePleasureList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteGamePleasure() throws Exception {
        // Initialize the database
        gamePleasureService.save(gamePleasure);

        int databaseSizeBeforeDelete = gamePleasureRepository.findAll().size();

        // Get the gamePleasure
        restGamePleasureMockMvc.perform(delete("/api/game-pleasures/{id}", gamePleasure.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean gamePleasureExistsInEs = gamePleasureSearchRepository.exists(gamePleasure.getId());
        assertThat(gamePleasureExistsInEs).isFalse();

        // Validate the database is empty
        List<GamePleasure> gamePleasureList = gamePleasureRepository.findAll();
        assertThat(gamePleasureList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchGamePleasure() throws Exception {
        // Initialize the database
        gamePleasureService.save(gamePleasure);

        // Search the gamePleasure
        restGamePleasureMockMvc.perform(get("/api/_search/game-pleasures?query=id:" + gamePleasure.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(gamePleasure.getId().intValue())))
            .andExpect(jsonPath("$.[*].rating").value(hasItem(DEFAULT_RATING.intValue())))
            .andExpect(jsonPath("$.[*].review").value(hasItem(DEFAULT_REVIEW.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(GamePleasure.class);
        GamePleasure gamePleasure1 = new GamePleasure();
        gamePleasure1.setId(1L);
        GamePleasure gamePleasure2 = new GamePleasure();
        gamePleasure2.setId(gamePleasure1.getId());
        assertThat(gamePleasure1).isEqualTo(gamePleasure2);
        gamePleasure2.setId(2L);
        assertThat(gamePleasure1).isNotEqualTo(gamePleasure2);
        gamePleasure1.setId(null);
        assertThat(gamePleasure1).isNotEqualTo(gamePleasure2);
    }
}
