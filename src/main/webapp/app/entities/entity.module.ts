import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { PsClubManagerClubModule } from './club/club.module';
import { PsClubManagerZoneModule } from './zone/zone.module';
import { PsClubManagerClientModule } from './client/client.module';
import { PsClubManagerClientLevelModule } from './client-level/client-level.module';
import { PsClubManagerPricePackageModule } from './price-package/price-package.module';
import { PsClubManagerJoySchedulerModule } from './scheduler/scheduler.module';
import { PsClubManagerTariffModule } from './tariff/tariff.module';
import { PsClubManagerDayRegimeModule } from './day-regime/day-regime.module';
import { PsClubManagerSessionModule } from './session/session.module';
import { PsClubManagerExtraJoystickModule } from './extra-joystick/extra-joystick.module';
import { PsClubManagerZoneDictionaryModule } from './zone-dictionary/zone-dictionary.module';
import { PsClubManagerConsoleDictionaryModule } from './console-dictionary/console-dictionary.module';
import { PsClubManagerJoystickDictionaryModule } from './joystick-dictionary/joystick-dictionary.module';
import { PsClubManagerGameDictionaryModule } from './game-dictionary/game-dictionary.module';
import { PsClubManagerClubJoystickModule } from './club-joystick/club-joystick.module';
import { PsClubManagerAttachmentFileModule } from './attachment-file/attachment-file.module';
import { PsClubManagerGamePleasureModule } from './game-pleasure/game-pleasure.module';
import { PsClubManagerCityDictionaryModule } from './city-dictionary/city-dictionary.module';
import { PsClubManagerLocationModule } from './location/location.module';
import { PsClubManagerJoySessionLifeModule } from './joy-session-life/joy-session-life.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        PsClubManagerClubModule,
        PsClubManagerZoneModule,
        PsClubManagerClientModule,
        PsClubManagerClientLevelModule,
        PsClubManagerPricePackageModule,
        PsClubManagerJoySchedulerModule,
        PsClubManagerTariffModule,
        PsClubManagerDayRegimeModule,
        PsClubManagerSessionModule,
        PsClubManagerExtraJoystickModule,
        PsClubManagerZoneDictionaryModule,
        PsClubManagerConsoleDictionaryModule,
        PsClubManagerJoystickDictionaryModule,
        PsClubManagerGameDictionaryModule,
        PsClubManagerClubJoystickModule,
        PsClubManagerAttachmentFileModule,
        PsClubManagerGamePleasureModule,
        PsClubManagerCityDictionaryModule,
        PsClubManagerLocationModule,
        PsClubManagerJoySessionLifeModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PsClubManagerEntityModule {}
