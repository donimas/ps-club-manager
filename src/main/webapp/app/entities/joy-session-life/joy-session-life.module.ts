import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PsClubManagerSharedModule } from '../../shared';
import {
    JoySessionLifeService,
    JoySessionLifePopupService,
    JoySessionLifeComponent,
    JoySessionLifeDetailComponent,
    JoySessionLifeDialogComponent,
    JoySessionLifePopupComponent,
    JoySessionLifeDeletePopupComponent,
    JoySessionLifeDeleteDialogComponent,
    joySessionLifeRoute,
    joySessionLifePopupRoute,
} from './';

const ENTITY_STATES = [
    ...joySessionLifeRoute,
    ...joySessionLifePopupRoute,
];

@NgModule({
    imports: [
        PsClubManagerSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        JoySessionLifeComponent,
        JoySessionLifeDetailComponent,
        JoySessionLifeDialogComponent,
        JoySessionLifeDeleteDialogComponent,
        JoySessionLifePopupComponent,
        JoySessionLifeDeletePopupComponent,
    ],
    entryComponents: [
        JoySessionLifeComponent,
        JoySessionLifeDialogComponent,
        JoySessionLifePopupComponent,
        JoySessionLifeDeleteDialogComponent,
        JoySessionLifeDeletePopupComponent,
    ],
    providers: [
        JoySessionLifeService,
        JoySessionLifePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PsClubManagerJoySessionLifeModule {}
