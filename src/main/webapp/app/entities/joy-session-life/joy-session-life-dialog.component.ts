import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { JoySessionLife } from './joy-session-life.model';
import { JoySessionLifePopupService } from './joy-session-life-popup.service';
import { JoySessionLifeService } from './joy-session-life.service';
import { PricePackage, PricePackageService } from '../price-package';

@Component({
    selector: 'jhi-joy-session-life-dialog',
    templateUrl: './joy-session-life-dialog.component.html'
})
export class JoySessionLifeDialogComponent implements OnInit {

    joySessionLife: JoySessionLife;
    isSaving: boolean;

    pricepackages: PricePackage[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private joySessionLifeService: JoySessionLifeService,
        private pricePackageService: PricePackageService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.pricePackageService.query()
            .subscribe((res: HttpResponse<PricePackage[]>) => { this.pricepackages = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.joySessionLife.id !== undefined) {
            this.subscribeToSaveResponse(
                this.joySessionLifeService.update(this.joySessionLife));
        } else {
            this.subscribeToSaveResponse(
                this.joySessionLifeService.create(this.joySessionLife));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<JoySessionLife>>) {
        result.subscribe((res: HttpResponse<JoySessionLife>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: JoySessionLife) {
        this.eventManager.broadcast({ name: 'joySessionLifeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackPricePackageById(index: number, item: PricePackage) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-joy-session-life-popup',
    template: ''
})
export class JoySessionLifePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private joySessionLifePopupService: JoySessionLifePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.joySessionLifePopupService
                    .open(JoySessionLifeDialogComponent as Component, params['id']);
            } else {
                this.joySessionLifePopupService
                    .open(JoySessionLifeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
