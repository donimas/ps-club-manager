import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { JoySessionLife } from './joy-session-life.model';
import { JoySessionLifeService } from './joy-session-life.service';

@Injectable()
export class JoySessionLifePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private joySessionLifeService: JoySessionLifeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.joySessionLifeService.find(id)
                    .subscribe((joySessionLifeResponse: HttpResponse<JoySessionLife>) => {
                        const joySessionLife: JoySessionLife = joySessionLifeResponse.body;
                        joySessionLife.startedAt = this.datePipe
                            .transform(joySessionLife.startedAt, 'yyyy-MM-ddTHH:mm:ss');
                        joySessionLife.finishedAt = this.datePipe
                            .transform(joySessionLife.finishedAt, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.joySessionLifeModalRef(component, joySessionLife);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.joySessionLifeModalRef(component, new JoySessionLife());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    joySessionLifeModalRef(component: Component, joySessionLife: JoySessionLife): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.joySessionLife = joySessionLife;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
