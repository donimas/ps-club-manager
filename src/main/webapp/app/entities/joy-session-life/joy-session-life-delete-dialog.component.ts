import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { JoySessionLife } from './joy-session-life.model';
import { JoySessionLifePopupService } from './joy-session-life-popup.service';
import { JoySessionLifeService } from './joy-session-life.service';

@Component({
    selector: 'jhi-joy-session-life-delete-dialog',
    templateUrl: './joy-session-life-delete-dialog.component.html'
})
export class JoySessionLifeDeleteDialogComponent {

    joySessionLife: JoySessionLife;

    constructor(
        private joySessionLifeService: JoySessionLifeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.joySessionLifeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'joySessionLifeListModification',
                content: 'Deleted an joySessionLife'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-joy-session-life-delete-popup',
    template: ''
})
export class JoySessionLifeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private joySessionLifePopupService: JoySessionLifePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.joySessionLifePopupService
                .open(JoySessionLifeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
