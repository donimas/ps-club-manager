import { BaseEntity } from './../../shared';

export class JoySessionLife implements BaseEntity {
    constructor(
        public id?: number,
        public startedAt?: any,
        public finishedAt?: any,
        public joyHours?: number,
        public joyMinutes?: number,
        public totalPrice?: number,
        public pricePackage?: BaseEntity,
    ) {
    }
}
