import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { JoySessionLife } from './joy-session-life.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<JoySessionLife>;

@Injectable()
export class JoySessionLifeService {

    private resourceUrl =  SERVER_API_URL + 'api/joy-session-lives';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/joy-session-lives';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(joySessionLife: JoySessionLife): Observable<EntityResponseType> {
        const copy = this.convert(joySessionLife);
        return this.http.post<JoySessionLife>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(joySessionLife: JoySessionLife): Observable<EntityResponseType> {
        const copy = this.convert(joySessionLife);
        return this.http.put<JoySessionLife>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<JoySessionLife>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<JoySessionLife[]>> {
        const options = createRequestOption(req);
        return this.http.get<JoySessionLife[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<JoySessionLife[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<JoySessionLife[]>> {
        const options = createRequestOption(req);
        return this.http.get<JoySessionLife[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<JoySessionLife[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: JoySessionLife = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<JoySessionLife[]>): HttpResponse<JoySessionLife[]> {
        const jsonResponse: JoySessionLife[] = res.body;
        const body: JoySessionLife[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to JoySessionLife.
     */
    private convertItemFromServer(joySessionLife: JoySessionLife): JoySessionLife {
        const copy: JoySessionLife = Object.assign({}, joySessionLife);
        copy.startedAt = this.dateUtils
            .convertDateTimeFromServer(joySessionLife.startedAt);
        copy.finishedAt = this.dateUtils
            .convertDateTimeFromServer(joySessionLife.finishedAt);
        return copy;
    }

    /**
     * Convert a JoySessionLife to a JSON which can be sent to the server.
     */
    private convert(joySessionLife: JoySessionLife): JoySessionLife {
        const copy: JoySessionLife = Object.assign({}, joySessionLife);

        copy.startedAt = this.dateUtils.toDate(joySessionLife.startedAt);

        copy.finishedAt = this.dateUtils.toDate(joySessionLife.finishedAt);
        return copy;
    }
}
