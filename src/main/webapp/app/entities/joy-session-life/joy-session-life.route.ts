import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JoySessionLifeComponent } from './joy-session-life.component';
import { JoySessionLifeDetailComponent } from './joy-session-life-detail.component';
import { JoySessionLifePopupComponent } from './joy-session-life-dialog.component';
import { JoySessionLifeDeletePopupComponent } from './joy-session-life-delete-dialog.component';

export const joySessionLifeRoute: Routes = [
    {
        path: 'joy-session-life',
        component: JoySessionLifeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.joySessionLife.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'joy-session-life/:id',
        component: JoySessionLifeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.joySessionLife.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const joySessionLifePopupRoute: Routes = [
    {
        path: 'joy-session-life-new',
        component: JoySessionLifePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.joySessionLife.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'joy-session-life/:id/edit',
        component: JoySessionLifePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.joySessionLife.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'joy-session-life/:id/delete',
        component: JoySessionLifeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.joySessionLife.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
