import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { JoySessionLife } from './joy-session-life.model';
import { JoySessionLifeService } from './joy-session-life.service';

@Component({
    selector: 'jhi-joy-session-life-detail',
    templateUrl: './joy-session-life-detail.component.html'
})
export class JoySessionLifeDetailComponent implements OnInit, OnDestroy {

    joySessionLife: JoySessionLife;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private joySessionLifeService: JoySessionLifeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInJoySessionLives();
    }

    load(id) {
        this.joySessionLifeService.find(id)
            .subscribe((joySessionLifeResponse: HttpResponse<JoySessionLife>) => {
                this.joySessionLife = joySessionLifeResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInJoySessionLives() {
        this.eventSubscriber = this.eventManager.subscribe(
            'joySessionLifeListModification',
            (response) => this.load(this.joySessionLife.id)
        );
    }
}
