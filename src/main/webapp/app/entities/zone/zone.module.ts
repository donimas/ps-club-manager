import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PsClubManagerSharedModule } from '../../shared';
import {
    ZoneService,
    ZonePopupService,
    ZoneComponent,
    ZoneDetailComponent,
    ZoneDialogComponent,
    ZonePopupComponent,
    ZoneDeletePopupComponent,
    ZoneDeleteDialogComponent,
    zoneRoute,
    zonePopupRoute,
    ZoneResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...zoneRoute,
    ...zonePopupRoute,
];

@NgModule({
    imports: [
        PsClubManagerSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ZoneComponent,
        ZoneDetailComponent,
        ZoneDialogComponent,
        ZoneDeleteDialogComponent,
        ZonePopupComponent,
        ZoneDeletePopupComponent,
    ],
    entryComponents: [
        ZoneComponent,
        ZoneDialogComponent,
        ZonePopupComponent,
        ZoneDeleteDialogComponent,
        ZoneDeletePopupComponent,
    ],
    providers: [
        ZoneService,
        ZonePopupService,
        ZoneResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PsClubManagerZoneModule {}
