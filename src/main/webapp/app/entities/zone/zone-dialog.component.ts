import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Zone } from './zone.model';
import { ZonePopupService } from './zone-popup.service';
import { ZoneService } from './zone.service';
import { ConsoleDictionary, ConsoleDictionaryService } from '../console-dictionary';
import { ZoneDictionary, ZoneDictionaryService } from '../zone-dictionary';
import { Club, ClubService } from '../club';

@Component({
    selector: 'jhi-zone-dialog',
    templateUrl: './zone-dialog.component.html'
})
export class ZoneDialogComponent implements OnInit {

    zone: Zone;
    isSaving: boolean;

    consoledictionaries: ConsoleDictionary[];

    zonedictionaries: ZoneDictionary[];

    club: Club;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private zoneService: ZoneService,
        private consoleDictionaryService: ConsoleDictionaryService,
        private zoneDictionaryService: ZoneDictionaryService,
        private clubService: ClubService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;

        console.log('on init');
        console.log(this.club);

        this.consoleDictionaryService.query()
            .subscribe((res: HttpResponse<ConsoleDictionary[]>) => { this.consoledictionaries = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.zoneDictionaryService.query()
            .subscribe((res: HttpResponse<ZoneDictionary[]>) => { this.zonedictionaries = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        this.zone.club = new Club();
        this.zone.club.id = this.club.id;
        console.log(this.zone);
        if (this.zone.id !== undefined) {
            this.subscribeToSaveResponse(
                this.zoneService.update(this.zone));
        } else {
            this.subscribeToSaveResponse(
                this.zoneService.create(this.zone));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Zone>>) {
        result.subscribe((res: HttpResponse<Zone>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Zone) {
        this.eventManager.broadcast({ name: 'zoneListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackConsoleDictionaryById(index: number, item: ConsoleDictionary) {
        return item.id;
    }

    trackZoneDictionaryById(index: number, item: ZoneDictionary) {
        return item.id;
    }

    trackClubById(index: number, item: Club) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-zone-popup',
    template: ''
})
export class ZonePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private zonePopupService: ZonePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.zonePopupService
                    .open(ZoneDialogComponent as Component, params['id']);
            } else {
                this.zonePopupService
                    .open(ZoneDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
