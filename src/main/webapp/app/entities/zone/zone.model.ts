import { BaseEntity } from './../../shared';
import {ZoneDictionary} from '../zone-dictionary/zone-dictionary.model';
import {Session} from '../session/session.model';

export class Zone implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public seatsAmount?: number,
        public tvDiagonal?: number,
        public tvResolution?: number,
        public flagDeleted?: boolean,
        public console?: BaseEntity,
        public type?: ZoneDictionary,
        public club?: BaseEntity,
        public sessions?: Session[],
    ) {
        this.flagDeleted = false;
    }
}
