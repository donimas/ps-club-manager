import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PsClubManagerSharedModule } from '../../shared';
import {
    JoySchedulerService,
    ScheduleSessionDialogModalService,
    JoySchedulerComponent,
    joySchedulerRoute,
} from './';

const ENTITY_STATES = [
    ...joySchedulerRoute,
];

@NgModule({
    imports: [
        PsClubManagerSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        JoySchedulerComponent,
    ],
    entryComponents: [
        JoySchedulerComponent,
    ],
    providers: [
        JoySchedulerService,
        ScheduleSessionDialogModalService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PsClubManagerJoySchedulerModule {}
