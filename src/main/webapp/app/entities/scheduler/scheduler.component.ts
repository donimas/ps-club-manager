import { Component, OnInit, OnDestroy } from '@angular/core';
import {JoySchedulerService} from './scheduler.service';
import {Club} from "../club/club.model";
import {HttpResponse} from '@angular/common/http';

@Component({
    selector: 'joy-scheduler',
    templateUrl: './scheduler.component.html'
})
export class JoySchedulerComponent implements OnInit, OnDestroy {

    club: Club;

    constructor(
        private joySchedulerService: JoySchedulerService,
    ) {
    }

    loadAll() {
        this.joySchedulerService.getJoyScheduler().subscribe((res: HttpResponse<Club>) => {
            this.club = res.body;
            console.log('found club scheduler -> ', this.club);
        });
    }

    ngOnInit() {
        this.loadAll();
    }

    ngOnDestroy() {
    }

    private onError(error) {
    }
}
