import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import {JoySchedulerComponent} from './scheduler.component';

export const joySchedulerRoute: Routes = [
    {
        path: 'joy-scheduler',
        component: JoySchedulerComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.pricePackage.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
