import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {Zone} from "../../zone/zone.model";
import {SessionDialogComponent} from '../../session/session-dialog.component';
import {Session} from '../../session/session.model';
import {Club} from '../../club/club.model';

@Injectable()
export class ScheduleSessionDialogModalService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
    ) {}

    open(start: any, end: any, club?: Club, zone?: Zone): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;
        const modalRef = this.modalService.open(SessionDialogComponent, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.club = club;
        if(!zone) {
            zone = new Zone();
        }
        modalRef.componentInstance.startArg = start.value;
        modalRef.componentInstance.endArg = end.value;
        modalRef.componentInstance.zone = zone;
        modalRef.componentInstance.zones = club.zoneDtos;
        modalRef.componentInstance.session = new Session();
        modalRef.componentInstance.pricepackages = club.pricePackages;
        modalRef.result.then((result) => {
            this.isOpen = false;
        }, (reason) => {
            this.isOpen = false;
        });
        return modalRef;
    }
}
