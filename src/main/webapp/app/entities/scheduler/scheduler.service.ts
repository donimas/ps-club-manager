import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Club } from '../club/club.model';

export type EntityResponseType = HttpResponse<Club>;

@Injectable()
export class JoySchedulerService {

    private resourceUrl =  SERVER_API_URL + 'api/joy-scheduler';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    getJoyScheduler(): Observable<EntityResponseType> {
        return this.http.get<Club>(this.resourceUrl, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Club = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Club[]>): HttpResponse<Club[]> {
        const jsonResponse: Club[] = res.body;
        const body: Club[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Club.
     */
    private convertItemFromServer(club: Club): Club {
        const copy: Club = Object.assign({}, club);
        return copy;
    }

    /**
     * Convert a Club to a JSON which can be sent to the server.
     */
    private convert(club: Club): Club {
        const copy: Club = Object.assign({}, club);
        return copy;
    }
}
