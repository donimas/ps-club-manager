import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PsClubManagerSharedModule } from '../../shared';
import {
    DayRegimeService,
    DayRegimePopupService,
    DayRegimeComponent,
    DayRegimeDetailComponent,
    DayRegimeDialogComponent,
    DayRegimePopupComponent,
    DayRegimeDeletePopupComponent,
    DayRegimeDeleteDialogComponent,
    dayRegimeRoute,
    dayRegimePopupRoute,
} from './';

const ENTITY_STATES = [
    ...dayRegimeRoute,
    ...dayRegimePopupRoute,
];

@NgModule({
    imports: [
        PsClubManagerSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        DayRegimeComponent,
        DayRegimeDetailComponent,
        DayRegimeDialogComponent,
        DayRegimeDeleteDialogComponent,
        DayRegimePopupComponent,
        DayRegimeDeletePopupComponent,
    ],
    entryComponents: [
        DayRegimeComponent,
        DayRegimeDialogComponent,
        DayRegimePopupComponent,
        DayRegimeDeleteDialogComponent,
        DayRegimeDeletePopupComponent,
    ],
    providers: [
        DayRegimeService,
        DayRegimePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PsClubManagerDayRegimeModule {}
