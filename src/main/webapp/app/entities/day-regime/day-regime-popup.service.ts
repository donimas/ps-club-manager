import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { DayRegime } from './day-regime.model';
import { DayRegimeService } from './day-regime.service';

@Injectable()
export class DayRegimePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private dayRegimeService: DayRegimeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.dayRegimeService.find(id)
                    .subscribe((dayRegimeResponse: HttpResponse<DayRegime>) => {
                        const dayRegime: DayRegime = dayRegimeResponse.body;
                        dayRegime.openedAt = this.datePipe
                            .transform(dayRegime.openedAt, 'yyyy-MM-ddTHH:mm:ss');
                        dayRegime.closedAt = this.datePipe
                            .transform(dayRegime.closedAt, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.dayRegimeModalRef(component, dayRegime);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.dayRegimeModalRef(component, new DayRegime());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    dayRegimeModalRef(component: Component, dayRegime: DayRegime): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.dayRegime = dayRegime;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
