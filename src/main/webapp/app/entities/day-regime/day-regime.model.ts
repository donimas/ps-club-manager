import { BaseEntity } from './../../shared';

export class DayRegime implements BaseEntity {
    constructor(
        public id?: number,
        public daySeq?: number,
        public openedAt?: any,
        public closedAt?: any,
        public flagAllDayLong?: boolean,
        // transient
        public flagTillLastClient?: boolean,
        public flagTime?: boolean,
        public flagRestDay?: boolean,
        public club?: BaseEntity,

        public oHour?: number,
        public oMinute?: number,
        public cHour?: number,
        public cMinute?: number,

        public openedTime?: string,
        public closedTime?: string,
    ) {
        this.flagAllDayLong = false;
        this.flagTillLastClient = false;
        this.flagTime = false;
        this.flagRestDay = false;
    }
}
