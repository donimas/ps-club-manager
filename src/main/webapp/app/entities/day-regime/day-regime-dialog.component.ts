import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { DayRegime } from './day-regime.model';
import { DayRegimePopupService } from './day-regime-popup.service';
import { DayRegimeService } from './day-regime.service';
import { Club, ClubService } from '../club';

@Component({
    selector: 'jhi-day-regime-dialog',
    templateUrl: './day-regime-dialog.component.html'
})
export class DayRegimeDialogComponent implements OnInit {

    dayRegime: DayRegime;
    isSaving: boolean;

    clubs: Club[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private dayRegimeService: DayRegimeService,
        private clubService: ClubService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.clubService.query()
            .subscribe((res: HttpResponse<Club[]>) => { this.clubs = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        console.log(this.dayRegime);
        console.log(new Date());
        const today = new Date();
        const day = today.getDay();
        const month = today.getMonth() + 1;
        const year = today.getFullYear();
        // 2018-04-16T01:00
        const instant = `${year}-${month}-${day}T01:00`;
        console.log(instant);
        /*this.isSaving = true;
        if (this.dayRegime.id !== undefined) {
            this.subscribeToSaveResponse(
                this.dayRegimeService.update(this.dayRegime));
        } else {
            this.subscribeToSaveResponse(
                this.dayRegimeService.create(this.dayRegime));
        }*/
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<DayRegime>>) {
        result.subscribe((res: HttpResponse<DayRegime>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: DayRegime) {
        this.eventManager.broadcast({ name: 'dayRegimeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClubById(index: number, item: Club) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-day-regime-popup',
    template: ''
})
export class DayRegimePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dayRegimePopupService: DayRegimePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.dayRegimePopupService
                    .open(DayRegimeDialogComponent as Component, params['id']);
            } else {
                this.dayRegimePopupService
                    .open(DayRegimeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
