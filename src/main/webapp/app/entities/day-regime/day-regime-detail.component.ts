import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { DayRegime } from './day-regime.model';
import { DayRegimeService } from './day-regime.service';

@Component({
    selector: 'jhi-day-regime-detail',
    templateUrl: './day-regime-detail.component.html'
})
export class DayRegimeDetailComponent implements OnInit, OnDestroy {

    dayRegime: DayRegime;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dayRegimeService: DayRegimeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDayRegimes();
    }

    load(id) {
        this.dayRegimeService.find(id)
            .subscribe((dayRegimeResponse: HttpResponse<DayRegime>) => {
                this.dayRegime = dayRegimeResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDayRegimes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'dayRegimeListModification',
            (response) => this.load(this.dayRegime.id)
        );
    }
}
