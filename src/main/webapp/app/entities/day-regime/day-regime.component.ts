import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { DayRegime } from './day-regime.model';
import { DayRegimeService } from './day-regime.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-day-regime',
    templateUrl: './day-regime.component.html'
})
export class DayRegimeComponent implements OnInit, OnDestroy {
dayRegimes: DayRegime[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private dayRegimeService: DayRegimeService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal
    ) {
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.dayRegimeService.search({
                query: this.currentSearch,
                }).subscribe(
                    (res: HttpResponse<DayRegime[]>) => this.dayRegimes = res.body,
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
       }
        this.dayRegimeService.query().subscribe(
            (res: HttpResponse<DayRegime[]>) => {
                this.dayRegimes = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInDayRegimes();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: DayRegime) {
        return item.id;
    }
    registerChangeInDayRegimes() {
        this.eventSubscriber = this.eventManager.subscribe('dayRegimeListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
