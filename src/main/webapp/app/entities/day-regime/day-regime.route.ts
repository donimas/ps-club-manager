import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { DayRegimeComponent } from './day-regime.component';
import { DayRegimeDetailComponent } from './day-regime-detail.component';
import { DayRegimePopupComponent } from './day-regime-dialog.component';
import { DayRegimeDeletePopupComponent } from './day-regime-delete-dialog.component';

export const dayRegimeRoute: Routes = [
    {
        path: 'day-regime',
        component: DayRegimeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.dayRegime.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'day-regime/:id',
        component: DayRegimeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.dayRegime.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const dayRegimePopupRoute: Routes = [
    {
        path: 'day-regime-new',
        component: DayRegimePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.dayRegime.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'day-regime/:id/edit',
        component: DayRegimePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.dayRegime.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'day-regime/:id/delete',
        component: DayRegimeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.dayRegime.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
