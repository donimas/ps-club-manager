import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DayRegime } from './day-regime.model';
import { DayRegimePopupService } from './day-regime-popup.service';
import { DayRegimeService } from './day-regime.service';

@Component({
    selector: 'jhi-day-regime-delete-dialog',
    templateUrl: './day-regime-delete-dialog.component.html'
})
export class DayRegimeDeleteDialogComponent {

    dayRegime: DayRegime;

    constructor(
        private dayRegimeService: DayRegimeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.dayRegimeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'dayRegimeListModification',
                content: 'Deleted an dayRegime'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-day-regime-delete-popup',
    template: ''
})
export class DayRegimeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dayRegimePopupService: DayRegimePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.dayRegimePopupService
                .open(DayRegimeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
