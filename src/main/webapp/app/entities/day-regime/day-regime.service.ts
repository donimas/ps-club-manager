import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { DayRegime } from './day-regime.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<DayRegime>;

@Injectable()
export class DayRegimeService {

    private resourceUrl =  SERVER_API_URL + 'api/day-regimes';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/day-regimes';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(dayRegime: DayRegime): Observable<EntityResponseType> {
        const copy = this.convert(dayRegime);
        return this.http.post<DayRegime>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(dayRegime: DayRegime): Observable<EntityResponseType> {
        const copy = this.convert(dayRegime);
        return this.http.put<DayRegime>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<DayRegime>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<DayRegime[]>> {
        const options = createRequestOption(req);
        return this.http.get<DayRegime[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<DayRegime[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<DayRegime[]>> {
        const options = createRequestOption(req);
        return this.http.get<DayRegime[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<DayRegime[]>) => this.convertArrayResponse(res));
    }

    findAllOfClub(id: number): Observable<HttpResponse<DayRegime[]>> {
        return this.http.get<DayRegime[]>(`${this.resourceUrl}/by-club/${id}`, { observe: 'response' })
            .map((res: HttpResponse<DayRegime[]>) => this.convertArrayResponse(res));
    }

    getRegimeHours(): number[] {
        return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];
    }

    getRegimeMinutes(): number[] {
        return [0, 15, 30, 45, 59];
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: DayRegime = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<DayRegime[]>): HttpResponse<DayRegime[]> {
        const jsonResponse: DayRegime[] = res.body;
        const body: DayRegime[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to DayRegime.
     */
    private convertItemFromServer(dayRegime: DayRegime): DayRegime {
        const copy: DayRegime = Object.assign({}, dayRegime);
        copy.openedAt = this.dateUtils
            .convertDateTimeFromServer(dayRegime.openedAt);
        copy.closedAt = this.dateUtils
            .convertDateTimeFromServer(dayRegime.closedAt);
        return copy;
    }

    /**
     * Convert a DayRegime to a JSON which can be sent to the server.
     */
    private convert(dayRegime: DayRegime): DayRegime {
        const copy: DayRegime = Object.assign({}, dayRegime);

        copy.openedAt = this.dateUtils.toDate(dayRegime.openedAt);

        copy.closedAt = this.dateUtils.toDate(dayRegime.closedAt);
        return copy;
    }
}
