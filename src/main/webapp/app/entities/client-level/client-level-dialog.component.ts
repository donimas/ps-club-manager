import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ClientLevel } from './client-level.model';
import { ClientLevelPopupService } from './client-level-popup.service';
import { ClientLevelService } from './client-level.service';
import {Club} from '../club/club.model';

@Component({
    selector: 'jhi-client-level-dialog',
    templateUrl: './client-level-dialog.component.html'
})
export class ClientLevelDialogComponent implements OnInit {

    clientLevel: ClientLevel;
    isSaving: boolean;

    club: Club;

    constructor(
        public activeModal: NgbActiveModal,
        private clientLevelService: ClientLevelService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        this.clientLevel.club = new Club();
        this.clientLevel.club.id = this.club.id;
        console.log(this.clientLevel);

        if (this.clientLevel.id !== undefined) {
            this.subscribeToSaveResponse(
                this.clientLevelService.update(this.clientLevel));
        } else {
            this.subscribeToSaveResponse(
                this.clientLevelService.create(this.clientLevel));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ClientLevel>>) {
        result.subscribe((res: HttpResponse<ClientLevel>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ClientLevel) {
        this.eventManager.broadcast({ name: 'clientLevelListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-client-level-popup',
    template: ''
})
export class ClientLevelPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientLevelPopupService: ClientLevelPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.clientLevelPopupService
                    .open(ClientLevelDialogComponent as Component, params['id']);
            } else {
                this.clientLevelPopupService
                    .open(ClientLevelDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
