import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PsClubManagerSharedModule } from '../../shared';
import {
    ClientLevelService,
    ClientLevelPopupService,
    ClientLevelComponent,
    ClientLevelDetailComponent,
    ClientLevelDialogComponent,
    ClientLevelPopupComponent,
    ClientLevelDeletePopupComponent,
    ClientLevelDeleteDialogComponent,
    clientLevelRoute,
    clientLevelPopupRoute,
} from './';

const ENTITY_STATES = [
    ...clientLevelRoute,
    ...clientLevelPopupRoute,
];

@NgModule({
    imports: [
        PsClubManagerSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ClientLevelComponent,
        ClientLevelDetailComponent,
        ClientLevelDialogComponent,
        ClientLevelDeleteDialogComponent,
        ClientLevelPopupComponent,
        ClientLevelDeletePopupComponent,
    ],
    entryComponents: [
        ClientLevelComponent,
        ClientLevelDialogComponent,
        ClientLevelPopupComponent,
        ClientLevelDeleteDialogComponent,
        ClientLevelDeletePopupComponent,
    ],
    providers: [
        ClientLevelService,
        ClientLevelPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PsClubManagerClientLevelModule {}
