import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { ClientLevelComponent } from './client-level.component';
import { ClientLevelDetailComponent } from './client-level-detail.component';
import { ClientLevelPopupComponent } from './client-level-dialog.component';
import { ClientLevelDeletePopupComponent } from './client-level-delete-dialog.component';

export const clientLevelRoute: Routes = [
    {
        path: 'client-level',
        component: ClientLevelComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.clientLevel.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'client-level/:id',
        component: ClientLevelDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.clientLevel.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const clientLevelPopupRoute: Routes = [
    {
        path: 'client-level-new',
        component: ClientLevelPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.clientLevel.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-level/:id/edit',
        component: ClientLevelPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.clientLevel.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-level/:id/delete',
        component: ClientLevelDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.clientLevel.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
