import { BaseEntity } from './../../shared';
import {Club} from '../club/club.model';

export class ClientLevel implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public gamesAmount?: number,
        public discountPercent?: number,
        public flagDeleted?: boolean,
        public club?: Club,
    ) {
        this.flagDeleted = false;
    }
}
