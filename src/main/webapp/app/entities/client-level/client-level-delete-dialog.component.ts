import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ClientLevel } from './client-level.model';
import { ClientLevelPopupService } from './client-level-popup.service';
import { ClientLevelService } from './client-level.service';

@Component({
    selector: 'jhi-client-level-delete-dialog',
    templateUrl: './client-level-delete-dialog.component.html'
})
export class ClientLevelDeleteDialogComponent {

    clientLevel: ClientLevel;

    constructor(
        private clientLevelService: ClientLevelService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.clientLevelService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'clientLevelListModification',
                content: 'Deleted an clientLevel'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-client-level-delete-popup',
    template: ''
})
export class ClientLevelDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientLevelPopupService: ClientLevelPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.clientLevelPopupService
                .open(ClientLevelDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
