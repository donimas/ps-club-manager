import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ClientLevel } from './client-level.model';
import { ClientLevelService } from './client-level.service';

@Component({
    selector: 'jhi-client-level-detail',
    templateUrl: './client-level-detail.component.html'
})
export class ClientLevelDetailComponent implements OnInit, OnDestroy {

    clientLevel: ClientLevel;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private clientLevelService: ClientLevelService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInClientLevels();
    }

    load(id) {
        this.clientLevelService.find(id)
            .subscribe((clientLevelResponse: HttpResponse<ClientLevel>) => {
                this.clientLevel = clientLevelResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClientLevels() {
        this.eventSubscriber = this.eventManager.subscribe(
            'clientLevelListModification',
            (response) => this.load(this.clientLevel.id)
        );
    }
}
