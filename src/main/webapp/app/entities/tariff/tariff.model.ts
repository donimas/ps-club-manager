import { BaseEntity } from './../../shared';

export const enum CurrencyType {
    'KZT',
    'RU',
    'USD',
    'EUR'
}

export class Tariff implements BaseEntity {
    constructor(
        public id?: number,
        public price?: number,
        public currency?: CurrencyType,
        public createdDate?: any,
        public flagDeleted?: boolean,
        public containerClass?: string,
        public containerId?: number,
    ) {
        this.flagDeleted = false;
    }
}
