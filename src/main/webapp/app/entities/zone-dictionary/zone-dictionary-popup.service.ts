import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { ZoneDictionary } from './zone-dictionary.model';
import { ZoneDictionaryService } from './zone-dictionary.service';

@Injectable()
export class ZoneDictionaryPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private zoneDictionaryService: ZoneDictionaryService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.zoneDictionaryService.find(id)
                    .subscribe((zoneDictionaryResponse: HttpResponse<ZoneDictionary>) => {
                        const zoneDictionary: ZoneDictionary = zoneDictionaryResponse.body;
                        this.ngbModalRef = this.zoneDictionaryModalRef(component, zoneDictionary);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.zoneDictionaryModalRef(component, new ZoneDictionary());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    zoneDictionaryModalRef(component: Component, zoneDictionary: ZoneDictionary): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.zoneDictionary = zoneDictionary;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
