import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { ZoneDictionaryComponent } from './zone-dictionary.component';
import { ZoneDictionaryDetailComponent } from './zone-dictionary-detail.component';
import { ZoneDictionaryPopupComponent } from './zone-dictionary-dialog.component';
import { ZoneDictionaryDeletePopupComponent } from './zone-dictionary-delete-dialog.component';

export const zoneDictionaryRoute: Routes = [
    {
        path: 'zone-dictionary',
        component: ZoneDictionaryComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.zoneDictionary.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'zone-dictionary/:id',
        component: ZoneDictionaryDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.zoneDictionary.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const zoneDictionaryPopupRoute: Routes = [
    {
        path: 'zone-dictionary-new',
        component: ZoneDictionaryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.zoneDictionary.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'zone-dictionary/:id/edit',
        component: ZoneDictionaryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.zoneDictionary.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'zone-dictionary/:id/delete',
        component: ZoneDictionaryDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.zoneDictionary.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
