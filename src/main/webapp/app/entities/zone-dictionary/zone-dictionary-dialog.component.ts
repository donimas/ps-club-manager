import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ZoneDictionary } from './zone-dictionary.model';
import { ZoneDictionaryPopupService } from './zone-dictionary-popup.service';
import { ZoneDictionaryService } from './zone-dictionary.service';

@Component({
    selector: 'jhi-zone-dictionary-dialog',
    templateUrl: './zone-dictionary-dialog.component.html'
})
export class ZoneDictionaryDialogComponent implements OnInit {

    zoneDictionary: ZoneDictionary;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private zoneDictionaryService: ZoneDictionaryService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.zoneDictionary.id !== undefined) {
            this.subscribeToSaveResponse(
                this.zoneDictionaryService.update(this.zoneDictionary));
        } else {
            this.subscribeToSaveResponse(
                this.zoneDictionaryService.create(this.zoneDictionary));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ZoneDictionary>>) {
        result.subscribe((res: HttpResponse<ZoneDictionary>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ZoneDictionary) {
        this.eventManager.broadcast({ name: 'zoneDictionaryListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-zone-dictionary-popup',
    template: ''
})
export class ZoneDictionaryPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private zoneDictionaryPopupService: ZoneDictionaryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.zoneDictionaryPopupService
                    .open(ZoneDictionaryDialogComponent as Component, params['id']);
            } else {
                this.zoneDictionaryPopupService
                    .open(ZoneDictionaryDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
