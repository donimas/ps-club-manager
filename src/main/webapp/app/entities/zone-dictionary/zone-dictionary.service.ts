import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ZoneDictionary } from './zone-dictionary.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<ZoneDictionary>;

@Injectable()
export class ZoneDictionaryService {

    private resourceUrl =  SERVER_API_URL + 'api/zone-dictionaries';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/zone-dictionaries';

    constructor(private http: HttpClient) { }

    create(zoneDictionary: ZoneDictionary): Observable<EntityResponseType> {
        const copy = this.convert(zoneDictionary);
        return this.http.post<ZoneDictionary>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(zoneDictionary: ZoneDictionary): Observable<EntityResponseType> {
        const copy = this.convert(zoneDictionary);
        return this.http.put<ZoneDictionary>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ZoneDictionary>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ZoneDictionary[]>> {
        const options = createRequestOption(req);
        return this.http.get<ZoneDictionary[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ZoneDictionary[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<ZoneDictionary[]>> {
        const options = createRequestOption(req);
        return this.http.get<ZoneDictionary[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ZoneDictionary[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ZoneDictionary = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ZoneDictionary[]>): HttpResponse<ZoneDictionary[]> {
        const jsonResponse: ZoneDictionary[] = res.body;
        const body: ZoneDictionary[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ZoneDictionary.
     */
    private convertItemFromServer(zoneDictionary: ZoneDictionary): ZoneDictionary {
        const copy: ZoneDictionary = Object.assign({}, zoneDictionary);
        return copy;
    }

    /**
     * Convert a ZoneDictionary to a JSON which can be sent to the server.
     */
    private convert(zoneDictionary: ZoneDictionary): ZoneDictionary {
        const copy: ZoneDictionary = Object.assign({}, zoneDictionary);
        return copy;
    }
}
