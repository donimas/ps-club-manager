import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ZoneDictionary } from './zone-dictionary.model';
import { ZoneDictionaryService } from './zone-dictionary.service';

@Component({
    selector: 'jhi-zone-dictionary-detail',
    templateUrl: './zone-dictionary-detail.component.html'
})
export class ZoneDictionaryDetailComponent implements OnInit, OnDestroy {

    zoneDictionary: ZoneDictionary;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private zoneDictionaryService: ZoneDictionaryService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInZoneDictionaries();
    }

    load(id) {
        this.zoneDictionaryService.find(id)
            .subscribe((zoneDictionaryResponse: HttpResponse<ZoneDictionary>) => {
                this.zoneDictionary = zoneDictionaryResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInZoneDictionaries() {
        this.eventSubscriber = this.eventManager.subscribe(
            'zoneDictionaryListModification',
            (response) => this.load(this.zoneDictionary.id)
        );
    }
}
