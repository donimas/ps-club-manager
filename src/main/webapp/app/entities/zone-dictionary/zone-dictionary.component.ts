import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ZoneDictionary } from './zone-dictionary.model';
import { ZoneDictionaryService } from './zone-dictionary.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-zone-dictionary',
    templateUrl: './zone-dictionary.component.html'
})
export class ZoneDictionaryComponent implements OnInit, OnDestroy {
zoneDictionaries: ZoneDictionary[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private zoneDictionaryService: ZoneDictionaryService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal
    ) {
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.zoneDictionaryService.search({
                query: this.currentSearch,
                }).subscribe(
                    (res: HttpResponse<ZoneDictionary[]>) => this.zoneDictionaries = res.body,
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
       }
        this.zoneDictionaryService.query().subscribe(
            (res: HttpResponse<ZoneDictionary[]>) => {
                this.zoneDictionaries = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInZoneDictionaries();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ZoneDictionary) {
        return item.id;
    }
    registerChangeInZoneDictionaries() {
        this.eventSubscriber = this.eventManager.subscribe('zoneDictionaryListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
