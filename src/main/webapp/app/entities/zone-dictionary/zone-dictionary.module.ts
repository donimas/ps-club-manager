import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PsClubManagerSharedModule } from '../../shared';
import {
    ZoneDictionaryService,
    ZoneDictionaryPopupService,
    ZoneDictionaryComponent,
    ZoneDictionaryDetailComponent,
    ZoneDictionaryDialogComponent,
    ZoneDictionaryPopupComponent,
    ZoneDictionaryDeletePopupComponent,
    ZoneDictionaryDeleteDialogComponent,
    zoneDictionaryRoute,
    zoneDictionaryPopupRoute,
} from './';

const ENTITY_STATES = [
    ...zoneDictionaryRoute,
    ...zoneDictionaryPopupRoute,
];

@NgModule({
    imports: [
        PsClubManagerSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ZoneDictionaryComponent,
        ZoneDictionaryDetailComponent,
        ZoneDictionaryDialogComponent,
        ZoneDictionaryDeleteDialogComponent,
        ZoneDictionaryPopupComponent,
        ZoneDictionaryDeletePopupComponent,
    ],
    entryComponents: [
        ZoneDictionaryComponent,
        ZoneDictionaryDialogComponent,
        ZoneDictionaryPopupComponent,
        ZoneDictionaryDeleteDialogComponent,
        ZoneDictionaryDeletePopupComponent,
    ],
    providers: [
        ZoneDictionaryService,
        ZoneDictionaryPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PsClubManagerZoneDictionaryModule {}
