import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ZoneDictionary } from './zone-dictionary.model';
import { ZoneDictionaryPopupService } from './zone-dictionary-popup.service';
import { ZoneDictionaryService } from './zone-dictionary.service';

@Component({
    selector: 'jhi-zone-dictionary-delete-dialog',
    templateUrl: './zone-dictionary-delete-dialog.component.html'
})
export class ZoneDictionaryDeleteDialogComponent {

    zoneDictionary: ZoneDictionary;

    constructor(
        private zoneDictionaryService: ZoneDictionaryService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.zoneDictionaryService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'zoneDictionaryListModification',
                content: 'Deleted an zoneDictionary'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-zone-dictionary-delete-popup',
    template: ''
})
export class ZoneDictionaryDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private zoneDictionaryPopupService: ZoneDictionaryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.zoneDictionaryPopupService
                .open(ZoneDictionaryDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
