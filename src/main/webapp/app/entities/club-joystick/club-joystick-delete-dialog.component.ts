import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ClubJoystick } from './club-joystick.model';
import { ClubJoystickPopupService } from './club-joystick-popup.service';
import { ClubJoystickService } from './club-joystick.service';

@Component({
    selector: 'jhi-club-joystick-delete-dialog',
    templateUrl: './club-joystick-delete-dialog.component.html'
})
export class ClubJoystickDeleteDialogComponent {

    clubJoystick: ClubJoystick;

    constructor(
        private clubJoystickService: ClubJoystickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.clubJoystickService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'clubJoystickListModification',
                content: 'Deleted an clubJoystick'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-club-joystick-delete-popup',
    template: ''
})
export class ClubJoystickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clubJoystickPopupService: ClubJoystickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.clubJoystickPopupService
                .open(ClubJoystickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
