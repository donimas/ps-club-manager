import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ClubJoystick } from './club-joystick.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<ClubJoystick>;

@Injectable()
export class ClubJoystickService {

    private resourceUrl =  SERVER_API_URL + 'api/club-joysticks';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/club-joysticks';

    constructor(private http: HttpClient) { }

    create(clubJoystick: ClubJoystick): Observable<EntityResponseType> {
        const copy = this.convert(clubJoystick);
        return this.http.post<ClubJoystick>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(clubJoystick: ClubJoystick): Observable<EntityResponseType> {
        const copy = this.convert(clubJoystick);
        return this.http.put<ClubJoystick>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ClubJoystick>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ClubJoystick[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClubJoystick[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ClubJoystick[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<ClubJoystick[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClubJoystick[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ClubJoystick[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ClubJoystick = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ClubJoystick[]>): HttpResponse<ClubJoystick[]> {
        const jsonResponse: ClubJoystick[] = res.body;
        const body: ClubJoystick[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    findAllByClub(id: number): Observable<HttpResponse<ClubJoystick[]>> {
        return this.http.get<ClubJoystick[]>(`${this.resourceUrl}/by-club/${id}`, { observe: 'response' })
            .map((res: HttpResponse<ClubJoystick[]>) => this.convertArrayResponse(res));
    }

    /**
     * Convert a returned JSON object to ClubJoystick.
     */
    private convertItemFromServer(clubJoystick: ClubJoystick): ClubJoystick {
        const copy: ClubJoystick = Object.assign({}, clubJoystick);
        return copy;
    }

    /**
     * Convert a ClubJoystick to a JSON which can be sent to the server.
     */
    private convert(clubJoystick: ClubJoystick): ClubJoystick {
        const copy: ClubJoystick = Object.assign({}, clubJoystick);
        return copy;
    }
}
