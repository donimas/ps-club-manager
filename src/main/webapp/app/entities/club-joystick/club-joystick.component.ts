import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ClubJoystick } from './club-joystick.model';
import { ClubJoystickService } from './club-joystick.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-club-joystick',
    templateUrl: './club-joystick.component.html'
})
export class ClubJoystickComponent implements OnInit, OnDestroy {
clubJoysticks: ClubJoystick[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private clubJoystickService: ClubJoystickService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal
    ) {
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.clubJoystickService.search({
                query: this.currentSearch,
                }).subscribe(
                    (res: HttpResponse<ClubJoystick[]>) => this.clubJoysticks = res.body,
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
       }
        this.clubJoystickService.query().subscribe(
            (res: HttpResponse<ClubJoystick[]>) => {
                this.clubJoysticks = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInClubJoysticks();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ClubJoystick) {
        return item.id;
    }
    registerChangeInClubJoysticks() {
        this.eventSubscriber = this.eventManager.subscribe('clubJoystickListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
