import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { ClubJoystickComponent } from './club-joystick.component';
import { ClubJoystickDetailComponent } from './club-joystick-detail.component';
import { ClubJoystickPopupComponent } from './club-joystick-dialog.component';
import { ClubJoystickDeletePopupComponent } from './club-joystick-delete-dialog.component';

export const clubJoystickRoute: Routes = [
    {
        path: 'club-joystick',
        component: ClubJoystickComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.clubJoystick.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'club-joystick/:id',
        component: ClubJoystickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.clubJoystick.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const clubJoystickPopupRoute: Routes = [
    {
        path: 'club-joystick-new',
        component: ClubJoystickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.clubJoystick.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'club-joystick/:id/edit',
        component: ClubJoystickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.clubJoystick.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'club-joystick/:id/delete',
        component: ClubJoystickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.clubJoystick.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
