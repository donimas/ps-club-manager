import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ClubJoystick } from './club-joystick.model';
import { ClubJoystickPopupService } from './club-joystick-popup.service';
import { ClubJoystickService } from './club-joystick.service';
import { Club, ClubService } from '../club';
import { JoystickDictionary, JoystickDictionaryService } from '../joystick-dictionary';
import { Tariff, TariffService } from '../tariff';

@Component({
    selector: 'jhi-club-joystick-dialog',
    templateUrl: './club-joystick-dialog.component.html'
})
export class ClubJoystickDialogComponent implements OnInit {

    clubJoystick: ClubJoystick;
    isSaving: boolean;

    clubs: Club[];

    joystickdictionaries: JoystickDictionary[];

    tariffs: Tariff[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private clubJoystickService: ClubJoystickService,
        private clubService: ClubService,
        private joystickDictionaryService: JoystickDictionaryService,
        private tariffService: TariffService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.clubService.query()
            .subscribe((res: HttpResponse<Club[]>) => { this.clubs = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.joystickDictionaryService.query()
            .subscribe((res: HttpResponse<JoystickDictionary[]>) => { this.joystickdictionaries = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.tariffService.query()
            .subscribe((res: HttpResponse<Tariff[]>) => { this.tariffs = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.clubJoystick.id !== undefined) {
            this.subscribeToSaveResponse(
                this.clubJoystickService.update(this.clubJoystick));
        } else {
            this.subscribeToSaveResponse(
                this.clubJoystickService.create(this.clubJoystick));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ClubJoystick>>) {
        result.subscribe((res: HttpResponse<ClubJoystick>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ClubJoystick) {
        this.eventManager.broadcast({ name: 'clubJoystickListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClubById(index: number, item: Club) {
        return item.id;
    }

    trackJoystickDictionaryById(index: number, item: JoystickDictionary) {
        return item.id;
    }

    trackTariffById(index: number, item: Tariff) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-club-joystick-popup',
    template: ''
})
export class ClubJoystickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clubJoystickPopupService: ClubJoystickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.clubJoystickPopupService
                    .open(ClubJoystickDialogComponent as Component, params['id']);
            } else {
                this.clubJoystickPopupService
                    .open(ClubJoystickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
