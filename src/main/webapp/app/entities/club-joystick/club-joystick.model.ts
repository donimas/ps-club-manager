import { BaseEntity } from './../../shared';
import {Tariff} from '../tariff/tariff.model';
import {JoystickDictionary} from '../joystick-dictionary/joystick-dictionary.model';

export class ClubJoystick implements BaseEntity {
    constructor(
        public id?: number,
        public amount?: number,
        public club?: BaseEntity,
        public joystickDictionary?: JoystickDictionary,
        public priceForExtraOnePerHour?: Tariff,
    ) {
        this.priceForExtraOnePerHour = new Tariff();
    }
}
