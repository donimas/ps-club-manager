import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ClubJoystick } from './club-joystick.model';
import { ClubJoystickService } from './club-joystick.service';

@Component({
    selector: 'jhi-club-joystick-detail',
    templateUrl: './club-joystick-detail.component.html'
})
export class ClubJoystickDetailComponent implements OnInit, OnDestroy {

    clubJoystick: ClubJoystick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private clubJoystickService: ClubJoystickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInClubJoysticks();
    }

    load(id) {
        this.clubJoystickService.find(id)
            .subscribe((clubJoystickResponse: HttpResponse<ClubJoystick>) => {
                this.clubJoystick = clubJoystickResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClubJoysticks() {
        this.eventSubscriber = this.eventManager.subscribe(
            'clubJoystickListModification',
            (response) => this.load(this.clubJoystick.id)
        );
    }
}
