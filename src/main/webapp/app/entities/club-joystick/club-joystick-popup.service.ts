import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { ClubJoystick } from './club-joystick.model';
import { ClubJoystickService } from './club-joystick.service';

@Injectable()
export class ClubJoystickPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private clubJoystickService: ClubJoystickService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.clubJoystickService.find(id)
                    .subscribe((clubJoystickResponse: HttpResponse<ClubJoystick>) => {
                        const clubJoystick: ClubJoystick = clubJoystickResponse.body;
                        this.ngbModalRef = this.clubJoystickModalRef(component, clubJoystick);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.clubJoystickModalRef(component, new ClubJoystick());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    clubJoystickModalRef(component: Component, clubJoystick: ClubJoystick): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.clubJoystick = clubJoystick;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
