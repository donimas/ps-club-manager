import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PsClubManagerSharedModule } from '../../shared';
import {
    ClubJoystickService,
    ClubJoystickPopupService,
    ClubJoystickComponent,
    ClubJoystickDetailComponent,
    ClubJoystickDialogComponent,
    ClubJoystickPopupComponent,
    ClubJoystickDeletePopupComponent,
    ClubJoystickDeleteDialogComponent,
    clubJoystickRoute,
    clubJoystickPopupRoute,
} from './';

const ENTITY_STATES = [
    ...clubJoystickRoute,
    ...clubJoystickPopupRoute,
];

@NgModule({
    imports: [
        PsClubManagerSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ClubJoystickComponent,
        ClubJoystickDetailComponent,
        ClubJoystickDialogComponent,
        ClubJoystickDeleteDialogComponent,
        ClubJoystickPopupComponent,
        ClubJoystickDeletePopupComponent,
    ],
    entryComponents: [
        ClubJoystickComponent,
        ClubJoystickDialogComponent,
        ClubJoystickPopupComponent,
        ClubJoystickDeleteDialogComponent,
        ClubJoystickDeletePopupComponent,
    ],
    providers: [
        ClubJoystickService,
        ClubJoystickPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PsClubManagerClubJoystickModule {}
