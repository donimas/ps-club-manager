export * from './game-dictionary.model';
export * from './game-dictionary-popup.service';
export * from './game-dictionary.service';
export * from './game-dictionary-dialog.component';
export * from './game-dictionary-delete-dialog.component';
export * from './game-dictionary-detail.component';
export * from './game-dictionary.component';
export * from './game-dictionary.route';
