import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { GameDictionary } from './game-dictionary.model';
import { GameDictionaryService } from './game-dictionary.service';

@Component({
    selector: 'jhi-game-dictionary-detail',
    templateUrl: './game-dictionary-detail.component.html'
})
export class GameDictionaryDetailComponent implements OnInit, OnDestroy {

    gameDictionary: GameDictionary;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private gameDictionaryService: GameDictionaryService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInGameDictionaries();
    }

    load(id) {
        this.gameDictionaryService.find(id)
            .subscribe((gameDictionaryResponse: HttpResponse<GameDictionary>) => {
                this.gameDictionary = gameDictionaryResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInGameDictionaries() {
        this.eventSubscriber = this.eventManager.subscribe(
            'gameDictionaryListModification',
            (response) => this.load(this.gameDictionary.id)
        );
    }
}
