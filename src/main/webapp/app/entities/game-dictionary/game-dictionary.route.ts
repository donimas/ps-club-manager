import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { GameDictionaryComponent } from './game-dictionary.component';
import { GameDictionaryDetailComponent } from './game-dictionary-detail.component';
import { GameDictionaryPopupComponent } from './game-dictionary-dialog.component';
import { GameDictionaryDeletePopupComponent } from './game-dictionary-delete-dialog.component';

export const gameDictionaryRoute: Routes = [
    {
        path: 'game-dictionary',
        component: GameDictionaryComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.gameDictionary.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'game-dictionary/:id',
        component: GameDictionaryDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.gameDictionary.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const gameDictionaryPopupRoute: Routes = [
    {
        path: 'game-dictionary-new',
        component: GameDictionaryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.gameDictionary.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'game-dictionary/:id/edit',
        component: GameDictionaryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.gameDictionary.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'game-dictionary/:id/delete',
        component: GameDictionaryDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.gameDictionary.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
