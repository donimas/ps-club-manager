import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { GameDictionary } from './game-dictionary.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<GameDictionary>;

@Injectable()
export class GameDictionaryService {

    private resourceUrl =  SERVER_API_URL + 'api/game-dictionaries';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/game-dictionaries';

    constructor(private http: HttpClient) { }

    create(gameDictionary: GameDictionary): Observable<EntityResponseType> {
        const copy = this.convert(gameDictionary);
        return this.http.post<GameDictionary>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(gameDictionary: GameDictionary): Observable<EntityResponseType> {
        const copy = this.convert(gameDictionary);
        return this.http.put<GameDictionary>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<GameDictionary>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<GameDictionary[]>> {
        const options = createRequestOption(req);
        return this.http.get<GameDictionary[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<GameDictionary[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<GameDictionary[]>> {
        const options = createRequestOption(req);
        return this.http.get<GameDictionary[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<GameDictionary[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: GameDictionary = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<GameDictionary[]>): HttpResponse<GameDictionary[]> {
        const jsonResponse: GameDictionary[] = res.body;
        const body: GameDictionary[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to GameDictionary.
     */
    private convertItemFromServer(gameDictionary: GameDictionary): GameDictionary {
        const copy: GameDictionary = Object.assign({}, gameDictionary);
        return copy;
    }

    /**
     * Convert a GameDictionary to a JSON which can be sent to the server.
     */
    private convert(gameDictionary: GameDictionary): GameDictionary {
        const copy: GameDictionary = Object.assign({}, gameDictionary);
        return copy;
    }
}
