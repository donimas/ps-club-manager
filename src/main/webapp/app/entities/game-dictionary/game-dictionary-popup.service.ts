import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { GameDictionary } from './game-dictionary.model';
import { GameDictionaryService } from './game-dictionary.service';

@Injectable()
export class GameDictionaryPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private gameDictionaryService: GameDictionaryService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.gameDictionaryService.find(id)
                    .subscribe((gameDictionaryResponse: HttpResponse<GameDictionary>) => {
                        const gameDictionary: GameDictionary = gameDictionaryResponse.body;
                        this.ngbModalRef = this.gameDictionaryModalRef(component, gameDictionary);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.gameDictionaryModalRef(component, new GameDictionary());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    gameDictionaryModalRef(component: Component, gameDictionary: GameDictionary): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.gameDictionary = gameDictionary;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
