import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { GameDictionary } from './game-dictionary.model';
import { GameDictionaryPopupService } from './game-dictionary-popup.service';
import { GameDictionaryService } from './game-dictionary.service';

@Component({
    selector: 'jhi-game-dictionary-delete-dialog',
    templateUrl: './game-dictionary-delete-dialog.component.html'
})
export class GameDictionaryDeleteDialogComponent {

    gameDictionary: GameDictionary;

    constructor(
        private gameDictionaryService: GameDictionaryService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.gameDictionaryService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'gameDictionaryListModification',
                content: 'Deleted an gameDictionary'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-game-dictionary-delete-popup',
    template: ''
})
export class GameDictionaryDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private gameDictionaryPopupService: GameDictionaryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.gameDictionaryPopupService
                .open(GameDictionaryDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
