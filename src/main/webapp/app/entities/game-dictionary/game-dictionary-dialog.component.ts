import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { GameDictionary } from './game-dictionary.model';
import { GameDictionaryPopupService } from './game-dictionary-popup.service';
import { GameDictionaryService } from './game-dictionary.service';
import { AttachmentFile, AttachmentFileService } from '../attachment-file';

@Component({
    selector: 'jhi-game-dictionary-dialog',
    templateUrl: './game-dictionary-dialog.component.html'
})
export class GameDictionaryDialogComponent implements OnInit {

    gameDictionary: GameDictionary;
    isSaving: boolean;

    attachmentfiles: AttachmentFile[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private gameDictionaryService: GameDictionaryService,
        private attachmentFileService: AttachmentFileService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.attachmentFileService.query()
            .subscribe((res: HttpResponse<AttachmentFile[]>) => { this.attachmentfiles = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.gameDictionary.id !== undefined) {
            this.subscribeToSaveResponse(
                this.gameDictionaryService.update(this.gameDictionary));
        } else {
            this.subscribeToSaveResponse(
                this.gameDictionaryService.create(this.gameDictionary));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<GameDictionary>>) {
        result.subscribe((res: HttpResponse<GameDictionary>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: GameDictionary) {
        this.eventManager.broadcast({ name: 'gameDictionaryListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAttachmentFileById(index: number, item: AttachmentFile) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-game-dictionary-popup',
    template: ''
})
export class GameDictionaryPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private gameDictionaryPopupService: GameDictionaryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.gameDictionaryPopupService
                    .open(GameDictionaryDialogComponent as Component, params['id']);
            } else {
                this.gameDictionaryPopupService
                    .open(GameDictionaryDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
