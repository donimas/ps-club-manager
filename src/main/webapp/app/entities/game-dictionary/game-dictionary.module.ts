import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PsClubManagerSharedModule } from '../../shared';
import {
    GameDictionaryService,
    GameDictionaryPopupService,
    GameDictionaryComponent,
    GameDictionaryDetailComponent,
    GameDictionaryDialogComponent,
    GameDictionaryPopupComponent,
    GameDictionaryDeletePopupComponent,
    GameDictionaryDeleteDialogComponent,
    gameDictionaryRoute,
    gameDictionaryPopupRoute,
} from './';

const ENTITY_STATES = [
    ...gameDictionaryRoute,
    ...gameDictionaryPopupRoute,
];

@NgModule({
    imports: [
        PsClubManagerSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        GameDictionaryComponent,
        GameDictionaryDetailComponent,
        GameDictionaryDialogComponent,
        GameDictionaryDeleteDialogComponent,
        GameDictionaryPopupComponent,
        GameDictionaryDeletePopupComponent,
    ],
    entryComponents: [
        GameDictionaryComponent,
        GameDictionaryDialogComponent,
        GameDictionaryPopupComponent,
        GameDictionaryDeleteDialogComponent,
        GameDictionaryDeletePopupComponent,
    ],
    providers: [
        GameDictionaryService,
        GameDictionaryPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PsClubManagerGameDictionaryModule {}
