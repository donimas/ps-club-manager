import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { ClubComponent } from './club.component';
import { ClubDetailComponent } from './club-detail.component';
import {ClubDialogComponent, ClubPopupComponent} from './club-dialog.component';
import { ClubDeletePopupComponent } from './club-delete-dialog.component';

export const clubRoute: Routes = [
    {
        path: 'club',
        component: ClubComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.club.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'club/:id',
        component: ClubDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.club.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'club-new',
        component: ClubDialogComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.club.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'club/:id/edit',
        component: ClubDialogComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.club.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const clubPopupRoute: Routes = [
    {
        path: 'club/:id/delete',
        component: ClubDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.club.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
