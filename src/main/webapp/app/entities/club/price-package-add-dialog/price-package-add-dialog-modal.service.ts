import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {Club} from '../club.model';
import {PricePackageDialogComponent} from '../../price-package/price-package-dialog.component';
import {PricePackage} from '../../price-package/price-package.model';

@Injectable()
export class PricePackageAddDialogModalService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
    ) {}

    open(club: Club, pp: PricePackage): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;
        const modalRef = this.modalService.open(PricePackageDialogComponent);
        modalRef.componentInstance.club = club;
        if(!pp) {
            pp = new PricePackage();
        }
        modalRef.componentInstance.pricePackage = pp;
        modalRef.result.then((result) => {
            this.isOpen = false;
        }, (reason) => {
            this.isOpen = false;
        });
        return modalRef;
    }
}
