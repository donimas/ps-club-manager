import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PsClubManagerSharedModule } from '../../shared';
import {
    ClubService,
    ClubPopupService,
    ClubComponent,
    ClubDetailComponent,
    ClubDialogComponent,
    ClubPopupComponent,
    ClubDeletePopupComponent,
    ClubDeleteDialogComponent,
    clubRoute,
    clubPopupRoute,

    GameAddDialogModalService,
    GameAddDialogModalComponent,
    ConsoleAddDialogModalService,
    ConsoleAddDialogModalComponent,
    ZoneAddDialogModalService,
    PricePackageAddDialogModalService,
    ClientLevelAddDialogModalService,
} from './';

const ENTITY_STATES = [
    ...clubRoute,
    ...clubPopupRoute,
];

@NgModule({
    imports: [
        PsClubManagerSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ClubComponent,
        ClubDetailComponent,
        ClubDialogComponent,
        ClubDeleteDialogComponent,
        ClubPopupComponent,
        ClubDeletePopupComponent,
        GameAddDialogModalComponent,
        ConsoleAddDialogModalComponent,
    ],
    entryComponents: [
        ClubComponent,
        ClubDialogComponent,
        ClubPopupComponent,
        ClubDeleteDialogComponent,
        ClubDeletePopupComponent,
        GameAddDialogModalComponent,
        ConsoleAddDialogModalComponent,
    ],
    providers: [
        ClubService,
        ClubPopupService,
        GameAddDialogModalService,
        ConsoleAddDialogModalService,
        ZoneAddDialogModalService,
        PricePackageAddDialogModalService,
        ClientLevelAddDialogModalService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PsClubManagerClubModule {}
