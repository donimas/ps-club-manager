import {Component, AfterViewInit, OnInit} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Club} from '../club.model';
import {ClubService} from '../club.service';
import {Observable} from 'rxjs/Observable';
import {ConsoleDictionary} from '../../console-dictionary/console-dictionary.model';
import {ConsoleDictionaryService} from '../../console-dictionary/console-dictionary.service';

@Component({
    selector: 'console-add-dialog-modal',
    templateUrl: './console-add-dialog.component.html'
})
export class ConsoleAddDialogModalComponent implements OnInit, AfterViewInit {

    isSaving: boolean;
    collectedConsoles: any = [];
    selectedToRejectConsoles: any = [];
    clubConsoles: any = [];
    consolesDictionary: ConsoleDictionary[];
    club: Club;

    constructor(
        private eventManager: JhiEventManager,
        public activeModal: NgbActiveModal,
        private consoleDictionaryService: ConsoleDictionaryService,
        private jhiAlertService: JhiAlertService,
        private clubService: ClubService
    ) {
    }

    ngOnInit() {
        console.log(this.club);
        if(this.club.consoles) {
            this.clubConsoles = Object.assign([], this.club.consoles);
        }
        this.consoleDictionaryService.query()
            .subscribe((res: HttpResponse<ConsoleDictionary[]>) => { this.consolesDictionary = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    ngAfterViewInit() {
    }

    cancel() {
        this.activeModal.dismiss('cancel');
    }

    save(consolesCollection: ConsoleDictionary[]) {
        let requestClub = new Club();
        requestClub.id = this.club.id;
        requestClub.consoles = consolesCollection;
        console.log('requestClub');
        console.log(requestClub);
        this.subscribeToSaveResponse(
            this.clubService.saveEquipment(requestClub));
    }

    submit() {
        this.isSaving = true;
        console.log(this.collectedConsoles);
        let consolesCollection = Object.assign([], this.clubConsoles);
        this.collectedConsoles.forEach(gId => {
            let console = new ConsoleDictionary();
            console.id = gId;
            consolesCollection.push(console);
        });

        this.save(consolesCollection);
    }

    removeConsole() {
        console.log(this.selectedToRejectConsoles);
        const restConsoles = this.clubConsoles.filter((console) => {
            let rejectedConsole = null;
            for (let i=0; i < this.selectedToRejectConsoles.length; i++) {
                if(console.id === this.selectedToRejectConsoles[i]) {
                    rejectedConsole = this.selectedToRejectConsoles[i];
                }
            }
            if(rejectedConsole) {
                return false;
            }
            return true;
        });
        console.log(restConsoles);

        this.save(restConsoles);
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Club>>) {
        result.subscribe((res: HttpResponse<Club>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Club) {
        console.log('on save result ->');
        console.log(result);
        this.eventManager.broadcast({ name: 'newClubConsolesListModification', content: 'OK', clubConsoles: result.consoles});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

}
