import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import {Club} from '../club.model';
import {ConsoleAddDialogModalComponent} from './console-add-dialog.component';

@Injectable()
export class ConsoleAddDialogModalService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
    ) {}

    open(club: Club): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;
        const modalRef = this.modalService.open(ConsoleAddDialogModalComponent);
        modalRef.componentInstance.club = club;
        modalRef.result.then((result) => {
            this.isOpen = false;
        }, (reason) => {
            this.isOpen = false;
        });
        return modalRef;
    }
}
