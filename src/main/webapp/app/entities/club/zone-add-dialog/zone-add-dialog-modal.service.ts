import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {Club} from '../club.model';
import {ZoneDialogComponent} from '../../zone/zone-dialog.component';
import {Zone} from "../../zone/zone.model";

@Injectable()
export class ZoneAddDialogModalService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
    ) {}

    open(club: Club, zone: Zone): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;
        const modalRef = this.modalService.open(ZoneDialogComponent);
        modalRef.componentInstance.club = club;
        if(!zone) {
            zone = new Zone();
        }
        modalRef.componentInstance.zone = zone;
        modalRef.result.then((result) => {
            this.isOpen = false;
        }, (reason) => {
            this.isOpen = false;
        });
        return modalRef;
    }
}
