import { BaseEntity } from './../../shared';
import {DayRegime} from '../day-regime/day-regime.model';
import {PricePackage} from '../price-package/price-package.model';
import {ClientLevel} from '../client-level/client-level.model';
import {Zone} from '../zone/zone.model';
import {AttachmentFile} from '../attachment-file/attachment-file.model';
import {GameDictionary} from '../game-dictionary/game-dictionary.model';
import {ClubJoystick} from '../club-joystick/club-joystick.model';
import {Location} from '../location/location.model';
import {ConsoleDictionary} from '../console-dictionary/console-dictionary.model';

export class Club implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public fullAddress?: string,
        public lon?: string,
        public lat?: string,
        public timeScheduleRegulation?: number,     // max 60 min
        public flaghookah?: boolean,
        public phone?: string,
        public phoneOptional?: string,
        public webUrl?: string,
        public vkUrl?: string,
        public instaUrl?: string,
        public telegramUrl?: string,
        public facebookUrl?: string,
        public twitterUrl?: string,
        public avatar?: AttachmentFile,
        public consoles?: ConsoleDictionary[],
        public games?: GameDictionary[],
        public joysticks?: ClubJoystick[],
        public zones?: Zone[],
        public zoneDtos?: Zone[],
        public workRegimes?: DayRegime[],
        public galleries?: AttachmentFile[],
        public dayRegimes?: DayRegime[],
        public pricePackages?: PricePackage[],
        public clientLevels?: ClientLevel[],
        public flagPublished?: boolean,
        public flagMainInfoSaved?: boolean,
        public flagContactSaved?: boolean,
        public flagLocationSaved?: boolean,
        public flagZoneSaved?: boolean,
        public notifyBefore?: number,       // in minutes
        public location?: Location,

        public flagRegulationSaved? :boolean,
        public flagEquipmentSaved? :boolean,
        public flagPricePackageSaved? :boolean,
        public flagClientLevelSaved? :boolean,
    ) {
        this.flaghookah = false;

        this.flagRegulationSaved = false;
        this.flagEquipmentSaved = false;
        this.flagPricePackageSaved = false;
        this.flagClientLevelSaved = false;
    }
}
