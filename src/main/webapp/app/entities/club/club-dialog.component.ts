import {Component, OnInit, OnDestroy, ElementRef, ViewChild, AfterViewInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {HttpResponse, HttpErrorResponse, HttpClient} from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
// import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService, JhiDataUtils} from 'ng-jhipster';

import { Club } from './club.model';
import { ClubPopupService } from './club-popup.service';
import { ClubService } from './club.service';
import { AttachmentFile, AttachmentFileService } from '../attachment-file';
import { ConsoleDictionary, ConsoleDictionaryService } from '../console-dictionary';
import { GameDictionary, GameDictionaryService } from '../game-dictionary';
import {DayRegime} from '../day-regime/day-regime.model';
import {DayRegimeService} from '../day-regime/day-regime.service';
import {ClubJoystick} from '../club-joystick/club-joystick.model';
import {PricePackage} from '../price-package/price-package.model';
import {ClientLevel} from '../client-level/client-level.model';
import {Zone} from '../zone/zone.model';
import {of} from 'rxjs/observable/of';
import {NgbModalRef, NgbTimeStruct, NgbTypeahead} from '@ng-bootstrap/ng-bootstrap';
import {Subject} from 'rxjs/Subject';
import {JoystickDictionary} from '../joystick-dictionary/joystick-dictionary.model';
import {JoystickDictionaryService} from '../joystick-dictionary/joystick-dictionary.service';
import {ClubJoystickService} from '../club-joystick/club-joystick.service';
import {Subscription} from 'rxjs/Subscription';
import {JoyTimeService} from '../../shared/joy-time/joy-time.service';
import {CityDictionary} from '../city-dictionary/city-dictionary.model';
import {CityDictionaryService} from '../city-dictionary/city-dictionary.service';
import {Location} from '../location/location.model';
import {GameAddDialogModalService} from './game-add-dialog/game-add-dialog-modal.service';
import {ConsoleAddDialogModalService} from './console-add-dialog/console-add-dialog-modal.service';
import {ZoneAddDialogModalService} from './zone-add-dialog/zone-add-dialog-modal.service';
import {ZoneService} from '../zone/zone.service';
import {PricePackageService} from '../price-package/price-package.service';
import {PricePackageAddDialogModalService} from './price-package-add-dialog/price-package-add-dialog-modal.service';
import {ClientLevelAddDialogModalService} from './client-level-add-dialog/client-level-add-dialog-modal.service';
import {ClientLevelService} from '../client-level/client-level.service';

@Component({
    selector: 'jhi-club-dialog',
    templateUrl: './club-dialog.component.html'
})
export class ClubDialogComponent implements OnInit, AfterViewInit, OnDestroy {

    club: Club;
    isSaving: boolean;
    searching: boolean;
    searchFailed: boolean;
    fullFilled: boolean;

    timeScheduleRegulation = {hour: 0, minute: 30};
    notifyBefore = {hour: 0, minute: 30};

    avatar: AttachmentFile;
    attachmentfiles: AttachmentFile[];

    consoledictionaries: ConsoleDictionary[];
    gamedictionaries: GameDictionary[];
    joystickdictionaries: JoystickDictionary[];
    cities: CityDictionary[];
    location: Location;

    dayRegimeType: any;
    dayRegimes: DayRegime[];
    regimeHours: number[];
    regimeMinutes: number[];
    newJoystick: ClubJoystick;
    joysticks: ClubJoystick[];
    pricePackages: PricePackage[];
    clientLevels: ClientLevel[];
    zones: Zone[];

    modelOsm: any;
    hideSearchingWhenUnsubscribedOsm = new Observable(() => () => this.searching = false);
    modelJoystick: any;

    eventSubscriber: Subscription;
    modalRef: NgbModalRef;

    @ViewChild('instance') instance: NgbTypeahead;
    focus$ = new Subject<string>();
    click$ = new Subject<string>();

    constructor(
        // public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private clubService: ClubService,
        private attachmentFileService: AttachmentFileService,
        private consoleDictionaryService: ConsoleDictionaryService,
        private gameDictionaryService: GameDictionaryService,
        private joystickDictionaryService: JoystickDictionaryService,
        private eventManager: JhiEventManager,
        private dayRegimeService: DayRegimeService,
        private elementRef: ElementRef,
        private dataUtils: JhiDataUtils,
        private http: HttpClient,
        private clubJoystickService: ClubJoystickService,
        private joyTimeService: JoyTimeService,
        private cityDictionaryService: CityDictionaryService,
        private gameAddDialogService: GameAddDialogModalService,
        private consoleAddDialogService: ConsoleAddDialogModalService,
        private zoneAddDialogService: ZoneAddDialogModalService,
        private zoneService: ZoneService,
        private pricePackageService: PricePackageService,
        private pricePackageAddDialogService: PricePackageAddDialogModalService,
        private clientLevelAddDialogService: ClientLevelAddDialogModalService,
        private clientLevelService: ClientLevelService,
    ) {
        this.club = new Club();
        this.avatar = new AttachmentFile();
        this.regimeHours = this.dayRegimeService.getRegimeHours();
        this.regimeMinutes = this.dayRegimeService.getRegimeMinutes();
        this.newJoystick = new ClubJoystick();
        this.location = new Location();
    }

    ngOnInit() {
        this.isSaving = false;
        this.searching = false;
        this.searchFailed = false;

        this.loadClub();

        this.joystickDictionaryService.query()
            .subscribe((res: HttpResponse<JoystickDictionary[]>) => { this.joystickdictionaries = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.consoleDictionaryService.query()
            .subscribe((res: HttpResponse<ConsoleDictionary[]>) => { this.consoledictionaries = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.gameDictionaryService.query()
            .subscribe((res: HttpResponse<GameDictionary[]>) => { this.gamedictionaries = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.cityDictionaryService.query()
            .subscribe((res: HttpResponse<GameDictionary[]>) => { this.cities = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));

        this.registerChangeInClubJoysticks();
        this.registerChangeInClubGames();
        this.registerChangeInClubConsoles();
        this.registerChangeInClubZones();
        this.registerChangeInClubPricePackages();
        this.registerChangeInClubClientLevels();

    }

    ngAfterViewInit() {
        console.log('after view init');
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    loadClub() {
        this.clubService.ownerVisit().subscribe((clubResponse: HttpResponse<Club>) => {
            this.club = clubResponse.body;
            console.log('owners club');
            console.log(this.club);
            if(this.club.flagMainInfoSaved && this.club.flagContactSaved && this.club.flagLocationSaved && this.club.flagZoneSaved) {
                this.fullFilled = true;
            }

            this.avatar = this.initAvatar(this.club);
            this.initDayRegimes(this.club);
            this.initLocation(this.club);
            this.initJoysticks(this.club);
            this.initPricePackages(this.club);
            this.initClientLevels(this.club);
            this.initZones(this.club);
        }, (error) => console.log(error));
    }

    initAvatar(club: Club): AttachmentFile {
        let result = new AttachmentFile();
        if(club.avatar) {
            result = club.avatar;
        }
        return result;
    }

    initLocation(club: Club) {
        if(!club.location) {
            return;
        }
        this.location = club.location;
        this.modelOsm = {
            display_name: `${club.location.city.postalCode} ${club.location.city.name}, ${club.location.street} ${club.location.homeNumber}`
        };
    }

    initDayRegimes(club: Club) {
        if(club.timeScheduleRegulation) {
            this.timeScheduleRegulation = this.joyTimeService.minutesToTimePicker(club.timeScheduleRegulation);
        }
        if(club.notifyBefore) {
            this.notifyBefore = this.joyTimeService.minutesToTimePicker(club.notifyBefore);
        }

        this.dayRegimeService.findAllOfClub(club.id).subscribe(
            (res: HttpResponse<DayRegime[]>) => {
                console.log('day regimes =>');
                console.log(res);
                this.dayRegimes = res.body;
                if(!this.dayRegimes.length) {
                    for(let i=1; i<=7; i++) {
                        let dayRegime = new DayRegime();
                        dayRegime.daySeq = i;
                        // dayRegime.flagTillLastClient = true;
                        dayRegime.flagTime = true;
                        dayRegime.flagAllDayLong = false;

                        dayRegime.oHour = 12;
                        dayRegime.oMinute = 0;
                        dayRegime.cHour = 3;
                        dayRegime.cMinute = 59;

                        dayRegime.openedAt = {hour: 12, minute: 0, second: 0};
                        dayRegime.closedAt = {hour: 3, minute: 59, second: 59};

                        this.dayRegimes.push(dayRegime);
                    }
                } else {
                    this.dayRegimes.forEach((dr) => {
                        dr.openedAt = {hour: dr.oHour, minute: dr.oMinute};
                        dr.closedAt = {hour: dr.cHour, minute: dr.cMinute};
                    });
                }
            }
        );
    }

    initJoysticks(club: Club) {
        this.clubJoystickService.findAllByClub(club.id).subscribe(
            (res: HttpResponse<ClubJoystick[]>) => {
                this.joysticks = res.body;
            }
        );
    }

    initPricePackages(club: Club) {
        this.pricePackageService.getClubPricePackages(club.id).subscribe((res: HttpResponse<PricePackage[]>) => {
            this.pricePackages = res.body;
        });
    }

    initClientLevels(club: Club) {
        this.clientLevelService.getClubClientLevels(club.id).subscribe((res: HttpResponse<ClientLevel[]>) => {
            this.clientLevels = res.body;
        });
    }

    initZones(club: Club) {
        this.zoneService.getClubZones(club.id).subscribe((res: HttpResponse<Zone[]>) => {
            console.log('zones:');
            console.log(res);
            this.zones = res.body;
        }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        // this.activeModal.dismiss('cancel');
    }
    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;

        this.clubService.publish(this.club.id).subscribe((res: any) => {
            this.isSaving = false;
            this.club.flagPublished = true;
        });
        /*if (this.club.id !== undefined) {
            this.subscribeToSaveResponse(
                this.clubService.update(this.club));
        } else {
            this.subscribeToSaveResponse(
                this.clubService.create(this.club));
        }*/
    }

    // ================================ Main Info ================================ //
    saveMainInfo() {
        console.log('save main info ->');
        this.club.avatar = this.avatar;

        console.log(this.club);

        this.isSaving = true;
        this.subscribeToSaveResponse(
            this.clubService.saveMainInfo(this.club));
    }
    // ================================ Regulation ================================ //
    saveRegulation() {
        console.log('save regulation ->');
        this.dayRegimes.forEach((dr) => {
            dr.oHour = dr.openedAt.hour;
            dr.oMinute = dr.openedAt.minute;

            dr.cHour = dr.closedAt.hour;
            dr.cMinute = dr.closedAt.minute;
        });
        let copy = Object.assign([], this.dayRegimes);
        copy.forEach((wr) => {
            wr.openedAt = null;
            wr.closedAt = null;
        });
        this.club.workRegimes = copy;
        this.club.timeScheduleRegulation = this.joyTimeService.timePickerToMinutes(this.timeScheduleRegulation);
        this.club.notifyBefore = this.joyTimeService.timePickerToMinutes(this.notifyBefore);
        console.log(this.club);

        this.isSaving = true;
        this.clubService.saveRegulation(this.club).subscribe((res: HttpResponse<Club>) =>
        {
            this.isSaving = false;
            this.initDayRegimes(this.club);
        }, (res: HttpErrorResponse) => this.onSaveError());
    }
    // ================================ Address ================================ //
    saveAddress() {
        console.log('save address ->');
        this.club.location = this.location;
        console.log(this.club);

        this.isSaving = true;
        this.subscribeToSaveClubJoystickResponse(
            this.clubService.saveCoordinate(this.club)
        );
    }
    // ================================ Contacts ================================ //
    saveContact() {
        console.log('save contact ->');
        console.log(this.club);

        this.isSaving = true;
        this.subscribeToSaveClubJoystickResponse(
            this.clubService.saveContact(this.club)
        );
    }
    // ================================ Equipments ================================ //
    saveEquipments() {
        console.log('save eq ->');
        console.log(this.club);
    }

    addJoystick(joystick: ClubJoystick) {
        if(!joystick.joystickDictionary) {
            return;
        }
        this.isSaving = true;
        joystick.club = new Club();
        joystick.club.id = this.club.id;
        if(joystick.id !== undefined) {
            this.subscribeToSaveClubJoystickResponse(this.clubJoystickService.update(joystick));
        } else {
            this.subscribeToSaveClubJoystickResponse(this.clubJoystickService.create(joystick));
        }
    }

    private subscribeToSaveClubJoystickResponse(result: Observable<HttpResponse<ClubJoystick>>) {
        result.subscribe(
            (res: HttpResponse<ClubJoystick>) => {
                this.isSaving = false;
                this.resetNewJoystick();
                // this.joysticks.push(res.body);
                this.initJoysticks(this.club);
            },
            (res: HttpErrorResponse) => this.onSaveError());
    }

    editJoystick(joystick: ClubJoystick) {
        this.newJoystick = joystick;
        this.modelJoystick = joystick.joystickDictionary;
    }

    resetNewJoystick() {
        this.newJoystick = new ClubJoystick();
        this.modelJoystick = null;
    }

    registerChangeInClubJoysticks() {
        this.eventSubscriber = this.eventManager.subscribe('clubJoystickListModification', (response) => this.initJoysticks(this.club));
    }
    // ================================ Zones ================================ //
    saveZones() {

    }
    // ================================ Price Package ================================ //
    savePricePackage() {

    }
    // ================================ Client level ================================ //
    saveClientLevel() {

    }

    searchAddressFromOsm(query) {
        const osmQuery = 'https://nominatim.openstreetmap.org/search?q='+query+'&format=json&polygon=1&addressdetails=1';
        console.log(osmQuery);
        return this.http.get(osmQuery).map((res: Response) => {
            return res;
        });
    }

    searchOsm = (text$: Observable<string>) =>
        text$
            .debounceTime(300)
            .distinctUntilChanged()
            .do((v) => {
                console.log('do1:');
                console.log(v);
                this.searching = true;
            })
            .switchMap((term) => {
                return this.searchAddressFromOsm(term)
                    .do((v) => {
                        console.log('do2:');
                        console.log(v);
                        this.searchFailed = false;
                    })
                    .catch((v) => {
                        console.log('catch1:');
                        console.log(v);
                        this.searchFailed = true;
                        return of([]);
                    })
            })
            .do((v) => {
                console.log('do3:');
                console.log(v);
                this.searching = false;
            })
            .merge(this.hideSearchingWhenUnsubscribedOsm);

    formatterOsm = (x: {display_name: string}) => x.display_name;

    public selectedOsmItem(event) {
        console.log(event);
        const osm = event.item;
        this.club.lon = osm.lon;
        this.club.lat = osm.lat;
        this.club.fullAddress = osm.display_name;

        this.osmToLocation(event);

        console.log(this.club);
    }

    osmToLocation(event: any) {
        console.log('request to convert osm to Location');
        console.log(event);
        const osm = event.item;
        this.location.homeNumber = osm.address.house_number;
        this.location.street = osm.address.road;
        this.location.lat = osm.lat;
        this.location.lon = osm.lon;
        this.location.osmId = osm.osm_id;
        console.log(this.location);
    }

    // ========== typeahead joystick =========== //

    searchJoystick = (text$: Observable<string>) =>
        text$
            .debounceTime(200).distinctUntilChanged()
            .merge(this.focus$)
            .merge(this.click$.filter(() => !this.instance.isPopupOpen()))
            .map(term => (term === '' ? this.joystickdictionaries :
                this.joystickdictionaries.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10));

    formatterJoystick = (x: {name: string}) => x.name;

    selectJoystick(event) {
        console.log(event);
        this.newJoystick.joystickDictionary = event.item;
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Club>>) {
        result.subscribe((res: HttpResponse<Club>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Club) {
        // this.eventManager.broadcast({ name: 'clubListModification', content: 'OK'});
        this.isSaving = false;
        // this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    cityById(index: number, item: CityDictionary) {
        return item.id;
    }

    trackAttachmentFileById(index: number, item: AttachmentFile) {
        return item.id;
    }

    trackConsoleDictionaryById(index: number, item: ConsoleDictionary) {
        return item.id;
    }

    trackGameDictionaryById(index: number, item: GameDictionary) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.avatar, this.elementRef, field, fieldContentType, idInput);
    }

    selectDayRegimeType(alias, dayRegime) {
        dayRegime.flagAllDayLong = false;
        // dayRegime.flagTillLastClient = false;
        dayRegime.flagRestDay = false;
        dayRegime.flagTime = false;
        if(1 === alias) {
            dayRegime.flagTime = true;
        } else if (2 === alias) {
            dayRegime.flagAllDayLong = true;
        } else {
            // dayRegime.flagTillLastClient = true;
            dayRegime.flagRestDay = true;
        }
    }

    addGame() {
        this.modalRef = this.gameAddDialogService.open(this.club);
    }
    registerChangeInClubGames() {
        this.eventSubscriber = this.eventManager.subscribe('newClubGamesListModification', (response) => {
            this.club.games = response.clubGames;
        });
    }

    addConsole() {
        this.modalRef = this.consoleAddDialogService.open(this.club);
    }
    registerChangeInClubConsoles() {
        this.eventSubscriber = this.eventManager.subscribe('newClubConsolesListModification', (response) => {
            this.club.consoles = response.clubConsoles;
        });
    }

    addZone() {
        this.modalRef = this.zoneAddDialogService.open(this.club, null);
    }
    editZone(zone: Zone) {
        this.modalRef = this.zoneAddDialogService.open(this.club, zone);
    }
    registerChangeInClubZones() {
        this.eventSubscriber = this.eventManager.subscribe('zoneListModification', (response) => {
            this.initZones(this.club);
        });
    }

    addPricePackage() {
        this.modalRef = this.pricePackageAddDialogService.open(this.club, null);
    }
    registerChangeInClubPricePackages() {
        this.eventSubscriber = this.eventManager.subscribe('pricePackageListModification', (response) => {
            this.initPricePackages(this.club);
        });
    }

    addClientLevel() {
        this.modalRef = this.clientLevelAddDialogService.open(this.club, null);
    }
    registerChangeInClubClientLevels() {
        this.eventSubscriber = this.eventManager.subscribe('clientLevelListModification', (response) => {
            this.initClientLevels(this.club);
        });
    }

}

@Component({
    selector: 'jhi-club-popup',
    template: ''
})
export class ClubPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clubPopupService: ClubPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.clubPopupService
                    .open(ClubDialogComponent as Component, params['id']);
            } else {
                this.clubPopupService
                    .open(ClubDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
