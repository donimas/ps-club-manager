import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {Club} from '../club.model';
import {ClientLevel} from '../../client-level/client-level.model';
import {ClientLevelDialogComponent} from '../../client-level/client-level-dialog.component';

@Injectable()
export class ClientLevelAddDialogModalService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
    ) {}

    open(club: Club, clientLevel: ClientLevel): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;
        const modalRef = this.modalService.open(ClientLevelDialogComponent);
        modalRef.componentInstance.club = club;
        if(!clientLevel) {
            clientLevel = new ClientLevel();
        }
        modalRef.componentInstance.clientLevel = clientLevel;
        modalRef.result.then((result) => {
            this.isOpen = false;
        }, (reason) => {
            this.isOpen = false;
        });
        return modalRef;
    }
}
