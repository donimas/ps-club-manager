import {Component, AfterViewInit, OnInit} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';
import {GameDictionary} from '../../game-dictionary/game-dictionary.model';
import {GameDictionaryService} from '../../game-dictionary/game-dictionary.service';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Club} from '../club.model';
import {ClubService} from '../club.service';
import {Observable} from "rxjs/Observable";

@Component({
    selector: 'game-add-dialog-modal',
    templateUrl: './game-add-dialog.component.html'
})
export class GameAddDialogModalComponent implements OnInit, AfterViewInit {

    isSaving: boolean;
    collectedGames: any = [];
    selectedToRejectGames: any = [];
    clubGames: any = [];
    gamesDictionary: GameDictionary[];
    club: Club;

    constructor(
        private eventManager: JhiEventManager,
        public activeModal: NgbActiveModal,
        private gameDictionaryService: GameDictionaryService,
        private jhiAlertService: JhiAlertService,
        private clubService: ClubService
    ) {
    }

    ngOnInit() {
        console.log(this.club);
        if(this.club.games) {
            this.clubGames = Object.assign([], this.club.games);
        }
        this.gameDictionaryService.query()
            .subscribe((res: HttpResponse<GameDictionary[]>) => { this.gamesDictionary = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    ngAfterViewInit() {
    }

    cancel() {
        this.activeModal.dismiss('cancel');
    }

    save(gamesCollection: GameDictionary[]) {
        let requestClub = new Club();
        requestClub.id = this.club.id;
        requestClub.games = gamesCollection;
        console.log('requestClub');
        console.log(requestClub);
        this.subscribeToSaveResponse(
            this.clubService.saveEquipment(requestClub));
    }

    submit() {
        this.isSaving = true;
        console.log(this.collectedGames);
        let gamesCollection = Object.assign([], this.clubGames);
        this.collectedGames.forEach(gId => {
            let game = new GameDictionary();
            game.id = gId;
            gamesCollection.push(game);
        });

        this.save(gamesCollection);
    }

    removeGame() {
        console.log(this.selectedToRejectGames);
        const restGames = this.clubGames.filter((game) => {
            let rejectedGame = null;
            for (let i=0; i < this.selectedToRejectGames.length; i++) {
                if(game.id === this.selectedToRejectGames[i]) {
                    rejectedGame = this.selectedToRejectGames[i];
                }
            }
            if(rejectedGame) {
                return false;
            }
            return true;
        });
        console.log(restGames);

        this.save(restGames);
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Club>>) {
        result.subscribe((res: HttpResponse<Club>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Club) {
        console.log('on save result ->');
        console.log(result);
        this.eventManager.broadcast({ name: 'newClubGamesListModification', content: 'OK', clubGames: result.games});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

}
