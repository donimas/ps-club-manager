import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { GameAddDialogModalComponent } from './game-add-dialog.component';
import {GameDictionary} from '../../game-dictionary/game-dictionary.model';
import {Club} from '../club.model';

@Injectable()
export class GameAddDialogModalService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
    ) {}

    open(club: Club): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;
        const modalRef = this.modalService.open(GameAddDialogModalComponent);
        modalRef.componentInstance.club = club;
        modalRef.result.then((result) => {
            this.isOpen = false;
        }, (reason) => {
            this.isOpen = false;
        });
        return modalRef;
    }
}
