import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PsClubManagerSharedModule } from '../../shared';
import {
    PricePackageService,
    PricePackagePopupService,
    PricePackageComponent,
    PricePackageDetailComponent,
    PricePackageDialogComponent,
    PricePackagePopupComponent,
    PricePackageDeletePopupComponent,
    PricePackageDeleteDialogComponent,
    pricePackageRoute,
    pricePackagePopupRoute,
} from './';

const ENTITY_STATES = [
    ...pricePackageRoute,
    ...pricePackagePopupRoute,
];

@NgModule({
    imports: [
        PsClubManagerSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        PricePackageComponent,
        PricePackageDetailComponent,
        PricePackageDialogComponent,
        PricePackageDeleteDialogComponent,
        PricePackagePopupComponent,
        PricePackageDeletePopupComponent,
    ],
    entryComponents: [
        PricePackageComponent,
        PricePackageDialogComponent,
        PricePackagePopupComponent,
        PricePackageDeleteDialogComponent,
        PricePackageDeletePopupComponent,
    ],
    providers: [
        PricePackageService,
        PricePackagePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PsClubManagerPricePackageModule {}
