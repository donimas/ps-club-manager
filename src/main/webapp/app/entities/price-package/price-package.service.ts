import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { PricePackage } from './price-package.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<PricePackage>;

@Injectable()
export class PricePackageService {

    private resourceUrl =  SERVER_API_URL + 'api/price-packages';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/price-packages';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(pricePackage: PricePackage): Observable<EntityResponseType> {
        const copy = this.convert(pricePackage);
        return this.http.post<PricePackage>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(pricePackage: PricePackage): Observable<EntityResponseType> {
        const copy = this.convert(pricePackage);
        return this.http.put<PricePackage>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<PricePackage>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<PricePackage[]>> {
        const options = createRequestOption(req);
        return this.http.get<PricePackage[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<PricePackage[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<PricePackage[]>> {
        const options = createRequestOption(req);
        return this.http.get<PricePackage[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<PricePackage[]>) => this.convertArrayResponse(res));
    }

    getClubPricePackages(id: number, req?: any): Observable<HttpResponse<PricePackage[]>> {
        const options = createRequestOption(req);
        return this.http.get<PricePackage[]>(`${this.resourceUrl}/club/${id}`, { params: options, observe: 'response' })
            .map((res: HttpResponse<PricePackage[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: PricePackage = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<PricePackage[]>): HttpResponse<PricePackage[]> {
        const jsonResponse: PricePackage[] = res.body;
        const body: PricePackage[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to PricePackage.
     */
    private convertItemFromServer(pricePackage: PricePackage): PricePackage {
        const copy: PricePackage = Object.assign({}, pricePackage);
        copy.startAt = this.dateUtils
            .convertDateTimeFromServer(pricePackage.startAt);
        copy.finishAt = this.dateUtils
            .convertDateTimeFromServer(pricePackage.finishAt);
        return copy;
    }

    /**
     * Convert a PricePackage to a JSON which can be sent to the server.
     */
    private convert(pricePackage: PricePackage): PricePackage {
        const copy: PricePackage = Object.assign({}, pricePackage);

        copy.startAt = this.dateUtils.toDate(pricePackage.startAt);

        copy.finishAt = this.dateUtils.toDate(pricePackage.finishAt);
        return copy;
    }
}
