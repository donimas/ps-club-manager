import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { PricePackage } from './price-package.model';
import { PricePackageService } from './price-package.service';

@Component({
    selector: 'jhi-price-package-detail',
    templateUrl: './price-package-detail.component.html'
})
export class PricePackageDetailComponent implements OnInit, OnDestroy {

    pricePackage: PricePackage;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private pricePackageService: PricePackageService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPricePackages();
    }

    load(id) {
        this.pricePackageService.find(id)
            .subscribe((pricePackageResponse: HttpResponse<PricePackage>) => {
                this.pricePackage = pricePackageResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPricePackages() {
        this.eventSubscriber = this.eventManager.subscribe(
            'pricePackageListModification',
            (response) => this.load(this.pricePackage.id)
        );
    }
}
