import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PricePackage } from './price-package.model';
import { PricePackagePopupService } from './price-package-popup.service';
import { PricePackageService } from './price-package.service';
import {JoyTimeService} from '../../shared/joy-time/joy-time.service';
import {Club} from '../club/club.model';

@Component({
    selector: 'jhi-price-package-dialog',
    templateUrl: './price-package-dialog.component.html'
})
export class PricePackageDialogComponent implements OnInit {

    pricePackage: PricePackage;
    isSaving: boolean;
    club: Club;

    // joyDuration = {hour: 0, minute: 30};
    joyDuration: any;
    // minimumJoyDurationInMinutes = {hour: 0, minute: 15};
    minimumJoyDurationInMinutes: any;
    startAt = {hour: 0, minute: 0};
    finishAt = {hour: 8, minute: 0};
    minDurationMask = [/[0]/, /[0-1]/, ':', /[0-5]/, /[0-9]/];
    joyDurationMask = [/[0-2]/, /[0-9]/, ':', /[0-5]/, /[0-9]/];

    constructor(
        public activeModal: NgbActiveModal,
        private pricePackageService: PricePackageService,
        private eventManager: JhiEventManager,
        private joyTimeService: JoyTimeService,
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        // this.pricePackage.minutesAmount = this.joyTimeService.timePickerToMinutes(this.joyDuration);
        this.pricePackage.minutesAmount = this.joyTimeService.formattedStringTimeToMinutes(this.joyDuration);
        if(this.pricePackage.minutesAmount > 1440) {
            alert('Minutes amount more than 24 hours');
            return;
        }
        this.pricePackage.minimumJoyDurationInMinutes = this.joyTimeService.formattedStringTimeToMinutes(this.minimumJoyDurationInMinutes);
        if(this.pricePackage.minimumJoyDurationInMinutes > 60) {
            alert('Minimum amount of minutes more than an hour');
            return;
        }
        this.pricePackage.startHour = this.startAt.hour;
        this.pricePackage.startMinute = this.startAt.minute;
        this.pricePackage.finishHour = this.finishAt.hour;
        this.pricePackage.finishMinute = this.finishAt.minute;

        this.pricePackage.club = new Club();
        this.pricePackage.club.id = this.club.id;

        console.log(this.pricePackage);

        if (this.pricePackage.id !== undefined) {
            this.subscribeToSaveResponse(
                this.pricePackageService.update(this.pricePackage));
        } else {
            this.subscribeToSaveResponse(
                this.pricePackageService.create(this.pricePackage));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<PricePackage>>) {
        result.subscribe((res: HttpResponse<PricePackage>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: PricePackage) {
        this.eventManager.broadcast({ name: 'pricePackageListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-price-package-popup',
    template: ''
})
export class PricePackagePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private pricePackagePopupService: PricePackagePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.pricePackagePopupService
                    .open(PricePackageDialogComponent as Component, params['id']);
            } else {
                this.pricePackagePopupService
                    .open(PricePackageDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
