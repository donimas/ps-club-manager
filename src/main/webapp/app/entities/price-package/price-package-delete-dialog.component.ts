import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PricePackage } from './price-package.model';
import { PricePackagePopupService } from './price-package-popup.service';
import { PricePackageService } from './price-package.service';

@Component({
    selector: 'jhi-price-package-delete-dialog',
    templateUrl: './price-package-delete-dialog.component.html'
})
export class PricePackageDeleteDialogComponent {

    pricePackage: PricePackage;

    constructor(
        private pricePackageService: PricePackageService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.pricePackageService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'pricePackageListModification',
                content: 'Deleted an pricePackage'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-price-package-delete-popup',
    template: ''
})
export class PricePackageDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private pricePackagePopupService: PricePackagePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.pricePackagePopupService
                .open(PricePackageDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
