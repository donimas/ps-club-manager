import { BaseEntity } from './../../shared';
import {Club} from '../club/club.model';

export const enum DurationType {
    // transient
    'HOURS',
    'STARTEND',
    'MINUTES',
    'AN_HOUR',
    'SERVICE'
}

export class PricePackage implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public flagDeleted?: boolean,
        public type?: DurationType,
        public startAt?: any,
        public finishAt?: any,
        public hoursAmount?: number,    // transient
        public minutesAmount?: number,
        public minimumJoyDurationInMinutes?: number,
        public price?: number,
        public startHour?: number,
        public startMinute?: number,
        public finishHour?: number,
        public finishMinute?: number,
        public club?: Club,
        public amount?: number,
        public minDurationPrice?: number,
    ) {
        this.flagDeleted = false;
    }
}
