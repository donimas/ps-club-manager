import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { PricePackage } from './price-package.model';
import { PricePackageService } from './price-package.service';

@Injectable()
export class PricePackagePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private pricePackageService: PricePackageService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.pricePackageService.find(id)
                    .subscribe((pricePackageResponse: HttpResponse<PricePackage>) => {
                        const pricePackage: PricePackage = pricePackageResponse.body;
                        pricePackage.startAt = this.datePipe
                            .transform(pricePackage.startAt, 'yyyy-MM-ddTHH:mm:ss');
                        pricePackage.finishAt = this.datePipe
                            .transform(pricePackage.finishAt, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.pricePackageModalRef(component, pricePackage);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.pricePackageModalRef(component, new PricePackage());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    pricePackageModalRef(component: Component, pricePackage: PricePackage): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.pricePackage = pricePackage;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
