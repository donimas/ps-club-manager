import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PricePackageComponent } from './price-package.component';
import { PricePackageDetailComponent } from './price-package-detail.component';
import { PricePackagePopupComponent } from './price-package-dialog.component';
import { PricePackageDeletePopupComponent } from './price-package-delete-dialog.component';

export const pricePackageRoute: Routes = [
    {
        path: 'price-package',
        component: PricePackageComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.pricePackage.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'price-package/:id',
        component: PricePackageDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.pricePackage.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const pricePackagePopupRoute: Routes = [
    {
        path: 'price-package-new',
        component: PricePackagePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.pricePackage.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'price-package/:id/edit',
        component: PricePackagePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.pricePackage.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'price-package/:id/delete',
        component: PricePackageDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.pricePackage.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
