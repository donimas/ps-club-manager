import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { ExtraJoystick } from './extra-joystick.model';
import { ExtraJoystickService } from './extra-joystick.service';

@Injectable()
export class ExtraJoystickPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private extraJoystickService: ExtraJoystickService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.extraJoystickService.find(id)
                    .subscribe((extraJoystickResponse: HttpResponse<ExtraJoystick>) => {
                        const extraJoystick: ExtraJoystick = extraJoystickResponse.body;
                        extraJoystick.startedAt = this.datePipe
                            .transform(extraJoystick.startedAt, 'yyyy-MM-ddTHH:mm:ss');
                        extraJoystick.finishedAt = this.datePipe
                            .transform(extraJoystick.finishedAt, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.extraJoystickModalRef(component, extraJoystick);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.extraJoystickModalRef(component, new ExtraJoystick());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    extraJoystickModalRef(component: Component, extraJoystick: ExtraJoystick): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.extraJoystick = extraJoystick;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
