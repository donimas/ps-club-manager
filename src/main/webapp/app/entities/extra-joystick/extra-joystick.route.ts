import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { ExtraJoystickComponent } from './extra-joystick.component';
import { ExtraJoystickDetailComponent } from './extra-joystick-detail.component';
import { ExtraJoystickPopupComponent } from './extra-joystick-dialog.component';
import { ExtraJoystickDeletePopupComponent } from './extra-joystick-delete-dialog.component';

export const extraJoystickRoute: Routes = [
    {
        path: 'extra-joystick',
        component: ExtraJoystickComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.extraJoystick.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'extra-joystick/:id',
        component: ExtraJoystickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.extraJoystick.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const extraJoystickPopupRoute: Routes = [
    {
        path: 'extra-joystick-new',
        component: ExtraJoystickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.extraJoystick.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'extra-joystick/:id/edit',
        component: ExtraJoystickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.extraJoystick.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'extra-joystick/:id/delete',
        component: ExtraJoystickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.extraJoystick.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
