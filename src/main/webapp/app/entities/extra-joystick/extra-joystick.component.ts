import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ExtraJoystick } from './extra-joystick.model';
import { ExtraJoystickService } from './extra-joystick.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-extra-joystick',
    templateUrl: './extra-joystick.component.html'
})
export class ExtraJoystickComponent implements OnInit, OnDestroy {
extraJoysticks: ExtraJoystick[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private extraJoystickService: ExtraJoystickService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal
    ) {
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.extraJoystickService.search({
                query: this.currentSearch,
                }).subscribe(
                    (res: HttpResponse<ExtraJoystick[]>) => this.extraJoysticks = res.body,
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
       }
        this.extraJoystickService.query().subscribe(
            (res: HttpResponse<ExtraJoystick[]>) => {
                this.extraJoysticks = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInExtraJoysticks();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ExtraJoystick) {
        return item.id;
    }
    registerChangeInExtraJoysticks() {
        this.eventSubscriber = this.eventManager.subscribe('extraJoystickListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
