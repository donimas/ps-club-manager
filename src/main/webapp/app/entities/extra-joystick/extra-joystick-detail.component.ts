import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ExtraJoystick } from './extra-joystick.model';
import { ExtraJoystickService } from './extra-joystick.service';

@Component({
    selector: 'jhi-extra-joystick-detail',
    templateUrl: './extra-joystick-detail.component.html'
})
export class ExtraJoystickDetailComponent implements OnInit, OnDestroy {

    extraJoystick: ExtraJoystick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private extraJoystickService: ExtraJoystickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInExtraJoysticks();
    }

    load(id) {
        this.extraJoystickService.find(id)
            .subscribe((extraJoystickResponse: HttpResponse<ExtraJoystick>) => {
                this.extraJoystick = extraJoystickResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInExtraJoysticks() {
        this.eventSubscriber = this.eventManager.subscribe(
            'extraJoystickListModification',
            (response) => this.load(this.extraJoystick.id)
        );
    }
}
