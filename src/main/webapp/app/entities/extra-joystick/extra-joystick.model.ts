import { BaseEntity } from './../../shared';

export class ExtraJoystick implements BaseEntity {
    constructor(
        public id?: number,
        public amount?: number,
        public startedAt?: any,
        public finishedAt?: any,
        public playedHours?: number,
        public playedMinutes?: number,
        public totalPrice?: number,
        public session?: BaseEntity,
        public clubJoystick?: BaseEntity,
        public pricePerHour?: BaseEntity,
    ) {
    }
}
