import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PsClubManagerSharedModule } from '../../shared';
import {
    ExtraJoystickService,
    ExtraJoystickPopupService,
    ExtraJoystickComponent,
    ExtraJoystickDetailComponent,
    ExtraJoystickDialogComponent,
    ExtraJoystickPopupComponent,
    ExtraJoystickDeletePopupComponent,
    ExtraJoystickDeleteDialogComponent,
    extraJoystickRoute,
    extraJoystickPopupRoute,
} from './';

const ENTITY_STATES = [
    ...extraJoystickRoute,
    ...extraJoystickPopupRoute,
];

@NgModule({
    imports: [
        PsClubManagerSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ExtraJoystickComponent,
        ExtraJoystickDetailComponent,
        ExtraJoystickDialogComponent,
        ExtraJoystickDeleteDialogComponent,
        ExtraJoystickPopupComponent,
        ExtraJoystickDeletePopupComponent,
    ],
    entryComponents: [
        ExtraJoystickComponent,
        ExtraJoystickDialogComponent,
        ExtraJoystickPopupComponent,
        ExtraJoystickDeleteDialogComponent,
        ExtraJoystickDeletePopupComponent,
    ],
    providers: [
        ExtraJoystickService,
        ExtraJoystickPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PsClubManagerExtraJoystickModule {}
