import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ExtraJoystick } from './extra-joystick.model';
import { ExtraJoystickPopupService } from './extra-joystick-popup.service';
import { ExtraJoystickService } from './extra-joystick.service';

@Component({
    selector: 'jhi-extra-joystick-delete-dialog',
    templateUrl: './extra-joystick-delete-dialog.component.html'
})
export class ExtraJoystickDeleteDialogComponent {

    extraJoystick: ExtraJoystick;

    constructor(
        private extraJoystickService: ExtraJoystickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.extraJoystickService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'extraJoystickListModification',
                content: 'Deleted an extraJoystick'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-extra-joystick-delete-popup',
    template: ''
})
export class ExtraJoystickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private extraJoystickPopupService: ExtraJoystickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.extraJoystickPopupService
                .open(ExtraJoystickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
