import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ExtraJoystick } from './extra-joystick.model';
import { ExtraJoystickPopupService } from './extra-joystick-popup.service';
import { ExtraJoystickService } from './extra-joystick.service';
import { Session, SessionService } from '../session';
import { ClubJoystick, ClubJoystickService } from '../club-joystick';
import { Tariff, TariffService } from '../tariff';

@Component({
    selector: 'jhi-extra-joystick-dialog',
    templateUrl: './extra-joystick-dialog.component.html'
})
export class ExtraJoystickDialogComponent implements OnInit {

    extraJoystick: ExtraJoystick;
    isSaving: boolean;

    sessions: Session[];

    clubjoysticks: ClubJoystick[];

    tariffs: Tariff[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private extraJoystickService: ExtraJoystickService,
        private sessionService: SessionService,
        private clubJoystickService: ClubJoystickService,
        private tariffService: TariffService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.sessionService.query()
            .subscribe((res: HttpResponse<Session[]>) => { this.sessions = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.clubJoystickService.query()
            .subscribe((res: HttpResponse<ClubJoystick[]>) => { this.clubjoysticks = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.tariffService.query()
            .subscribe((res: HttpResponse<Tariff[]>) => { this.tariffs = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.extraJoystick.id !== undefined) {
            this.subscribeToSaveResponse(
                this.extraJoystickService.update(this.extraJoystick));
        } else {
            this.subscribeToSaveResponse(
                this.extraJoystickService.create(this.extraJoystick));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ExtraJoystick>>) {
        result.subscribe((res: HttpResponse<ExtraJoystick>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ExtraJoystick) {
        this.eventManager.broadcast({ name: 'extraJoystickListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackSessionById(index: number, item: Session) {
        return item.id;
    }

    trackClubJoystickById(index: number, item: ClubJoystick) {
        return item.id;
    }

    trackTariffById(index: number, item: Tariff) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-extra-joystick-popup',
    template: ''
})
export class ExtraJoystickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private extraJoystickPopupService: ExtraJoystickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.extraJoystickPopupService
                    .open(ExtraJoystickDialogComponent as Component, params['id']);
            } else {
                this.extraJoystickPopupService
                    .open(ExtraJoystickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
