import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { ExtraJoystick } from './extra-joystick.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<ExtraJoystick>;

@Injectable()
export class ExtraJoystickService {

    private resourceUrl =  SERVER_API_URL + 'api/extra-joysticks';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/extra-joysticks';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(extraJoystick: ExtraJoystick): Observable<EntityResponseType> {
        const copy = this.convert(extraJoystick);
        return this.http.post<ExtraJoystick>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(extraJoystick: ExtraJoystick): Observable<EntityResponseType> {
        const copy = this.convert(extraJoystick);
        return this.http.put<ExtraJoystick>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ExtraJoystick>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ExtraJoystick[]>> {
        const options = createRequestOption(req);
        return this.http.get<ExtraJoystick[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ExtraJoystick[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<ExtraJoystick[]>> {
        const options = createRequestOption(req);
        return this.http.get<ExtraJoystick[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ExtraJoystick[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ExtraJoystick = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ExtraJoystick[]>): HttpResponse<ExtraJoystick[]> {
        const jsonResponse: ExtraJoystick[] = res.body;
        const body: ExtraJoystick[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ExtraJoystick.
     */
    private convertItemFromServer(extraJoystick: ExtraJoystick): ExtraJoystick {
        const copy: ExtraJoystick = Object.assign({}, extraJoystick);
        copy.startedAt = this.dateUtils
            .convertDateTimeFromServer(extraJoystick.startedAt);
        copy.finishedAt = this.dateUtils
            .convertDateTimeFromServer(extraJoystick.finishedAt);
        return copy;
    }

    /**
     * Convert a ExtraJoystick to a JSON which can be sent to the server.
     */
    private convert(extraJoystick: ExtraJoystick): ExtraJoystick {
        const copy: ExtraJoystick = Object.assign({}, extraJoystick);

        copy.startedAt = this.dateUtils.toDate(extraJoystick.startedAt);

        copy.finishedAt = this.dateUtils.toDate(extraJoystick.finishedAt);
        return copy;
    }
}
