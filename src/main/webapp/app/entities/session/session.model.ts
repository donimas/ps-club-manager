import { BaseEntity } from './../../shared';
import {PricePackage} from '../price-package/price-package.model';

export const enum SessionStatus {
    'STARTED',
    'STOPPED',
    'BOOKED'
}

export class Session implements BaseEntity {
    constructor(
        public id?: number,
        public startedAt?: any,
        public finishedAt?: any,
        public playedHours?: number,
        public playedMinutes?: number,
        public plannedHours?: number,
        public plannedMinutes?: number,
        public amount?: number,
        public status?: SessionStatus,
        public discountPercent?: number,
        public discountMinutes?: number,
        public flagOpenedTime?: boolean,
        public flagCancelled?: boolean,
        public plannedPrice?: number,
        public totalPrice?: number,
        public zone?: BaseEntity,
        public pricePackage?: PricePackage,
        public pricePerHour?: BaseEntity,
        public client?: BaseEntity,
        public extraJoysticks?: BaseEntity[],
    ) {
        flagOpenedTime = false;
        flagCancelled = false;
    }
}
