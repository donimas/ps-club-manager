import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService, JhiDateUtils} from 'ng-jhipster';

import { Session } from './session.model';
import { SessionPopupService } from './session-popup.service';
import { SessionService } from './session.service';
import { Zone, ZoneService } from '../zone';
import { PricePackage, PricePackageService } from '../price-package';
import { Tariff, TariffService } from '../tariff';
import { Client, ClientService } from '../client';
import {DurationType} from "../price-package/price-package.model";
import {JoyTimeService} from '../../shared/joy-time/joy-time.service';
import {DatePipe} from '@angular/common';
import {isUndefined} from "util";

@Component({
    selector: 'jhi-session-dialog',
    templateUrl: './session-dialog.component.html'
})
export class SessionDialogComponent implements OnInit {

    session: Session;
    isSaving: boolean;

    // from DayPilot
    startArg: any;
    endArg: any;

    // converted to Date
    startDate: any;
    endDate: any;

    // converted to datepicker
    startDatePicker: any;
    endDatePicker: any;

    startTimeText: string;
    endTimeText: string;

    zones: Zone[];
    zone: Zone;

    pricepackages: PricePackage[];
    tariffs: Tariff[];
    clients: Client[];
    otherSessions: Session[] = [];

    flagTimeSlider: boolean = false;

    durationH: any;
    durationHConfig: any = {
        behaviour: 'drag',
        connect: false,
        start: 1,
        keyboard: true,  // same as [keyboard]="true"
        tooltips: true,
        step: 1,
        pageSteps: 10,  // number of page steps, defaults to 10
        range: {
            min: 0,
            max: 23
        },
        pips: {
            mode: 'steps',
            density: 4,
            stepped: true
        }
    };

    durationM: any;
    durationMConfig: any = {
        behaviour: 'drag',
        connect: false,
        start: 0,
        keyboard: true,  // same as [keyboard]="true"
        tooltips: true,
        step: 5,
        range: {
            min: 0,
            max: 59
        },
        pips: {
            mode: 'steps',
            density: 10
        }
    };

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private sessionService: SessionService,
        private zoneService: ZoneService,
        private pricePackageService: PricePackageService,
        private tariffService: TariffService,
        private clientService: ClientService,
        private eventManager: JhiEventManager,
        private joyTimeService: JoyTimeService,
        private datePipe: DatePipe,
        private dateUtils: JhiDateUtils,
    ) {
    }

    ngOnInit() {
        this.isSaving = false;

        this.startDate = new Date(this.startArg);
        this.endDate = new Date(this.endArg);

        this.startDatePicker = this.convertDate(this.startDate);
        this.endDatePicker = this.convertDate(this.endDate);

        console.log(this.startDate);
        console.log(this.endDate);
        this.startTimeText = this.convertTimeToText(this.startDate);
        this.endTimeText = this.convertTimeToText(this.endDate);
        console.log(this.startTimeText);
        console.log(this.endTimeText);
        this.tariffService.query()
            .subscribe((res: HttpResponse<Tariff[]>) => { this.tariffs = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.clientService.query()
            .subscribe((res: HttpResponse<Client[]>) => { this.clients = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    convertDate(date: Date): any {
        return {
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            day: date.getDate()
        };
    }

    convertTimeToText(date: Date): string {
        return `${date.getHours()}:${date.getMinutes()}`;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        console.log(this.session);
        console.log('arg', this.startArg);
        console.log('date', this.startDate);
        /*this.isSaving = true;
        if (this.session.id !== undefined) {
            this.subscribeToSaveResponse(
                this.sessionService.update(this.session));
        } else {
            this.subscribeToSaveResponse(
                this.sessionService.create(this.session));
        }*/
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Session>>) {
        result.subscribe((res: HttpResponse<Session>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Session) {
        this.eventManager.broadcast({ name: 'sessionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackZoneById(index: number, item: Zone) {
        return item.id;
    }

    trackPricePackageById(index: number, item: PricePackage) {
        return item.id;
    }

    trackTariffById(index: number, item: Tariff) {
        return item.id;
    }

    trackClientById(index: number, item: Client) {
        return item.id;
    }

    addPricePackage(pricePackage: PricePackage) {
        if(!this.session.pricePackage) {
            this.setMainSession(pricePackage);
            return;
        }

        this.setUnit(pricePackage).then((res: Session) => {
            console.log('other packages setted', pricePackage);
            res.pricePackage = pricePackage;
            this.otherSessions.push(res);
            /*if(pricePackage.type+'' === 'SERVICE') {
            } else {
            }*/
        });
    }

    setMainSession(pricePackage: PricePackage) {
        this.setUnit(pricePackage).then((res: Session) => {
            this.session = res;
            console.log('main session inited', this.session);
            this.session.pricePackage = pricePackage;

            if(pricePackage.type+'' === 'MINUTES' || pricePackage.type+'' === 'SERVICE') {
                this.endArg = this.calculateFinishTime(this.session, this.session.pricePackage.minutesAmount);
            }
            setTimeout((e) => {
                console.log('timeout', this.endArg);
            });

        }, (err) => console.log(err));
    }

    calculateFinishTime(session: Session, minutesAmount: number): string {
        console.log('request to set start finish time', session);
        let tempDate = new Date(this.startDate.getTime() + (minutesAmount * 60 * 1000));
        console.log('temp date', tempDate);
        // 2018-04-27T14:15:00
        const result = this.datePipe.transform(tempDate, 'yyyy-MM-ddTHH:mm:ss');
        console.log('result:', result);
        return result;
    }

    parseDateToStringWithFormat(date: Date): string {
        let result: string;
        let dd = date.getDate().toString();
        let mm = (date.getMonth() + 1).toString();
        let hh = date.getHours().toString();
        let min = date.getMinutes().toString();
        dd = dd.length === 2 ? dd : "0" + dd;
        mm = mm.length === 2 ? mm : "0" + mm;
        hh = hh.length === 2 ? hh : "0" + hh;
        min = min.length === 2 ? min : "0" + min;
        result = [date.getFullYear(), '-', mm, '-', dd, 'T', hh, ':', min, ':', '00'].join('');

        return result;
    }

    setUnit(pricePackage: PricePackage): Promise<Session> {
        console.log('setting unit', pricePackage);
        return new Promise<Session>((resolve, reject) => {
            let result: Session = new Session();
            let duration: any;
            switch(pricePackage.type+'') {
                case 'AN_HOUR':
                    duration = this.joyTimeService.getDurationFromHM({
                        startHour: this.startDate.getHours(),
                        startMinute: this.startDate.getMinutes(),
                        finishHour: this.endDate.getHours(),
                        finishMinute: this.endDate.getMinutes()
                    });
                    result.plannedHours = duration.hour;
                    result.plannedMinutes = duration.minute;
                    result.plannedPrice = pricePackage.price;

                    break;
                case 'MINUTES':
                    duration = this.joyTimeService.minutesToTimePicker(pricePackage.minutesAmount);
                    result.plannedHours = duration.hour;
                    result.plannedMinutes = duration.minute;
                    result.plannedPrice = pricePackage.price;

                    break;
                case 'STARTEND':
                    duration = this.joyTimeService.getDurationFromHM(pricePackage);
                    result.plannedHours = duration.hour;
                    result.plannedMinutes = duration.minute;
                    result.plannedPrice = pricePackage.price;

                    break;
                case 'SERVICE':
                    duration = this.joyTimeService.minutesToTimePicker(pricePackage.minutesAmount);
                    result.plannedHours = duration.hour;
                    result.plannedMinutes = duration.minute;
                    result.plannedPrice = pricePackage.price;
                    break;
                default: reject(result);
            }

            resolve(result);
        });
    }

    lockTime(session: Session) {
        session.flagOpenedTime = !session.flagOpenedTime;
    }

    showTimeSlider() {
        this.durationHConfig.start = this.session.plannedHours;
        this.durationMConfig.start = this.session.plannedMinutes;
        this.flagTimeSlider = true;
    }

    setDuration() {
        console.log(this.durationH);
        console.log(this.durationM);
        const h = this.durationH;
        const m = this.durationM;
        if(!isUndefined(h)) {
            this.session.plannedHours = h;
        }
        if(!isUndefined(m)) {
            this.session.plannedMinutes = m;
        }
        const minutesAmount = this.session.plannedHours * 60 + this.session.plannedMinutes;
        this.endArg = this.calculateFinishTime(this.session, minutesAmount);
        this.flagTimeSlider = false;
    }
}

@Component({
    selector: 'jhi-session-popup',
    template: ''
})
export class SessionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private sessionPopupService: SessionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.sessionPopupService
                    .open(SessionDialogComponent as Component, params['id']);
            } else {
                this.sessionPopupService
                    .open(SessionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
