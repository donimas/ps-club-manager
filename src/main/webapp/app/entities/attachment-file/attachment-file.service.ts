import { Injectable} from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { AttachmentFile } from './attachment-file.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<AttachmentFile>;

@Injectable()
export class AttachmentFileService {

    private resourceUrl =  SERVER_API_URL + 'api/attachment-files';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/attachment-files';

    constructor(
        private http: HttpClient,
    ) { }

    create(attachmentFile: AttachmentFile): Observable<EntityResponseType> {
        const copy = this.convert(attachmentFile);
        return this.http.post<AttachmentFile>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(attachmentFile: AttachmentFile): Observable<EntityResponseType> {
        const copy = this.convert(attachmentFile);
        return this.http.put<AttachmentFile>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<AttachmentFile>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<AttachmentFile[]>> {
        const options = createRequestOption(req);
        return this.http.get<AttachmentFile[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<AttachmentFile[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<AttachmentFile[]>> {
        const options = createRequestOption(req);
        return this.http.get<AttachmentFile[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<AttachmentFile[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: AttachmentFile = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<AttachmentFile[]>): HttpResponse<AttachmentFile[]> {
        const jsonResponse: AttachmentFile[] = res.body;
        const body: AttachmentFile[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to AttachmentFile.
     */
    private convertItemFromServer(attachmentFile: AttachmentFile): AttachmentFile {
        const copy: AttachmentFile = Object.assign({}, attachmentFile);
        return copy;
    }

    /**
     * Convert a AttachmentFile to a JSON which can be sent to the server.
     */
    private convert(attachmentFile: AttachmentFile): AttachmentFile {
        const copy: AttachmentFile = Object.assign({}, attachmentFile);
        return copy;
    }
}
