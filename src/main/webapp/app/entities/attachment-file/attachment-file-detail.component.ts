import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { AttachmentFile } from './attachment-file.model';
import { AttachmentFileService } from './attachment-file.service';

@Component({
    selector: 'jhi-attachment-file-detail',
    templateUrl: './attachment-file-detail.component.html'
})
export class AttachmentFileDetailComponent implements OnInit, OnDestroy {

    attachmentFile: AttachmentFile;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private attachmentFileService: AttachmentFileService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAttachmentFiles();
    }

    load(id) {
        this.attachmentFileService.find(id)
            .subscribe((attachmentFileResponse: HttpResponse<AttachmentFile>) => {
                this.attachmentFile = attachmentFileResponse.body;
            });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAttachmentFiles() {
        this.eventSubscriber = this.eventManager.subscribe(
            'attachmentFileListModification',
            (response) => this.load(this.attachmentFile.id)
        );
    }
}
