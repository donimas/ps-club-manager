import { BaseEntity } from './../../shared';

export class AttachmentFile implements BaseEntity {
    constructor(
        public id?: number,
        public attachmentContentType?: string,
        public attachment?: any,
        public containerClass?: string,
        public containerId?: number,
        public uid?: string,
        public name?: string,
        public contentType?: string,
        public fileSize?: number,
        public fileHash?: string,
    ) {
    }
}
