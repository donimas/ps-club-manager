import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PsClubManagerSharedModule } from '../../shared';
import {
    AttachmentFileService,
    AttachmentFilePopupService,
    AttachmentFileComponent,
    AttachmentFileDetailComponent,
    AttachmentFileDialogComponent,
    AttachmentFilePopupComponent,
    AttachmentFileDeletePopupComponent,
    AttachmentFileDeleteDialogComponent,
    attachmentFileRoute,
    attachmentFilePopupRoute,
} from './';

const ENTITY_STATES = [
    ...attachmentFileRoute,
    ...attachmentFilePopupRoute,
];

@NgModule({
    imports: [
        PsClubManagerSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        AttachmentFileComponent,
        AttachmentFileDetailComponent,
        AttachmentFileDialogComponent,
        AttachmentFileDeleteDialogComponent,
        AttachmentFilePopupComponent,
        AttachmentFileDeletePopupComponent,
    ],
    entryComponents: [
        AttachmentFileComponent,
        AttachmentFileDialogComponent,
        AttachmentFilePopupComponent,
        AttachmentFileDeleteDialogComponent,
        AttachmentFileDeletePopupComponent,
    ],
    providers: [
        AttachmentFileService,
        AttachmentFilePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PsClubManagerAttachmentFileModule {}
