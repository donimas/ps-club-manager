import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { AttachmentFileComponent } from './attachment-file.component';
import { AttachmentFileDetailComponent } from './attachment-file-detail.component';
import { AttachmentFilePopupComponent } from './attachment-file-dialog.component';
import { AttachmentFileDeletePopupComponent } from './attachment-file-delete-dialog.component';

export const attachmentFileRoute: Routes = [
    {
        path: 'attachment-file',
        component: AttachmentFileComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.attachmentFile.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'attachment-file/:id',
        component: AttachmentFileDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.attachmentFile.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const attachmentFilePopupRoute: Routes = [
    {
        path: 'attachment-file-new',
        component: AttachmentFilePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.attachmentFile.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'attachment-file/:id/edit',
        component: AttachmentFilePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.attachmentFile.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'attachment-file/:id/delete',
        component: AttachmentFileDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.attachmentFile.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
