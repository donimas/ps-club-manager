import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { AttachmentFile } from './attachment-file.model';
import { AttachmentFilePopupService } from './attachment-file-popup.service';
import { AttachmentFileService } from './attachment-file.service';

@Component({
    selector: 'jhi-attachment-file-dialog',
    templateUrl: './attachment-file-dialog.component.html'
})
export class AttachmentFileDialogComponent implements OnInit {

    attachmentFile: AttachmentFile;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private attachmentFileService: AttachmentFileService,
        private elementRef: ElementRef,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.attachmentFile, this.elementRef, field, fieldContentType, idInput);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.attachmentFile.id !== undefined) {
            this.subscribeToSaveResponse(
                this.attachmentFileService.update(this.attachmentFile));
        } else {
            this.subscribeToSaveResponse(
                this.attachmentFileService.create(this.attachmentFile));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<AttachmentFile>>) {
        result.subscribe((res: HttpResponse<AttachmentFile>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: AttachmentFile) {
        this.eventManager.broadcast({ name: 'attachmentFileListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-attachment-file-popup',
    template: ''
})
export class AttachmentFilePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private attachmentFilePopupService: AttachmentFilePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.attachmentFilePopupService
                    .open(AttachmentFileDialogComponent as Component, params['id']);
            } else {
                this.attachmentFilePopupService
                    .open(AttachmentFileDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
