import { BaseEntity } from './../../shared';
import {CityDictionary} from '../city-dictionary/city-dictionary.model';

export class Location implements BaseEntity {
    constructor(
        public id?: number,
        public street?: string,
        public homeNumber?: string,
        public aptNumber?: string,
        public lon?: string,
        public lat?: string,
        public osmId?: number,
        public city?: CityDictionary,
    ) {
        city = new CityDictionary();
    }
}
