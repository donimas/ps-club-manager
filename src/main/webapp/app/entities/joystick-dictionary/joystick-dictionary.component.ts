import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { JoystickDictionary } from './joystick-dictionary.model';
import { JoystickDictionaryService } from './joystick-dictionary.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-joystick-dictionary',
    templateUrl: './joystick-dictionary.component.html'
})
export class JoystickDictionaryComponent implements OnInit, OnDestroy {
joystickDictionaries: JoystickDictionary[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private joystickDictionaryService: JoystickDictionaryService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal
    ) {
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.joystickDictionaryService.search({
                query: this.currentSearch,
                }).subscribe(
                    (res: HttpResponse<JoystickDictionary[]>) => this.joystickDictionaries = res.body,
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
       }
        this.joystickDictionaryService.query().subscribe(
            (res: HttpResponse<JoystickDictionary[]>) => {
                this.joystickDictionaries = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInJoystickDictionaries();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: JoystickDictionary) {
        return item.id;
    }
    registerChangeInJoystickDictionaries() {
        this.eventSubscriber = this.eventManager.subscribe('joystickDictionaryListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
