import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { JoystickDictionary } from './joystick-dictionary.model';
import { JoystickDictionaryService } from './joystick-dictionary.service';

@Injectable()
export class JoystickDictionaryPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private joystickDictionaryService: JoystickDictionaryService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.joystickDictionaryService.find(id)
                    .subscribe((joystickDictionaryResponse: HttpResponse<JoystickDictionary>) => {
                        const joystickDictionary: JoystickDictionary = joystickDictionaryResponse.body;
                        this.ngbModalRef = this.joystickDictionaryModalRef(component, joystickDictionary);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.joystickDictionaryModalRef(component, new JoystickDictionary());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    joystickDictionaryModalRef(component: Component, joystickDictionary: JoystickDictionary): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.joystickDictionary = joystickDictionary;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
