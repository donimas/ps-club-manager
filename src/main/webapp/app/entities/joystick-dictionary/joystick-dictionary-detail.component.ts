import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { JoystickDictionary } from './joystick-dictionary.model';
import { JoystickDictionaryService } from './joystick-dictionary.service';

@Component({
    selector: 'jhi-joystick-dictionary-detail',
    templateUrl: './joystick-dictionary-detail.component.html'
})
export class JoystickDictionaryDetailComponent implements OnInit, OnDestroy {

    joystickDictionary: JoystickDictionary;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private joystickDictionaryService: JoystickDictionaryService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInJoystickDictionaries();
    }

    load(id) {
        this.joystickDictionaryService.find(id)
            .subscribe((joystickDictionaryResponse: HttpResponse<JoystickDictionary>) => {
                this.joystickDictionary = joystickDictionaryResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInJoystickDictionaries() {
        this.eventSubscriber = this.eventManager.subscribe(
            'joystickDictionaryListModification',
            (response) => this.load(this.joystickDictionary.id)
        );
    }
}
