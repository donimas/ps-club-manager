import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { JoystickDictionary } from './joystick-dictionary.model';
import { JoystickDictionaryPopupService } from './joystick-dictionary-popup.service';
import { JoystickDictionaryService } from './joystick-dictionary.service';

@Component({
    selector: 'jhi-joystick-dictionary-delete-dialog',
    templateUrl: './joystick-dictionary-delete-dialog.component.html'
})
export class JoystickDictionaryDeleteDialogComponent {

    joystickDictionary: JoystickDictionary;

    constructor(
        private joystickDictionaryService: JoystickDictionaryService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.joystickDictionaryService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'joystickDictionaryListModification',
                content: 'Deleted an joystickDictionary'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-joystick-dictionary-delete-popup',
    template: ''
})
export class JoystickDictionaryDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private joystickDictionaryPopupService: JoystickDictionaryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.joystickDictionaryPopupService
                .open(JoystickDictionaryDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
