import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PsClubManagerSharedModule } from '../../shared';
import {
    JoystickDictionaryService,
    JoystickDictionaryPopupService,
    JoystickDictionaryComponent,
    JoystickDictionaryDetailComponent,
    JoystickDictionaryDialogComponent,
    JoystickDictionaryPopupComponent,
    JoystickDictionaryDeletePopupComponent,
    JoystickDictionaryDeleteDialogComponent,
    joystickDictionaryRoute,
    joystickDictionaryPopupRoute,
} from './';

const ENTITY_STATES = [
    ...joystickDictionaryRoute,
    ...joystickDictionaryPopupRoute,
];

@NgModule({
    imports: [
        PsClubManagerSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        JoystickDictionaryComponent,
        JoystickDictionaryDetailComponent,
        JoystickDictionaryDialogComponent,
        JoystickDictionaryDeleteDialogComponent,
        JoystickDictionaryPopupComponent,
        JoystickDictionaryDeletePopupComponent,
    ],
    entryComponents: [
        JoystickDictionaryComponent,
        JoystickDictionaryDialogComponent,
        JoystickDictionaryPopupComponent,
        JoystickDictionaryDeleteDialogComponent,
        JoystickDictionaryDeletePopupComponent,
    ],
    providers: [
        JoystickDictionaryService,
        JoystickDictionaryPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PsClubManagerJoystickDictionaryModule {}
