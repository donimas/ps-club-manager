import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { JoystickDictionary } from './joystick-dictionary.model';
import { JoystickDictionaryPopupService } from './joystick-dictionary-popup.service';
import { JoystickDictionaryService } from './joystick-dictionary.service';
import { AttachmentFile, AttachmentFileService } from '../attachment-file';

@Component({
    selector: 'jhi-joystick-dictionary-dialog',
    templateUrl: './joystick-dictionary-dialog.component.html'
})
export class JoystickDictionaryDialogComponent implements OnInit {

    joystickDictionary: JoystickDictionary;
    isSaving: boolean;

    attachmentfiles: AttachmentFile[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private joystickDictionaryService: JoystickDictionaryService,
        private attachmentFileService: AttachmentFileService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.attachmentFileService.query()
            .subscribe((res: HttpResponse<AttachmentFile[]>) => { this.attachmentfiles = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.joystickDictionary.id !== undefined) {
            this.subscribeToSaveResponse(
                this.joystickDictionaryService.update(this.joystickDictionary));
        } else {
            this.subscribeToSaveResponse(
                this.joystickDictionaryService.create(this.joystickDictionary));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<JoystickDictionary>>) {
        result.subscribe((res: HttpResponse<JoystickDictionary>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: JoystickDictionary) {
        this.eventManager.broadcast({ name: 'joystickDictionaryListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAttachmentFileById(index: number, item: AttachmentFile) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-joystick-dictionary-popup',
    template: ''
})
export class JoystickDictionaryPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private joystickDictionaryPopupService: JoystickDictionaryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.joystickDictionaryPopupService
                    .open(JoystickDictionaryDialogComponent as Component, params['id']);
            } else {
                this.joystickDictionaryPopupService
                    .open(JoystickDictionaryDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
