import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JoystickDictionaryComponent } from './joystick-dictionary.component';
import { JoystickDictionaryDetailComponent } from './joystick-dictionary-detail.component';
import { JoystickDictionaryPopupComponent } from './joystick-dictionary-dialog.component';
import { JoystickDictionaryDeletePopupComponent } from './joystick-dictionary-delete-dialog.component';

export const joystickDictionaryRoute: Routes = [
    {
        path: 'joystick-dictionary',
        component: JoystickDictionaryComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.joystickDictionary.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'joystick-dictionary/:id',
        component: JoystickDictionaryDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.joystickDictionary.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const joystickDictionaryPopupRoute: Routes = [
    {
        path: 'joystick-dictionary-new',
        component: JoystickDictionaryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.joystickDictionary.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'joystick-dictionary/:id/edit',
        component: JoystickDictionaryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.joystickDictionary.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'joystick-dictionary/:id/delete',
        component: JoystickDictionaryDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.joystickDictionary.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
