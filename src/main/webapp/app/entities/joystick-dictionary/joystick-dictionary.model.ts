import { BaseEntity } from './../../shared';

export class JoystickDictionary implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public code?: string,
        public avatar?: BaseEntity,
    ) {
    }
}
