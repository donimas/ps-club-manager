import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { CityDictionary } from './city-dictionary.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<CityDictionary>;

@Injectable()
export class CityDictionaryService {

    private resourceUrl =  SERVER_API_URL + 'api/city-dictionaries';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/city-dictionaries';

    constructor(private http: HttpClient) { }

    create(cityDictionary: CityDictionary): Observable<EntityResponseType> {
        const copy = this.convert(cityDictionary);
        return this.http.post<CityDictionary>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(cityDictionary: CityDictionary): Observable<EntityResponseType> {
        const copy = this.convert(cityDictionary);
        return this.http.put<CityDictionary>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<CityDictionary>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<CityDictionary[]>> {
        const options = createRequestOption(req);
        return this.http.get<CityDictionary[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<CityDictionary[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<CityDictionary[]>> {
        const options = createRequestOption(req);
        return this.http.get<CityDictionary[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<CityDictionary[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: CityDictionary = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<CityDictionary[]>): HttpResponse<CityDictionary[]> {
        const jsonResponse: CityDictionary[] = res.body;
        const body: CityDictionary[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to CityDictionary.
     */
    private convertItemFromServer(cityDictionary: CityDictionary): CityDictionary {
        const copy: CityDictionary = Object.assign({}, cityDictionary);
        return copy;
    }

    /**
     * Convert a CityDictionary to a JSON which can be sent to the server.
     */
    private convert(cityDictionary: CityDictionary): CityDictionary {
        const copy: CityDictionary = Object.assign({}, cityDictionary);
        return copy;
    }
}
