import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CityDictionary } from './city-dictionary.model';
import { CityDictionaryPopupService } from './city-dictionary-popup.service';
import { CityDictionaryService } from './city-dictionary.service';

@Component({
    selector: 'jhi-city-dictionary-dialog',
    templateUrl: './city-dictionary-dialog.component.html'
})
export class CityDictionaryDialogComponent implements OnInit {

    cityDictionary: CityDictionary;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private cityDictionaryService: CityDictionaryService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.cityDictionary.id !== undefined) {
            this.subscribeToSaveResponse(
                this.cityDictionaryService.update(this.cityDictionary));
        } else {
            this.subscribeToSaveResponse(
                this.cityDictionaryService.create(this.cityDictionary));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<CityDictionary>>) {
        result.subscribe((res: HttpResponse<CityDictionary>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: CityDictionary) {
        this.eventManager.broadcast({ name: 'cityDictionaryListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-city-dictionary-popup',
    template: ''
})
export class CityDictionaryPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private cityDictionaryPopupService: CityDictionaryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.cityDictionaryPopupService
                    .open(CityDictionaryDialogComponent as Component, params['id']);
            } else {
                this.cityDictionaryPopupService
                    .open(CityDictionaryDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
