import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CityDictionary } from './city-dictionary.model';
import { CityDictionaryPopupService } from './city-dictionary-popup.service';
import { CityDictionaryService } from './city-dictionary.service';

@Component({
    selector: 'jhi-city-dictionary-delete-dialog',
    templateUrl: './city-dictionary-delete-dialog.component.html'
})
export class CityDictionaryDeleteDialogComponent {

    cityDictionary: CityDictionary;

    constructor(
        private cityDictionaryService: CityDictionaryService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.cityDictionaryService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'cityDictionaryListModification',
                content: 'Deleted an cityDictionary'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-city-dictionary-delete-popup',
    template: ''
})
export class CityDictionaryDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private cityDictionaryPopupService: CityDictionaryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.cityDictionaryPopupService
                .open(CityDictionaryDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
