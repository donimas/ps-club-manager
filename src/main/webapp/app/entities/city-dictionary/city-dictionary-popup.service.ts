import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { CityDictionary } from './city-dictionary.model';
import { CityDictionaryService } from './city-dictionary.service';

@Injectable()
export class CityDictionaryPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private cityDictionaryService: CityDictionaryService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.cityDictionaryService.find(id)
                    .subscribe((cityDictionaryResponse: HttpResponse<CityDictionary>) => {
                        const cityDictionary: CityDictionary = cityDictionaryResponse.body;
                        this.ngbModalRef = this.cityDictionaryModalRef(component, cityDictionary);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.cityDictionaryModalRef(component, new CityDictionary());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    cityDictionaryModalRef(component: Component, cityDictionary: CityDictionary): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.cityDictionary = cityDictionary;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
