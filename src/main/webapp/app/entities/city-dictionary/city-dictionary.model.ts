import { BaseEntity } from './../../shared';

export class CityDictionary implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public code?: string,
        public postalCode?: string,
        public lat?: string,
        public lon?: string,
    ) {
    }
}
