import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PsClubManagerSharedModule } from '../../shared';
import {
    CityDictionaryService,
    CityDictionaryPopupService,
    CityDictionaryComponent,
    CityDictionaryDetailComponent,
    CityDictionaryDialogComponent,
    CityDictionaryPopupComponent,
    CityDictionaryDeletePopupComponent,
    CityDictionaryDeleteDialogComponent,
    cityDictionaryRoute,
    cityDictionaryPopupRoute,
    CityDictionaryResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...cityDictionaryRoute,
    ...cityDictionaryPopupRoute,
];

@NgModule({
    imports: [
        PsClubManagerSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CityDictionaryComponent,
        CityDictionaryDetailComponent,
        CityDictionaryDialogComponent,
        CityDictionaryDeleteDialogComponent,
        CityDictionaryPopupComponent,
        CityDictionaryDeletePopupComponent,
    ],
    entryComponents: [
        CityDictionaryComponent,
        CityDictionaryDialogComponent,
        CityDictionaryPopupComponent,
        CityDictionaryDeleteDialogComponent,
        CityDictionaryDeletePopupComponent,
    ],
    providers: [
        CityDictionaryService,
        CityDictionaryPopupService,
        CityDictionaryResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PsClubManagerCityDictionaryModule {}
