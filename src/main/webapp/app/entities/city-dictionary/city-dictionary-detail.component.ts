import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { CityDictionary } from './city-dictionary.model';
import { CityDictionaryService } from './city-dictionary.service';

@Component({
    selector: 'jhi-city-dictionary-detail',
    templateUrl: './city-dictionary-detail.component.html'
})
export class CityDictionaryDetailComponent implements OnInit, OnDestroy {

    cityDictionary: CityDictionary;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private cityDictionaryService: CityDictionaryService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCityDictionaries();
    }

    load(id) {
        this.cityDictionaryService.find(id)
            .subscribe((cityDictionaryResponse: HttpResponse<CityDictionary>) => {
                this.cityDictionary = cityDictionaryResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCityDictionaries() {
        this.eventSubscriber = this.eventManager.subscribe(
            'cityDictionaryListModification',
            (response) => this.load(this.cityDictionary.id)
        );
    }
}
