import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { CityDictionaryComponent } from './city-dictionary.component';
import { CityDictionaryDetailComponent } from './city-dictionary-detail.component';
import { CityDictionaryPopupComponent } from './city-dictionary-dialog.component';
import { CityDictionaryDeletePopupComponent } from './city-dictionary-delete-dialog.component';

@Injectable()
export class CityDictionaryResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const cityDictionaryRoute: Routes = [
    {
        path: 'city-dictionary',
        component: CityDictionaryComponent,
        resolve: {
            'pagingParams': CityDictionaryResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.cityDictionary.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'city-dictionary/:id',
        component: CityDictionaryDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.cityDictionary.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const cityDictionaryPopupRoute: Routes = [
    {
        path: 'city-dictionary-new',
        component: CityDictionaryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.cityDictionary.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'city-dictionary/:id/edit',
        component: CityDictionaryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.cityDictionary.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'city-dictionary/:id/delete',
        component: CityDictionaryDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.cityDictionary.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
