import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PsClubManagerSharedModule } from '../../shared';
import {
    GamePleasureService,
    GamePleasurePopupService,
    GamePleasureComponent,
    GamePleasureDetailComponent,
    GamePleasureDialogComponent,
    GamePleasurePopupComponent,
    GamePleasureDeletePopupComponent,
    GamePleasureDeleteDialogComponent,
    gamePleasureRoute,
    gamePleasurePopupRoute,
    GamePleasureResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...gamePleasureRoute,
    ...gamePleasurePopupRoute,
];

@NgModule({
    imports: [
        PsClubManagerSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        GamePleasureComponent,
        GamePleasureDetailComponent,
        GamePleasureDialogComponent,
        GamePleasureDeleteDialogComponent,
        GamePleasurePopupComponent,
        GamePleasureDeletePopupComponent,
    ],
    entryComponents: [
        GamePleasureComponent,
        GamePleasureDialogComponent,
        GamePleasurePopupComponent,
        GamePleasureDeleteDialogComponent,
        GamePleasureDeletePopupComponent,
    ],
    providers: [
        GamePleasureService,
        GamePleasurePopupService,
        GamePleasureResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PsClubManagerGamePleasureModule {}
