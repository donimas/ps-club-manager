import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { GamePleasure } from './game-pleasure.model';
import { GamePleasurePopupService } from './game-pleasure-popup.service';
import { GamePleasureService } from './game-pleasure.service';

@Component({
    selector: 'jhi-game-pleasure-delete-dialog',
    templateUrl: './game-pleasure-delete-dialog.component.html'
})
export class GamePleasureDeleteDialogComponent {

    gamePleasure: GamePleasure;

    constructor(
        private gamePleasureService: GamePleasureService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.gamePleasureService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'gamePleasureListModification',
                content: 'Deleted an gamePleasure'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-game-pleasure-delete-popup',
    template: ''
})
export class GamePleasureDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private gamePleasurePopupService: GamePleasurePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.gamePleasurePopupService
                .open(GamePleasureDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
