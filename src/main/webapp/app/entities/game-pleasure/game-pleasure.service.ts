import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { GamePleasure } from './game-pleasure.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<GamePleasure>;

@Injectable()
export class GamePleasureService {

    private resourceUrl =  SERVER_API_URL + 'api/game-pleasures';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/game-pleasures';

    constructor(private http: HttpClient) { }

    create(gamePleasure: GamePleasure): Observable<EntityResponseType> {
        const copy = this.convert(gamePleasure);
        return this.http.post<GamePleasure>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(gamePleasure: GamePleasure): Observable<EntityResponseType> {
        const copy = this.convert(gamePleasure);
        return this.http.put<GamePleasure>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<GamePleasure>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<GamePleasure[]>> {
        const options = createRequestOption(req);
        return this.http.get<GamePleasure[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<GamePleasure[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<GamePleasure[]>> {
        const options = createRequestOption(req);
        return this.http.get<GamePleasure[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<GamePleasure[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: GamePleasure = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<GamePleasure[]>): HttpResponse<GamePleasure[]> {
        const jsonResponse: GamePleasure[] = res.body;
        const body: GamePleasure[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to GamePleasure.
     */
    private convertItemFromServer(gamePleasure: GamePleasure): GamePleasure {
        const copy: GamePleasure = Object.assign({}, gamePleasure);
        return copy;
    }

    /**
     * Convert a GamePleasure to a JSON which can be sent to the server.
     */
    private convert(gamePleasure: GamePleasure): GamePleasure {
        const copy: GamePleasure = Object.assign({}, gamePleasure);
        return copy;
    }
}
