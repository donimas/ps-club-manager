import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { GamePleasure } from './game-pleasure.model';
import { GamePleasureService } from './game-pleasure.service';

@Component({
    selector: 'jhi-game-pleasure-detail',
    templateUrl: './game-pleasure-detail.component.html'
})
export class GamePleasureDetailComponent implements OnInit, OnDestroy {

    gamePleasure: GamePleasure;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private gamePleasureService: GamePleasureService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInGamePleasures();
    }

    load(id) {
        this.gamePleasureService.find(id)
            .subscribe((gamePleasureResponse: HttpResponse<GamePleasure>) => {
                this.gamePleasure = gamePleasureResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInGamePleasures() {
        this.eventSubscriber = this.eventManager.subscribe(
            'gamePleasureListModification',
            (response) => this.load(this.gamePleasure.id)
        );
    }
}
