import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { GamePleasure } from './game-pleasure.model';
import { GamePleasureService } from './game-pleasure.service';

@Injectable()
export class GamePleasurePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private gamePleasureService: GamePleasureService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.gamePleasureService.find(id)
                    .subscribe((gamePleasureResponse: HttpResponse<GamePleasure>) => {
                        const gamePleasure: GamePleasure = gamePleasureResponse.body;
                        this.ngbModalRef = this.gamePleasureModalRef(component, gamePleasure);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.gamePleasureModalRef(component, new GamePleasure());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    gamePleasureModalRef(component: Component, gamePleasure: GamePleasure): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.gamePleasure = gamePleasure;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
