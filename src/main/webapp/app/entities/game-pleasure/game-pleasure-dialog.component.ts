import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { GamePleasure } from './game-pleasure.model';
import { GamePleasurePopupService } from './game-pleasure-popup.service';
import { GamePleasureService } from './game-pleasure.service';
import { Session, SessionService } from '../session';
import { Club, ClubService } from '../club';
import { Client, ClientService } from '../client';

@Component({
    selector: 'jhi-game-pleasure-dialog',
    templateUrl: './game-pleasure-dialog.component.html'
})
export class GamePleasureDialogComponent implements OnInit {

    gamePleasure: GamePleasure;
    isSaving: boolean;

    sessions: Session[];

    clubs: Club[];

    clients: Client[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private gamePleasureService: GamePleasureService,
        private sessionService: SessionService,
        private clubService: ClubService,
        private clientService: ClientService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.sessionService.query()
            .subscribe((res: HttpResponse<Session[]>) => { this.sessions = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.clubService.query()
            .subscribe((res: HttpResponse<Club[]>) => { this.clubs = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.clientService.query()
            .subscribe((res: HttpResponse<Client[]>) => { this.clients = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.gamePleasure.id !== undefined) {
            this.subscribeToSaveResponse(
                this.gamePleasureService.update(this.gamePleasure));
        } else {
            this.subscribeToSaveResponse(
                this.gamePleasureService.create(this.gamePleasure));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<GamePleasure>>) {
        result.subscribe((res: HttpResponse<GamePleasure>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: GamePleasure) {
        this.eventManager.broadcast({ name: 'gamePleasureListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackSessionById(index: number, item: Session) {
        return item.id;
    }

    trackClubById(index: number, item: Club) {
        return item.id;
    }

    trackClientById(index: number, item: Client) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-game-pleasure-popup',
    template: ''
})
export class GamePleasurePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private gamePleasurePopupService: GamePleasurePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.gamePleasurePopupService
                    .open(GamePleasureDialogComponent as Component, params['id']);
            } else {
                this.gamePleasurePopupService
                    .open(GamePleasureDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
