import { BaseEntity } from './../../shared';

export class GamePleasure implements BaseEntity {
    constructor(
        public id?: number,
        public rating?: number,
        public review?: string,
        public session?: BaseEntity,
        public club?: BaseEntity,
        public client?: BaseEntity,
    ) {
    }
}
