import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { GamePleasureComponent } from './game-pleasure.component';
import { GamePleasureDetailComponent } from './game-pleasure-detail.component';
import { GamePleasurePopupComponent } from './game-pleasure-dialog.component';
import { GamePleasureDeletePopupComponent } from './game-pleasure-delete-dialog.component';

@Injectable()
export class GamePleasureResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const gamePleasureRoute: Routes = [
    {
        path: 'game-pleasure',
        component: GamePleasureComponent,
        resolve: {
            'pagingParams': GamePleasureResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.gamePleasure.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'game-pleasure/:id',
        component: GamePleasureDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.gamePleasure.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const gamePleasurePopupRoute: Routes = [
    {
        path: 'game-pleasure-new',
        component: GamePleasurePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.gamePleasure.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'game-pleasure/:id/edit',
        component: GamePleasurePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.gamePleasure.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'game-pleasure/:id/delete',
        component: GamePleasureDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.gamePleasure.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
