import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ConsoleDictionary } from './console-dictionary.model';
import { ConsoleDictionaryPopupService } from './console-dictionary-popup.service';
import { ConsoleDictionaryService } from './console-dictionary.service';

@Component({
    selector: 'jhi-console-dictionary-delete-dialog',
    templateUrl: './console-dictionary-delete-dialog.component.html'
})
export class ConsoleDictionaryDeleteDialogComponent {

    consoleDictionary: ConsoleDictionary;

    constructor(
        private consoleDictionaryService: ConsoleDictionaryService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.consoleDictionaryService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'consoleDictionaryListModification',
                content: 'Deleted an consoleDictionary'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-console-dictionary-delete-popup',
    template: ''
})
export class ConsoleDictionaryDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private consoleDictionaryPopupService: ConsoleDictionaryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.consoleDictionaryPopupService
                .open(ConsoleDictionaryDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
