import { BaseEntity } from './../../shared';

export class ConsoleDictionary implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public code?: string,
        public avatar?: BaseEntity,
    ) {
    }
}
