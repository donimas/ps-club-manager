import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { ConsoleDictionaryComponent } from './console-dictionary.component';
import { ConsoleDictionaryDetailComponent } from './console-dictionary-detail.component';
import { ConsoleDictionaryPopupComponent } from './console-dictionary-dialog.component';
import { ConsoleDictionaryDeletePopupComponent } from './console-dictionary-delete-dialog.component';

export const consoleDictionaryRoute: Routes = [
    {
        path: 'console-dictionary',
        component: ConsoleDictionaryComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.consoleDictionary.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'console-dictionary/:id',
        component: ConsoleDictionaryDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.consoleDictionary.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const consoleDictionaryPopupRoute: Routes = [
    {
        path: 'console-dictionary-new',
        component: ConsoleDictionaryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.consoleDictionary.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'console-dictionary/:id/edit',
        component: ConsoleDictionaryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.consoleDictionary.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'console-dictionary/:id/delete',
        component: ConsoleDictionaryDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'psClubManagerApp.consoleDictionary.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
