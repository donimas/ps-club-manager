import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ConsoleDictionary } from './console-dictionary.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<ConsoleDictionary>;

@Injectable()
export class ConsoleDictionaryService {

    private resourceUrl =  SERVER_API_URL + 'api/console-dictionaries';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/console-dictionaries';

    constructor(private http: HttpClient) { }

    create(consoleDictionary: ConsoleDictionary): Observable<EntityResponseType> {
        const copy = this.convert(consoleDictionary);
        return this.http.post<ConsoleDictionary>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(consoleDictionary: ConsoleDictionary): Observable<EntityResponseType> {
        const copy = this.convert(consoleDictionary);
        return this.http.put<ConsoleDictionary>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ConsoleDictionary>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ConsoleDictionary[]>> {
        const options = createRequestOption(req);
        return this.http.get<ConsoleDictionary[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ConsoleDictionary[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<ConsoleDictionary[]>> {
        const options = createRequestOption(req);
        return this.http.get<ConsoleDictionary[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ConsoleDictionary[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ConsoleDictionary = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ConsoleDictionary[]>): HttpResponse<ConsoleDictionary[]> {
        const jsonResponse: ConsoleDictionary[] = res.body;
        const body: ConsoleDictionary[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ConsoleDictionary.
     */
    private convertItemFromServer(consoleDictionary: ConsoleDictionary): ConsoleDictionary {
        const copy: ConsoleDictionary = Object.assign({}, consoleDictionary);
        return copy;
    }

    /**
     * Convert a ConsoleDictionary to a JSON which can be sent to the server.
     */
    private convert(consoleDictionary: ConsoleDictionary): ConsoleDictionary {
        const copy: ConsoleDictionary = Object.assign({}, consoleDictionary);
        return copy;
    }
}
