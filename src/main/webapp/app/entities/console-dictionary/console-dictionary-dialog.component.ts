import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ConsoleDictionary } from './console-dictionary.model';
import { ConsoleDictionaryPopupService } from './console-dictionary-popup.service';
import { ConsoleDictionaryService } from './console-dictionary.service';
import { AttachmentFile, AttachmentFileService } from '../attachment-file';

@Component({
    selector: 'jhi-console-dictionary-dialog',
    templateUrl: './console-dictionary-dialog.component.html'
})
export class ConsoleDictionaryDialogComponent implements OnInit {

    consoleDictionary: ConsoleDictionary;
    isSaving: boolean;

    attachmentfiles: AttachmentFile[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private consoleDictionaryService: ConsoleDictionaryService,
        private attachmentFileService: AttachmentFileService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.attachmentFileService.query()
            .subscribe((res: HttpResponse<AttachmentFile[]>) => { this.attachmentfiles = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.consoleDictionary.id !== undefined) {
            this.subscribeToSaveResponse(
                this.consoleDictionaryService.update(this.consoleDictionary));
        } else {
            this.subscribeToSaveResponse(
                this.consoleDictionaryService.create(this.consoleDictionary));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ConsoleDictionary>>) {
        result.subscribe((res: HttpResponse<ConsoleDictionary>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ConsoleDictionary) {
        this.eventManager.broadcast({ name: 'consoleDictionaryListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAttachmentFileById(index: number, item: AttachmentFile) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-console-dictionary-popup',
    template: ''
})
export class ConsoleDictionaryPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private consoleDictionaryPopupService: ConsoleDictionaryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.consoleDictionaryPopupService
                    .open(ConsoleDictionaryDialogComponent as Component, params['id']);
            } else {
                this.consoleDictionaryPopupService
                    .open(ConsoleDictionaryDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
