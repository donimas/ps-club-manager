import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { ConsoleDictionary } from './console-dictionary.model';
import { ConsoleDictionaryService } from './console-dictionary.service';

@Injectable()
export class ConsoleDictionaryPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private consoleDictionaryService: ConsoleDictionaryService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.consoleDictionaryService.find(id)
                    .subscribe((consoleDictionaryResponse: HttpResponse<ConsoleDictionary>) => {
                        const consoleDictionary: ConsoleDictionary = consoleDictionaryResponse.body;
                        this.ngbModalRef = this.consoleDictionaryModalRef(component, consoleDictionary);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.consoleDictionaryModalRef(component, new ConsoleDictionary());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    consoleDictionaryModalRef(component: Component, consoleDictionary: ConsoleDictionary): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.consoleDictionary = consoleDictionary;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
