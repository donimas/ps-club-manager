import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PsClubManagerSharedModule } from '../../shared';
import {
    ConsoleDictionaryService,
    ConsoleDictionaryPopupService,
    ConsoleDictionaryComponent,
    ConsoleDictionaryDetailComponent,
    ConsoleDictionaryDialogComponent,
    ConsoleDictionaryPopupComponent,
    ConsoleDictionaryDeletePopupComponent,
    ConsoleDictionaryDeleteDialogComponent,
    consoleDictionaryRoute,
    consoleDictionaryPopupRoute,
} from './';

const ENTITY_STATES = [
    ...consoleDictionaryRoute,
    ...consoleDictionaryPopupRoute,
];

@NgModule({
    imports: [
        PsClubManagerSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ConsoleDictionaryComponent,
        ConsoleDictionaryDetailComponent,
        ConsoleDictionaryDialogComponent,
        ConsoleDictionaryDeleteDialogComponent,
        ConsoleDictionaryPopupComponent,
        ConsoleDictionaryDeletePopupComponent,
    ],
    entryComponents: [
        ConsoleDictionaryComponent,
        ConsoleDictionaryDialogComponent,
        ConsoleDictionaryPopupComponent,
        ConsoleDictionaryDeleteDialogComponent,
        ConsoleDictionaryDeletePopupComponent,
    ],
    providers: [
        ConsoleDictionaryService,
        ConsoleDictionaryPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PsClubManagerConsoleDictionaryModule {}
