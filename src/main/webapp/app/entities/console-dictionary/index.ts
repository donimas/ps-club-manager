export * from './console-dictionary.model';
export * from './console-dictionary-popup.service';
export * from './console-dictionary.service';
export * from './console-dictionary-dialog.component';
export * from './console-dictionary-delete-dialog.component';
export * from './console-dictionary-detail.component';
export * from './console-dictionary.component';
export * from './console-dictionary.route';
