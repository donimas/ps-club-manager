import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ConsoleDictionary } from './console-dictionary.model';
import { ConsoleDictionaryService } from './console-dictionary.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-console-dictionary',
    templateUrl: './console-dictionary.component.html'
})
export class ConsoleDictionaryComponent implements OnInit, OnDestroy {
consoleDictionaries: ConsoleDictionary[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private consoleDictionaryService: ConsoleDictionaryService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal
    ) {
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.consoleDictionaryService.search({
                query: this.currentSearch,
                }).subscribe(
                    (res: HttpResponse<ConsoleDictionary[]>) => this.consoleDictionaries = res.body,
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
       }
        this.consoleDictionaryService.query().subscribe(
            (res: HttpResponse<ConsoleDictionary[]>) => {
                this.consoleDictionaries = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInConsoleDictionaries();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ConsoleDictionary) {
        return item.id;
    }
    registerChangeInConsoleDictionaries() {
        this.eventSubscriber = this.eventManager.subscribe('consoleDictionaryListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
