import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ConsoleDictionary } from './console-dictionary.model';
import { ConsoleDictionaryService } from './console-dictionary.service';

@Component({
    selector: 'jhi-console-dictionary-detail',
    templateUrl: './console-dictionary-detail.component.html'
})
export class ConsoleDictionaryDetailComponent implements OnInit, OnDestroy {

    consoleDictionary: ConsoleDictionary;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private consoleDictionaryService: ConsoleDictionaryService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInConsoleDictionaries();
    }

    load(id) {
        this.consoleDictionaryService.find(id)
            .subscribe((consoleDictionaryResponse: HttpResponse<ConsoleDictionary>) => {
                this.consoleDictionary = consoleDictionaryResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInConsoleDictionaries() {
        this.eventSubscriber = this.eventManager.subscribe(
            'consoleDictionaryListModification',
            (response) => this.load(this.consoleDictionary.id)
        );
    }
}
