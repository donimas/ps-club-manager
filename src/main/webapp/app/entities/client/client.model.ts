import { BaseEntity } from './../../shared';

export class Client implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public phoneNumber?: string,
        public level?: BaseEntity,
        public club?: BaseEntity,
    ) {
    }
}
