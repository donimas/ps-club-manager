import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DatePipe } from '@angular/common';
import { NouisliderModule } from 'ng2-nouislider';
import { NgSelectModule } from '@ng-select/ng-select'

import {
    PsClubManagerSharedLibsModule,
    PsClubManagerSharedCommonModule,
    CSRFService,
    AuthServerProvider,
    AccountService,
    UserService,
    StateStorageService,
    LoginService,
    LoginModalService,
    JhiLoginModalComponent,
    JoyTimeService,
    Principal,
    JhiTrackerService,
    HasAnyAuthorityDirective,
    JhiSocialComponent,
    SocialService,
    DataService,
    TimesheetComponent,
} from './';
import {DayPilotModule} from 'daypilot-pro-angular';

@NgModule({
    imports: [
        PsClubManagerSharedLibsModule,
        PsClubManagerSharedCommonModule,
        NouisliderModule,
        NgSelectModule,
        DayPilotModule,

    ],
    declarations: [
        JhiSocialComponent,
        JhiLoginModalComponent,
        TimesheetComponent,
        HasAnyAuthorityDirective
    ],
    providers: [
        LoginService,
        LoginModalService,
        JoyTimeService,
        AccountService,
        StateStorageService,
        Principal,
        CSRFService,
        JhiTrackerService,
        AuthServerProvider,
        SocialService,
        UserService,
        DatePipe,
        DataService,
    ],
    entryComponents: [JhiLoginModalComponent],
    exports: [
        PsClubManagerSharedCommonModule,
        JhiSocialComponent,
        JhiLoginModalComponent,
        TimesheetComponent,
        HasAnyAuthorityDirective,
        DatePipe,
        NouisliderModule,
        NgSelectModule,
        DayPilotModule,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class PsClubManagerSharedModule {}
