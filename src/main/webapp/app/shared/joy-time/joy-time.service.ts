import { Injectable } from '@angular/core';

@Injectable()
export class JoyTimeService {

    constructor(
    ) {}

    timePickerToMinutes(time: any): number {
        console.log(time);
        return time.hour * 60 + time.minute;
    }

    minutesToTimePicker(minutes: number): any {
        console.log(minutes);
        const hour = Math.floor(minutes / 60);
        const minute = minutes - hour * 60;

        return {
            hour: hour,
            minute: minute
        };
    }

    sliderDurationToMinutes(duration: number): number {
        return duration * 60;
    }

    minutesToSliderDuration(minutes: number) {
        return minutes / 60;
    }

    clubTimeToSliderTime(oHour: number, oMinute: number, cHour: number, cMinute: number): number[] {
        const openedAt = oHour + this.minutesToSliderDuration(oMinute);
        const closedAt = cHour + this.minutesToSliderDuration(cMinute);
        return [openedAt, closedAt];
    }

    sliderTimeToClubTime(time: number[]): number[] {
        const oHour = Math.floor(time[0]);
        const oMinute = this.sliderDurationToMinutes(time[0] - oHour);
        const cHour = Math.floor(time[1]);
        const cMinute = this.sliderDurationToMinutes(time[1] - cHour);
        return [oHour, oMinute, cHour, cMinute];
    }

    formattedStringTimeToMinutes(timeText: string): any {
        console.log('formattedStringTimeToNumber', timeText);
        if(!timeText) {
            return timeText;
        }
        const time = timeText.split(':');
        const hours: number = parseInt(time[0]);
        const mins: number = parseInt(time[1]);
        return hours * 60 + mins;
    }

    getDurationFromHM(seHM: any): any {
        console.log('request to get duration from hours and minutes', seHM);
        const sHour = seHM.startHour;
        const sMin = seHM.startMinute;
        const fHour = seHM.finishHour;
        const fMin = seHM.finishMinute;
        let rHour = 0;
        if(fHour < sHour) {
            rHour = (24 - sHour) + fHour;
        } else {
            rHour = fHour - sHour;
        }
        let rMin = fMin - sMin;
        if(rMin < 0) {
            rHour--;
            rMin = 60 + rMin;
        }

        return {hour: rHour, minute: rMin};
    }
}
