import {Component, ViewChild, AfterViewInit, Input} from '@angular/core';
import {DayPilot, DayPilotSchedulerComponent} from 'daypilot-pro-angular';
import {DataService} from './data.service';
import {Club} from "../../entities/club/club.model";
import {ScheduleSessionDialogModalService} from '../../entities/scheduler/schedule-session/schedule-session-dialog-modal.service';

{
}

@Component({
    selector: 'timesheet-component',
    template: `
        <daypilot-scheduler [config]='config' [events]='events' #timesheet></daypilot-scheduler>`,
    styles: [``]
})
export class TimesheetComponent implements AfterViewInit {

    @ViewChild('timesheet')
    timesheet: DayPilotSchedulerComponent;

    @Input() club: Club;
    resources: any[] = [];

    events: any[] = [
        {
            'id': '8',
            'text': 'Ms. Williams',
            'start': '2018-04-17T18:00:00',
            'end': '2018-04-17T19:30:00',
            'resource': '1'
        }
    ];

    config: any = {
        locale: 'ru-ru',
        cellWidthSpec: 'Auto',
        crosshairType: 'Header',
        heightSpec: 'Max',
        height: 400,
        timeHeaders: [
            {'groupBy': 'Day', format: 'dddd, d MMMM yyyy'},
            {'groupBy': 'Hour'},
            {'groupBy': 'Cell', 'format': 'mm'}
        ],
        scale: 'CellDuration',
        cellDuration: 15,
        // viewType: 'Days',
        treeEnabled: true,
        resources: this.resources,
        rowHeaderColumns: [
            {title: 'Зоны'},
            {title: 'Итого'}
        ],
        onBeforeRowHeaderRender: args => {
            if (args.row.data.seats && args.row.columns[0]) {
                args.row.columns[0].html = args.row.data.seats + ' seats';
            }
            args.row.horizontalAlignment = 'center';
        },
        onBeforeCellRender: args => {
            let saturday = 6;
            let sunday = 0;
            let dayOfWeek = args.cell.start.dayOfWeek();
            if (dayOfWeek === sunday || dayOfWeek === saturday) {
                args.cell.backColor = '#fcfcfc'; // apply highlighting
            }
        },
        businessBeginsHour: 11,
        businessEndsHour: 22,
        showNonBusiness: false,
        businessWeekends: true,
        floatingEvents: true,
        eventHeight: 30,
        eventMovingStartEndEnabled: false,
        eventResizingStartEndEnabled: false,
        timeRangeSelectingStartEndEnabled: false,
        groupConcurrentEvents: false,
        eventStackingLineHeight: 100,
        allowEventOverlap: true,
        timeRangeSelectedHandling: 'Enabled',
        onTimeRangeSelected: (args) => {
            console.log('time range selected', args);
            var dp = this;
            this.sessionModalDialogService.open(args.start, args.end, this.club, this.club.zoneDtos[0]);
            /*DayPilot.Modal.prompt('Create a new event:', 'Event 1').then(function (modal) {
                dp.clearSelection();
                if (!modal.result) {
                    return;
                }
                dp.events.add(new DayPilot.Event({
                    start: args.start,
                    end: args.end,
                    id: DayPilot.guid(),
                    resource: args.resource,
                    text: modal.result
                }));
            });*/
        },
        eventMoveHandling: 'Update',
        onEventMoved: function (args) {
            this.message('Event moved');
        },
        eventResizeHandling: 'Update',
        onEventResized: function (args) {
            this.message('Event resized');
        },
        eventDeleteHandling: 'Update',
        onEventDeleted: function (args) {
            this.message('Event deleted');
        },
        eventClickHandling: 'Disabled',
        eventHoverHandling: 'Disabled',
        contextMenu: new DayPilot.Menu({
            items: [
                {
                    text: 'Delete', onClick: function (args) {
                    var dp = args.source.calendar;
                    dp.events.remove(args.source);
                }
                }
            ]
        }),
    };

    constructor(
        private ds: DataService,
        private sessionModalDialogService: ScheduleSessionDialogModalService,
        ) {
    }

    ngAfterViewInit(): void {
        console.log('after view init ->', this.club);
        this.club.zoneDtos.forEach((zone) => {
            let resource = {
                id: zone.id,
                name: zone.name,
                expanded: false
            };
            this.resources.push(resource);
        });
        var from = this.timesheet.control.visibleStart();
        var to = this.timesheet.control.visibleEnd();
        this.ds.getEvents(from, to).subscribe(result => {
            this.events = result;
        });
    }

}

