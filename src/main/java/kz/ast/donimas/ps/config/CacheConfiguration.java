package kz.ast.donimas.ps.config;

import io.github.jhipster.config.JHipsterProperties;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;

import java.util.concurrent.TimeUnit;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
@AutoConfigureAfter(value = { MetricsConfiguration.class })
@AutoConfigureBefore(value = { WebConfigurer.class, DatabaseConfiguration.class })
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(kz.ast.donimas.ps.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.SocialUserConnection.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.Club.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.Club.class.getName() + ".consoles", jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.Club.class.getName() + ".games", jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.Club.class.getName() + ".joysticks", jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.Club.class.getName() + ".zones", jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.Club.class.getName() + ".pricePackages", jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.Club.class.getName() + ".clientLevels", jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.Club.class.getName() + ".workRegimes", jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.Zone.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.Client.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.ClientLevel.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.PricePackage.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.Tariff.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.DayRegime.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.Session.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.Session.class.getName() + ".extraJoysticks", jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.ExtraJoystick.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.ZoneDictionary.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.ConsoleDictionary.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.JoystickDictionary.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.GameDictionary.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.ClubJoystick.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.AttachmentFile.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.Club.class.getName() + ".galleries", jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.GamePleasure.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.CityDictionary.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.Location.class.getName(), jcacheConfiguration);
            cm.createCache(kz.ast.donimas.ps.domain.JoySessionLife.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
            cm.createCache(kz.ast.donimas.ps.domain.Session.class.getName() + ".joySessionLives", jcacheConfiguration);
        };
    }
}
