package kz.ast.donimas.ps.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.donimas.ps.domain.DayRegime;
import kz.ast.donimas.ps.service.DayRegimeService;
import kz.ast.donimas.ps.web.rest.errors.BadRequestAlertException;
import kz.ast.donimas.ps.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing DayRegime.
 */
@RestController
@RequestMapping("/api")
public class DayRegimeResource {

    private final Logger log = LoggerFactory.getLogger(DayRegimeResource.class);

    private static final String ENTITY_NAME = "dayRegime";

    private final DayRegimeService dayRegimeService;

    public DayRegimeResource(DayRegimeService dayRegimeService) {
        this.dayRegimeService = dayRegimeService;
    }

    /**
     * POST  /day-regimes : Create a new dayRegime.
     *
     * @param dayRegime the dayRegime to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dayRegime, or with status 400 (Bad Request) if the dayRegime has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/day-regimes")
    @Timed
    public ResponseEntity<DayRegime> createDayRegime(@Valid @RequestBody DayRegime dayRegime) throws URISyntaxException {
        log.debug("REST request to save DayRegime : {}", dayRegime);
        if (dayRegime.getId() != null) {
            throw new BadRequestAlertException("A new dayRegime cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DayRegime result = dayRegimeService.save(dayRegime);
        return ResponseEntity.created(new URI("/api/day-regimes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /day-regimes : Updates an existing dayRegime.
     *
     * @param dayRegime the dayRegime to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dayRegime,
     * or with status 400 (Bad Request) if the dayRegime is not valid,
     * or with status 500 (Internal Server Error) if the dayRegime couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/day-regimes")
    @Timed
    public ResponseEntity<DayRegime> updateDayRegime(@Valid @RequestBody DayRegime dayRegime) throws URISyntaxException {
        log.debug("REST request to update DayRegime : {}", dayRegime);
        if (dayRegime.getId() == null) {
            return createDayRegime(dayRegime);
        }
        DayRegime result = dayRegimeService.save(dayRegime);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dayRegime.getId().toString()))
            .body(result);
    }

    /**
     * GET  /day-regimes : get all the dayRegimes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of dayRegimes in body
     */
    @GetMapping("/day-regimes")
    @Timed
    public List<DayRegime> getAllDayRegimes() {
        log.debug("REST request to get all DayRegimes");
        return dayRegimeService.findAll();
        }

    /**
     * GET  /day-regimes/:id : get the "id" dayRegime.
     *
     * @param id the id of the dayRegime to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dayRegime, or with status 404 (Not Found)
     */
    @GetMapping("/day-regimes/{id}")
    @Timed
    public ResponseEntity<DayRegime> getDayRegime(@PathVariable Long id) {
        log.debug("REST request to get DayRegime : {}", id);
        DayRegime dayRegime = dayRegimeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dayRegime));
    }

    /**
     * DELETE  /day-regimes/:id : delete the "id" dayRegime.
     *
     * @param id the id of the dayRegime to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/day-regimes/{id}")
    @Timed
    public ResponseEntity<Void> deleteDayRegime(@PathVariable Long id) {
        log.debug("REST request to delete DayRegime : {}", id);
        dayRegimeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/day-regimes?query=:query : search for the dayRegime corresponding
     * to the query.
     *
     * @param query the query of the dayRegime search
     * @return the result of the search
     */
    @GetMapping("/_search/day-regimes")
    @Timed
    public List<DayRegime> searchDayRegimes(@RequestParam String query) {
        log.debug("REST request to search DayRegimes for query {}", query);
        return dayRegimeService.search(query);
    }

    @GetMapping("/day-regimes/by-club/{id}")
    @Timed
    public List<DayRegime> getAllDayRegimesOfClub(@PathVariable Long id) {
        log.debug("REST request to get all DayRegimes of club: {}", id);
        return dayRegimeService.findAllByClub(id);
    }

}
