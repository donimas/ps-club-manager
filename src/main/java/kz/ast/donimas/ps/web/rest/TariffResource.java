package kz.ast.donimas.ps.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.donimas.ps.domain.Tariff;
import kz.ast.donimas.ps.service.TariffService;
import kz.ast.donimas.ps.web.rest.errors.BadRequestAlertException;
import kz.ast.donimas.ps.web.rest.util.HeaderUtil;
import kz.ast.donimas.ps.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Tariff.
 */
@RestController
@RequestMapping("/api")
public class TariffResource {

    private final Logger log = LoggerFactory.getLogger(TariffResource.class);

    private static final String ENTITY_NAME = "tariff";

    private final TariffService tariffService;

    public TariffResource(TariffService tariffService) {
        this.tariffService = tariffService;
    }

    /**
     * POST  /tariffs : Create a new tariff.
     *
     * @param tariff the tariff to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tariff, or with status 400 (Bad Request) if the tariff has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tariffs")
    @Timed
    public ResponseEntity<Tariff> createTariff(@Valid @RequestBody Tariff tariff) throws URISyntaxException {
        log.debug("REST request to save Tariff : {}", tariff);
        if (tariff.getId() != null) {
            throw new BadRequestAlertException("A new tariff cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Tariff result = tariffService.save(tariff);
        return ResponseEntity.created(new URI("/api/tariffs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tariffs : Updates an existing tariff.
     *
     * @param tariff the tariff to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tariff,
     * or with status 400 (Bad Request) if the tariff is not valid,
     * or with status 500 (Internal Server Error) if the tariff couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tariffs")
    @Timed
    public ResponseEntity<Tariff> updateTariff(@Valid @RequestBody Tariff tariff) throws URISyntaxException {
        log.debug("REST request to update Tariff : {}", tariff);
        if (tariff.getId() == null) {
            return createTariff(tariff);
        }
        Tariff result = tariffService.save(tariff);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tariff.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tariffs : get all the tariffs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of tariffs in body
     */
    @GetMapping("/tariffs")
    @Timed
    public ResponseEntity<List<Tariff>> getAllTariffs(Pageable pageable) {
        log.debug("REST request to get a page of Tariffs");
        Page<Tariff> page = tariffService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tariffs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /tariffs/:id : get the "id" tariff.
     *
     * @param id the id of the tariff to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tariff, or with status 404 (Not Found)
     */
    @GetMapping("/tariffs/{id}")
    @Timed
    public ResponseEntity<Tariff> getTariff(@PathVariable Long id) {
        log.debug("REST request to get Tariff : {}", id);
        Tariff tariff = tariffService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tariff));
    }

    /**
     * DELETE  /tariffs/:id : delete the "id" tariff.
     *
     * @param id the id of the tariff to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tariffs/{id}")
    @Timed
    public ResponseEntity<Void> deleteTariff(@PathVariable Long id) {
        log.debug("REST request to delete Tariff : {}", id);
        tariffService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/tariffs?query=:query : search for the tariff corresponding
     * to the query.
     *
     * @param query the query of the tariff search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/tariffs")
    @Timed
    public ResponseEntity<List<Tariff>> searchTariffs(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Tariffs for query {}", query);
        Page<Tariff> page = tariffService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/tariffs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
