package kz.ast.donimas.ps.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.donimas.ps.domain.ZoneDictionary;
import kz.ast.donimas.ps.service.ZoneDictionaryService;
import kz.ast.donimas.ps.web.rest.errors.BadRequestAlertException;
import kz.ast.donimas.ps.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ZoneDictionary.
 */
@RestController
@RequestMapping("/api")
public class ZoneDictionaryResource {

    private final Logger log = LoggerFactory.getLogger(ZoneDictionaryResource.class);

    private static final String ENTITY_NAME = "zoneDictionary";

    private final ZoneDictionaryService zoneDictionaryService;

    public ZoneDictionaryResource(ZoneDictionaryService zoneDictionaryService) {
        this.zoneDictionaryService = zoneDictionaryService;
    }

    /**
     * POST  /zone-dictionaries : Create a new zoneDictionary.
     *
     * @param zoneDictionary the zoneDictionary to create
     * @return the ResponseEntity with status 201 (Created) and with body the new zoneDictionary, or with status 400 (Bad Request) if the zoneDictionary has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/zone-dictionaries")
    @Timed
    public ResponseEntity<ZoneDictionary> createZoneDictionary(@Valid @RequestBody ZoneDictionary zoneDictionary) throws URISyntaxException {
        log.debug("REST request to save ZoneDictionary : {}", zoneDictionary);
        if (zoneDictionary.getId() != null) {
            throw new BadRequestAlertException("A new zoneDictionary cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ZoneDictionary result = zoneDictionaryService.save(zoneDictionary);
        return ResponseEntity.created(new URI("/api/zone-dictionaries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /zone-dictionaries : Updates an existing zoneDictionary.
     *
     * @param zoneDictionary the zoneDictionary to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated zoneDictionary,
     * or with status 400 (Bad Request) if the zoneDictionary is not valid,
     * or with status 500 (Internal Server Error) if the zoneDictionary couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/zone-dictionaries")
    @Timed
    public ResponseEntity<ZoneDictionary> updateZoneDictionary(@Valid @RequestBody ZoneDictionary zoneDictionary) throws URISyntaxException {
        log.debug("REST request to update ZoneDictionary : {}", zoneDictionary);
        if (zoneDictionary.getId() == null) {
            return createZoneDictionary(zoneDictionary);
        }
        ZoneDictionary result = zoneDictionaryService.save(zoneDictionary);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, zoneDictionary.getId().toString()))
            .body(result);
    }

    /**
     * GET  /zone-dictionaries : get all the zoneDictionaries.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of zoneDictionaries in body
     */
    @GetMapping("/zone-dictionaries")
    @Timed
    public List<ZoneDictionary> getAllZoneDictionaries() {
        log.debug("REST request to get all ZoneDictionaries");
        return zoneDictionaryService.findAll();
        }

    /**
     * GET  /zone-dictionaries/:id : get the "id" zoneDictionary.
     *
     * @param id the id of the zoneDictionary to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the zoneDictionary, or with status 404 (Not Found)
     */
    @GetMapping("/zone-dictionaries/{id}")
    @Timed
    public ResponseEntity<ZoneDictionary> getZoneDictionary(@PathVariable Long id) {
        log.debug("REST request to get ZoneDictionary : {}", id);
        ZoneDictionary zoneDictionary = zoneDictionaryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(zoneDictionary));
    }

    /**
     * DELETE  /zone-dictionaries/:id : delete the "id" zoneDictionary.
     *
     * @param id the id of the zoneDictionary to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/zone-dictionaries/{id}")
    @Timed
    public ResponseEntity<Void> deleteZoneDictionary(@PathVariable Long id) {
        log.debug("REST request to delete ZoneDictionary : {}", id);
        zoneDictionaryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/zone-dictionaries?query=:query : search for the zoneDictionary corresponding
     * to the query.
     *
     * @param query the query of the zoneDictionary search
     * @return the result of the search
     */
    @GetMapping("/_search/zone-dictionaries")
    @Timed
    public List<ZoneDictionary> searchZoneDictionaries(@RequestParam String query) {
        log.debug("REST request to search ZoneDictionaries for query {}", query);
        return zoneDictionaryService.search(query);
    }

}
