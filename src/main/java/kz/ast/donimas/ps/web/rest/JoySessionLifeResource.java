package kz.ast.donimas.ps.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.donimas.ps.domain.JoySessionLife;
import kz.ast.donimas.ps.service.JoySessionLifeService;
import kz.ast.donimas.ps.web.rest.errors.BadRequestAlertException;
import kz.ast.donimas.ps.web.rest.util.HeaderUtil;
import kz.ast.donimas.ps.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing JoySessionLife.
 */
@RestController
@RequestMapping("/api")
public class JoySessionLifeResource {

    private final Logger log = LoggerFactory.getLogger(JoySessionLifeResource.class);

    private static final String ENTITY_NAME = "joySessionLife";

    private final JoySessionLifeService joySessionLifeService;

    public JoySessionLifeResource(JoySessionLifeService joySessionLifeService) {
        this.joySessionLifeService = joySessionLifeService;
    }

    /**
     * POST  /joy-session-lives : Create a new joySessionLife.
     *
     * @param joySessionLife the joySessionLife to create
     * @return the ResponseEntity with status 201 (Created) and with body the new joySessionLife, or with status 400 (Bad Request) if the joySessionLife has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/joy-session-lives")
    @Timed
    public ResponseEntity<JoySessionLife> createJoySessionLife(@RequestBody JoySessionLife joySessionLife) throws URISyntaxException {
        log.debug("REST request to save JoySessionLife : {}", joySessionLife);
        if (joySessionLife.getId() != null) {
            throw new BadRequestAlertException("A new joySessionLife cannot already have an ID", ENTITY_NAME, "idexists");
        }
        JoySessionLife result = joySessionLifeService.save(joySessionLife);
        return ResponseEntity.created(new URI("/api/joy-session-lives/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /joy-session-lives : Updates an existing joySessionLife.
     *
     * @param joySessionLife the joySessionLife to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated joySessionLife,
     * or with status 400 (Bad Request) if the joySessionLife is not valid,
     * or with status 500 (Internal Server Error) if the joySessionLife couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/joy-session-lives")
    @Timed
    public ResponseEntity<JoySessionLife> updateJoySessionLife(@RequestBody JoySessionLife joySessionLife) throws URISyntaxException {
        log.debug("REST request to update JoySessionLife : {}", joySessionLife);
        if (joySessionLife.getId() == null) {
            return createJoySessionLife(joySessionLife);
        }
        JoySessionLife result = joySessionLifeService.save(joySessionLife);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, joySessionLife.getId().toString()))
            .body(result);
    }

    /**
     * GET  /joy-session-lives : get all the joySessionLives.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of joySessionLives in body
     */
    @GetMapping("/joy-session-lives")
    @Timed
    public ResponseEntity<List<JoySessionLife>> getAllJoySessionLives(Pageable pageable) {
        log.debug("REST request to get a page of JoySessionLives");
        Page<JoySessionLife> page = joySessionLifeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/joy-session-lives");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /joy-session-lives/:id : get the "id" joySessionLife.
     *
     * @param id the id of the joySessionLife to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the joySessionLife, or with status 404 (Not Found)
     */
    @GetMapping("/joy-session-lives/{id}")
    @Timed
    public ResponseEntity<JoySessionLife> getJoySessionLife(@PathVariable Long id) {
        log.debug("REST request to get JoySessionLife : {}", id);
        JoySessionLife joySessionLife = joySessionLifeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(joySessionLife));
    }

    /**
     * DELETE  /joy-session-lives/:id : delete the "id" joySessionLife.
     *
     * @param id the id of the joySessionLife to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/joy-session-lives/{id}")
    @Timed
    public ResponseEntity<Void> deleteJoySessionLife(@PathVariable Long id) {
        log.debug("REST request to delete JoySessionLife : {}", id);
        joySessionLifeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/joy-session-lives?query=:query : search for the joySessionLife corresponding
     * to the query.
     *
     * @param query the query of the joySessionLife search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/joy-session-lives")
    @Timed
    public ResponseEntity<List<JoySessionLife>> searchJoySessionLives(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of JoySessionLives for query {}", query);
        Page<JoySessionLife> page = joySessionLifeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/joy-session-lives");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
