/**
 * View Models used by Spring MVC REST controllers.
 */
package kz.ast.donimas.ps.web.rest.vm;
