package kz.ast.donimas.ps.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.donimas.ps.domain.Club;
import kz.ast.donimas.ps.domain.User;
import kz.ast.donimas.ps.service.ClubService;
import kz.ast.donimas.ps.service.UserService;
import kz.ast.donimas.ps.service.dto.ClubDto;
import kz.ast.donimas.ps.service.util.StringUtils;
import kz.ast.donimas.ps.web.rest.errors.BadRequestAlertException;
import kz.ast.donimas.ps.web.rest.util.HeaderUtil;
import kz.ast.donimas.ps.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.checkerframework.checker.units.qual.Time;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Club.
 */
@RestController
@RequestMapping("/api")
public class ClubResource {

    private final Logger log = LoggerFactory.getLogger(ClubResource.class);

    private static final String ENTITY_NAME = "club";

    private final ClubService clubService;
    private final UserService userService;

    public ClubResource(ClubService clubService, UserService userService) {
        this.clubService = clubService;
        this.userService = userService;
    }

    /**
     * POST  /clubs : Create a new club.
     *
     * @param club the club to create
     * @return the ResponseEntity with status 201 (Created) and with body the new club, or with status 400 (Bad Request) if the club has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/clubs")
    @Timed
    public ResponseEntity<Club> createClub(@Valid @RequestBody Club club) throws URISyntaxException {
        log.debug("REST request to save Club : {}", club);
        if (club.getId() != null) {
            throw new BadRequestAlertException("A new club cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Club result = clubService.save(club);
        return ResponseEntity.created(new URI("/api/clubs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /clubs : Updates an existing club.
     *
     * @param club the club to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated club,
     * or with status 400 (Bad Request) if the club is not valid,
     * or with status 500 (Internal Server Error) if the club couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/clubs")
    @Timed
    public ResponseEntity<Club> updateClub(@Valid @RequestBody Club club) throws URISyntaxException {
        log.debug("REST request to update Club : {}", club);
        if (club.getId() == null) {
            return createClub(club);
        }
        Club result = clubService.save(club);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, club.getId().toString()))
            .body(result);
    }

    /**
     * GET  /clubs : get all the clubs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of clubs in body
     */
    @GetMapping("/clubs")
    @Timed
    public ResponseEntity<List<Club>> getAllClubs(Pageable pageable) {
        log.debug("REST request to get a page of Clubs");
        Page<Club> page = clubService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/clubs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /clubs/:id : get the "id" club.
     *
     * @param id the id of the club to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the club, or with status 404 (Not Found)
     */
    @GetMapping("/clubs/{id}")
    @Timed
    public ResponseEntity<Club> getClub(@PathVariable Long id) {
        log.debug("REST request to get Club : {}", id);
        Club club = clubService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(club));
    }

    /**
     * DELETE  /clubs/:id : delete the "id" club.
     *
     * @param id the id of the club to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/clubs/{id}")
    @Timed
    public ResponseEntity<Void> deleteClub(@PathVariable Long id) {
        log.debug("REST request to delete Club : {}", id);
        clubService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/clubs?query=:query : search for the club corresponding
     * to the query.
     *
     * @param query the query of the club search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/clubs")
    @Timed
    public ResponseEntity<List<Club>> searchClubs(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Clubs for query {}", query);
        Page<Club> page = clubService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/clubs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/clubs/owner-visit")
    @Timed
    public ResponseEntity<Club> ownerVisit() {
        log.debug("REST request to get Club to owner");
        Optional<User> optionalUser = userService.getUserWithAuthorities();
        if(!optionalUser.isPresent()) {
            return new ResponseEntity<Club>(HttpStatus.FORBIDDEN);
        }
        User user = optionalUser.get();

        Club club = clubService.getOwnerClub(user);
        if(user.getClub() == null) {
            user.setClub(club);
            userService.save(user);
        }

        return new ResponseEntity<Club>(club, HttpStatus.OK);
    }

    @PutMapping("/clubs/save-main-info")
    @Timed
    public ResponseEntity<Club> saveMainInfo(@RequestBody Club club) throws URISyntaxException {
        log.debug("REST request to save main info: {}", club);
        if (club.getId() == null) {
            throw new BadRequestAlertException("Id doesn't exist", ENTITY_NAME, "idnotexists");
        }
        if(club.getName() == null || club.getName().trim().isEmpty()) {
            throw new BadRequestAlertException("Validation error", ENTITY_NAME, "emptyfield");
        }
        Club result = clubService.saveMainInfo(club);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, club.getId().toString()))
            .body(result);
    }

    @PutMapping("/clubs/save-regulation")
    @Timed
    public ResponseEntity<Club> saveRegulation(@RequestBody ClubDto clubDto) throws URISyntaxException {
        log.debug("REST request to save main regulation: {}", clubDto);
        if (clubDto.getId() == null) {
            throw new BadRequestAlertException("Id doesn't exist", ENTITY_NAME, "idnotexists");
        }
        Club result = clubService.saveClubRegulation(clubDto);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("regulation", clubDto.getId().toString()))
            .body(result);
    }

    @PutMapping("/clubs/save-contact")
    @Timed
    public ResponseEntity<Club> saveContact(@RequestBody Club club) throws URISyntaxException {
        log.debug("REST request to save contact: {}", club);
        if (club.getId() == null) {
            throw new BadRequestAlertException("Id doesn't exist", ENTITY_NAME, "idnotexists");
        }
        if(StringUtils.isEmpty(club.getPhone()) && StringUtils.isEmpty(club.getFullAddress())) {
            throw new BadRequestAlertException("Validation error", ENTITY_NAME, "emptyfield");
        }
        Club result = clubService.saveContact(club);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("contact", club.getId().toString()))
            .body(result);
    }

    @PutMapping("/clubs/save-coordinate")
    @Timed
    public ResponseEntity<Club> saveCoordinate(@RequestBody Club club) throws URISyntaxException {
        log.debug("REST request to save coordinate: {}", club);
        if (club.getId() == null) {
            throw new BadRequestAlertException("Id doesn't exist", ENTITY_NAME, "idnotexists");
        }
        if(club.getLocation() == null || club.getLocation().getCity() == null) {
            throw new BadRequestAlertException("Validation error", ENTITY_NAME, "emptyfield");
        }
        Club result = clubService.saveCoordinate(club);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("coordinate", club.getId().toString()))
            .body(result);
    }

    @PutMapping("/clubs/save-equipment")
    @Timed
    public ResponseEntity<Club> saveEquipment(@RequestBody Club club) throws URISyntaxException {
        log.debug("Rest request to save equipment: {}", club);
        if(club.getId() == null) {
            throw new BadRequestAlertException("Id doesn't exist", ENTITY_NAME, "idnotexists");
        }
        Club result = clubService.saveEquipment(club);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("equipment", club.getId().toString()))
            .body(result);
    }

    @GetMapping("/clubs/publish-club/{id}")
    @Timed
    public ResponseEntity<Void> publish(@PathVariable Long id) {
        log.debug("Rest request to publish the club: {}", id);
        Club club = clubService.findOne(id);
        if(club.getFlagMainInfoSaved() &&
            club.getFlagContactSaved() &&
            club.getFlagLocationSaved() &&
            club.getFlagZoneSaved()) {
            club.setFlagPublished(true);
            clubService.save(club);
            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityPublucationAlert(ENTITY_NAME, club.getId().toString()))
                .build();
        }

        throw new BadRequestAlertException("Not ready to publication", ENTITY_NAME, "notReadyToPublication");
    }

}
