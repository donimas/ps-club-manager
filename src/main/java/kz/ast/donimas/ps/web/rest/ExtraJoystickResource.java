package kz.ast.donimas.ps.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.donimas.ps.domain.ExtraJoystick;
import kz.ast.donimas.ps.service.ExtraJoystickService;
import kz.ast.donimas.ps.web.rest.errors.BadRequestAlertException;
import kz.ast.donimas.ps.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ExtraJoystick.
 */
@RestController
@RequestMapping("/api")
public class ExtraJoystickResource {

    private final Logger log = LoggerFactory.getLogger(ExtraJoystickResource.class);

    private static final String ENTITY_NAME = "extraJoystick";

    private final ExtraJoystickService extraJoystickService;

    public ExtraJoystickResource(ExtraJoystickService extraJoystickService) {
        this.extraJoystickService = extraJoystickService;
    }

    /**
     * POST  /extra-joysticks : Create a new extraJoystick.
     *
     * @param extraJoystick the extraJoystick to create
     * @return the ResponseEntity with status 201 (Created) and with body the new extraJoystick, or with status 400 (Bad Request) if the extraJoystick has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/extra-joysticks")
    @Timed
    public ResponseEntity<ExtraJoystick> createExtraJoystick(@RequestBody ExtraJoystick extraJoystick) throws URISyntaxException {
        log.debug("REST request to save ExtraJoystick : {}", extraJoystick);
        if (extraJoystick.getId() != null) {
            throw new BadRequestAlertException("A new extraJoystick cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ExtraJoystick result = extraJoystickService.save(extraJoystick);
        return ResponseEntity.created(new URI("/api/extra-joysticks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /extra-joysticks : Updates an existing extraJoystick.
     *
     * @param extraJoystick the extraJoystick to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated extraJoystick,
     * or with status 400 (Bad Request) if the extraJoystick is not valid,
     * or with status 500 (Internal Server Error) if the extraJoystick couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/extra-joysticks")
    @Timed
    public ResponseEntity<ExtraJoystick> updateExtraJoystick(@RequestBody ExtraJoystick extraJoystick) throws URISyntaxException {
        log.debug("REST request to update ExtraJoystick : {}", extraJoystick);
        if (extraJoystick.getId() == null) {
            return createExtraJoystick(extraJoystick);
        }
        ExtraJoystick result = extraJoystickService.save(extraJoystick);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, extraJoystick.getId().toString()))
            .body(result);
    }

    /**
     * GET  /extra-joysticks : get all the extraJoysticks.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of extraJoysticks in body
     */
    @GetMapping("/extra-joysticks")
    @Timed
    public List<ExtraJoystick> getAllExtraJoysticks() {
        log.debug("REST request to get all ExtraJoysticks");
        return extraJoystickService.findAll();
        }

    /**
     * GET  /extra-joysticks/:id : get the "id" extraJoystick.
     *
     * @param id the id of the extraJoystick to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the extraJoystick, or with status 404 (Not Found)
     */
    @GetMapping("/extra-joysticks/{id}")
    @Timed
    public ResponseEntity<ExtraJoystick> getExtraJoystick(@PathVariable Long id) {
        log.debug("REST request to get ExtraJoystick : {}", id);
        ExtraJoystick extraJoystick = extraJoystickService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(extraJoystick));
    }

    /**
     * DELETE  /extra-joysticks/:id : delete the "id" extraJoystick.
     *
     * @param id the id of the extraJoystick to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/extra-joysticks/{id}")
    @Timed
    public ResponseEntity<Void> deleteExtraJoystick(@PathVariable Long id) {
        log.debug("REST request to delete ExtraJoystick : {}", id);
        extraJoystickService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/extra-joysticks?query=:query : search for the extraJoystick corresponding
     * to the query.
     *
     * @param query the query of the extraJoystick search
     * @return the result of the search
     */
    @GetMapping("/_search/extra-joysticks")
    @Timed
    public List<ExtraJoystick> searchExtraJoysticks(@RequestParam String query) {
        log.debug("REST request to search ExtraJoysticks for query {}", query);
        return extraJoystickService.search(query);
    }

}
