package kz.ast.donimas.ps.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.donimas.ps.domain.GamePleasure;
import kz.ast.donimas.ps.service.GamePleasureService;
import kz.ast.donimas.ps.web.rest.errors.BadRequestAlertException;
import kz.ast.donimas.ps.web.rest.util.HeaderUtil;
import kz.ast.donimas.ps.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing GamePleasure.
 */
@RestController
@RequestMapping("/api")
public class GamePleasureResource {

    private final Logger log = LoggerFactory.getLogger(GamePleasureResource.class);

    private static final String ENTITY_NAME = "gamePleasure";

    private final GamePleasureService gamePleasureService;

    public GamePleasureResource(GamePleasureService gamePleasureService) {
        this.gamePleasureService = gamePleasureService;
    }

    /**
     * POST  /game-pleasures : Create a new gamePleasure.
     *
     * @param gamePleasure the gamePleasure to create
     * @return the ResponseEntity with status 201 (Created) and with body the new gamePleasure, or with status 400 (Bad Request) if the gamePleasure has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/game-pleasures")
    @Timed
    public ResponseEntity<GamePleasure> createGamePleasure(@RequestBody GamePleasure gamePleasure) throws URISyntaxException {
        log.debug("REST request to save GamePleasure : {}", gamePleasure);
        if (gamePleasure.getId() != null) {
            throw new BadRequestAlertException("A new gamePleasure cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GamePleasure result = gamePleasureService.save(gamePleasure);
        return ResponseEntity.created(new URI("/api/game-pleasures/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /game-pleasures : Updates an existing gamePleasure.
     *
     * @param gamePleasure the gamePleasure to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated gamePleasure,
     * or with status 400 (Bad Request) if the gamePleasure is not valid,
     * or with status 500 (Internal Server Error) if the gamePleasure couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/game-pleasures")
    @Timed
    public ResponseEntity<GamePleasure> updateGamePleasure(@RequestBody GamePleasure gamePleasure) throws URISyntaxException {
        log.debug("REST request to update GamePleasure : {}", gamePleasure);
        if (gamePleasure.getId() == null) {
            return createGamePleasure(gamePleasure);
        }
        GamePleasure result = gamePleasureService.save(gamePleasure);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, gamePleasure.getId().toString()))
            .body(result);
    }

    /**
     * GET  /game-pleasures : get all the gamePleasures.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of gamePleasures in body
     */
    @GetMapping("/game-pleasures")
    @Timed
    public ResponseEntity<List<GamePleasure>> getAllGamePleasures(Pageable pageable) {
        log.debug("REST request to get a page of GamePleasures");
        Page<GamePleasure> page = gamePleasureService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/game-pleasures");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /game-pleasures/:id : get the "id" gamePleasure.
     *
     * @param id the id of the gamePleasure to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the gamePleasure, or with status 404 (Not Found)
     */
    @GetMapping("/game-pleasures/{id}")
    @Timed
    public ResponseEntity<GamePleasure> getGamePleasure(@PathVariable Long id) {
        log.debug("REST request to get GamePleasure : {}", id);
        GamePleasure gamePleasure = gamePleasureService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(gamePleasure));
    }

    /**
     * DELETE  /game-pleasures/:id : delete the "id" gamePleasure.
     *
     * @param id the id of the gamePleasure to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/game-pleasures/{id}")
    @Timed
    public ResponseEntity<Void> deleteGamePleasure(@PathVariable Long id) {
        log.debug("REST request to delete GamePleasure : {}", id);
        gamePleasureService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/game-pleasures?query=:query : search for the gamePleasure corresponding
     * to the query.
     *
     * @param query the query of the gamePleasure search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/game-pleasures")
    @Timed
    public ResponseEntity<List<GamePleasure>> searchGamePleasures(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of GamePleasures for query {}", query);
        Page<GamePleasure> page = gamePleasureService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/game-pleasures");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
