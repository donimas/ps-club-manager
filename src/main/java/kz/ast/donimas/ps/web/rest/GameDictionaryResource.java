package kz.ast.donimas.ps.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.donimas.ps.domain.GameDictionary;
import kz.ast.donimas.ps.service.GameDictionaryService;
import kz.ast.donimas.ps.web.rest.errors.BadRequestAlertException;
import kz.ast.donimas.ps.web.rest.util.HeaderUtil;
import kz.ast.donimas.ps.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing GameDictionary.
 */
@RestController
@RequestMapping("/api")
public class GameDictionaryResource {

    private final Logger log = LoggerFactory.getLogger(GameDictionaryResource.class);

    private static final String ENTITY_NAME = "gameDictionary";

    private final GameDictionaryService gameDictionaryService;

    public GameDictionaryResource(GameDictionaryService gameDictionaryService) {
        this.gameDictionaryService = gameDictionaryService;
    }

    /**
     * POST  /game-dictionaries : Create a new gameDictionary.
     *
     * @param gameDictionary the gameDictionary to create
     * @return the ResponseEntity with status 201 (Created) and with body the new gameDictionary, or with status 400 (Bad Request) if the gameDictionary has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/game-dictionaries")
    @Timed
    public ResponseEntity<GameDictionary> createGameDictionary(@Valid @RequestBody GameDictionary gameDictionary) throws URISyntaxException {
        log.debug("REST request to save GameDictionary : {}", gameDictionary);
        if (gameDictionary.getId() != null) {
            throw new BadRequestAlertException("A new gameDictionary cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GameDictionary result = gameDictionaryService.save(gameDictionary);
        return ResponseEntity.created(new URI("/api/game-dictionaries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /game-dictionaries : Updates an existing gameDictionary.
     *
     * @param gameDictionary the gameDictionary to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated gameDictionary,
     * or with status 400 (Bad Request) if the gameDictionary is not valid,
     * or with status 500 (Internal Server Error) if the gameDictionary couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/game-dictionaries")
    @Timed
    public ResponseEntity<GameDictionary> updateGameDictionary(@Valid @RequestBody GameDictionary gameDictionary) throws URISyntaxException {
        log.debug("REST request to update GameDictionary : {}", gameDictionary);
        if (gameDictionary.getId() == null) {
            return createGameDictionary(gameDictionary);
        }
        GameDictionary result = gameDictionaryService.save(gameDictionary);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, gameDictionary.getId().toString()))
            .body(result);
    }

    /**
     * GET  /game-dictionaries : get all the gameDictionaries.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of gameDictionaries in body
     */
    @GetMapping("/game-dictionaries")
    @Timed
    public ResponseEntity<List<GameDictionary>> getAllGameDictionaries(Pageable pageable) {
        log.debug("REST request to get a page of GameDictionaries");
        Page<GameDictionary> page = gameDictionaryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/game-dictionaries");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /game-dictionaries/:id : get the "id" gameDictionary.
     *
     * @param id the id of the gameDictionary to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the gameDictionary, or with status 404 (Not Found)
     */
    @GetMapping("/game-dictionaries/{id}")
    @Timed
    public ResponseEntity<GameDictionary> getGameDictionary(@PathVariable Long id) {
        log.debug("REST request to get GameDictionary : {}", id);
        GameDictionary gameDictionary = gameDictionaryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(gameDictionary));
    }

    /**
     * DELETE  /game-dictionaries/:id : delete the "id" gameDictionary.
     *
     * @param id the id of the gameDictionary to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/game-dictionaries/{id}")
    @Timed
    public ResponseEntity<Void> deleteGameDictionary(@PathVariable Long id) {
        log.debug("REST request to delete GameDictionary : {}", id);
        gameDictionaryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/game-dictionaries?query=:query : search for the gameDictionary corresponding
     * to the query.
     *
     * @param query the query of the gameDictionary search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/game-dictionaries")
    @Timed
    public ResponseEntity<List<GameDictionary>> searchGameDictionaries(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of GameDictionaries for query {}", query);
        Page<GameDictionary> page = gameDictionaryService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/game-dictionaries");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
