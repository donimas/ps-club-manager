package kz.ast.donimas.ps.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.donimas.ps.domain.CityDictionary;
import kz.ast.donimas.ps.service.CityDictionaryService;
import kz.ast.donimas.ps.web.rest.errors.BadRequestAlertException;
import kz.ast.donimas.ps.web.rest.util.HeaderUtil;
import kz.ast.donimas.ps.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CityDictionary.
 */
@RestController
@RequestMapping("/api")
public class CityDictionaryResource {

    private final Logger log = LoggerFactory.getLogger(CityDictionaryResource.class);

    private static final String ENTITY_NAME = "cityDictionary";

    private final CityDictionaryService cityDictionaryService;

    public CityDictionaryResource(CityDictionaryService cityDictionaryService) {
        this.cityDictionaryService = cityDictionaryService;
    }

    /**
     * POST  /city-dictionaries : Create a new cityDictionary.
     *
     * @param cityDictionary the cityDictionary to create
     * @return the ResponseEntity with status 201 (Created) and with body the new cityDictionary, or with status 400 (Bad Request) if the cityDictionary has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/city-dictionaries")
    @Timed
    public ResponseEntity<CityDictionary> createCityDictionary(@RequestBody CityDictionary cityDictionary) throws URISyntaxException {
        log.debug("REST request to save CityDictionary : {}", cityDictionary);
        if (cityDictionary.getId() != null) {
            throw new BadRequestAlertException("A new cityDictionary cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CityDictionary result = cityDictionaryService.save(cityDictionary);
        return ResponseEntity.created(new URI("/api/city-dictionaries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /city-dictionaries : Updates an existing cityDictionary.
     *
     * @param cityDictionary the cityDictionary to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated cityDictionary,
     * or with status 400 (Bad Request) if the cityDictionary is not valid,
     * or with status 500 (Internal Server Error) if the cityDictionary couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/city-dictionaries")
    @Timed
    public ResponseEntity<CityDictionary> updateCityDictionary(@RequestBody CityDictionary cityDictionary) throws URISyntaxException {
        log.debug("REST request to update CityDictionary : {}", cityDictionary);
        if (cityDictionary.getId() == null) {
            return createCityDictionary(cityDictionary);
        }
        CityDictionary result = cityDictionaryService.save(cityDictionary);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, cityDictionary.getId().toString()))
            .body(result);
    }

    /**
     * GET  /city-dictionaries : get all the cityDictionaries.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of cityDictionaries in body
     */
    @GetMapping("/city-dictionaries")
    @Timed
    public ResponseEntity<List<CityDictionary>> getAllCityDictionaries(Pageable pageable) {
        log.debug("REST request to get a page of CityDictionaries");
        Page<CityDictionary> page = cityDictionaryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/city-dictionaries");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /city-dictionaries/:id : get the "id" cityDictionary.
     *
     * @param id the id of the cityDictionary to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the cityDictionary, or with status 404 (Not Found)
     */
    @GetMapping("/city-dictionaries/{id}")
    @Timed
    public ResponseEntity<CityDictionary> getCityDictionary(@PathVariable Long id) {
        log.debug("REST request to get CityDictionary : {}", id);
        CityDictionary cityDictionary = cityDictionaryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(cityDictionary));
    }

    /**
     * DELETE  /city-dictionaries/:id : delete the "id" cityDictionary.
     *
     * @param id the id of the cityDictionary to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/city-dictionaries/{id}")
    @Timed
    public ResponseEntity<Void> deleteCityDictionary(@PathVariable Long id) {
        log.debug("REST request to delete CityDictionary : {}", id);
        cityDictionaryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/city-dictionaries?query=:query : search for the cityDictionary corresponding
     * to the query.
     *
     * @param query the query of the cityDictionary search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/city-dictionaries")
    @Timed
    public ResponseEntity<List<CityDictionary>> searchCityDictionaries(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of CityDictionaries for query {}", query);
        Page<CityDictionary> page = cityDictionaryService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/city-dictionaries");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
