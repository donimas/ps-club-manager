package kz.ast.donimas.ps.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.donimas.ps.domain.ClientLevel;
import kz.ast.donimas.ps.service.ClientLevelService;
import kz.ast.donimas.ps.web.rest.errors.BadRequestAlertException;
import kz.ast.donimas.ps.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import kz.ast.donimas.ps.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ClientLevel.
 */
@RestController
@RequestMapping("/api")
public class ClientLevelResource {

    private final Logger log = LoggerFactory.getLogger(ClientLevelResource.class);

    private static final String ENTITY_NAME = "clientLevel";

    private final ClientLevelService clientLevelService;

    public ClientLevelResource(ClientLevelService clientLevelService) {
        this.clientLevelService = clientLevelService;
    }

    /**
     * POST  /client-levels : Create a new clientLevel.
     *
     * @param clientLevel the clientLevel to create
     * @return the ResponseEntity with status 201 (Created) and with body the new clientLevel, or with status 400 (Bad Request) if the clientLevel has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/client-levels")
    @Timed
    public ResponseEntity<ClientLevel> createClientLevel(@Valid @RequestBody ClientLevel clientLevel) throws URISyntaxException {
        log.debug("REST request to save ClientLevel : {}", clientLevel);
        if (clientLevel.getId() != null) {
            throw new BadRequestAlertException("A new clientLevel cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClientLevel result = clientLevelService.create(clientLevel);
        return ResponseEntity.created(new URI("/api/client-levels/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /client-levels : Updates an existing clientLevel.
     *
     * @param clientLevel the clientLevel to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated clientLevel,
     * or with status 400 (Bad Request) if the clientLevel is not valid,
     * or with status 500 (Internal Server Error) if the clientLevel couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/client-levels")
    @Timed
    public ResponseEntity<ClientLevel> updateClientLevel(@Valid @RequestBody ClientLevel clientLevel) throws URISyntaxException {
        log.debug("REST request to update ClientLevel : {}", clientLevel);
        if (clientLevel.getId() == null) {
            return createClientLevel(clientLevel);
        }
        ClientLevel result = clientLevelService.save(clientLevel);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, clientLevel.getId().toString()))
            .body(result);
    }

    /**
     * GET  /client-levels : get all the clientLevels.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of clientLevels in body
     */
    @GetMapping("/client-levels")
    @Timed
    public List<ClientLevel> getAllClientLevels() {
        log.debug("REST request to get all ClientLevels");
        return clientLevelService.findAll();
    }

    /**
     * GET  /client-levels/:id : get the "id" clientLevel.
     *
     * @param id the id of the clientLevel to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clientLevel, or with status 404 (Not Found)
     */
    @GetMapping("/client-levels/{id}")
    @Timed
    public ResponseEntity<ClientLevel> getClientLevel(@PathVariable Long id) {
        log.debug("REST request to get ClientLevel : {}", id);
        ClientLevel clientLevel = clientLevelService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clientLevel));
    }

    /**
     * DELETE  /client-levels/:id : delete the "id" clientLevel.
     *
     * @param id the id of the clientLevel to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/client-levels/{id}")
    @Timed
    public ResponseEntity<Void> deleteClientLevel(@PathVariable Long id) {
        log.debug("REST request to delete ClientLevel : {}", id);
        clientLevelService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/client-levels?query=:query : search for the clientLevel corresponding
     * to the query.
     *
     * @param query the query of the clientLevel search
     * @return the result of the search
     */
    @GetMapping("/_search/client-levels")
    @Timed
    public List<ClientLevel> searchClientLevels(@RequestParam String query) {
        log.debug("REST request to search ClientLevels for query {}", query);
        return clientLevelService.search(query);
    }

    @GetMapping("/client-levels/club/{id}")
    @Timed
    public ResponseEntity<List<ClientLevel>> getAllClientLevels(@PathVariable Long id, Pageable pageable) {
        log.debug("REST request to get all ClientLevels of club: {}", id);
        Page<ClientLevel> page = clientLevelService.getAllClubClientLevels(id, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-levels/club");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
