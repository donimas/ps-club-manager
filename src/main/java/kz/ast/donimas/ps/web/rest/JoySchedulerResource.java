package kz.ast.donimas.ps.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import kz.ast.donimas.ps.domain.Club;
import kz.ast.donimas.ps.domain.User;
import kz.ast.donimas.ps.service.ClubService;
import kz.ast.donimas.ps.service.JoySchedulerService;
import kz.ast.donimas.ps.service.UserService;
import kz.ast.donimas.ps.service.dto.ClubDto;
import kz.ast.donimas.ps.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * REST controller for managing Club.
 */
@RestController
@RequestMapping("/api")
public class JoySchedulerResource {

    private final Logger log = LoggerFactory.getLogger(JoySchedulerResource.class);

    private static final String ENTITY_NAME = "joyScheduler";

    private final ClubService clubService;
    private final UserService userService;
    private final JoySchedulerService joySchedulerService;

    public JoySchedulerResource(ClubService clubService, UserService userService, JoySchedulerService joySchedulerService) {
        this.clubService = clubService;
        this.userService = userService;
        this.joySchedulerService = joySchedulerService;
    }

    /**
     * GET  /clubs : get all the clubs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of clubs in body
     */
    @GetMapping("/joy-scheduler")
    @Timed
    public ResponseEntity<ClubDto> getJoySchedule() {
        log.debug("REST request to get a page of Clubs");
        Optional<User> oUser = userService.getUserWithAuthorities();
        if(!oUser.isPresent()) {
            throw new BadRequestAlertException("User not found", ENTITY_NAME, "usernotexists");
        }
        User user = oUser.get();
        if(user.getClub() == null || !user.getClub().getFlagPublished()) {
            throw new BadRequestAlertException("Club not published", ENTITY_NAME, "clubnotpublished");
        }

        Club club = user.getClub();
        ClubDto result = joySchedulerService.getJoyScheduler(club.getId());

        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(result));
    }

}
