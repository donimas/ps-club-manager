package kz.ast.donimas.ps.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.donimas.ps.domain.PricePackage;
import kz.ast.donimas.ps.service.PricePackageService;
import kz.ast.donimas.ps.web.rest.errors.BadRequestAlertException;
import kz.ast.donimas.ps.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import kz.ast.donimas.ps.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PricePackage.
 */
@RestController
@RequestMapping("/api")
public class PricePackageResource {

    private final Logger log = LoggerFactory.getLogger(PricePackageResource.class);

    private static final String ENTITY_NAME = "pricePackage";

    private final PricePackageService pricePackageService;

    public PricePackageResource(PricePackageService pricePackageService) {
        this.pricePackageService = pricePackageService;
    }

    /**
     * POST  /price-packages : Create a new pricePackage.
     *
     * @param pricePackage the pricePackage to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pricePackage, or with status 400 (Bad Request) if the pricePackage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/price-packages")
    @Timed
    public ResponseEntity<PricePackage> createPricePackage(@Valid @RequestBody PricePackage pricePackage) throws URISyntaxException {
        log.debug("REST request to save PricePackage : {}", pricePackage);
        if (pricePackage.getId() != null) {
            throw new BadRequestAlertException("A new pricePackage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PricePackage result = pricePackageService.create(pricePackage);
        return ResponseEntity.created(new URI("/api/price-packages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /price-packages : Updates an existing pricePackage.
     *
     * @param pricePackage the pricePackage to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pricePackage,
     * or with status 400 (Bad Request) if the pricePackage is not valid,
     * or with status 500 (Internal Server Error) if the pricePackage couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/price-packages")
    @Timed
    public ResponseEntity<PricePackage> updatePricePackage(@Valid @RequestBody PricePackage pricePackage) throws URISyntaxException {
        log.debug("REST request to update PricePackage : {}", pricePackage);
        if (pricePackage.getId() == null) {
            return createPricePackage(pricePackage);
        }
        PricePackage result = pricePackageService.save(pricePackage);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pricePackage.getId().toString()))
            .body(result);
    }

    /**
     * GET  /price-packages : get all the pricePackages.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of pricePackages in body
     */
    @GetMapping("/price-packages")
    @Timed
    public List<PricePackage> getAllPricePackages() {
        log.debug("REST request to get all PricePackages");
        return pricePackageService.findAll();
    }

    /**
     * GET  /price-packages/:id : get the "id" pricePackage.
     *
     * @param id the id of the pricePackage to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pricePackage, or with status 404 (Not Found)
     */
    @GetMapping("/price-packages/{id}")
    @Timed
    public ResponseEntity<PricePackage> getPricePackage(@PathVariable Long id) {
        log.debug("REST request to get PricePackage : {}", id);
        PricePackage pricePackage = pricePackageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pricePackage));
    }

    /**
     * DELETE  /price-packages/:id : delete the "id" pricePackage.
     *
     * @param id the id of the pricePackage to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/price-packages/{id}")
    @Timed
    public ResponseEntity<Void> deletePricePackage(@PathVariable Long id) {
        log.debug("REST request to delete PricePackage : {}", id);
        pricePackageService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/price-packages?query=:query : search for the pricePackage corresponding
     * to the query.
     *
     * @param query the query of the pricePackage search
     * @return the result of the search
     */
    @GetMapping("/_search/price-packages")
    @Timed
    public List<PricePackage> searchPricePackages(@RequestParam String query) {
        log.debug("REST request to search PricePackages for query {}", query);
        return pricePackageService.search(query);
    }

    @GetMapping("/price-packages/club/{id}")
    @Timed
    public ResponseEntity<List<PricePackage>> getAllPricePackages(@PathVariable Long id, Pageable pageable) {
        log.debug("REST request to get all PricePackages");
        Page<PricePackage> page = pricePackageService.getAllClubPricePackages(id, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/price-packages");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
