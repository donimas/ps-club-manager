package kz.ast.donimas.ps.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.donimas.ps.domain.ClubJoystick;
import kz.ast.donimas.ps.service.ClubJoystickService;
import kz.ast.donimas.ps.web.rest.errors.BadRequestAlertException;
import kz.ast.donimas.ps.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ClubJoystick.
 */
@RestController
@RequestMapping("/api")
public class ClubJoystickResource {

    private final Logger log = LoggerFactory.getLogger(ClubJoystickResource.class);

    private static final String ENTITY_NAME = "clubJoystick";

    private final ClubJoystickService clubJoystickService;

    public ClubJoystickResource(ClubJoystickService clubJoystickService) {
        this.clubJoystickService = clubJoystickService;
    }

    /**
     * POST  /club-joysticks : Create a new clubJoystick.
     *
     * @param clubJoystick the clubJoystick to create
     * @return the ResponseEntity with status 201 (Created) and with body the new clubJoystick, or with status 400 (Bad Request) if the clubJoystick has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/club-joysticks")
    @Timed
    public ResponseEntity<ClubJoystick> createClubJoystick(@RequestBody ClubJoystick clubJoystick) throws URISyntaxException {
        log.debug("REST request to save ClubJoystick : {}", clubJoystick);
        if (clubJoystick.getId() != null) {
            throw new BadRequestAlertException("A new clubJoystick cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClubJoystick result = clubJoystickService.create(clubJoystick);
        return ResponseEntity.created(new URI("/api/club-joysticks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /club-joysticks : Updates an existing clubJoystick.
     *
     * @param clubJoystick the clubJoystick to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated clubJoystick,
     * or with status 400 (Bad Request) if the clubJoystick is not valid,
     * or with status 500 (Internal Server Error) if the clubJoystick couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/club-joysticks")
    @Timed
    public ResponseEntity<ClubJoystick> updateClubJoystick(@RequestBody ClubJoystick clubJoystick) throws URISyntaxException {
        log.debug("REST request to update ClubJoystick : {}", clubJoystick);
        if (clubJoystick.getId() == null) {
            return createClubJoystick(clubJoystick);
        }
        ClubJoystick result = clubJoystickService.create(clubJoystick);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, clubJoystick.getId().toString()))
            .body(result);
    }

    /**
     * GET  /club-joysticks : get all the clubJoysticks.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of clubJoysticks in body
     */
    @GetMapping("/club-joysticks")
    @Timed
    public List<ClubJoystick> getAllClubJoysticks() {
        log.debug("REST request to get all ClubJoysticks");
        return clubJoystickService.findAll();
        }

    /**
     * GET  /club-joysticks/:id : get the "id" clubJoystick.
     *
     * @param id the id of the clubJoystick to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clubJoystick, or with status 404 (Not Found)
     */
    @GetMapping("/club-joysticks/{id}")
    @Timed
    public ResponseEntity<ClubJoystick> getClubJoystick(@PathVariable Long id) {
        log.debug("REST request to get ClubJoystick : {}", id);
        ClubJoystick clubJoystick = clubJoystickService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clubJoystick));
    }

    /**
     * DELETE  /club-joysticks/:id : delete the "id" clubJoystick.
     *
     * @param id the id of the clubJoystick to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/club-joysticks/{id}")
    @Timed
    public ResponseEntity<Void> deleteClubJoystick(@PathVariable Long id) {
        log.debug("REST request to delete ClubJoystick : {}", id);
        clubJoystickService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/club-joysticks?query=:query : search for the clubJoystick corresponding
     * to the query.
     *
     * @param query the query of the clubJoystick search
     * @return the result of the search
     */
    @GetMapping("/_search/club-joysticks")
    @Timed
    public List<ClubJoystick> searchClubJoysticks(@RequestParam String query) {
        log.debug("REST request to search ClubJoysticks for query {}", query);
        return clubJoystickService.search(query);
    }

    @GetMapping("/club-joysticks/by-club/{id}")
    @Timed
    public List<ClubJoystick> getAllClubJoysticks(@PathVariable Long id) {
        log.debug("REST request to get all ClubJoysticks of club: {}", id);
        return clubJoystickService.findAllOfClub(id);
    }

}
