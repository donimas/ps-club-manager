package kz.ast.donimas.ps.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.donimas.ps.domain.JoystickDictionary;
import kz.ast.donimas.ps.service.JoystickDictionaryService;
import kz.ast.donimas.ps.web.rest.errors.BadRequestAlertException;
import kz.ast.donimas.ps.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing JoystickDictionary.
 */
@RestController
@RequestMapping("/api")
public class JoystickDictionaryResource {

    private final Logger log = LoggerFactory.getLogger(JoystickDictionaryResource.class);

    private static final String ENTITY_NAME = "joystickDictionary";

    private final JoystickDictionaryService joystickDictionaryService;

    public JoystickDictionaryResource(JoystickDictionaryService joystickDictionaryService) {
        this.joystickDictionaryService = joystickDictionaryService;
    }

    /**
     * POST  /joystick-dictionaries : Create a new joystickDictionary.
     *
     * @param joystickDictionary the joystickDictionary to create
     * @return the ResponseEntity with status 201 (Created) and with body the new joystickDictionary, or with status 400 (Bad Request) if the joystickDictionary has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/joystick-dictionaries")
    @Timed
    public ResponseEntity<JoystickDictionary> createJoystickDictionary(@Valid @RequestBody JoystickDictionary joystickDictionary) throws URISyntaxException {
        log.debug("REST request to save JoystickDictionary : {}", joystickDictionary);
        if (joystickDictionary.getId() != null) {
            throw new BadRequestAlertException("A new joystickDictionary cannot already have an ID", ENTITY_NAME, "idexists");
        }
        JoystickDictionary result = joystickDictionaryService.save(joystickDictionary);
        return ResponseEntity.created(new URI("/api/joystick-dictionaries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /joystick-dictionaries : Updates an existing joystickDictionary.
     *
     * @param joystickDictionary the joystickDictionary to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated joystickDictionary,
     * or with status 400 (Bad Request) if the joystickDictionary is not valid,
     * or with status 500 (Internal Server Error) if the joystickDictionary couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/joystick-dictionaries")
    @Timed
    public ResponseEntity<JoystickDictionary> updateJoystickDictionary(@Valid @RequestBody JoystickDictionary joystickDictionary) throws URISyntaxException {
        log.debug("REST request to update JoystickDictionary : {}", joystickDictionary);
        if (joystickDictionary.getId() == null) {
            return createJoystickDictionary(joystickDictionary);
        }
        JoystickDictionary result = joystickDictionaryService.save(joystickDictionary);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, joystickDictionary.getId().toString()))
            .body(result);
    }

    /**
     * GET  /joystick-dictionaries : get all the joystickDictionaries.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of joystickDictionaries in body
     */
    @GetMapping("/joystick-dictionaries")
    @Timed
    public List<JoystickDictionary> getAllJoystickDictionaries() {
        log.debug("REST request to get all JoystickDictionaries");
        return joystickDictionaryService.findAll();
        }

    /**
     * GET  /joystick-dictionaries/:id : get the "id" joystickDictionary.
     *
     * @param id the id of the joystickDictionary to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the joystickDictionary, or with status 404 (Not Found)
     */
    @GetMapping("/joystick-dictionaries/{id}")
    @Timed
    public ResponseEntity<JoystickDictionary> getJoystickDictionary(@PathVariable Long id) {
        log.debug("REST request to get JoystickDictionary : {}", id);
        JoystickDictionary joystickDictionary = joystickDictionaryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(joystickDictionary));
    }

    /**
     * DELETE  /joystick-dictionaries/:id : delete the "id" joystickDictionary.
     *
     * @param id the id of the joystickDictionary to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/joystick-dictionaries/{id}")
    @Timed
    public ResponseEntity<Void> deleteJoystickDictionary(@PathVariable Long id) {
        log.debug("REST request to delete JoystickDictionary : {}", id);
        joystickDictionaryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/joystick-dictionaries?query=:query : search for the joystickDictionary corresponding
     * to the query.
     *
     * @param query the query of the joystickDictionary search
     * @return the result of the search
     */
    @GetMapping("/_search/joystick-dictionaries")
    @Timed
    public List<JoystickDictionary> searchJoystickDictionaries(@RequestParam String query) {
        log.debug("REST request to search JoystickDictionaries for query {}", query);
        return joystickDictionaryService.search(query);
    }

}
