package kz.ast.donimas.ps.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.donimas.ps.domain.AttachmentFile;
import kz.ast.donimas.ps.service.AttachmentFileService;
import kz.ast.donimas.ps.web.rest.errors.BadRequestAlertException;
import kz.ast.donimas.ps.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AttachmentFile.
 */
@RestController
@RequestMapping("/api")
public class AttachmentFileResource {

    private final Logger log = LoggerFactory.getLogger(AttachmentFileResource.class);

    private static final String ENTITY_NAME = "attachmentFile";

    private final AttachmentFileService attachmentFileService;

    public AttachmentFileResource(AttachmentFileService attachmentFileService) {
        this.attachmentFileService = attachmentFileService;
    }

    /**
     * POST  /attachment-files : Create a new attachmentFile.
     *
     * @param attachmentFile the attachmentFile to create
     * @return the ResponseEntity with status 201 (Created) and with body the new attachmentFile, or with status 400 (Bad Request) if the attachmentFile has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/attachment-files")
    @Timed
    public ResponseEntity<AttachmentFile> createAttachmentFile(@RequestBody AttachmentFile attachmentFile) throws URISyntaxException {
        log.debug("REST request to save AttachmentFile : {}", attachmentFile);
        if (attachmentFile.getId() != null) {
            throw new BadRequestAlertException("A new attachmentFile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AttachmentFile result = attachmentFileService.save(attachmentFile);
        return ResponseEntity.created(new URI("/api/attachment-files/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /attachment-files : Updates an existing attachmentFile.
     *
     * @param attachmentFile the attachmentFile to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated attachmentFile,
     * or with status 400 (Bad Request) if the attachmentFile is not valid,
     * or with status 500 (Internal Server Error) if the attachmentFile couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/attachment-files")
    @Timed
    public ResponseEntity<AttachmentFile> updateAttachmentFile(@RequestBody AttachmentFile attachmentFile) throws URISyntaxException {
        log.debug("REST request to update AttachmentFile : {}", attachmentFile);
        if (attachmentFile.getId() == null) {
            return createAttachmentFile(attachmentFile);
        }
        AttachmentFile result = attachmentFileService.save(attachmentFile);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, attachmentFile.getId().toString()))
            .body(result);
    }

    /**
     * GET  /attachment-files : get all the attachmentFiles.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of attachmentFiles in body
     */
    @GetMapping("/attachment-files")
    @Timed
    public List<AttachmentFile> getAllAttachmentFiles() {
        log.debug("REST request to get all AttachmentFiles");
        return attachmentFileService.findAll();
        }

    /**
     * GET  /attachment-files/:id : get the "id" attachmentFile.
     *
     * @param id the id of the attachmentFile to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the attachmentFile, or with status 404 (Not Found)
     */
    @GetMapping("/attachment-files/{id}")
    @Timed
    public ResponseEntity<AttachmentFile> getAttachmentFile(@PathVariable Long id) {
        log.debug("REST request to get AttachmentFile : {}", id);
        AttachmentFile attachmentFile = attachmentFileService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(attachmentFile));
    }

    /**
     * DELETE  /attachment-files/:id : delete the "id" attachmentFile.
     *
     * @param id the id of the attachmentFile to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/attachment-files/{id}")
    @Timed
    public ResponseEntity<Void> deleteAttachmentFile(@PathVariable Long id) {
        log.debug("REST request to delete AttachmentFile : {}", id);
        attachmentFileService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/attachment-files?query=:query : search for the attachmentFile corresponding
     * to the query.
     *
     * @param query the query of the attachmentFile search
     * @return the result of the search
     */
    @GetMapping("/_search/attachment-files")
    @Timed
    public List<AttachmentFile> searchAttachmentFiles(@RequestParam String query) {
        log.debug("REST request to search AttachmentFiles for query {}", query);
        return attachmentFileService.search(query);
    }

}
