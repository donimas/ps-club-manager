package kz.ast.donimas.ps.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.ast.donimas.ps.domain.ConsoleDictionary;
import kz.ast.donimas.ps.service.ConsoleDictionaryService;
import kz.ast.donimas.ps.web.rest.errors.BadRequestAlertException;
import kz.ast.donimas.ps.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ConsoleDictionary.
 */
@RestController
@RequestMapping("/api")
public class ConsoleDictionaryResource {

    private final Logger log = LoggerFactory.getLogger(ConsoleDictionaryResource.class);

    private static final String ENTITY_NAME = "consoleDictionary";

    private final ConsoleDictionaryService consoleDictionaryService;

    public ConsoleDictionaryResource(ConsoleDictionaryService consoleDictionaryService) {
        this.consoleDictionaryService = consoleDictionaryService;
    }

    /**
     * POST  /console-dictionaries : Create a new consoleDictionary.
     *
     * @param consoleDictionary the consoleDictionary to create
     * @return the ResponseEntity with status 201 (Created) and with body the new consoleDictionary, or with status 400 (Bad Request) if the consoleDictionary has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/console-dictionaries")
    @Timed
    public ResponseEntity<ConsoleDictionary> createConsoleDictionary(@Valid @RequestBody ConsoleDictionary consoleDictionary) throws URISyntaxException {
        log.debug("REST request to save ConsoleDictionary : {}", consoleDictionary);
        if (consoleDictionary.getId() != null) {
            throw new BadRequestAlertException("A new consoleDictionary cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ConsoleDictionary result = consoleDictionaryService.save(consoleDictionary);
        return ResponseEntity.created(new URI("/api/console-dictionaries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /console-dictionaries : Updates an existing consoleDictionary.
     *
     * @param consoleDictionary the consoleDictionary to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated consoleDictionary,
     * or with status 400 (Bad Request) if the consoleDictionary is not valid,
     * or with status 500 (Internal Server Error) if the consoleDictionary couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/console-dictionaries")
    @Timed
    public ResponseEntity<ConsoleDictionary> updateConsoleDictionary(@Valid @RequestBody ConsoleDictionary consoleDictionary) throws URISyntaxException {
        log.debug("REST request to update ConsoleDictionary : {}", consoleDictionary);
        if (consoleDictionary.getId() == null) {
            return createConsoleDictionary(consoleDictionary);
        }
        ConsoleDictionary result = consoleDictionaryService.save(consoleDictionary);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, consoleDictionary.getId().toString()))
            .body(result);
    }

    /**
     * GET  /console-dictionaries : get all the consoleDictionaries.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of consoleDictionaries in body
     */
    @GetMapping("/console-dictionaries")
    @Timed
    public List<ConsoleDictionary> getAllConsoleDictionaries() {
        log.debug("REST request to get all ConsoleDictionaries");
        return consoleDictionaryService.findAll();
        }

    /**
     * GET  /console-dictionaries/:id : get the "id" consoleDictionary.
     *
     * @param id the id of the consoleDictionary to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the consoleDictionary, or with status 404 (Not Found)
     */
    @GetMapping("/console-dictionaries/{id}")
    @Timed
    public ResponseEntity<ConsoleDictionary> getConsoleDictionary(@PathVariable Long id) {
        log.debug("REST request to get ConsoleDictionary : {}", id);
        ConsoleDictionary consoleDictionary = consoleDictionaryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(consoleDictionary));
    }

    /**
     * DELETE  /console-dictionaries/:id : delete the "id" consoleDictionary.
     *
     * @param id the id of the consoleDictionary to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/console-dictionaries/{id}")
    @Timed
    public ResponseEntity<Void> deleteConsoleDictionary(@PathVariable Long id) {
        log.debug("REST request to delete ConsoleDictionary : {}", id);
        consoleDictionaryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/console-dictionaries?query=:query : search for the consoleDictionary corresponding
     * to the query.
     *
     * @param query the query of the consoleDictionary search
     * @return the result of the search
     */
    @GetMapping("/_search/console-dictionaries")
    @Timed
    public List<ConsoleDictionary> searchConsoleDictionaries(@RequestParam String query) {
        log.debug("REST request to search ConsoleDictionaries for query {}", query);
        return consoleDictionaryService.search(query);
    }

}
