package kz.ast.donimas.ps.repository.search;

import kz.ast.donimas.ps.domain.Club;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Club entity.
 */
public interface ClubSearchRepository extends ElasticsearchRepository<Club, Long> {
}
