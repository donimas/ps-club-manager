package kz.ast.donimas.ps.repository;

import kz.ast.donimas.ps.domain.CityDictionary;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CityDictionary entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CityDictionaryRepository extends JpaRepository<CityDictionary, Long> {

}
