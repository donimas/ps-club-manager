package kz.ast.donimas.ps.repository.search;

import kz.ast.donimas.ps.domain.ConsoleDictionary;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ConsoleDictionary entity.
 */
public interface ConsoleDictionarySearchRepository extends ElasticsearchRepository<ConsoleDictionary, Long> {
}
