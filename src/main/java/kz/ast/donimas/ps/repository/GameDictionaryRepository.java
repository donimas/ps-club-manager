package kz.ast.donimas.ps.repository;

import kz.ast.donimas.ps.domain.GameDictionary;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the GameDictionary entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GameDictionaryRepository extends JpaRepository<GameDictionary, Long> {

}
