package kz.ast.donimas.ps.repository.search;

import kz.ast.donimas.ps.domain.JoySessionLife;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the JoySessionLife entity.
 */
public interface JoySessionLifeSearchRepository extends ElasticsearchRepository<JoySessionLife, Long> {
}
