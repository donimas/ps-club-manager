package kz.ast.donimas.ps.repository.search;

import kz.ast.donimas.ps.domain.AttachmentFile;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the AttachmentFile entity.
 */
public interface AttachmentFileSearchRepository extends ElasticsearchRepository<AttachmentFile, Long> {
}
