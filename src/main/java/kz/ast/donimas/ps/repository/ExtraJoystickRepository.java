package kz.ast.donimas.ps.repository;

import kz.ast.donimas.ps.domain.ExtraJoystick;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ExtraJoystick entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExtraJoystickRepository extends JpaRepository<ExtraJoystick, Long> {

}
