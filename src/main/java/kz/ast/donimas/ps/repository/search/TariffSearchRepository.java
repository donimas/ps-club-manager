package kz.ast.donimas.ps.repository.search;

import kz.ast.donimas.ps.domain.Tariff;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Tariff entity.
 */
public interface TariffSearchRepository extends ElasticsearchRepository<Tariff, Long> {
}
