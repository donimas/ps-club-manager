package kz.ast.donimas.ps.repository.search;

import kz.ast.donimas.ps.domain.ClubJoystick;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ClubJoystick entity.
 */
public interface ClubJoystickSearchRepository extends ElasticsearchRepository<ClubJoystick, Long> {
}
