package kz.ast.donimas.ps.repository.search;

import kz.ast.donimas.ps.domain.PricePackage;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PricePackage entity.
 */
public interface PricePackageSearchRepository extends ElasticsearchRepository<PricePackage, Long> {
}
