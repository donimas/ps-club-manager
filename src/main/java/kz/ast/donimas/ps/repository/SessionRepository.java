package kz.ast.donimas.ps.repository;

import kz.ast.donimas.ps.domain.Session;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the Session entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SessionRepository extends JpaRepository<Session, Long> {

    List<Session> findAllByZoneId(Long id);

}
