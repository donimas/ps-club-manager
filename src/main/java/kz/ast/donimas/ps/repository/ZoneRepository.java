package kz.ast.donimas.ps.repository;

import kz.ast.donimas.ps.domain.Zone;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the Zone entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ZoneRepository extends JpaRepository<Zone, Long> {

    List<Zone> findAllByClubIdAndFlagDeletedFalse(Long id);

}
