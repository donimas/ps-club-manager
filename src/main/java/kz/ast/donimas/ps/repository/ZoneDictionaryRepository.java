package kz.ast.donimas.ps.repository;

import kz.ast.donimas.ps.domain.ZoneDictionary;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ZoneDictionary entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ZoneDictionaryRepository extends JpaRepository<ZoneDictionary, Long> {

}
