package kz.ast.donimas.ps.repository;

import kz.ast.donimas.ps.domain.Club;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Club entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClubRepository extends JpaRepository<Club, Long> {
    @Query("select distinct club from Club club left join fetch club.consoles left join fetch club.games left join fetch club.galleries")
    List<Club> findAllWithEagerRelationships();

    @Query("select club from Club club left join fetch club.consoles left join fetch club.games left join fetch club.galleries where club.id =:id")
    Club findOneWithEagerRelationships(@Param("id") Long id);

}
