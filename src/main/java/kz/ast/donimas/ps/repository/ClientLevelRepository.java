package kz.ast.donimas.ps.repository;

import kz.ast.donimas.ps.domain.ClientLevel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ClientLevel entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientLevelRepository extends JpaRepository<ClientLevel, Long> {

    Page<ClientLevel> findAllByClubIdAndFlagDeletedFalse(Long id, Pageable pageable);

}
