package kz.ast.donimas.ps.repository;

import kz.ast.donimas.ps.domain.JoySessionLife;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the JoySessionLife entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JoySessionLifeRepository extends JpaRepository<JoySessionLife, Long> {

}
