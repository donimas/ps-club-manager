package kz.ast.donimas.ps.repository;

import kz.ast.donimas.ps.domain.PricePackage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the PricePackage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PricePackageRepository extends JpaRepository<PricePackage, Long> {

    Page<PricePackage> findAllByClubIdAndFlagDeletedFalse(Long id, Pageable pageable);

}
