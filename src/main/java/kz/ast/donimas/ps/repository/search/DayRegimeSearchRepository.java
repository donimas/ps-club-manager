package kz.ast.donimas.ps.repository.search;

import kz.ast.donimas.ps.domain.DayRegime;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the DayRegime entity.
 */
public interface DayRegimeSearchRepository extends ElasticsearchRepository<DayRegime, Long> {
}
