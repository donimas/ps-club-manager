package kz.ast.donimas.ps.repository.search;

import kz.ast.donimas.ps.domain.GamePleasure;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the GamePleasure entity.
 */
public interface GamePleasureSearchRepository extends ElasticsearchRepository<GamePleasure, Long> {
}
