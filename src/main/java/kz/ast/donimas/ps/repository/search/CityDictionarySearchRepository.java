package kz.ast.donimas.ps.repository.search;

import kz.ast.donimas.ps.domain.CityDictionary;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the CityDictionary entity.
 */
public interface CityDictionarySearchRepository extends ElasticsearchRepository<CityDictionary, Long> {
}
