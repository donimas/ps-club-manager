package kz.ast.donimas.ps.repository.search;

import kz.ast.donimas.ps.domain.Zone;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Zone entity.
 */
public interface ZoneSearchRepository extends ElasticsearchRepository<Zone, Long> {
}
