package kz.ast.donimas.ps.repository;

import kz.ast.donimas.ps.domain.AttachmentFile;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AttachmentFile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AttachmentFileRepository extends JpaRepository<AttachmentFile, Long> {

}
