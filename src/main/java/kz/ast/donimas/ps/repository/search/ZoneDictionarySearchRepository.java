package kz.ast.donimas.ps.repository.search;

import kz.ast.donimas.ps.domain.ZoneDictionary;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ZoneDictionary entity.
 */
public interface ZoneDictionarySearchRepository extends ElasticsearchRepository<ZoneDictionary, Long> {
}
