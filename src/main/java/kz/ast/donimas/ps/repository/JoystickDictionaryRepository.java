package kz.ast.donimas.ps.repository;

import kz.ast.donimas.ps.domain.JoystickDictionary;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the JoystickDictionary entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JoystickDictionaryRepository extends JpaRepository<JoystickDictionary, Long> {

}
