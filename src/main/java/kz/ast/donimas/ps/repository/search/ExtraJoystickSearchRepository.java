package kz.ast.donimas.ps.repository.search;

import kz.ast.donimas.ps.domain.ExtraJoystick;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ExtraJoystick entity.
 */
public interface ExtraJoystickSearchRepository extends ElasticsearchRepository<ExtraJoystick, Long> {
}
