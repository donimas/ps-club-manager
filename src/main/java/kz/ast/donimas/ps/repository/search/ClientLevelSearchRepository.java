package kz.ast.donimas.ps.repository.search;

import kz.ast.donimas.ps.domain.ClientLevel;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ClientLevel entity.
 */
public interface ClientLevelSearchRepository extends ElasticsearchRepository<ClientLevel, Long> {
}
