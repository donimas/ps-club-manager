package kz.ast.donimas.ps.repository;

import kz.ast.donimas.ps.domain.GamePleasure;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the GamePleasure entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GamePleasureRepository extends JpaRepository<GamePleasure, Long> {

}
