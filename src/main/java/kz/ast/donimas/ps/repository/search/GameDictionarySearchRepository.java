package kz.ast.donimas.ps.repository.search;

import kz.ast.donimas.ps.domain.GameDictionary;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the GameDictionary entity.
 */
public interface GameDictionarySearchRepository extends ElasticsearchRepository<GameDictionary, Long> {
}
