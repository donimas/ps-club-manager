package kz.ast.donimas.ps.repository;

import kz.ast.donimas.ps.domain.DayRegime;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the DayRegime entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DayRegimeRepository extends JpaRepository<DayRegime, Long> {

    List<DayRegime> findAllByClubId(Long id);

}
