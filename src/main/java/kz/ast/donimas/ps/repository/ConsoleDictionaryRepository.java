package kz.ast.donimas.ps.repository;

import kz.ast.donimas.ps.domain.ConsoleDictionary;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ConsoleDictionary entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConsoleDictionaryRepository extends JpaRepository<ConsoleDictionary, Long> {

}
