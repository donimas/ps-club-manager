package kz.ast.donimas.ps.repository;

import kz.ast.donimas.ps.domain.ClubJoystick;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the ClubJoystick entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClubJoystickRepository extends JpaRepository<ClubJoystick, Long> {

    List<ClubJoystick> findAllByClubIdAndFlagDeletedFalse(Long id);

}
