package kz.ast.donimas.ps.repository.search;

import kz.ast.donimas.ps.domain.JoystickDictionary;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the JoystickDictionary entity.
 */
public interface JoystickDictionarySearchRepository extends ElasticsearchRepository<JoystickDictionary, Long> {
}
