package kz.ast.donimas.ps.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A JoySessionLife.
 */
@Entity
@Table(name = "joy_session_life")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "joysessionlife")
public class JoySessionLife implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "started_at")
    private ZonedDateTime startedAt;

    @Column(name = "finished_at")
    private ZonedDateTime finishedAt;

    @Column(name = "joy_hours", precision=10, scale=2)
    private BigDecimal joyHours;

    @Column(name = "joy_minutes", precision=10, scale=2)
    private BigDecimal joyMinutes;

    @Column(name = "total_price", precision=10, scale=2)
    private BigDecimal totalPrice;

    @ManyToOne
    private PricePackage pricePackage;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getStartedAt() {
        return startedAt;
    }

    public JoySessionLife startedAt(ZonedDateTime startedAt) {
        this.startedAt = startedAt;
        return this;
    }

    public void setStartedAt(ZonedDateTime startedAt) {
        this.startedAt = startedAt;
    }

    public ZonedDateTime getFinishedAt() {
        return finishedAt;
    }

    public JoySessionLife finishedAt(ZonedDateTime finishedAt) {
        this.finishedAt = finishedAt;
        return this;
    }

    public void setFinishedAt(ZonedDateTime finishedAt) {
        this.finishedAt = finishedAt;
    }

    public BigDecimal getJoyHours() {
        return joyHours;
    }

    public JoySessionLife joyHours(BigDecimal joyHours) {
        this.joyHours = joyHours;
        return this;
    }

    public void setJoyHours(BigDecimal joyHours) {
        this.joyHours = joyHours;
    }

    public BigDecimal getJoyMinutes() {
        return joyMinutes;
    }

    public JoySessionLife joyMinutes(BigDecimal joyMinutes) {
        this.joyMinutes = joyMinutes;
        return this;
    }

    public void setJoyMinutes(BigDecimal joyMinutes) {
        this.joyMinutes = joyMinutes;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public JoySessionLife totalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public PricePackage getPricePackage() {
        return pricePackage;
    }

    public JoySessionLife pricePackage(PricePackage pricePackage) {
        this.pricePackage = pricePackage;
        return this;
    }

    public void setPricePackage(PricePackage pricePackage) {
        this.pricePackage = pricePackage;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        JoySessionLife joySessionLife = (JoySessionLife) o;
        if (joySessionLife.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), joySessionLife.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "JoySessionLife{" +
            "id=" + getId() +
            ", startedAt='" + getStartedAt() + "'" +
            ", finishedAt='" + getFinishedAt() + "'" +
            ", joyHours=" + getJoyHours() +
            ", joyMinutes=" + getJoyMinutes() +
            ", totalPrice=" + getTotalPrice() +
            "}";
    }
}
