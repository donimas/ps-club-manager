package kz.ast.donimas.ps.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import kz.ast.donimas.ps.domain.enumeration.SessionStatus;

/**
 * A Session.
 */
@Entity
@Table(name = "session")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "session")
public class Session implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "started_at", nullable = false)
    private ZonedDateTime startedAt;

    @NotNull
    @Column(name = "finished_at", nullable = false)
    private ZonedDateTime finishedAt;

    @Column(name = "played_hours", precision=10, scale=2)
    private BigDecimal playedHours;

    @Column(name = "played_minutes", precision=10, scale=2)
    private BigDecimal playedMinutes;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private SessionStatus status;

    @Column(name = "discount_percent", precision=10, scale=2)
    private BigDecimal discountPercent;

    @Column(name = "discount_minutes", precision=10, scale=2)
    private BigDecimal discountMinutes;

    @Column(name = "flag_opened_time")
    private Boolean flagOpenedTime;

    @Column(name = "flag_cancelled")
    private Boolean flagCancelled;

    @Column(name = "planned_price", precision=10, scale=2)
    private BigDecimal plannedPrice;

    @Column(name = "total_price", precision=10, scale=2)
    private BigDecimal totalPrice;

    @Column(name = "planned_hours")
    private BigDecimal plannedHours;
    @Column(name = "planned_minutes")
    private BigDecimal plannedMinutes;

    @ManyToOne
    private Zone zone;

    @ManyToOne
    private PricePackage pricePackage;

    @ManyToOne
    private Tariff pricePerHour;

    @ManyToOne
    private Client client;

    @OneToMany(mappedBy = "session")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ExtraJoystick> extraJoysticks = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "session_joy_session_life",
        joinColumns = @JoinColumn(name="session_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="joy_session_life_id", referencedColumnName="id"))
    private Set<JoySessionLife> joySessionLives = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getStartedAt() {
        return startedAt;
    }

    public Session startedAt(ZonedDateTime startedAt) {
        this.startedAt = startedAt;
        return this;
    }

    public void setStartedAt(ZonedDateTime startedAt) {
        this.startedAt = startedAt;
    }

    public ZonedDateTime getFinishedAt() {
        return finishedAt;
    }

    public Session finishedAt(ZonedDateTime finishedAt) {
        this.finishedAt = finishedAt;
        return this;
    }

    public void setFinishedAt(ZonedDateTime finishedAt) {
        this.finishedAt = finishedAt;
    }

    public BigDecimal getPlayedHours() {
        return playedHours;
    }

    public Session playedHours(BigDecimal playedHours) {
        this.playedHours = playedHours;
        return this;
    }

    public void setPlayedHours(BigDecimal playedHours) {
        this.playedHours = playedHours;
    }

    public BigDecimal getPlayedMinutes() {
        return playedMinutes;
    }

    public Session playedMinutes(BigDecimal playedMinutes) {
        this.playedMinutes = playedMinutes;
        return this;
    }

    public void setPlayedMinutes(BigDecimal playedMinutes) {
        this.playedMinutes = playedMinutes;
    }

    public SessionStatus getStatus() {
        return status;
    }

    public Session status(SessionStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(SessionStatus status) {
        this.status = status;
    }

    public BigDecimal getDiscountPercent() {
        return discountPercent;
    }

    public Session discountPercent(BigDecimal discountPercent) {
        this.discountPercent = discountPercent;
        return this;
    }

    public void setDiscountPercent(BigDecimal discountPercent) {
        this.discountPercent = discountPercent;
    }

    public BigDecimal getDiscountMinutes() {
        return discountMinutes;
    }

    public Session discountMinutes(BigDecimal discountMinutes) {
        this.discountMinutes = discountMinutes;
        return this;
    }

    public void setDiscountMinutes(BigDecimal discountMinutes) {
        this.discountMinutes = discountMinutes;
    }

    public Boolean isFlagOpenedTime() {
        return flagOpenedTime;
    }

    public Session flagOpenedTime(Boolean flagOpenedTime) {
        this.flagOpenedTime = flagOpenedTime;
        return this;
    }

    public void setFlagOpenedTime(Boolean flagOpenedTime) {
        this.flagOpenedTime = flagOpenedTime;
    }

    public Boolean isFlagCancelled() {
        return flagCancelled;
    }

    public Session flagCancelled(Boolean flagCancelled) {
        this.flagCancelled = flagCancelled;
        return this;
    }

    public void setFlagCancelled(Boolean flagCancelled) {
        this.flagCancelled = flagCancelled;
    }

    public BigDecimal getPlannedPrice() {
        return plannedPrice;
    }

    public Session plannedPrice(BigDecimal plannedPrice) {
        this.plannedPrice = plannedPrice;
        return this;
    }

    public void setPlannedPrice(BigDecimal plannedPrice) {
        this.plannedPrice = plannedPrice;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public Session totalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Zone getZone() {
        return zone;
    }

    public Session zone(Zone zone) {
        this.zone = zone;
        return this;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }

    public PricePackage getPricePackage() {
        return pricePackage;
    }

    public Session pricePackage(PricePackage pricePackage) {
        this.pricePackage = pricePackage;
        return this;
    }

    public void setPricePackage(PricePackage pricePackage) {
        this.pricePackage = pricePackage;
    }

    public Tariff getPricePerHour() {
        return pricePerHour;
    }

    public Session pricePerHour(Tariff tariff) {
        this.pricePerHour = tariff;
        return this;
    }

    public void setPricePerHour(Tariff tariff) {
        this.pricePerHour = tariff;
    }

    public Client getClient() {
        return client;
    }

    public Session client(Client client) {
        this.client = client;
        return this;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Set<ExtraJoystick> getExtraJoysticks() {
        return extraJoysticks;
    }

    public Session extraJoysticks(Set<ExtraJoystick> extraJoysticks) {
        this.extraJoysticks = extraJoysticks;
        return this;
    }

    public Session addExtraJoystick(ExtraJoystick extraJoystick) {
        this.extraJoysticks.add(extraJoystick);
        extraJoystick.setSession(this);
        return this;
    }

    public Session removeExtraJoystick(ExtraJoystick extraJoystick) {
        this.extraJoysticks.remove(extraJoystick);
        extraJoystick.setSession(null);
        return this;
    }

    public void setExtraJoysticks(Set<ExtraJoystick> extraJoysticks) {
        this.extraJoysticks = extraJoysticks;
    }

    public Set<JoySessionLife> getJoySessionLives() {
        return joySessionLives;
    }

    public void setJoySessionLives(Set<JoySessionLife> joySessionLives) {
        this.joySessionLives = joySessionLives;
    }

    public BigDecimal getPlannedHours() {
        return plannedHours;
    }

    public void setPlannedHours(BigDecimal plannedHours) {
        this.plannedHours = plannedHours;
    }

    public BigDecimal getPlannedMinutes() {
        return plannedMinutes;
    }

    public void setPlannedMinutes(BigDecimal plannedMinutes) {
        this.plannedMinutes = plannedMinutes;
    }
// jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Session session = (Session) o;
        if (session.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), session.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Session{" +
            "id=" + getId() +
            ", startedAt='" + getStartedAt() + "'" +
            ", finishedAt='" + getFinishedAt() + "'" +
            ", playedHours=" + getPlayedHours() +
            ", playedMinutes=" + getPlayedMinutes() +
            ", status='" + getStatus() + "'" +
            ", discountPercent=" + getDiscountPercent() +
            ", discountMinutes=" + getDiscountMinutes() +
            ", flagOpenedTime='" + isFlagOpenedTime() + "'" +
            ", flagCancelled='" + isFlagCancelled() + "'" +
            ", plannedPrice=" + getPlannedPrice() +
            ", totalPrice=" + getTotalPrice() +
            ", joySessionLives=" + getJoySessionLives() +
            ", plannedMinutes=" + getPlannedMinutes() +
            ", plannedHours=" + getPlannedHours() +
            "}";
    }
}
