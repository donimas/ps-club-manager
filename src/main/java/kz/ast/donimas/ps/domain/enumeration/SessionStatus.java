package kz.ast.donimas.ps.domain.enumeration;

/**
 * The SessionStatus enumeration.
 */
public enum SessionStatus {
    STARTED, STOPPED, BOOKED
}
