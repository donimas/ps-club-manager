package kz.ast.donimas.ps.domain.enumeration;

/**
 * The CurrencyType enumeration.
 */
public enum CurrencyType {
    KZT, RU, USD, EUR
}
