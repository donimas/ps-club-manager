package kz.ast.donimas.ps.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A ClubJoystick.
 */
@Entity
@Table(name = "club_joystick")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "clubjoystick")
public class ClubJoystick implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "amount")
    private Integer amount;

    @ManyToOne
    private Club club;

    @ManyToOne
    private JoystickDictionary joystickDictionary;

    @ManyToOne
    private Tariff priceForExtraOnePerHour;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted = false;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public ClubJoystick amount(Integer amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Club getClub() {
        return club;
    }

    public ClubJoystick club(Club club) {
        this.club = club;
        return this;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public JoystickDictionary getJoystickDictionary() {
        return joystickDictionary;
    }

    public ClubJoystick joystickDictionary(JoystickDictionary joystickDictionary) {
        this.joystickDictionary = joystickDictionary;
        return this;
    }

    public void setJoystickDictionary(JoystickDictionary joystickDictionary) {
        this.joystickDictionary = joystickDictionary;
    }

    public Tariff getPriceForExtraOnePerHour() {
        return priceForExtraOnePerHour;
    }

    public ClubJoystick priceForExtraOnePerHour(Tariff tariff) {
        this.priceForExtraOnePerHour = tariff;
        return this;
    }

    public Boolean getFlagDeleted() {
        return flagDeleted;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public void setPriceForExtraOnePerHour(Tariff tariff) {

        this.priceForExtraOnePerHour = tariff;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ClubJoystick clubJoystick = (ClubJoystick) o;
        if (clubJoystick.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clubJoystick.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClubJoystick{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", flagDeleted=" + getFlagDeleted() +
            "}";
    }
}
