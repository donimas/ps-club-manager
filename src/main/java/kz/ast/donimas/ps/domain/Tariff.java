package kz.ast.donimas.ps.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

import kz.ast.donimas.ps.domain.enumeration.CurrencyType;

/**
 * A Tariff.
 */
@Entity
@Table(name = "tariff")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "tariff")
public class Tariff implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "price", precision=10, scale=2, nullable = false)
    private BigDecimal price;

    @Enumerated(EnumType.STRING)
    @Column(name = "currency")
    private CurrencyType currency;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    @Column(name = "container_class")
    private String containerClass;

    @Column(name = "container_id")
    private Long containerId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Tariff price(BigDecimal price) {
        this.price = price;
        return this;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public CurrencyType getCurrency() {
        return currency;
    }

    public Tariff currency(CurrencyType currency) {
        this.currency = currency;
        return this;
    }

    public void setCurrency(CurrencyType currency) {
        this.currency = currency;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public Tariff createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public Boolean isFlagDeleted() {
        return flagDeleted;
    }

    public Tariff flagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
        return this;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public String getContainerClass() {
        return containerClass;
    }

    public Tariff containerClass(String containerClass) {
        this.containerClass = containerClass;
        return this;
    }

    public void setContainerClass(String containerClass) {
        this.containerClass = containerClass;
    }

    public Long getContainerId() {
        return containerId;
    }

    public Tariff containerId(Long containerId) {
        this.containerId = containerId;
        return this;
    }

    public void setContainerId(Long containerId) {
        this.containerId = containerId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tariff tariff = (Tariff) o;
        if (tariff.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tariff.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Tariff{" +
            "id=" + getId() +
            ", price=" + getPrice() +
            ", currency='" + getCurrency() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", flagDeleted='" + isFlagDeleted() + "'" +
            ", containerClass='" + getContainerClass() + "'" +
            ", containerId=" + getContainerId() +
            "}";
    }
}
