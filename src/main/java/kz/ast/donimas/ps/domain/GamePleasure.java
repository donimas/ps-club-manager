package kz.ast.donimas.ps.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A GamePleasure.
 */
@Entity
@Table(name = "game_pleasure")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "gamepleasure")
public class GamePleasure implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "rating", precision=10, scale=2)
    private BigDecimal rating;

    @Column(name = "review")
    private String review;

    @ManyToOne
    private Session session;

    @ManyToOne
    private Club club;

    @ManyToOne
    private Client client;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getRating() {
        return rating;
    }

    public GamePleasure rating(BigDecimal rating) {
        this.rating = rating;
        return this;
    }

    public void setRating(BigDecimal rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public GamePleasure review(String review) {
        this.review = review;
        return this;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public Session getSession() {
        return session;
    }

    public GamePleasure session(Session session) {
        this.session = session;
        return this;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public Club getClub() {
        return club;
    }

    public GamePleasure club(Club club) {
        this.club = club;
        return this;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public Client getClient() {
        return client;
    }

    public GamePleasure client(Client client) {
        this.client = client;
        return this;
    }

    public void setClient(Client client) {
        this.client = client;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GamePleasure gamePleasure = (GamePleasure) o;
        if (gamePleasure.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), gamePleasure.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "GamePleasure{" +
            "id=" + getId() +
            ", rating=" + getRating() +
            ", review='" + getReview() + "'" +
            "}";
    }
}
