package kz.ast.donimas.ps.domain.enumeration;

/**
 * The DurationType enumeration.
 */
public enum DurationType {
    // transient
    HOURS,
    STARTEND,
    MINUTES,
    AN_HOUR,
    SERVICE
}
