package kz.ast.donimas.ps.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Club.
 */
@Entity
@Table(name = "club")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "club")
public class Club implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "full_address", nullable = false)
    private String fullAddress;

    @Column(name = "lon")
    private String lon;

    @Column(name = "lat")
    private String lat;

    @Column(name = "time_schedule_regulation", precision=10, scale=2)
    private BigDecimal timeScheduleRegulation;

    @Column(name = "flaghookah")
    private Boolean flaghookah;

    @NotNull
    @Column(name = "phone", nullable = false)
    private String phone;

    @Column(name = "phone_optional")
    private String phoneOptional;

    @Column(name = "web_url")
    private String webUrl;

    @Column(name = "vk_url")
    private String vkUrl;

    @Column(name = "insta_url")
    private String instaUrl;

    @Column(name = "telegram_url")
    private String telegramUrl;

    @Column(name = "facebook_url")
    private String facebookUrl;

    @Column(name = "twitter_url")
    private String twitterUrl;

    @ManyToOne
    private AttachmentFile avatar;

    @Column(name = "flag_published")
    private Boolean flagPublished = false;
    @Column(name = "flag_main_info_saved")
    private Boolean flagMainInfoSaved = false;
    @Column(name = "flag_contact_saved")
    private Boolean flagContactSaved = false;
    @Column(name = "flag_location_saved")
    private Boolean flagLocationSaved = false;
    @Column(name = "flag_zone_saved")
    private Boolean flagZoneSaved = false;

    @Column(name = "flag_regulation_saved")
    private Boolean flagRegulationSaved = false;
    @Column(name = "flag_equipment_saved")
    private Boolean flagEquipmentSaved = false;
    @Column(name = "flag_price_package_saved")
    private Boolean flagPricePackageSaved = false;
    @Column(name = "flag_client_level_saved")
    private Boolean flagClientLevelSaved = false;

    // in minutes
    @Column(name = "notify_before")
    private BigDecimal notifyBefore;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "club_console",
               joinColumns = @JoinColumn(name="clubs_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="consoles_id", referencedColumnName="id"))
    private Set<ConsoleDictionary> consoles = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "club_game",
               joinColumns = @JoinColumn(name="clubs_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="games_id", referencedColumnName="id"))
    private Set<GameDictionary> games = new HashSet<>();

    @OneToMany(mappedBy = "club")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ClubJoystick> joysticks = new HashSet<>();

    @OneToMany(mappedBy = "club")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Zone> zones = new HashSet<>();

    @OneToMany(mappedBy = "club")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PricePackage> pricePackages = new HashSet<>();

    @OneToMany(mappedBy = "club")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ClientLevel> clientLevels = new HashSet<>();

    @OneToMany(mappedBy = "club")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DayRegime> workRegimes = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "club_gallery",
               joinColumns = @JoinColumn(name="clubs_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="galleries_id", referencedColumnName="id"))
    private Set<AttachmentFile> galleries = new HashSet<>();

    @ManyToOne
    private Location location;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Club name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Club description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public Club fullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
        return this;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getLon() {
        return lon;
    }

    public Club lon(String lon) {
        this.lon = lon;
        return this;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public Club lat(String lat) {
        this.lat = lat;
        return this;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public BigDecimal getTimeScheduleRegulation() {
        return timeScheduleRegulation;
    }

    public Club timeScheduleRegulation(BigDecimal timeScheduleRegulation) {
        this.timeScheduleRegulation = timeScheduleRegulation;
        return this;
    }

    public void setTimeScheduleRegulation(BigDecimal timeScheduleRegulation) {
        this.timeScheduleRegulation = timeScheduleRegulation;
    }

    public Boolean isFlaghookah() {
        return flaghookah;
    }

    public Club flaghookah(Boolean flaghookah) {
        this.flaghookah = flaghookah;
        return this;
    }

    public void setFlaghookah(Boolean flaghookah) {
        this.flaghookah = flaghookah;
    }

    public String getPhone() {
        return phone;
    }

    public Club phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneOptional() {
        return phoneOptional;
    }

    public Club phoneOptional(String phoneOptional) {
        this.phoneOptional = phoneOptional;
        return this;
    }

    public void setPhoneOptional(String phoneOptional) {
        this.phoneOptional = phoneOptional;
    }

    public String getVkUrl() {
        return vkUrl;
    }

    public Club vkUrl(String vkUrl) {
        this.vkUrl = vkUrl;
        return this;
    }

    public void setVkUrl(String vkUrl) {
        this.vkUrl = vkUrl;
    }

    public String getInstaUrl() {
        return instaUrl;
    }

    public Club instaUrl(String instaUrl) {
        this.instaUrl = instaUrl;
        return this;
    }

    public void setInstaUrl(String instaUrl) {
        this.instaUrl = instaUrl;
    }

    public String getTelegramUrl() {
        return telegramUrl;
    }

    public Club telegramUrl(String telegramUrl) {
        this.telegramUrl = telegramUrl;
        return this;
    }

    public void setTelegramUrl(String telegramUrl) {
        this.telegramUrl = telegramUrl;
    }

    public String getFacebookUrl() {
        return facebookUrl;
    }

    public Club facebookUrl(String facebookUrl) {
        this.facebookUrl = facebookUrl;
        return this;
    }

    public void setFacebookUrl(String facebookUrl) {
        this.facebookUrl = facebookUrl;
    }

    public String getTwitterUrl() {
        return twitterUrl;
    }

    public Club twitterUrl(String twitterUrl) {
        this.twitterUrl = twitterUrl;
        return this;
    }

    public void setTwitterUrl(String twitterUrl) {
        this.twitterUrl = twitterUrl;
    }

    public AttachmentFile getAvatar() {
        return avatar;
    }

    public Club avatar(AttachmentFile attachmentFile) {
        this.avatar = attachmentFile;
        return this;
    }

    public void setAvatar(AttachmentFile attachmentFile) {
        this.avatar = attachmentFile;
    }

    public Set<ConsoleDictionary> getConsoles() {
        return consoles;
    }

    public Club consoles(Set<ConsoleDictionary> consoleDictionaries) {
        this.consoles = consoleDictionaries;
        return this;
    }

    public Club addConsole(ConsoleDictionary consoleDictionary) {
        this.consoles.add(consoleDictionary);
        return this;
    }

    public Club removeConsole(ConsoleDictionary consoleDictionary) {
        this.consoles.remove(consoleDictionary);
        return this;
    }

    public void setConsoles(Set<ConsoleDictionary> consoleDictionaries) {
        this.consoles = consoleDictionaries;
    }

    public Set<GameDictionary> getGames() {
        return games;
    }

    public Club games(Set<GameDictionary> gameDictionaries) {
        this.games = gameDictionaries;
        return this;
    }

    public Club addGame(GameDictionary gameDictionary) {
        this.games.add(gameDictionary);
        return this;
    }

    public Club removeGame(GameDictionary gameDictionary) {
        this.games.remove(gameDictionary);
        return this;
    }

    public void setGames(Set<GameDictionary> gameDictionaries) {
        this.games = gameDictionaries;
    }

    public Set<ClubJoystick> getJoysticks() {
        return joysticks;
    }

    public Club joysticks(Set<ClubJoystick> clubJoysticks) {
        this.joysticks = clubJoysticks;
        return this;
    }

    public Club addJoystick(ClubJoystick clubJoystick) {
        this.joysticks.add(clubJoystick);
        clubJoystick.setClub(this);
        return this;
    }

    public Club removeJoystick(ClubJoystick clubJoystick) {
        this.joysticks.remove(clubJoystick);
        clubJoystick.setClub(null);
        return this;
    }

    public void setJoysticks(Set<ClubJoystick> clubJoysticks) {
        this.joysticks = clubJoysticks;
    }

    public Set<Zone> getZones() {
        return zones;
    }

    public Club zones(Set<Zone> zones) {
        this.zones = zones;
        return this;
    }

    public Club addZone(Zone zone) {
        this.zones.add(zone);
        zone.setClub(this);
        return this;
    }

    public Club removeZone(Zone zone) {
        this.zones.remove(zone);
        zone.setClub(null);
        return this;
    }

    public void setZones(Set<Zone> zones) {
        this.zones = zones;
    }

    public Set<DayRegime> getWorkRegimes() {
        return workRegimes;
    }

    public Club workRegimes(Set<DayRegime> dayRegimes) {
        this.workRegimes = dayRegimes;
        return this;
    }

    public Club addWorkRegime(DayRegime dayRegime) {
        this.workRegimes.add(dayRegime);
        dayRegime.setClub(this);
        return this;
    }

    public Club removeWorkRegime(DayRegime dayRegime) {
        this.workRegimes.remove(dayRegime);
        dayRegime.setClub(null);
        return this;
    }

    public void setWorkRegimes(Set<DayRegime> dayRegimes) {
        this.workRegimes = dayRegimes;
    }

    public Set<AttachmentFile> getGalleries() {
        return galleries;
    }

    public Club galleries(Set<AttachmentFile> attachmentFiles) {
        this.galleries = attachmentFiles;
        return this;
    }

    public Club addGallery(AttachmentFile attachmentFile) {
        this.galleries.add(attachmentFile);
        return this;
    }

    public Club removeGallery(AttachmentFile attachmentFile) {
        this.galleries.remove(attachmentFile);
        return this;
    }

    public void setGalleries(Set<AttachmentFile> attachmentFiles) {
        this.galleries = attachmentFiles;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public Boolean getFlaghookah() {
        return flaghookah;
    }

    public Boolean getFlagPublished() {
        return flagPublished;
    }

    public void setFlagPublished(Boolean flagPublished) {
        this.flagPublished = flagPublished;
    }

    public Boolean getFlagMainInfoSaved() {
        return flagMainInfoSaved;
    }

    public void setFlagMainInfoSaved(Boolean flagMainInfoSaved) {
        this.flagMainInfoSaved = flagMainInfoSaved;
    }

    public Boolean getFlagContactSaved() {
        return flagContactSaved;
    }

    public void setFlagContactSaved(Boolean flagContactSaved) {
        this.flagContactSaved = flagContactSaved;
    }

    public Boolean getFlagZoneSaved() {
        return flagZoneSaved;
    }

    public void setFlagZoneSaved(Boolean flagZoneSaved) {
        this.flagZoneSaved = flagZoneSaved;
    }

    public BigDecimal getNotifyBefore() {
        return notifyBefore;
    }

    public void setNotifyBefore(BigDecimal notifyBefore) {
        this.notifyBefore = notifyBefore;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Boolean getFlagLocationSaved() {
        return flagLocationSaved;
    }

    public void setFlagLocationSaved(Boolean flagLocationSaved) {
        this.flagLocationSaved = flagLocationSaved;
    }

    public Set<PricePackage> getPricePackages() {
        return pricePackages;
    }

    public void setPricePackages(Set<PricePackage> pricePackages) {
        this.pricePackages = pricePackages;
    }

    public Set<ClientLevel> getClientLevels() {
        return clientLevels;
    }

    public void setClientLevels(Set<ClientLevel> clientLevels) {
        this.clientLevels = clientLevels;
    }

    public Boolean getFlagRegulationSaved() {
        return flagRegulationSaved;
    }

    public void setFlagRegulationSaved(Boolean flagRegulationSaved) {
        this.flagRegulationSaved = flagRegulationSaved;
    }

    public Boolean getFlagEquipmentSaved() {
        return flagEquipmentSaved;
    }

    public void setFlagEquipmentSaved(Boolean flagEquipmentSaved) {
        this.flagEquipmentSaved = flagEquipmentSaved;
    }

    public Boolean getFlagPricePackageSaved() {
        return flagPricePackageSaved;
    }

    public void setFlagPricePackageSaved(Boolean flagPricePackageSaved) {
        this.flagPricePackageSaved = flagPricePackageSaved;
    }

    public Boolean getFlagClientLevelSaved() {
        return flagClientLevelSaved;
    }

    public void setFlagClientLevelSaved(Boolean flagClientLevelSaved) {
        this.flagClientLevelSaved = flagClientLevelSaved;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Club club = (Club) o;
        if (club.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), club.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Club{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", fullAddress='" + getFullAddress() + "'" +
            ", lon='" + getLon() + "'" +
            ", lat='" + getLat() + "'" +
            ", timeScheduleRegulation=" + getTimeScheduleRegulation() +
            ", flaghookah='" + isFlaghookah() + "'" +
            ", phone='" + getPhone() + "'" +
            ", phoneOptional='" + getPhoneOptional() + "'" +
            ", vkUrl='" + getVkUrl() + "'" +
            ", webUrl='" + getWebUrl() + "'" +
            ", instaUrl='" + getInstaUrl() + "'" +
            ", telegramUrl='" + getTelegramUrl() + "'" +
            ", facebookUrl='" + getFacebookUrl() + "'" +
            ", twitterUrl='" + getTwitterUrl() + "'" +
            ", notifyBefore='" + getNotifyBefore() + "'" +
            ", location='" + getLocation() + "'" +
            ", flagEquipment='" + getFlagEquipmentSaved() + "'" +
            ", flagClientLevel='" + getFlagClientLevelSaved() + "'" +
            ", flagPricePackage='" + getFlagPricePackageSaved() + "'" +
            ", flagRegulation='" + getFlagRegulationSaved() + "'" +
            "}";
    }
}
