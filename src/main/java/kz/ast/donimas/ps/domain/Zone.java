package kz.ast.donimas.ps.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A Zone.
 */
@Entity
@Table(name = "zone")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "zone")
public class Zone implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "seats_amount")
    private Integer seatsAmount;

    @Column(name = "tv_diagonal")
    private Float tvDiagonal;

    @Column(name = "tv_resolution")
    private Float tvResolution;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    @ManyToOne
    private ConsoleDictionary console;

    @ManyToOne
    private ZoneDictionary type;

    @ManyToOne
    private Club club;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Zone name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Zone description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSeatsAmount() {
        return seatsAmount;
    }

    public Zone seatsAmount(Integer seatsAmount) {
        this.seatsAmount = seatsAmount;
        return this;
    }

    public void setSeatsAmount(Integer seatsAmount) {
        this.seatsAmount = seatsAmount;
    }

    public Float getTvDiagonal() {
        return tvDiagonal;
    }

    public Zone tvDiagonal(Float tvDiagonal) {
        this.tvDiagonal = tvDiagonal;
        return this;
    }

    public void setTvDiagonal(Float tvDiagonal) {
        this.tvDiagonal = tvDiagonal;
    }

    public Float getTvResolution() {
        return tvResolution;
    }

    public Zone tvResolution(Float tvResolution) {
        this.tvResolution = tvResolution;
        return this;
    }

    public void setTvResolution(Float tvResolution) {
        this.tvResolution = tvResolution;
    }

    public Boolean isFlagDeleted() {
        return flagDeleted;
    }

    public Zone flagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
        return this;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public ConsoleDictionary getConsole() {
        return console;
    }

    public Zone console(ConsoleDictionary consoleDictionary) {
        this.console = consoleDictionary;
        return this;
    }

    public void setConsole(ConsoleDictionary consoleDictionary) {
        this.console = consoleDictionary;
    }

    public ZoneDictionary getType() {
        return type;
    }

    public Zone type(ZoneDictionary zoneDictionary) {
        this.type = zoneDictionary;
        return this;
    }

    public void setType(ZoneDictionary zoneDictionary) {
        this.type = zoneDictionary;
    }

    public Club getClub() {
        return club;
    }

    public Zone club(Club club) {
        this.club = club;
        return this;
    }

    public void setClub(Club club) {
        this.club = club;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Zone zone = (Zone) o;
        if (zone.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), zone.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Zone{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", seatsAmount=" + getSeatsAmount() +
            ", tvDiagonal=" + getTvDiagonal() +
            ", tvResolution=" + getTvResolution() +
            ", flagDeleted='" + isFlagDeleted() + "'" +
            "}";
    }
}
