package kz.ast.donimas.ps.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A AttachmentFile.
 */
@Entity
@Table(name = "attachment_file")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "attachmentfile")
public class AttachmentFile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Lob
    @Column(name = "attachment")
    private byte[] attachment;

    @Column(name = "attachment_content_type")
    private String attachmentContentType;

    @Column(name = "container_class")
    private String containerClass;

    @Column(name = "container_id")
    private Long containerId;

    @Column(name = "jhi_uid")
    private String uid;

    @Column(name = "name")
    private String name;

    @Column(name = "content_type")
    private String contentType;

    @Column(name = "file_size")
    private Long fileSize;

    @Column(name = "file_hash")
    private String fileHash;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getAttachment() {
        return attachment;
    }

    public AttachmentFile attachment(byte[] attachment) {
        this.attachment = attachment;
        return this;
    }

    public void setAttachment(byte[] attachment) {
        this.attachment = attachment;
    }

    public String getAttachmentContentType() {
        return attachmentContentType;
    }

    public AttachmentFile attachmentContentType(String attachmentContentType) {
        this.attachmentContentType = attachmentContentType;
        return this;
    }

    public void setAttachmentContentType(String attachmentContentType) {
        this.attachmentContentType = attachmentContentType;
    }

    public String getContainerClass() {
        return containerClass;
    }

    public AttachmentFile containerClass(String containerClass) {
        this.containerClass = containerClass;
        return this;
    }

    public void setContainerClass(String containerClass) {
        this.containerClass = containerClass;
    }

    public Long getContainerId() {
        return containerId;
    }

    public AttachmentFile containerId(Long containerId) {
        this.containerId = containerId;
        return this;
    }

    public void setContainerId(Long containerId) {
        this.containerId = containerId;
    }

    public String getUid() {
        return uid;
    }

    public AttachmentFile uid(String uid) {
        this.uid = uid;
        return this;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public AttachmentFile name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContentType() {
        return contentType;
    }

    public AttachmentFile contentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public AttachmentFile fileSize(Long fileSize) {
        this.fileSize = fileSize;
        return this;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileHash() {
        return fileHash;
    }

    public AttachmentFile fileHash(String fileHash) {
        this.fileHash = fileHash;
        return this;
    }

    public void setFileHash(String fileHash) {
        this.fileHash = fileHash;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AttachmentFile attachmentFile = (AttachmentFile) o;
        if (attachmentFile.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), attachmentFile.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AttachmentFile{" +
            "id=" + getId() +
            ", attachment='" + getAttachment() + "'" +
            ", attachmentContentType='" + getAttachmentContentType() + "'" +
            ", containerClass='" + getContainerClass() + "'" +
            ", containerId=" + getContainerId() +
            ", uid='" + getUid() + "'" +
            ", name='" + getName() + "'" +
            ", contentType='" + getContentType() + "'" +
            ", fileSize=" + getFileSize() +
            ", fileHash='" + getFileHash() + "'" +
            "}";
    }
}
