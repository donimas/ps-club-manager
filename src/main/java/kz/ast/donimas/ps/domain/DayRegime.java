package kz.ast.donimas.ps.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

/**
 * A DayRegime.
 */
@Entity
@Table(name = "day_regime")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "dayregime")
public class DayRegime implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "day_seq", nullable = false)
    private Integer daySeq;

    @Column(name = "opened_at")
    private Instant openedAt;

    @Column(name = "closed_at")
    private Instant closedAt;

    @Column(name = "opened_hour")
    private BigDecimal oHour;
    @Column(name = "opened_minute")
    private BigDecimal oMinute;

    @Column(name = "closed_hour")
    private BigDecimal cHour;
    @Column(name = "closed_minute")
    private BigDecimal cMinute;

    @Column(name = "flag_all_day_long")
    private Boolean flagAllDayLong;

    @Column(name = "flag_till_last_client")
    private Boolean flagTillLastClient;

    @Column(name = "flag_time")
    private Boolean flagTime;

    @Column(name = "flag_rest_day")
    private Boolean flagRestDay;

    @ManyToOne
    private Club club;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDaySeq() {
        return daySeq;
    }

    public DayRegime daySeq(Integer daySeq) {
        this.daySeq = daySeq;
        return this;
    }

    public void setDaySeq(Integer daySeq) {
        this.daySeq = daySeq;
    }

    public Instant getOpenedAt() {
        return openedAt;
    }

    public DayRegime openedAt(Instant openedAt) {
        this.openedAt = openedAt;
        return this;
    }

    public void setOpenedAt(Instant openedAt) {
        this.openedAt = openedAt;
    }

    public Instant getClosedAt() {
        return closedAt;
    }

    public DayRegime closedAt(Instant closedAt) {
        this.closedAt = closedAt;
        return this;
    }

    public void setClosedAt(Instant closedAt) {
        this.closedAt = closedAt;
    }

    public Boolean isFlagAllDayLong() {
        return flagAllDayLong;
    }

    public DayRegime flagAllDayLong(Boolean flagAllDayLong) {
        this.flagAllDayLong = flagAllDayLong;
        return this;
    }

    public void setFlagAllDayLong(Boolean flagAllDayLong) {
        this.flagAllDayLong = flagAllDayLong;
    }

    public Boolean isFlagTillLastClient() {
        return flagTillLastClient;
    }

    public DayRegime flagTillLastClient(Boolean flagTillLastClient) {
        this.flagTillLastClient = flagTillLastClient;
        return this;
    }

    public void setFlagTillLastClient(Boolean flagTillLastClient) {
        this.flagTillLastClient = flagTillLastClient;
    }

    public Club getClub() {
        return club;
    }

    public DayRegime club(Club club) {
        this.club = club;
        return this;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public BigDecimal getoHour() {
        return oHour;
    }

    public void setoHour(BigDecimal oHour) {
        this.oHour = oHour;
    }

    public BigDecimal getoMinute() {
        return oMinute;
    }

    public void setoMinute(BigDecimal oMinute) {
        this.oMinute = oMinute;
    }

    public BigDecimal getcHour() {
        return cHour;
    }

    public void setcHour(BigDecimal cHour) {
        this.cHour = cHour;
    }

    public BigDecimal getcMinute() {
        return cMinute;
    }

    public void setcMinute(BigDecimal cMinute) {
        this.cMinute = cMinute;
    }

    public Boolean getFlagTime() {
        return flagTime;
    }

    public void setFlagTime(Boolean flagTime) {
        this.flagTime = flagTime;
    }

    public Boolean getFlagRestDay() {
        return flagRestDay;
    }

    public void setFlagRestDay(Boolean flagRestDay) {
        this.flagRestDay = flagRestDay;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DayRegime dayRegime = (DayRegime) o;
        if (dayRegime.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dayRegime.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DayRegime{" +
            "id=" + getId() +
            ", daySeq=" + getDaySeq() +
            ", openedAt='" + getOpenedAt() + "'" +
            ", closedAt='" + getClosedAt() + "'" +
            ", flagAllDayLong='" + isFlagAllDayLong() + "'" +
            ", flagTillLastClient='" + isFlagTillLastClient() + "'" +
            ", flagTime='" + getFlagTime() + "'" +
            "}";
    }
}
