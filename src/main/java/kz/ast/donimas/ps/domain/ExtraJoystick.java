package kz.ast.donimas.ps.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A ExtraJoystick.
 */
@Entity
@Table(name = "extra_joystick")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "extrajoystick")
public class ExtraJoystick implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "amount")
    private Integer amount;

    @Column(name = "started_at")
    private ZonedDateTime startedAt;

    @Column(name = "finished_at")
    private ZonedDateTime finishedAt;

    @Column(name = "played_hours", precision=10, scale=2)
    private BigDecimal playedHours;

    @Column(name = "played_minutes", precision=10, scale=2)
    private BigDecimal playedMinutes;

    @Column(name = "total_price", precision=10, scale=2)
    private BigDecimal totalPrice;

    @ManyToOne
    private Session session;

    @ManyToOne
    private ClubJoystick clubJoystick;

    @ManyToOne
    private Tariff pricePerHour;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public ExtraJoystick amount(Integer amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public ZonedDateTime getStartedAt() {
        return startedAt;
    }

    public ExtraJoystick startedAt(ZonedDateTime startedAt) {
        this.startedAt = startedAt;
        return this;
    }

    public void setStartedAt(ZonedDateTime startedAt) {
        this.startedAt = startedAt;
    }

    public ZonedDateTime getFinishedAt() {
        return finishedAt;
    }

    public ExtraJoystick finishedAt(ZonedDateTime finishedAt) {
        this.finishedAt = finishedAt;
        return this;
    }

    public void setFinishedAt(ZonedDateTime finishedAt) {
        this.finishedAt = finishedAt;
    }

    public BigDecimal getPlayedHours() {
        return playedHours;
    }

    public ExtraJoystick playedHours(BigDecimal playedHours) {
        this.playedHours = playedHours;
        return this;
    }

    public void setPlayedHours(BigDecimal playedHours) {
        this.playedHours = playedHours;
    }

    public BigDecimal getPlayedMinutes() {
        return playedMinutes;
    }

    public ExtraJoystick playedMinutes(BigDecimal playedMinutes) {
        this.playedMinutes = playedMinutes;
        return this;
    }

    public void setPlayedMinutes(BigDecimal playedMinutes) {
        this.playedMinutes = playedMinutes;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public ExtraJoystick totalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Session getSession() {
        return session;
    }

    public ExtraJoystick session(Session session) {
        this.session = session;
        return this;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public ClubJoystick getClubJoystick() {
        return clubJoystick;
    }

    public ExtraJoystick clubJoystick(ClubJoystick clubJoystick) {
        this.clubJoystick = clubJoystick;
        return this;
    }

    public void setClubJoystick(ClubJoystick clubJoystick) {
        this.clubJoystick = clubJoystick;
    }

    public Tariff getPricePerHour() {
        return pricePerHour;
    }

    public ExtraJoystick pricePerHour(Tariff tariff) {
        this.pricePerHour = tariff;
        return this;
    }

    public void setPricePerHour(Tariff tariff) {
        this.pricePerHour = tariff;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ExtraJoystick extraJoystick = (ExtraJoystick) o;
        if (extraJoystick.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), extraJoystick.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ExtraJoystick{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", startedAt='" + getStartedAt() + "'" +
            ", finishedAt='" + getFinishedAt() + "'" +
            ", playedHours=" + getPlayedHours() +
            ", playedMinutes=" + getPlayedMinutes() +
            ", totalPrice=" + getTotalPrice() +
            "}";
    }
}
