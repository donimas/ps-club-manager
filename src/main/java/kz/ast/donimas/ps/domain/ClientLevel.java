package kz.ast.donimas.ps.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A ClientLevel.
 */
@Entity
@Table(name = "client_level")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "clientlevel")
public class ClientLevel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "games_amount")
    private Integer gamesAmount;

    @Column(name = "discount_percent", precision=10, scale=2)
    private BigDecimal discountPercent;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    @ManyToOne
    Club club;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ClientLevel name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public ClientLevel description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getGamesAmount() {
        return gamesAmount;
    }

    public ClientLevel gamesAmount(Integer gamesAmount) {
        this.gamesAmount = gamesAmount;
        return this;
    }

    public void setGamesAmount(Integer gamesAmount) {
        this.gamesAmount = gamesAmount;
    }

    public BigDecimal getDiscountPercent() {
        return discountPercent;
    }

    public ClientLevel discountPercent(BigDecimal discountPercent) {
        this.discountPercent = discountPercent;
        return this;
    }

    public void setDiscountPercent(BigDecimal discountPercent) {
        this.discountPercent = discountPercent;
    }

    public Boolean isFlagDeleted() {
        return flagDeleted;
    }

    public ClientLevel flagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
        return this;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ClientLevel clientLevel = (ClientLevel) o;
        if (clientLevel.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientLevel.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientLevel{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", gamesAmount=" + getGamesAmount() +
            ", discountPercent=" + getDiscountPercent() +
            ", flagDeleted='" + isFlagDeleted() + "'" +
            "}";
    }
}
