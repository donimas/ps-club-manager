package kz.ast.donimas.ps.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

import kz.ast.donimas.ps.domain.enumeration.DurationType;

/**
 * A PricePackage.
 */
@Entity
@Table(name = "price_package")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pricepackage")
public class PricePackage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_type", nullable = false)
    private DurationType type;

    @Column(name = "start_at")
    private Instant startAt;

    @Column(name = "finish_at")
    private Instant finishAt;

    // transient
    @Column(name = "hours_amount")
    private Integer hoursAmount;

    @Column(name = "minutes_amount")
    private BigDecimal minutesAmount;

    @Column(name = "minimum_joy_duration_in_minutes")
    private BigDecimal minimumJoyDurationInMinutes;

    @NotNull
    @Column(name = "price", precision=10, scale=2, nullable = false)
    private BigDecimal price;

    @Column(name = "start_hour")
    private BigDecimal startHour;
    @Column(name = "start_minute")
    private BigDecimal startMinute;
    @Column(name = "finish_hour")
    private BigDecimal finishHour;
    @Column(name = "finish_minute")
    private BigDecimal finishMinute;

    @Column(name = "min_duration_price")
    private BigDecimal minDurationPrice;

    @ManyToOne
    private Club club;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public PricePackage name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public PricePackage description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isFlagDeleted() {
        return flagDeleted;
    }

    public PricePackage flagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
        return this;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public DurationType getType() {
        return type;
    }

    public PricePackage type(DurationType type) {
        this.type = type;
        return this;
    }

    public void setType(DurationType type) {
        this.type = type;
    }

    public Instant getStartAt() {
        return startAt;
    }

    public PricePackage startAt(Instant startAt) {
        this.startAt = startAt;
        return this;
    }

    public void setStartAt(Instant startAt) {
        this.startAt = startAt;
    }

    public Instant getFinishAt() {
        return finishAt;
    }

    public PricePackage finishAt(Instant finishAt) {
        this.finishAt = finishAt;
        return this;
    }

    public void setFinishAt(Instant finishAt) {
        this.finishAt = finishAt;
    }

    public Integer getHoursAmount() {
        return hoursAmount;
    }

    public PricePackage hoursAmount(Integer hoursAmount) {
        this.hoursAmount = hoursAmount;
        return this;
    }

    public void setHoursAmount(Integer hoursAmount) {
        this.hoursAmount = hoursAmount;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public PricePackage price(BigDecimal price) {
        this.price = price;
        return this;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getMinutesAmount() {
        return minutesAmount;
    }

    public void setMinutesAmount(BigDecimal minutesAmount) {
        this.minutesAmount = minutesAmount;
    }

    public BigDecimal getMinimumJoyDurationInMinutes() {
        return minimumJoyDurationInMinutes;
    }

    public void setMinimumJoyDurationInMinutes(BigDecimal minimumJoyDurationInMinutes) {
        this.minimumJoyDurationInMinutes = minimumJoyDurationInMinutes;
    }

    public BigDecimal getStartHour() {
        return startHour;
    }

    public void setStartHour(BigDecimal startHour) {
        this.startHour = startHour;
    }

    public BigDecimal getStartMinute() {
        return startMinute;
    }

    public void setStartMinute(BigDecimal startMinute) {
        this.startMinute = startMinute;
    }

    public BigDecimal getFinishHour() {
        return finishHour;
    }

    public void setFinishHour(BigDecimal finishHour) {
        this.finishHour = finishHour;
    }

    public BigDecimal getFinishMinute() {
        return finishMinute;
    }

    public void setFinishMinute(BigDecimal finishMinute) {
        this.finishMinute = finishMinute;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public BigDecimal getMinDurationPrice() {
        return minDurationPrice;
    }

    public void setMinDurationPrice(BigDecimal minDurationPrice) {
        this.minDurationPrice = minDurationPrice;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PricePackage pricePackage = (PricePackage) o;
        if (pricePackage.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pricePackage.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PricePackage{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", flagDeleted='" + isFlagDeleted() + "'" +
            ", type='" + getType() + "'" +
            ", startAt='" + getStartAt() + "'" +
            ", finishAt='" + getFinishAt() + "'" +
            ", hoursAmount=" + getHoursAmount() +
            ", minutesAmount=" + getMinutesAmount() +
            ", minimumDurationInMinutes=" + getMinimumJoyDurationInMinutes() +
            ", price=" + getPrice() +
            ", startHour=" + getStartHour() +
            ", startMinute=" + getStartMinute() +
            ", finishHour=" + getFinishHour() +
            ", finishMinute=" + getFinishMinute() +
            ", finishMinute=" + getFinishMinute() +
            ", minDurationType=" + getMinDurationPrice() +
            "}";
    }
}
