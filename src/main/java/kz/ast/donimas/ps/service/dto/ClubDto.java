package kz.ast.donimas.ps.service.dto;

import kz.ast.donimas.ps.domain.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class ClubDto implements Serializable {

    private Long id;
    private String name;
    private String description;
    private String fullAddress;
    private String lon;
    private String lat;
    private BigDecimal timeScheduleRegulation;
    private Boolean flaghookah;
    private String phone;
    private String phoneOptional;
    private String webUrl;
    private String vkUrl;
    private String instaUrl;
    private String telegramUrl;
    private String facebookUrl;
    private String twitterUrl;
    private AttachmentFile avatar;
    private Boolean flagPublished = false;
    private Boolean flagMainInfoSaved = false;
    private Boolean flagContactSaved = false;
    private Boolean flagLocationSaved = false;
    private Boolean flagZoneSaved = false;

    private Boolean flagRegulationSaved = false;
    private Boolean flagEquipmentSaved = false;
    private Boolean flagPricePackageSaved = false;
    private Boolean flagClientLevelSaved = false;

    private BigDecimal notifyBefore;
    private Set<ConsoleDictionary> consoles = new HashSet<>();
    private Set<GameDictionary> games = new HashSet<>();
    private Set<ClubJoystick> joysticks = new HashSet<>();
    private Set<Zone> zones = new HashSet<>();
    private Set<ZoneDto> zoneDtos = new HashSet<>();
    private Set<PricePackage> pricePackages = new HashSet<>();
    private Set<ClientLevel> clientLevels = new HashSet<>();
    private Set<DayRegime> workRegimes = new HashSet<>();
    private Set<AttachmentFile> galleries = new HashSet<>();
    private Location location;

    public ClubDto() {}

    public ClubDto(Long id, BigDecimal timeScheduleRegulation,
                   BigDecimal notifyBefore, Set<ConsoleDictionary> consoles,
                   Set<GameDictionary> games, Set<ClubJoystick> joysticks,
                   Set<ZoneDto> zones, Set<PricePackage> pricePackages,
                   Set<ClientLevel> clientLevels, Set<DayRegime> workRegimes) {
        this.id = id;
        this.timeScheduleRegulation = timeScheduleRegulation;
        this.notifyBefore = notifyBefore;
        this.consoles = consoles;
        this.games = games;
        setJoysticks(joysticks);
        this.zoneDtos = zones;
        setPricePackages(pricePackages);
        setClientLevels(clientLevels);
        this.workRegimes = workRegimes;
    }

    public ClubDto(Club club) {
        id = club.getId();
        timeScheduleRegulation = club.getTimeScheduleRegulation();
        notifyBefore = club.getNotifyBefore();
        consoles = club.getConsoles();
        games = club.getGames();
        joysticks = club.getJoysticks();
        zones = club.getZones();
        pricePackages = club.getPricePackages();
        clientLevels = club.getClientLevels();
        workRegimes = club.getWorkRegimes();

        name = club.getName();
        description = club.getDescription();
        fullAddress = club.getFullAddress();
        lon = club.getLon();
        lat = club.getLat();
        flaghookah = club.getFlaghookah();
        phone = club.getPhone();
        phoneOptional = club.getPhoneOptional();
        webUrl = club.getWebUrl();
        vkUrl = club.getVkUrl();
        instaUrl = club.getInstaUrl();
        telegramUrl = club.getTelegramUrl();
        facebookUrl = club.getFacebookUrl();
        twitterUrl = club.getTwitterUrl();
        avatar = club.getAvatar();
        flagPublished = club.getFlagPublished();
        flagMainInfoSaved = club.getFlagMainInfoSaved();
        flagContactSaved = club.getFlagContactSaved();
        flagLocationSaved = club.getFlagLocationSaved();
        flagZoneSaved = club.getFlagZoneSaved();
        flagRegulationSaved = club.getFlagRegulationSaved();
        flagEquipmentSaved = club.getFlagEquipmentSaved();
        flagPricePackageSaved = club.getFlagPricePackageSaved();
        flagClientLevelSaved = club.getFlagClientLevelSaved();
        location = club.getLocation();
        galleries = club.getGalleries();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public BigDecimal getTimeScheduleRegulation() {
        return timeScheduleRegulation;
    }

    public void setTimeScheduleRegulation(BigDecimal timeScheduleRegulation) {
        this.timeScheduleRegulation = timeScheduleRegulation;
    }

    public Boolean getFlaghookah() {
        return flaghookah;
    }

    public void setFlaghookah(Boolean flaghookah) {
        this.flaghookah = flaghookah;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneOptional() {
        return phoneOptional;
    }

    public void setPhoneOptional(String phoneOptional) {
        this.phoneOptional = phoneOptional;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getVkUrl() {
        return vkUrl;
    }

    public void setVkUrl(String vkUrl) {
        this.vkUrl = vkUrl;
    }

    public String getInstaUrl() {
        return instaUrl;
    }

    public void setInstaUrl(String instaUrl) {
        this.instaUrl = instaUrl;
    }

    public String getTelegramUrl() {
        return telegramUrl;
    }

    public void setTelegramUrl(String telegramUrl) {
        this.telegramUrl = telegramUrl;
    }

    public String getFacebookUrl() {
        return facebookUrl;
    }

    public void setFacebookUrl(String facebookUrl) {
        this.facebookUrl = facebookUrl;
    }

    public String getTwitterUrl() {
        return twitterUrl;
    }

    public void setTwitterUrl(String twitterUrl) {
        this.twitterUrl = twitterUrl;
    }

    public AttachmentFile getAvatar() {
        return avatar;
    }

    public void setAvatar(AttachmentFile avatar) {
        this.avatar = avatar;
    }

    public Boolean getFlagPublished() {
        return flagPublished;
    }

    public void setFlagPublished(Boolean flagPublished) {
        this.flagPublished = flagPublished;
    }

    public Boolean getFlagMainInfoSaved() {
        return flagMainInfoSaved;
    }

    public void setFlagMainInfoSaved(Boolean flagMainInfoSaved) {
        this.flagMainInfoSaved = flagMainInfoSaved;
    }

    public Boolean getFlagContactSaved() {
        return flagContactSaved;
    }

    public void setFlagContactSaved(Boolean flagContactSaved) {
        this.flagContactSaved = flagContactSaved;
    }

    public Boolean getFlagLocationSaved() {
        return flagLocationSaved;
    }

    public void setFlagLocationSaved(Boolean flagLocationSaved) {
        this.flagLocationSaved = flagLocationSaved;
    }

    public Boolean getFlagZoneSaved() {
        return flagZoneSaved;
    }

    public void setFlagZoneSaved(Boolean flagZoneSaved) {
        this.flagZoneSaved = flagZoneSaved;
    }

    public Boolean getFlagRegulationSaved() {
        return flagRegulationSaved;
    }

    public void setFlagRegulationSaved(Boolean flagRegulationSaved) {
        this.flagRegulationSaved = flagRegulationSaved;
    }

    public Boolean getFlagEquipmentSaved() {
        return flagEquipmentSaved;
    }

    public void setFlagEquipmentSaved(Boolean flagEquipmentSaved) {
        this.flagEquipmentSaved = flagEquipmentSaved;
    }

    public Boolean getFlagPricePackageSaved() {
        return flagPricePackageSaved;
    }

    public void setFlagPricePackageSaved(Boolean flagPricePackageSaved) {
        this.flagPricePackageSaved = flagPricePackageSaved;
    }

    public Boolean getFlagClientLevelSaved() {
        return flagClientLevelSaved;
    }

    public void setFlagClientLevelSaved(Boolean flagClientLevelSaved) {
        this.flagClientLevelSaved = flagClientLevelSaved;
    }

    public BigDecimal getNotifyBefore() {
        return notifyBefore;
    }

    public void setNotifyBefore(BigDecimal notifyBefore) {
        this.notifyBefore = notifyBefore;
    }

    public Set<ConsoleDictionary> getConsoles() {
        return consoles;
    }

    public void setConsoles(Set<ConsoleDictionary> consoles) {
        this.consoles = consoles;
    }

    public Set<GameDictionary> getGames() {
        return games;
    }

    public void setGames(Set<GameDictionary> games) {
        this.games = games;
    }

    public Set<ClubJoystick> getJoysticks() {
        return joysticks;
    }

    public void setJoysticks(Set<ClubJoystick> joysticks) {
        if(joysticks != null && joysticks.size() > 0) {
            this.joysticks = joysticks.stream().filter(j -> j.getFlagDeleted() == null || !j.getFlagDeleted()).collect(Collectors.toSet());
        }
    }

    public Set<Zone> getZones() {
        return zones;
    }

    public void setZones(Set<Zone> zones) {
        this.zones = zones;
    }

    public Set<ZoneDto> getZoneDtos() {
        return zoneDtos;
    }

    public void setZoneDtos(Set<ZoneDto> zoneDtos) {
        this.zoneDtos = zoneDtos;
    }

    public Set<PricePackage> getPricePackages() {
        return pricePackages;
    }

    public void setPricePackages(Set<PricePackage> pricePackages) {
        if(pricePackages != null && pricePackages.size() > 0) {
            this.pricePackages = pricePackages
                .stream()
                .filter(p -> p.isFlagDeleted() == null || !p.isFlagDeleted())
                .collect(Collectors.toSet());
        }
    }

    public Set<ClientLevel> getClientLevels() {
        return clientLevels;
    }

    public void setClientLevels(Set<ClientLevel> clientLevels) {
        if(clientLevels != null && clientLevels.size() > 0) {
            this.clientLevels = clientLevels
                .stream()
                .filter(c -> c.isFlagDeleted() == null || !c.isFlagDeleted())
                .collect(Collectors.toSet());
        }
    }

    public Set<DayRegime> getWorkRegimes() {
        return workRegimes;
    }

    public void setWorkRegimes(Set<DayRegime> workRegimes) {
        this.workRegimes = workRegimes;
    }

    public Set<AttachmentFile> getGalleries() {
        return galleries;
    }

    public void setGalleries(Set<AttachmentFile> galleries) {
        this.galleries = galleries;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClubDto clubDto = (ClubDto) o;

        if (id != null ? !id.equals(clubDto.id) : clubDto.id != null) return false;
        if (name != null ? !name.equals(clubDto.name) : clubDto.name != null) return false;
        if (description != null ? !description.equals(clubDto.description) : clubDto.description != null) return false;
        if (fullAddress != null ? !fullAddress.equals(clubDto.fullAddress) : clubDto.fullAddress != null) return false;
        if (lon != null ? !lon.equals(clubDto.lon) : clubDto.lon != null) return false;
        if (lat != null ? !lat.equals(clubDto.lat) : clubDto.lat != null) return false;
        if (timeScheduleRegulation != null ? !timeScheduleRegulation.equals(clubDto.timeScheduleRegulation) : clubDto.timeScheduleRegulation != null)
            return false;
        if (flaghookah != null ? !flaghookah.equals(clubDto.flaghookah) : clubDto.flaghookah != null) return false;
        if (phone != null ? !phone.equals(clubDto.phone) : clubDto.phone != null) return false;
        if (phoneOptional != null ? !phoneOptional.equals(clubDto.phoneOptional) : clubDto.phoneOptional != null)
            return false;
        if (webUrl != null ? !webUrl.equals(clubDto.webUrl) : clubDto.webUrl != null) return false;
        if (vkUrl != null ? !vkUrl.equals(clubDto.vkUrl) : clubDto.vkUrl != null) return false;
        if (instaUrl != null ? !instaUrl.equals(clubDto.instaUrl) : clubDto.instaUrl != null) return false;
        if (telegramUrl != null ? !telegramUrl.equals(clubDto.telegramUrl) : clubDto.telegramUrl != null) return false;
        if (facebookUrl != null ? !facebookUrl.equals(clubDto.facebookUrl) : clubDto.facebookUrl != null) return false;
        if (twitterUrl != null ? !twitterUrl.equals(clubDto.twitterUrl) : clubDto.twitterUrl != null) return false;
        if (avatar != null ? !avatar.equals(clubDto.avatar) : clubDto.avatar != null) return false;
        if (flagPublished != null ? !flagPublished.equals(clubDto.flagPublished) : clubDto.flagPublished != null)
            return false;
        if (flagMainInfoSaved != null ? !flagMainInfoSaved.equals(clubDto.flagMainInfoSaved) : clubDto.flagMainInfoSaved != null)
            return false;
        if (flagContactSaved != null ? !flagContactSaved.equals(clubDto.flagContactSaved) : clubDto.flagContactSaved != null)
            return false;
        if (flagLocationSaved != null ? !flagLocationSaved.equals(clubDto.flagLocationSaved) : clubDto.flagLocationSaved != null)
            return false;
        if (flagZoneSaved != null ? !flagZoneSaved.equals(clubDto.flagZoneSaved) : clubDto.flagZoneSaved != null)
            return false;
        if (flagRegulationSaved != null ? !flagRegulationSaved.equals(clubDto.flagRegulationSaved) : clubDto.flagRegulationSaved != null)
            return false;
        if (flagEquipmentSaved != null ? !flagEquipmentSaved.equals(clubDto.flagEquipmentSaved) : clubDto.flagEquipmentSaved != null)
            return false;
        if (flagPricePackageSaved != null ? !flagPricePackageSaved.equals(clubDto.flagPricePackageSaved) : clubDto.flagPricePackageSaved != null)
            return false;
        if (flagClientLevelSaved != null ? !flagClientLevelSaved.equals(clubDto.flagClientLevelSaved) : clubDto.flagClientLevelSaved != null)
            return false;
        if (notifyBefore != null ? !notifyBefore.equals(clubDto.notifyBefore) : clubDto.notifyBefore != null)
            return false;
        if (consoles != null ? !consoles.equals(clubDto.consoles) : clubDto.consoles != null) return false;
        if (games != null ? !games.equals(clubDto.games) : clubDto.games != null) return false;
        if (joysticks != null ? !joysticks.equals(clubDto.joysticks) : clubDto.joysticks != null) return false;
        if (zones != null ? !zones.equals(clubDto.zones) : clubDto.zones != null) return false;
        if (pricePackages != null ? !pricePackages.equals(clubDto.pricePackages) : clubDto.pricePackages != null)
            return false;
        if (clientLevels != null ? !clientLevels.equals(clubDto.clientLevels) : clubDto.clientLevels != null)
            return false;
        if (workRegimes != null ? !workRegimes.equals(clubDto.workRegimes) : clubDto.workRegimes != null) return false;
        if (galleries != null ? !galleries.equals(clubDto.galleries) : clubDto.galleries != null) return false;
        return location != null ? location.equals(clubDto.location) : clubDto.location == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (fullAddress != null ? fullAddress.hashCode() : 0);
        result = 31 * result + (lon != null ? lon.hashCode() : 0);
        result = 31 * result + (lat != null ? lat.hashCode() : 0);
        result = 31 * result + (timeScheduleRegulation != null ? timeScheduleRegulation.hashCode() : 0);
        result = 31 * result + (flaghookah != null ? flaghookah.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (phoneOptional != null ? phoneOptional.hashCode() : 0);
        result = 31 * result + (webUrl != null ? webUrl.hashCode() : 0);
        result = 31 * result + (vkUrl != null ? vkUrl.hashCode() : 0);
        result = 31 * result + (instaUrl != null ? instaUrl.hashCode() : 0);
        result = 31 * result + (telegramUrl != null ? telegramUrl.hashCode() : 0);
        result = 31 * result + (facebookUrl != null ? facebookUrl.hashCode() : 0);
        result = 31 * result + (twitterUrl != null ? twitterUrl.hashCode() : 0);
        result = 31 * result + (avatar != null ? avatar.hashCode() : 0);
        result = 31 * result + (flagPublished != null ? flagPublished.hashCode() : 0);
        result = 31 * result + (flagMainInfoSaved != null ? flagMainInfoSaved.hashCode() : 0);
        result = 31 * result + (flagContactSaved != null ? flagContactSaved.hashCode() : 0);
        result = 31 * result + (flagLocationSaved != null ? flagLocationSaved.hashCode() : 0);
        result = 31 * result + (flagZoneSaved != null ? flagZoneSaved.hashCode() : 0);
        result = 31 * result + (flagRegulationSaved != null ? flagRegulationSaved.hashCode() : 0);
        result = 31 * result + (flagEquipmentSaved != null ? flagEquipmentSaved.hashCode() : 0);
        result = 31 * result + (flagPricePackageSaved != null ? flagPricePackageSaved.hashCode() : 0);
        result = 31 * result + (flagClientLevelSaved != null ? flagClientLevelSaved.hashCode() : 0);
        result = 31 * result + (notifyBefore != null ? notifyBefore.hashCode() : 0);
        result = 31 * result + (consoles != null ? consoles.hashCode() : 0);
        result = 31 * result + (games != null ? games.hashCode() : 0);
        result = 31 * result + (joysticks != null ? joysticks.hashCode() : 0);
        result = 31 * result + (zones != null ? zones.hashCode() : 0);
        result = 31 * result + (pricePackages != null ? pricePackages.hashCode() : 0);
        result = 31 * result + (clientLevels != null ? clientLevels.hashCode() : 0);
        result = 31 * result + (workRegimes != null ? workRegimes.hashCode() : 0);
        result = 31 * result + (galleries != null ? galleries.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ClubDto{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", description='" + description + '\'' +
            ", fullAddress='" + fullAddress + '\'' +
            ", lon='" + lon + '\'' +
            ", lat='" + lat + '\'' +
            ", timeScheduleRegulation=" + timeScheduleRegulation +
            ", flaghookah=" + flaghookah +
            ", phone='" + phone + '\'' +
            ", phoneOptional='" + phoneOptional + '\'' +
            ", webUrl='" + webUrl + '\'' +
            ", vkUrl='" + vkUrl + '\'' +
            ", instaUrl='" + instaUrl + '\'' +
            ", telegramUrl='" + telegramUrl + '\'' +
            ", facebookUrl='" + facebookUrl + '\'' +
            ", twitterUrl='" + twitterUrl + '\'' +
            ", avatar=" + avatar +
            ", flagPublished=" + flagPublished +
            ", flagMainInfoSaved=" + flagMainInfoSaved +
            ", flagContactSaved=" + flagContactSaved +
            ", flagLocationSaved=" + flagLocationSaved +
            ", flagZoneSaved=" + flagZoneSaved +
            ", flagRegulationSaved=" + flagRegulationSaved +
            ", flagEquipmentSaved=" + flagEquipmentSaved +
            ", flagPricePackageSaved=" + flagPricePackageSaved +
            ", flagClientLevelSaved=" + flagClientLevelSaved +
            ", notifyBefore=" + notifyBefore +
            ", consoles=" + consoles +
            ", games=" + games +
            ", joysticks=" + joysticks +
            ", zones=" + zones +
            ", zoneDtos=" + zoneDtos+
            ", pricePackages=" + pricePackages +
            ", clientLevels=" + clientLevels +
            ", workRegimes=" + workRegimes +
            ", galleries=" + galleries +
            ", location=" + location +
            '}';
    }
}
