package kz.ast.donimas.ps.service;

import kz.ast.donimas.ps.domain.ClientLevel;
import kz.ast.donimas.ps.domain.Club;
import kz.ast.donimas.ps.repository.ClientLevelRepository;
import kz.ast.donimas.ps.repository.search.ClientLevelSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ClientLevel.
 */
@Service
@Transactional
public class ClientLevelService {

    private final Logger log = LoggerFactory.getLogger(ClientLevelService.class);

    private final ClientLevelRepository clientLevelRepository;

    private final ClientLevelSearchRepository clientLevelSearchRepository;

    private final ClubService clubService;

    public ClientLevelService(ClientLevelRepository clientLevelRepository, ClientLevelSearchRepository clientLevelSearchRepository, ClubService clubService) {
        this.clientLevelRepository = clientLevelRepository;
        this.clientLevelSearchRepository = clientLevelSearchRepository;
        this.clubService = clubService;
    }

    /**
     * Save a clientLevel.
     *
     * @param clientLevel the entity to save
     * @return the persisted entity
     */
    public ClientLevel save(ClientLevel clientLevel) {
        log.debug("Request to save ClientLevel : {}", clientLevel);
        ClientLevel result = clientLevelRepository.save(clientLevel);
        clientLevelSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the clientLevels.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ClientLevel> findAll() {
        log.debug("Request to get all ClientLevels");
        return clientLevelRepository.findAll();
    }

    /**
     * Get one clientLevel by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ClientLevel findOne(Long id) {
        log.debug("Request to get ClientLevel : {}", id);
        return clientLevelRepository.findOne(id);
    }

    /**
     * Delete the clientLevel by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ClientLevel : {}", id);
        /*clientLevelRepository.delete(id);
        clientLevelSearchRepository.delete(id);*/
        ClientLevel clientLevel = findOne(id);
        clientLevel.setFlagDeleted(true);
        save(clientLevel);
    }

    /**
     * Search for the clientLevel corresponding to the query.
     *
     * @param query the query of the search
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ClientLevel> search(String query) {
        log.debug("Request to search ClientLevels for query {}", query);
        return StreamSupport
            .stream(clientLevelSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

    @Transactional
    public ClientLevel create(ClientLevel clientLevel) {
        log.debug("Request to create client level: {}", clientLevel);
        Club club = clubService.findOne(clientLevel.getClub().getId());
        clientLevel.setClub(club);
        club.setFlagClientLevelSaved(true);
        clubService.save(club);
        return save(clientLevel);
    }

    public Page<ClientLevel> getAllClubClientLevels(Long id, Pageable pageable) {
        return clientLevelRepository.findAllByClubIdAndFlagDeletedFalse(id, pageable);
    }
}
