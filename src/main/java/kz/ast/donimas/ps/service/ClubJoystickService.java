package kz.ast.donimas.ps.service;

import kz.ast.donimas.ps.domain.ClubJoystick;
import kz.ast.donimas.ps.domain.Tariff;
import kz.ast.donimas.ps.repository.ClubJoystickRepository;
import kz.ast.donimas.ps.repository.search.ClubJoystickSearchRepository;
import kz.ast.donimas.ps.service.constant.ContainerClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ClubJoystick.
 */
@Service
@Transactional
public class ClubJoystickService {

    private final Logger log = LoggerFactory.getLogger(ClubJoystickService.class);

    private final ClubJoystickRepository clubJoystickRepository;

    private final ClubJoystickSearchRepository clubJoystickSearchRepository;
    private final TariffService tariffService;

    public ClubJoystickService(ClubJoystickRepository clubJoystickRepository, ClubJoystickSearchRepository clubJoystickSearchRepository, TariffService tariffService) {
        this.clubJoystickRepository = clubJoystickRepository;
        this.clubJoystickSearchRepository = clubJoystickSearchRepository;
        this.tariffService = tariffService;
    }

    /**
     * Save a clubJoystick.
     *
     * @param clubJoystick the entity to save
     * @return the persisted entity
     */
    public ClubJoystick save(ClubJoystick clubJoystick) {
        log.debug("Request to save ClubJoystick : {}", clubJoystick);
        ClubJoystick result = clubJoystickRepository.save(clubJoystick);
        clubJoystickSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the clubJoysticks.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ClubJoystick> findAll() {
        log.debug("Request to get all ClubJoysticks");
        return clubJoystickRepository.findAll();
    }

    /**
     * Get one clubJoystick by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ClubJoystick findOne(Long id) {
        log.debug("Request to get ClubJoystick : {}", id);
        return clubJoystickRepository.findOne(id);
    }

    /**
     * Delete the clubJoystick by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ClubJoystick : {}", id);
        ClubJoystick clubJoystick = findOne(id);
        clubJoystick.setFlagDeleted(true);
        save(clubJoystick);
        //clubJoystickRepository.delete(id);
        //clubJoystickSearchRepository.delete(id);
    }

    /**
     * Search for the clubJoystick corresponding to the query.
     *
     * @param query the query of the search
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ClubJoystick> search(String query) {
        log.debug("Request to search ClubJoysticks for query {}", query);
        return StreamSupport
            .stream(clubJoystickSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<ClubJoystick> findAllOfClub(Long id) {
        log.debug("Request to get all ClubJoysticks of club: {}", id);
        return clubJoystickRepository.findAllByClubIdAndFlagDeletedFalse(id);
    }

    @Transactional
    public ClubJoystick create(ClubJoystick clubJoystick) {
        log.debug("Request to create club joystick: {}", clubJoystick);
        // Tariff pricePerHour = clubJoystick.getPriceForExtraOnePerHour();

        ClubJoystick savedClubJoystick = save(clubJoystick);
        // Tariff savedTariff = tariffService.createOrUpdate(pricePerHour, ContainerClass.JOYSTICK, savedClubJoystick.getId());
        // savedClubJoystick.setPriceForExtraOnePerHour(savedTariff);
        // save(savedClubJoystick);

        return savedClubJoystick;
    }
}
