package kz.ast.donimas.ps.service;

import kz.ast.donimas.ps.domain.DayRegime;
import kz.ast.donimas.ps.repository.DayRegimeRepository;
import kz.ast.donimas.ps.repository.search.DayRegimeSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing DayRegime.
 */
@Service
@Transactional
public class DayRegimeService {

    private final Logger log = LoggerFactory.getLogger(DayRegimeService.class);

    private final DayRegimeRepository dayRegimeRepository;

    private final DayRegimeSearchRepository dayRegimeSearchRepository;

    public DayRegimeService(DayRegimeRepository dayRegimeRepository, DayRegimeSearchRepository dayRegimeSearchRepository) {
        this.dayRegimeRepository = dayRegimeRepository;
        this.dayRegimeSearchRepository = dayRegimeSearchRepository;
    }

    /**
     * Save a dayRegime.
     *
     * @param dayRegime the entity to save
     * @return the persisted entity
     */
    public DayRegime save(DayRegime dayRegime) {
        log.debug("Request to save DayRegime : {}", dayRegime);
        DayRegime result = dayRegimeRepository.save(dayRegime);
        dayRegimeSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the dayRegimes.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<DayRegime> findAll() {
        log.debug("Request to get all DayRegimes");
        return dayRegimeRepository.findAll();
    }

    /**
     * Get one dayRegime by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public DayRegime findOne(Long id) {
        log.debug("Request to get DayRegime : {}", id);
        return dayRegimeRepository.findOne(id);
    }

    /**
     * Delete the dayRegime by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete DayRegime : {}", id);
        dayRegimeRepository.delete(id);
        dayRegimeSearchRepository.delete(id);
    }

    /**
     * Search for the dayRegime corresponding to the query.
     *
     * @param query the query of the search
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<DayRegime> search(String query) {
        log.debug("Request to search DayRegimes for query {}", query);
        return StreamSupport
            .stream(dayRegimeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<DayRegime> findAllByClub(Long id) {
        log.debug("Request to get all DayRegimes of club: {}", id);
        return dayRegimeRepository.findAllByClubId(id);
    }
}
