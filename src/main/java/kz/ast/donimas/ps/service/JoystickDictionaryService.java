package kz.ast.donimas.ps.service;

import kz.ast.donimas.ps.domain.JoystickDictionary;
import kz.ast.donimas.ps.repository.JoystickDictionaryRepository;
import kz.ast.donimas.ps.repository.search.JoystickDictionarySearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing JoystickDictionary.
 */
@Service
@Transactional
public class JoystickDictionaryService {

    private final Logger log = LoggerFactory.getLogger(JoystickDictionaryService.class);

    private final JoystickDictionaryRepository joystickDictionaryRepository;

    private final JoystickDictionarySearchRepository joystickDictionarySearchRepository;

    public JoystickDictionaryService(JoystickDictionaryRepository joystickDictionaryRepository, JoystickDictionarySearchRepository joystickDictionarySearchRepository) {
        this.joystickDictionaryRepository = joystickDictionaryRepository;
        this.joystickDictionarySearchRepository = joystickDictionarySearchRepository;
    }

    /**
     * Save a joystickDictionary.
     *
     * @param joystickDictionary the entity to save
     * @return the persisted entity
     */
    public JoystickDictionary save(JoystickDictionary joystickDictionary) {
        log.debug("Request to save JoystickDictionary : {}", joystickDictionary);
        JoystickDictionary result = joystickDictionaryRepository.save(joystickDictionary);
        joystickDictionarySearchRepository.save(result);
        return result;
    }

    /**
     * Get all the joystickDictionaries.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<JoystickDictionary> findAll() {
        log.debug("Request to get all JoystickDictionaries");
        return joystickDictionaryRepository.findAll();
    }

    /**
     * Get one joystickDictionary by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public JoystickDictionary findOne(Long id) {
        log.debug("Request to get JoystickDictionary : {}", id);
        return joystickDictionaryRepository.findOne(id);
    }

    /**
     * Delete the joystickDictionary by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete JoystickDictionary : {}", id);
        joystickDictionaryRepository.delete(id);
        joystickDictionarySearchRepository.delete(id);
    }

    /**
     * Search for the joystickDictionary corresponding to the query.
     *
     * @param query the query of the search
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<JoystickDictionary> search(String query) {
        log.debug("Request to search JoystickDictionaries for query {}", query);
        return StreamSupport
            .stream(joystickDictionarySearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
