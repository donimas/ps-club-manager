package kz.ast.donimas.ps.service;

import kz.ast.donimas.ps.domain.ExtraJoystick;
import kz.ast.donimas.ps.repository.ExtraJoystickRepository;
import kz.ast.donimas.ps.repository.search.ExtraJoystickSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ExtraJoystick.
 */
@Service
@Transactional
public class ExtraJoystickService {

    private final Logger log = LoggerFactory.getLogger(ExtraJoystickService.class);

    private final ExtraJoystickRepository extraJoystickRepository;

    private final ExtraJoystickSearchRepository extraJoystickSearchRepository;

    public ExtraJoystickService(ExtraJoystickRepository extraJoystickRepository, ExtraJoystickSearchRepository extraJoystickSearchRepository) {
        this.extraJoystickRepository = extraJoystickRepository;
        this.extraJoystickSearchRepository = extraJoystickSearchRepository;
    }

    /**
     * Save a extraJoystick.
     *
     * @param extraJoystick the entity to save
     * @return the persisted entity
     */
    public ExtraJoystick save(ExtraJoystick extraJoystick) {
        log.debug("Request to save ExtraJoystick : {}", extraJoystick);
        ExtraJoystick result = extraJoystickRepository.save(extraJoystick);
        extraJoystickSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the extraJoysticks.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ExtraJoystick> findAll() {
        log.debug("Request to get all ExtraJoysticks");
        return extraJoystickRepository.findAll();
    }

    /**
     * Get one extraJoystick by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ExtraJoystick findOne(Long id) {
        log.debug("Request to get ExtraJoystick : {}", id);
        return extraJoystickRepository.findOne(id);
    }

    /**
     * Delete the extraJoystick by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ExtraJoystick : {}", id);
        extraJoystickRepository.delete(id);
        extraJoystickSearchRepository.delete(id);
    }

    /**
     * Search for the extraJoystick corresponding to the query.
     *
     * @param query the query of the search
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ExtraJoystick> search(String query) {
        log.debug("Request to search ExtraJoysticks for query {}", query);
        return StreamSupport
            .stream(extraJoystickSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
