package kz.ast.donimas.ps.service;

import kz.ast.donimas.ps.domain.GameDictionary;
import kz.ast.donimas.ps.repository.GameDictionaryRepository;
import kz.ast.donimas.ps.repository.search.GameDictionarySearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing GameDictionary.
 */
@Service
@Transactional
public class GameDictionaryService {

    private final Logger log = LoggerFactory.getLogger(GameDictionaryService.class);

    private final GameDictionaryRepository gameDictionaryRepository;

    private final GameDictionarySearchRepository gameDictionarySearchRepository;

    public GameDictionaryService(GameDictionaryRepository gameDictionaryRepository, GameDictionarySearchRepository gameDictionarySearchRepository) {
        this.gameDictionaryRepository = gameDictionaryRepository;
        this.gameDictionarySearchRepository = gameDictionarySearchRepository;
    }

    /**
     * Save a gameDictionary.
     *
     * @param gameDictionary the entity to save
     * @return the persisted entity
     */
    public GameDictionary save(GameDictionary gameDictionary) {
        log.debug("Request to save GameDictionary : {}", gameDictionary);
        GameDictionary result = gameDictionaryRepository.save(gameDictionary);
        gameDictionarySearchRepository.save(result);
        return result;
    }

    /**
     * Get all the gameDictionaries.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GameDictionary> findAll(Pageable pageable) {
        log.debug("Request to get all GameDictionaries");
        return gameDictionaryRepository.findAll(pageable);
    }

    /**
     * Get one gameDictionary by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public GameDictionary findOne(Long id) {
        log.debug("Request to get GameDictionary : {}", id);
        return gameDictionaryRepository.findOne(id);
    }

    /**
     * Delete the gameDictionary by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete GameDictionary : {}", id);
        gameDictionaryRepository.delete(id);
        gameDictionarySearchRepository.delete(id);
    }

    /**
     * Search for the gameDictionary corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GameDictionary> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of GameDictionaries for query {}", query);
        Page<GameDictionary> result = gameDictionarySearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
