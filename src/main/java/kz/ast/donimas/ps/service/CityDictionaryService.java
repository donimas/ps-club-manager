package kz.ast.donimas.ps.service;

import kz.ast.donimas.ps.domain.CityDictionary;
import kz.ast.donimas.ps.repository.CityDictionaryRepository;
import kz.ast.donimas.ps.repository.search.CityDictionarySearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing CityDictionary.
 */
@Service
@Transactional
public class CityDictionaryService {

    private final Logger log = LoggerFactory.getLogger(CityDictionaryService.class);

    private final CityDictionaryRepository cityDictionaryRepository;

    private final CityDictionarySearchRepository cityDictionarySearchRepository;

    public CityDictionaryService(CityDictionaryRepository cityDictionaryRepository, CityDictionarySearchRepository cityDictionarySearchRepository) {
        this.cityDictionaryRepository = cityDictionaryRepository;
        this.cityDictionarySearchRepository = cityDictionarySearchRepository;
    }

    /**
     * Save a cityDictionary.
     *
     * @param cityDictionary the entity to save
     * @return the persisted entity
     */
    public CityDictionary save(CityDictionary cityDictionary) {
        log.debug("Request to save CityDictionary : {}", cityDictionary);
        CityDictionary result = cityDictionaryRepository.save(cityDictionary);
        cityDictionarySearchRepository.save(result);
        return result;
    }

    /**
     * Get all the cityDictionaries.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CityDictionary> findAll(Pageable pageable) {
        log.debug("Request to get all CityDictionaries");
        return cityDictionaryRepository.findAll(pageable);
    }

    /**
     * Get one cityDictionary by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public CityDictionary findOne(Long id) {
        log.debug("Request to get CityDictionary : {}", id);
        return cityDictionaryRepository.findOne(id);
    }

    /**
     * Delete the cityDictionary by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CityDictionary : {}", id);
        cityDictionaryRepository.delete(id);
        cityDictionarySearchRepository.delete(id);
    }

    /**
     * Search for the cityDictionary corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CityDictionary> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of CityDictionaries for query {}", query);
        Page<CityDictionary> result = cityDictionarySearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
