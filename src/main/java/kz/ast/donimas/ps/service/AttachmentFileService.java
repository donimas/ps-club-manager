package kz.ast.donimas.ps.service;

import kz.ast.donimas.ps.domain.AttachmentFile;
import kz.ast.donimas.ps.repository.AttachmentFileRepository;
import kz.ast.donimas.ps.repository.search.AttachmentFileSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing AttachmentFile.
 */
@Service
@Transactional
public class AttachmentFileService {

    private final Logger log = LoggerFactory.getLogger(AttachmentFileService.class);

    private final AttachmentFileRepository attachmentFileRepository;

    private final AttachmentFileSearchRepository attachmentFileSearchRepository;

    public AttachmentFileService(AttachmentFileRepository attachmentFileRepository, AttachmentFileSearchRepository attachmentFileSearchRepository) {
        this.attachmentFileRepository = attachmentFileRepository;
        this.attachmentFileSearchRepository = attachmentFileSearchRepository;
    }

    /**
     * Save a attachmentFile.
     *
     * @param attachmentFile the entity to save
     * @return the persisted entity
     */
    public AttachmentFile save(AttachmentFile attachmentFile) {
        log.debug("Request to save AttachmentFile : {}", attachmentFile);
        AttachmentFile result = attachmentFileRepository.save(attachmentFile);
        attachmentFileSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the attachmentFiles.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<AttachmentFile> findAll() {
        log.debug("Request to get all AttachmentFiles");
        return attachmentFileRepository.findAll();
    }

    /**
     * Get one attachmentFile by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public AttachmentFile findOne(Long id) {
        log.debug("Request to get AttachmentFile : {}", id);
        return attachmentFileRepository.findOne(id);
    }

    /**
     * Delete the attachmentFile by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AttachmentFile : {}", id);
        attachmentFileRepository.delete(id);
        attachmentFileSearchRepository.delete(id);
    }

    /**
     * Search for the attachmentFile corresponding to the query.
     *
     * @param query the query of the search
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<AttachmentFile> search(String query) {
        log.debug("Request to search AttachmentFiles for query {}", query);
        return StreamSupport
            .stream(attachmentFileSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
