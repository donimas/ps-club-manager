package kz.ast.donimas.ps.service;

import kz.ast.donimas.ps.domain.JoySessionLife;
import kz.ast.donimas.ps.repository.JoySessionLifeRepository;
import kz.ast.donimas.ps.repository.search.JoySessionLifeSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing JoySessionLife.
 */
@Service
@Transactional
public class JoySessionLifeService {

    private final Logger log = LoggerFactory.getLogger(JoySessionLifeService.class);

    private final JoySessionLifeRepository joySessionLifeRepository;

    private final JoySessionLifeSearchRepository joySessionLifeSearchRepository;

    public JoySessionLifeService(JoySessionLifeRepository joySessionLifeRepository, JoySessionLifeSearchRepository joySessionLifeSearchRepository) {
        this.joySessionLifeRepository = joySessionLifeRepository;
        this.joySessionLifeSearchRepository = joySessionLifeSearchRepository;
    }

    /**
     * Save a joySessionLife.
     *
     * @param joySessionLife the entity to save
     * @return the persisted entity
     */
    public JoySessionLife save(JoySessionLife joySessionLife) {
        log.debug("Request to save JoySessionLife : {}", joySessionLife);
        JoySessionLife result = joySessionLifeRepository.save(joySessionLife);
        joySessionLifeSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the joySessionLives.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<JoySessionLife> findAll(Pageable pageable) {
        log.debug("Request to get all JoySessionLives");
        return joySessionLifeRepository.findAll(pageable);
    }

    /**
     * Get one joySessionLife by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public JoySessionLife findOne(Long id) {
        log.debug("Request to get JoySessionLife : {}", id);
        return joySessionLifeRepository.findOne(id);
    }

    /**
     * Delete the joySessionLife by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete JoySessionLife : {}", id);
        joySessionLifeRepository.delete(id);
        joySessionLifeSearchRepository.delete(id);
    }

    /**
     * Search for the joySessionLife corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<JoySessionLife> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of JoySessionLives for query {}", query);
        Page<JoySessionLife> result = joySessionLifeSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
