package kz.ast.donimas.ps.service;

import kz.ast.donimas.ps.domain.GamePleasure;
import kz.ast.donimas.ps.repository.GamePleasureRepository;
import kz.ast.donimas.ps.repository.search.GamePleasureSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing GamePleasure.
 */
@Service
@Transactional
public class GamePleasureService {

    private final Logger log = LoggerFactory.getLogger(GamePleasureService.class);

    private final GamePleasureRepository gamePleasureRepository;

    private final GamePleasureSearchRepository gamePleasureSearchRepository;

    public GamePleasureService(GamePleasureRepository gamePleasureRepository, GamePleasureSearchRepository gamePleasureSearchRepository) {
        this.gamePleasureRepository = gamePleasureRepository;
        this.gamePleasureSearchRepository = gamePleasureSearchRepository;
    }

    /**
     * Save a gamePleasure.
     *
     * @param gamePleasure the entity to save
     * @return the persisted entity
     */
    public GamePleasure save(GamePleasure gamePleasure) {
        log.debug("Request to save GamePleasure : {}", gamePleasure);
        GamePleasure result = gamePleasureRepository.save(gamePleasure);
        gamePleasureSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the gamePleasures.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GamePleasure> findAll(Pageable pageable) {
        log.debug("Request to get all GamePleasures");
        return gamePleasureRepository.findAll(pageable);
    }

    /**
     * Get one gamePleasure by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public GamePleasure findOne(Long id) {
        log.debug("Request to get GamePleasure : {}", id);
        return gamePleasureRepository.findOne(id);
    }

    /**
     * Delete the gamePleasure by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete GamePleasure : {}", id);
        gamePleasureRepository.delete(id);
        gamePleasureSearchRepository.delete(id);
    }

    /**
     * Search for the gamePleasure corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GamePleasure> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of GamePleasures for query {}", query);
        Page<GamePleasure> result = gamePleasureSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
