package kz.ast.donimas.ps.service;

import kz.ast.donimas.ps.domain.AttachmentFile;
import kz.ast.donimas.ps.domain.Club;
import kz.ast.donimas.ps.domain.Location;
import kz.ast.donimas.ps.domain.User;
import kz.ast.donimas.ps.repository.ClubRepository;
import kz.ast.donimas.ps.repository.search.ClubSearchRepository;
import kz.ast.donimas.ps.service.dto.ClubDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Club.
 */
@Service
@Transactional
public class ClubService {

    private final Logger log = LoggerFactory.getLogger(ClubService.class);

    private final ClubRepository clubRepository;

    private final ClubSearchRepository clubSearchRepository;
    private final AttachmentFileService attachmentFileService;
    private final ClubJoystickService clubJoystickService;
    private final LocationService locationService;
    private final DayRegimeService dayRegimeService;

    public ClubService(ClubRepository clubRepository, ClubSearchRepository clubSearchRepository, AttachmentFileService attachmentFileService, ClubJoystickService clubJoystickService, LocationService locationService, DayRegimeService dayRegimeService) {
        this.clubRepository = clubRepository;
        this.clubSearchRepository = clubSearchRepository;
        this.attachmentFileService = attachmentFileService;
        this.clubJoystickService = clubJoystickService;
        this.locationService = locationService;
        this.dayRegimeService = dayRegimeService;
    }

    /**
     * Save a club.
     *
     * @param club the entity to save
     * @return the persisted entity
     */
    public Club save(Club club) {
        log.debug("Request to save Club : {}", club);
        Club result = clubRepository.save(club);
        clubSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the clubs.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Club> findAll(Pageable pageable) {
        log.debug("Request to get all Clubs");
        return clubRepository.findAll(pageable);
    }

    /**
     * Get one club by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Club findOne(Long id) {
        log.debug("Request to get Club : {}", id);
        return clubRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the club by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Club : {}", id);
        clubRepository.delete(id);
        clubSearchRepository.delete(id);
    }

    /**
     * Search for the club corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Club> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Clubs for query {}", query);
        Page<Club> result = clubSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }

    @Transactional
    public Club getOwnerClub(User user) {
        Club club = user.getClub();
        if(club != null) {
            club = clubRepository.findOneWithEagerRelationships(club.getId());
            if(club.getGames() != null) {
                log.debug("games size: {}", club.getGames().size());
            } else {
                log.debug("games null");
            }
            return club;
        }
        club = new Club();
        club.setName("");
        club.setPhone("");
        club.setFullAddress("");
        return save(club);
    }

    public Club saveMainInfo(Club club) {
        log.debug("Request to save main info: {}", club);
        Club result = findOne(club.getId());
        AttachmentFile avatar = club.getAvatar();
        if(avatar != null) {
            avatar = attachmentFileService.save(avatar);
        }

        result.setAvatar(avatar);
        result.setFlagMainInfoSaved(true);
        result.setName(club.getName());
        result.setDescription(club.getDescription());

        return save(result);
    }

    public Club saveClubRegulation(ClubDto clubDto) {
        log.debug("Request to save regulation: {}", clubDto);
        Club result = findOne(clubDto.getId());
        result.setNotifyBefore(clubDto.getNotifyBefore());
        result.setTimeScheduleRegulation(clubDto.getTimeScheduleRegulation());
        result.setFlaghookah(clubDto.getFlaghookah());
        clubDto.getWorkRegimes()
            .stream()
            .peek(day -> day.setClub(result))
            .forEach(dayRegimeService::save);
        result.setWorkRegimes(clubDto.getWorkRegimes());
        result.setFlagRegulationSaved(true);

        return save(result);
    }

    public Club saveContact(Club club) {
        log.debug("Request to save contact: {}", club);
        Club result = findOne(club.getId());
        result.setFlagContactSaved(true);

        result.setPhone(club.getPhone());
        result.setPhoneOptional(club.getPhoneOptional());
        result.setInstaUrl(club.getInstaUrl());
        result.vkUrl(club.getVkUrl());
        result.setTwitterUrl(club.getTwitterUrl());
        result.setFacebookUrl(club.getFacebookUrl());
        result.setTelegramUrl(club.getTelegramUrl());
        result.setWebUrl(club.getWebUrl());

        return save(result);
    }

    public Club saveCoordinate(Club club) {
        log.debug("Request to save coordinate: {}", club);
        Club result = findOne(club.getId());
        Location location = locationService.save(club.getLocation());
        result.setLocation(location);
        result.setFlagLocationSaved(true);

        return save(result);
    }

    public Club saveEquipment(Club club) {
        log.debug("request to save equipment: {}", club);
        Club result = findOne(club.getId());
        if(club.getGames() != null && club.getGames().size() > 0) {
            result.setGames(club.getGames());
        }
        if(club.getConsoles() != null && club.getConsoles().size() > 0) {
            result.setConsoles(club.getConsoles());
        }
        if(club.getJoysticks() != null && club.getJoysticks().size() > 0) {
            club.getJoysticks()
                .stream()
                .filter(j -> j.getId() == null)
                .peek(j -> j.setClub(result))
                .forEach(clubJoystickService::save);

            result.setJoysticks(club.getJoysticks());
        }

        result.setFlagEquipmentSaved(true);
        return save(result);
    }
}
