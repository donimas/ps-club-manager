package kz.ast.donimas.ps.service;

import kz.ast.donimas.ps.domain.Club;
import kz.ast.donimas.ps.domain.PricePackage;
import kz.ast.donimas.ps.repository.PricePackageRepository;
import kz.ast.donimas.ps.repository.search.PricePackageSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing PricePackage.
 */
@Service
@Transactional
public class PricePackageService {

    private final Logger log = LoggerFactory.getLogger(PricePackageService.class);

    private final PricePackageRepository pricePackageRepository;

    private final PricePackageSearchRepository pricePackageSearchRepository;
    private final ClubService clubService;

    public PricePackageService(PricePackageRepository pricePackageRepository, PricePackageSearchRepository pricePackageSearchRepository, ClubService clubService) {
        this.pricePackageRepository = pricePackageRepository;
        this.pricePackageSearchRepository = pricePackageSearchRepository;
        this.clubService = clubService;
    }

    /**
     * Save a pricePackage.
     *
     * @param pricePackage the entity to save
     * @return the persisted entity
     */
    public PricePackage save(PricePackage pricePackage) {
        log.debug("Request to save PricePackage : {}", pricePackage);
        PricePackage result = pricePackageRepository.save(pricePackage);
        pricePackageSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the pricePackages.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<PricePackage> findAll() {
        log.debug("Request to get all PricePackages");
        return pricePackageRepository.findAll();
    }

    /**
     * Get one pricePackage by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public PricePackage findOne(Long id) {
        log.debug("Request to get PricePackage : {}", id);
        return pricePackageRepository.findOne(id);
    }

    /**
     * Delete the pricePackage by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete PricePackage : {}", id);
        /*pricePackageRepository.delete(id);
        pricePackageSearchRepository.delete(id);*/
        PricePackage pricePackage = findOne(id);
        pricePackage.setFlagDeleted(true);
        save(pricePackage);
    }

    /**
     * Search for the pricePackage corresponding to the query.
     *
     * @param query the query of the search
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<PricePackage> search(String query) {
        log.debug("Request to search PricePackages for query {}", query);
        return StreamSupport
            .stream(pricePackageSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

    @Transactional
    public PricePackage create(PricePackage pricePackage) {
        log.debug("Request to create price package: {}", pricePackage);
        Club club = clubService.findOne(pricePackage.getClub().getId());
        pricePackage.setClub(club);
        club.setFlagPricePackageSaved(true);
        clubService.save(club);
        return save(pricePackage);
    }

    public Page<PricePackage> getAllClubPricePackages(Long id, Pageable pageable) {
        return pricePackageRepository.findAllByClubIdAndFlagDeletedFalse(id, pageable);
    }
}
