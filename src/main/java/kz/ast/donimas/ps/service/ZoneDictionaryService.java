package kz.ast.donimas.ps.service;

import kz.ast.donimas.ps.domain.ZoneDictionary;
import kz.ast.donimas.ps.repository.ZoneDictionaryRepository;
import kz.ast.donimas.ps.repository.search.ZoneDictionarySearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ZoneDictionary.
 */
@Service
@Transactional
public class ZoneDictionaryService {

    private final Logger log = LoggerFactory.getLogger(ZoneDictionaryService.class);

    private final ZoneDictionaryRepository zoneDictionaryRepository;

    private final ZoneDictionarySearchRepository zoneDictionarySearchRepository;

    public ZoneDictionaryService(ZoneDictionaryRepository zoneDictionaryRepository, ZoneDictionarySearchRepository zoneDictionarySearchRepository) {
        this.zoneDictionaryRepository = zoneDictionaryRepository;
        this.zoneDictionarySearchRepository = zoneDictionarySearchRepository;
    }

    /**
     * Save a zoneDictionary.
     *
     * @param zoneDictionary the entity to save
     * @return the persisted entity
     */
    public ZoneDictionary save(ZoneDictionary zoneDictionary) {
        log.debug("Request to save ZoneDictionary : {}", zoneDictionary);
        ZoneDictionary result = zoneDictionaryRepository.save(zoneDictionary);
        zoneDictionarySearchRepository.save(result);
        return result;
    }

    /**
     * Get all the zoneDictionaries.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ZoneDictionary> findAll() {
        log.debug("Request to get all ZoneDictionaries");
        return zoneDictionaryRepository.findAll();
    }

    /**
     * Get one zoneDictionary by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ZoneDictionary findOne(Long id) {
        log.debug("Request to get ZoneDictionary : {}", id);
        return zoneDictionaryRepository.findOne(id);
    }

    /**
     * Delete the zoneDictionary by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ZoneDictionary : {}", id);
        zoneDictionaryRepository.delete(id);
        zoneDictionarySearchRepository.delete(id);
    }

    /**
     * Search for the zoneDictionary corresponding to the query.
     *
     * @param query the query of the search
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ZoneDictionary> search(String query) {
        log.debug("Request to search ZoneDictionaries for query {}", query);
        return StreamSupport
            .stream(zoneDictionarySearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
