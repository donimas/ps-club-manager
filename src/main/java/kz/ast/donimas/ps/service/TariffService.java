package kz.ast.donimas.ps.service;

import kz.ast.donimas.ps.domain.Tariff;
import kz.ast.donimas.ps.repository.TariffRepository;
import kz.ast.donimas.ps.repository.search.TariffSearchRepository;
import kz.ast.donimas.ps.service.constant.ContainerClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Tariff.
 */
@Service
@Transactional
public class TariffService {

    private final Logger log = LoggerFactory.getLogger(TariffService.class);

    private final TariffRepository tariffRepository;

    private final TariffSearchRepository tariffSearchRepository;

    public TariffService(TariffRepository tariffRepository, TariffSearchRepository tariffSearchRepository) {
        this.tariffRepository = tariffRepository;
        this.tariffSearchRepository = tariffSearchRepository;
    }

    /**
     * Save a tariff.
     *
     * @param tariff the entity to save
     * @return the persisted entity
     */
    public Tariff save(Tariff tariff) {
        log.debug("Request to save Tariff : {}", tariff);
        Tariff result = tariffRepository.save(tariff);
        tariffSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the tariffs.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Tariff> findAll(Pageable pageable) {
        log.debug("Request to get all Tariffs");
        return tariffRepository.findAll(pageable);
    }

    /**
     * Get one tariff by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Tariff findOne(Long id) {
        log.debug("Request to get Tariff : {}", id);
        return tariffRepository.findOne(id);
    }

    /**
     * Delete the tariff by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Tariff : {}", id);
        tariffRepository.delete(id);
        tariffSearchRepository.delete(id);
    }

    /**
     * Search for the tariff corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Tariff> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Tariffs for query {}", query);
        Page<Tariff> result = tariffSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }

    public Tariff createOrUpdate(Tariff tariff, String containerClass, Long containerId) {
        log.debug("Request to create tariff: {}, {}, {}", tariff, containerClass, containerId);
        if(tariff.getPrice() == null) {
            tariff.setPrice(BigDecimal.ZERO);
        }
        if(tariff.getId() != null) {
            Tariff existedTariff = findOne(tariff.getId());
            if(existedTariff.getPrice().compareTo(tariff.getPrice()) != 0) {
                tariff.setId(null);
            }
        }
        if(tariff.getId() == null) {
            tariff.setContainerClass(containerClass);
            tariff.setContainerId(containerId);
            tariff.setCreatedDate(ZonedDateTime.now(ZoneId.systemDefault()));
        }

        Tariff result = save(tariff);
        return result;
    }
}
