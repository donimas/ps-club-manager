package kz.ast.donimas.ps.service;

import kz.ast.donimas.ps.domain.ConsoleDictionary;
import kz.ast.donimas.ps.repository.ConsoleDictionaryRepository;
import kz.ast.donimas.ps.repository.search.ConsoleDictionarySearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ConsoleDictionary.
 */
@Service
@Transactional
public class ConsoleDictionaryService {

    private final Logger log = LoggerFactory.getLogger(ConsoleDictionaryService.class);

    private final ConsoleDictionaryRepository consoleDictionaryRepository;

    private final ConsoleDictionarySearchRepository consoleDictionarySearchRepository;

    public ConsoleDictionaryService(ConsoleDictionaryRepository consoleDictionaryRepository, ConsoleDictionarySearchRepository consoleDictionarySearchRepository) {
        this.consoleDictionaryRepository = consoleDictionaryRepository;
        this.consoleDictionarySearchRepository = consoleDictionarySearchRepository;
    }

    /**
     * Save a consoleDictionary.
     *
     * @param consoleDictionary the entity to save
     * @return the persisted entity
     */
    public ConsoleDictionary save(ConsoleDictionary consoleDictionary) {
        log.debug("Request to save ConsoleDictionary : {}", consoleDictionary);
        ConsoleDictionary result = consoleDictionaryRepository.save(consoleDictionary);
        consoleDictionarySearchRepository.save(result);
        return result;
    }

    /**
     * Get all the consoleDictionaries.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ConsoleDictionary> findAll() {
        log.debug("Request to get all ConsoleDictionaries");
        return consoleDictionaryRepository.findAll();
    }

    /**
     * Get one consoleDictionary by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ConsoleDictionary findOne(Long id) {
        log.debug("Request to get ConsoleDictionary : {}", id);
        return consoleDictionaryRepository.findOne(id);
    }

    /**
     * Delete the consoleDictionary by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ConsoleDictionary : {}", id);
        consoleDictionaryRepository.delete(id);
        consoleDictionarySearchRepository.delete(id);
    }

    /**
     * Search for the consoleDictionary corresponding to the query.
     *
     * @param query the query of the search
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ConsoleDictionary> search(String query) {
        log.debug("Request to search ConsoleDictionaries for query {}", query);
        return StreamSupport
            .stream(consoleDictionarySearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
