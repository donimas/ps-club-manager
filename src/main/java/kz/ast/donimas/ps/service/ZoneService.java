package kz.ast.donimas.ps.service;

import kz.ast.donimas.ps.domain.Club;
import kz.ast.donimas.ps.domain.Tariff;
import kz.ast.donimas.ps.domain.Zone;
import kz.ast.donimas.ps.repository.ZoneRepository;
import kz.ast.donimas.ps.repository.search.ZoneSearchRepository;
import kz.ast.donimas.ps.service.constant.ContainerClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Zone.
 */
@Service
@Transactional
public class ZoneService {

    private final Logger log = LoggerFactory.getLogger(ZoneService.class);

    private final ZoneRepository zoneRepository;

    private final ZoneSearchRepository zoneSearchRepository;
    private final ClubService clubService;
    private final TariffService tariffService;

    public ZoneService(ZoneRepository zoneRepository, ZoneSearchRepository zoneSearchRepository, ClubService clubService, TariffService tariffService) {
        this.zoneRepository = zoneRepository;
        this.zoneSearchRepository = zoneSearchRepository;
        this.clubService = clubService;
        this.tariffService = tariffService;
    }

    /**
     * Save a zone.
     *
     * @param zone the entity to save
     * @return the persisted entity
     */
    public Zone save(Zone zone) {
        log.debug("Request to save Zone : {}", zone);
        Zone result = zoneRepository.save(zone);
        zoneSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the zones.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Zone> findAll(Pageable pageable) {
        log.debug("Request to get all Zones");
        return zoneRepository.findAll(pageable);
    }

    /**
     * Get one zone by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Zone findOne(Long id) {
        log.debug("Request to get Zone : {}", id);
        return zoneRepository.findOne(id);
    }

    /**
     * Delete the zone by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Zone : {}", id);
        /*zoneRepository.delete(id);
        zoneSearchRepository.delete(id);*/
        Zone zone = findOne(id);
        zone.setFlagDeleted(true);
        save(zone);
    }

    /**
     * Search for the zone corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Zone> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Zones for query {}", query);
        Page<Zone> result = zoneSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }

    public Zone create(Zone zone) {
        log.debug("Request to create zone");
        Club club = clubService.findOne(zone.getClub().getId());
        //Tariff pricePerHour = zone.getPricePerHour();

        Zone savedZone = save(zone);
        //Tariff savedPrice = tariffService.createOrUpdate(pricePerHour, ContainerClass.ZONE, savedZone.getId());
        //savedZone.setPricePerHour(savedPrice);
        //save(savedZone);

        club.setFlagZoneSaved(true);
        clubService.save(club);
        return savedZone;
    }

    @Transactional(readOnly = true)
    public List<Zone> findAllZonesOfClub(Long id) {
        log.debug("Request to get all Zones of club: {}", id);
        return zoneRepository.findAllByClubIdAndFlagDeletedFalse(id);
    }
}
