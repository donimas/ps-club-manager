package kz.ast.donimas.ps.service.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.*;

public class StringUtils {

    public static String TAB = "\t";

    public static String DEFAULT_DELIM = ", ";//разделитель по умолчанию

    public static String DEFAULT_EMPTY = "-";

    public static String nullToEmpty(String value){
        return isEmpty(value) ? "" : value;
    }

    public static boolean isNotEmpty(String value){
        return !isEmpty(value);
    }

    public static boolean isEmpty(String value){
        return value == null || value.trim().isEmpty();
    }

    /*public static String getBasicAuthorizationToken(String user,String password) throws UnsupportedEncodingException {
        return Base64.encodeBase64String((user + ":" + password).getBytes("UTF-8"));
    }

    public static String getBasicAuthorizationToken(String terminalId, String userId ,String password) throws UnsupportedEncodingException {
        return Base64.encodeBase64String((terminalId + "|" + userId + ":" + password).getBytes("UTF-8"));
    }


    public static boolean checkBasicAuthorizationToken(String user,String password, String token) throws UnsupportedEncodingException {
        String[] split = token.split(" ");
        if(split.length > 1){//отрезаем префикс "Basic "
            token = split[1];
        }
        return token.equals(getBasicAuthorizationToken(user, password));
    }*/

    public static List<String> split(String value, String delim){
        List<String> list = new ArrayList<>();
        if(isNotEmpty(value)){
            for (String s : value.split(delim)) {
                list.add(s.trim());
            }
        }
        return list;
    }

    /**
     * разбить текст на блоки фиксированной длины
     * @param text текст
     * @param blockSize размер блока
     * @return блоки
     */
    public static List<String> splitIntoBlocks(String text,int blockSize) {
        if(blockSize < 1) throw new IllegalArgumentException("Incorrect block size");
        if(text == null) return null;
        List<String> list = new ArrayList<>();
        int i = 0;
        while(i < text.length()) {
            list.add(text.substring(i, Math.min(i += blockSize, text.length())));
        }
        return list;
    }

    public static String getUUID() {
        return UUID.randomUUID().toString();
    }

    /**
     * получение полного ФИО
     * @param lastname фамилия
     * @param firstname имя
     * @param patronymic отчество
     * @return ФИО
     */
    public static String getFullName(String lastname, String firstname, String patronymic) {
        StringBuilder sb = new StringBuilder();
        if(isNotEmpty(lastname)){
            sb.append(lastname);
        }
        if(isNotEmpty(firstname)){
            sb.append(" ");
            sb.append(firstname);
        }
        if(isNotEmpty(patronymic)){
            sb.append(" ");
            sb.append(patronymic);
        }
        return sb.toString();
    }

    public static String getShortName(String lastname, String firstname, String patronymic) {
        StringBuilder sb = new StringBuilder();
        if (lastname != null) {
            sb.append(lastname);
        }
        if (firstname != null && firstname.length() > 0) {
            sb.append(" ").append(firstname.substring(0, 1).toUpperCase()).append(".");
        }
        if (patronymic != null && patronymic.length() > 0) {
            sb.append(" ").append(patronymic.substring(0, 1).toUpperCase()).append(".");
        }
        return sb.toString();
    }

    public static String[] splitByEndOfLine(String value){
        return splitByRegex(value, "\\r?\\n");
    }

    public static String[] splitByTab(String value){
        return splitByRegex(value, "\t");
    }

    public static String[] splitByRegex(String value, String regex){
        return value == null ? null : value.split(regex, -1);
    }

    public static List<Map<String,String>> dataToMap(String value){
        return dataToMap(value, TAB);
    }

    public static List<Map<String,String>> dataToMap(String value, String regex){
        String lines[] = StringUtils.splitByEndOfLine(value);

        String[] headers = StringUtils.splitByTab(lines[0]);

        List<Map<String, String>> list = new ArrayList<>();
        for (int i = 1; i < lines.length; i++) {
            String[] split = StringUtils.splitByRegex(lines[i], regex);
            Map<String, String> map = new HashMap<>();
            for (int j = 0; j < headers.length; j++) {
                map.put(headers[j], split[j]);
            }
            list.add(map);
        }
        return list;
    }

    public static String urlEncode(String value) throws UnsupportedEncodingException {
        return URLEncoder.encode(value, "UTF-8");
    }

    public static String urlDecode(String value) throws UnsupportedEncodingException {
        return URLDecoder.decode(value, "UTF-8");
    }

    public static String emptyToDefault(String value, String defaultValue){
        return isNotEmpty(value) ? value : defaultValue;
    }

    //склеивание строк с разделителем
    public static String merge(String[] arr){
        return merge(Arrays.asList(arr));
    }

    //склеивание строк с разделителем
    public static String merge(String[] arr,String delim){
        return merge(Arrays.asList(arr), delim);
    }

    //склеивание строк с разделителем
    public static String merge(List<String> list){
        return merge(list, DEFAULT_DELIM);
    }

    //склеивание строк с разделителем
    public static String merge(List<String> list,String delim){
        StringBuilder sb = new StringBuilder();
        boolean can = false;
        for (String text : list) {
            if(isEmpty(text)) continue;
            if(can) sb.append(delim);
            sb.append(text);
            can = true;
        }
        return sb.toString();
    }

    public static String lowerCase(final String str, final Locale locale) {
        if (str == null) {
            return null;
        }
        return str.toLowerCase(locale);
    }

    public static String notEmptyOrThrow(String value, RuntimeException e){
        if(isEmpty(value)){
            throw e;
        }
        return value;
    }

    public static String notEmptyOrThrow(String value, Exception e) throws Exception {
        if(isEmpty(value)){
            throw e;
        }
        return value;
    }

    public static boolean isNumeric(String text) {
        return org.apache.commons.lang.StringUtils.isNumeric(text);
    }
}
