package kz.ast.donimas.ps.service.dto;

import kz.ast.donimas.ps.domain.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ZoneDto implements Serializable {

    private Long id;
    private String name;
    private String description;
    private Integer seatsAmount;
    private Float tvDiagonal;
    private Float tvResolution;
    private Boolean flagDeleted;
    private ConsoleDictionary console;
    private ZoneDictionary type;
    private Club club;
    private List<Session> sessions = new ArrayList<>();

    public ZoneDto() {}

    public ZoneDto(Zone zone) {
        id = zone.getId();
        name = zone.getName();
        description = zone.getDescription();
        seatsAmount = zone.getSeatsAmount();
        tvDiagonal = zone.getTvDiagonal();
        tvResolution = zone.getTvResolution();
        flagDeleted = zone.isFlagDeleted();
        console = zone.getConsole();
        type = zone.getType();
        club = zone.getClub();
    }

    public ZoneDto(Long id, String name, Integer seatsAmount, ZoneDictionary type, List<Session> sessions) {
        this.id = id;
        this.name = name;
        this.seatsAmount = seatsAmount;
        this.type = type;
        this.sessions = sessions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSeatsAmount() {
        return seatsAmount;
    }

    public void setSeatsAmount(Integer seatsAmount) {
        this.seatsAmount = seatsAmount;
    }

    public Float getTvDiagonal() {
        return tvDiagonal;
    }

    public void setTvDiagonal(Float tvDiagonal) {
        this.tvDiagonal = tvDiagonal;
    }

    public Float getTvResolution() {
        return tvResolution;
    }

    public void setTvResolution(Float tvResolution) {
        this.tvResolution = tvResolution;
    }

    public Boolean getFlagDeleted() {
        return flagDeleted;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public ConsoleDictionary getConsole() {
        return console;
    }

    public void setConsole(ConsoleDictionary console) {
        this.console = console;
    }

    public ZoneDictionary getType() {
        return type;
    }

    public void setType(ZoneDictionary type) {
        this.type = type;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public List<Session> getSessions() {
        return sessions;
    }

    public void setSessions(List<Session> sessions) {
        this.sessions = sessions;
    }

    @Override
    public String toString() {
        return "ZoneDto{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", description='" + description + '\'' +
            ", seatsAmount=" + seatsAmount +
            ", tvDiagonal=" + tvDiagonal +
            ", tvResolution=" + tvResolution +
            ", flagDeleted=" + flagDeleted +
            ", console=" + console +
            ", type=" + type +
            ", club=" + club +
            ", sessions=" + sessions +
            '}';
    }
}
