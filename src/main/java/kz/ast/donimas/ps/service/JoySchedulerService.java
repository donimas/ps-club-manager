package kz.ast.donimas.ps.service;

import kz.ast.donimas.ps.domain.Club;
import kz.ast.donimas.ps.domain.Session;
import kz.ast.donimas.ps.repository.ClubRepository;
import kz.ast.donimas.ps.service.dto.ClubDto;
import kz.ast.donimas.ps.service.dto.ZoneDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class JoySchedulerService {

    private final Logger log = LoggerFactory.getLogger(JoySchedulerService.class);

    private final SessionService sessionService;
    private final ClubRepository clubRepository;

    public JoySchedulerService(SessionService sessionService, ClubRepository clubRepository) {
        this.sessionService = sessionService;
        this.clubRepository = clubRepository;
    }

    public ClubDto getJoyScheduler(Long id) {
        log.debug("Request to get joy scheduler: {}", id);
        Club club = clubRepository.findOneWithEagerRelationships(id);

        Set<ZoneDto> zoneDtos = club.getZones()
            .stream()
            .filter(z -> z.isFlagDeleted() == null || !z.isFlagDeleted())
            .map(z -> {
                List<Session> sessions = sessionService.findByZone(z.getId());
                return new ZoneDto(z.getId(), z.getName(), z.getSeatsAmount(), z.getType(), sessions);
            }).collect(Collectors.toSet());
        ClubDto result = new ClubDto(
            club.getId(), club.getTimeScheduleRegulation(),
            club.getNotifyBefore(), club.getConsoles(),
            club.getGames(), club.getJoysticks(),
            zoneDtos, club.getPricePackages(),
            club.getClientLevels(), club.getWorkRegimes()
        );

        return result;
    }

}
